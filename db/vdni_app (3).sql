-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 09:06 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vdni_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`) VALUES
(26, 'agungan', 'agung212', '04b4bd14385075d55144ee2a38d19241'),
(27, 'Iswan', 'Iswan', 'c93ccd78b2076528346216b3b2f701e6'),
(28, 'Tes', 'Tes', 'c93ccd78b2076528346216b3b2f701e6'),
(29, 'adi', 'adi', 'c93ccd78b2076528346216b3b2f701e6'),
(31, 'Ishwan', 'Ishwan', '901c7dfc39c7fd7a70f95fbdd72eecc4');

-- --------------------------------------------------------

--
-- Table structure for table `admin_master`
--

CREATE TABLE `admin_master` (
  `id_adminM` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_master`
--

INSERT INTO `admin_master` (`id_adminM`, `username`, `password`, `nama`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `buyer`
--

CREATE TABLE `buyer` (
  `id_buy` int(11) NOT NULL,
  `nm_buy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer`
--

INSERT INTO `buyer` (`id_buy`, `nm_buy`) VALUES
(1, 'VDNI');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_agent`
--

CREATE TABLE `buyer_agent` (
  `id_buyer` int(11) NOT NULL,
  `nm_buyer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer_agent`
--

INSERT INTO `buyer_agent` (`id_buyer`, `nm_buyer`) VALUES
(7, 'Hongkong Topway');

-- --------------------------------------------------------

--
-- Table structure for table `coal_contract`
--

CREATE TABLE `coal_contract` (
  `id_contract` int(11) NOT NULL,
  `contract_no` varchar(255) NOT NULL,
  `sales_purchase` varchar(255) NOT NULL,
  `id_sup` int(255) NOT NULL,
  `buyer` varchar(255) NOT NULL,
  `id_buyer` int(255) NOT NULL,
  `lc_no` varchar(255) DEFAULT NULL,
  `quantity_lc` varchar(255) NOT NULL,
  `amount_lc` varchar(255) NOT NULL,
  `id_terms` int(255) NOT NULL,
  `port_of_landing` varchar(255) NOT NULL,
  `port_of_discharge` varchar(255) NOT NULL,
  `bl` varchar(255) NOT NULL,
  `fob_sup` varchar(255) NOT NULL,
  `freight_sup` varchar(255) NOT NULL,
  `cfr_priceSup` varchar(255) NOT NULL,
  `fob_buyer` varchar(255) NOT NULL,
  `freight_buyer` varchar(255) NOT NULL,
  `cfr_priceBuyer` varchar(255) NOT NULL,
  `demurage` varchar(255) NOT NULL,
  `dispatch` varchar(255) DEFAULT NULL,
  `discharging` varchar(255) NOT NULL,
  `tgl_dibuat` datetime NOT NULL,
  `dibuat` varchar(255) NOT NULL,
  `payment_method` varchar(25) NOT NULL,
  `sat_des` varchar(255) NOT NULL,
  `sat_dem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coal_contract`
--

INSERT INTO `coal_contract` (`id_contract`, `contract_no`, `sales_purchase`, `id_sup`, `buyer`, `id_buyer`, `lc_no`, `quantity_lc`, `amount_lc`, `id_terms`, `port_of_landing`, `port_of_discharge`, `bl`, `fob_sup`, `freight_sup`, `cfr_priceSup`, `fob_buyer`, `freight_buyer`, `cfr_priceBuyer`, `demurage`, `dispatch`, `discharging`, `tgl_dibuat`, `dibuat`, `payment_method`, `sat_des`, `sat_dem`) VALUES
(4, 'CLX56406', 'T35080160011B01', 26, '1', 7, '001', '55000', '4639800', 6, 'DBCT Queensland, Australia', 'Morosi, Kendari', '55000', '', '', '82', '', '', '82', '6750', '3375', '5000', '0000-00-00 00:00:00', '', '', '', ''),
(5, ' AC16S114 ', '  T35080160012B01 ', 27, '1', 7, ' ', ' ', ' ', 6, ' Any Port in Australia ', 'Morosi, Kendari', ' 61428 ', '', '', '78.89', '', '', '78.89', '8000', '4000', '5000', '0000-00-00 00:00:00', '', 'TT', '$', '$');

-- --------------------------------------------------------

--
-- Table structure for table `coal_discharge`
--

CREATE TABLE `coal_discharge` (
  `id_disch` int(11) NOT NULL,
  `id_coalSch` int(11) NOT NULL,
  `voyage_no` varchar(225) NOT NULL,
  `bl_qty` varchar(225) NOT NULL,
  `bl_no` varchar(225) NOT NULL,
  `ta` date NOT NULL,
  `comm_disch` date NOT NULL,
  `comp_disch` date NOT NULL,
  `td` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coal_schedule`
--

CREATE TABLE `coal_schedule` (
  `id_coalSch` int(11) NOT NULL,
  `id_contract` int(11) NOT NULL,
  `material` varchar(255) NOT NULL,
  `laycan_loading` date NOT NULL,
  `eta` date NOT NULL,
  `estimate_daily` varchar(255) NOT NULL,
  `minumum_stock` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `eta_total` varchar(255) NOT NULL,
  `vessel_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `laycan_end` date NOT NULL,
  `premium_1` varchar(225) NOT NULL,
  `premium_2` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coal_schedule`
--

INSERT INTO `coal_schedule` (`id_coalSch`, `id_contract`, `material`, `laycan_loading`, `eta`, `estimate_daily`, `minumum_stock`, `type`, `eta_total`, `vessel_name`, `status`, `laycan_end`, `premium_1`, `premium_2`) VALUES
(6, 4, 'Australian Steam Coal (PCI)', '2016-12-01', '2016-12-04', '467', '4000', 'Vessel', '55000', 'MV Chesire', 0, '0000-00-00', '', ''),
(7, 5, 'Australian Steam Coal (Callide)', '0000-00-00', '2016-12-26', '10', '20', 'Vessel', ' 61428 ', 'MV KMARIN BUSAN', 0, '0000-00-00', '614280', '1228560');

-- --------------------------------------------------------

--
-- Table structure for table `coal_spec`
--

CREATE TABLE `coal_spec` (
  `id_spec` int(11) NOT NULL,
  `id_contract` int(11) NOT NULL,
  `id_parameter` int(11) DEFAULT NULL,
  `typical` varchar(255) NOT NULL,
  `id_satuan` varchar(225) DEFAULT NULL,
  `id_remarks` varchar(225) DEFAULT NULL,
  `rejection` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coal_spec`
--

INSERT INTO `coal_spec` (`id_spec`, `id_contract`, `id_parameter`, `typical`, `id_satuan`, `id_remarks`, `rejection`) VALUES
(10, 4, 10, '5740', 'kcal/kg', 'Tes Satuan', ''),
(12, 5, 10, '4600', 'kcal/kg', '', '4400');

-- --------------------------------------------------------

--
-- Table structure for table `contract_export`
--

CREATE TABLE `contract_export` (
  `id_contract` int(11) NOT NULL,
  `no_contract` varchar(30) NOT NULL,
  `name_shiper` varchar(255) NOT NULL,
  `description_goods` varchar(30) NOT NULL,
  `ni_content` varchar(100) DEFAULT NULL,
  `price_pct` varchar(100) DEFAULT NULL,
  `quantityC` varchar(100) DEFAULT NULL,
  `discharge_name` varchar(100) NOT NULL,
  `export_country` varchar(100) NOT NULL,
  `ETD_kendari` date DEFAULT NULL,
  `ETA_discharge_port` date DEFAULT NULL,
  `start_loading` date DEFAULT NULL,
  `complate_loading` date DEFAULT NULL,
  `PEB_process` date DEFAULT NULL,
  `LC_no` varchar(100) DEFAULT NULL,
  `submit_doc_bank` date DEFAULT NULL,
  `lead_time_loading` int(10) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `nilai_mata_uang` varchar(20) NOT NULL,
  `dateC` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contract_export`
--

INSERT INTO `contract_export` (`id_contract`, `no_contract`, `name_shiper`, `description_goods`, `ni_content`, `price_pct`, `quantityC`, `discharge_name`, `export_country`, `ETD_kendari`, `ETA_discharge_port`, `start_loading`, `complate_loading`, `PEB_process`, `LC_no`, `submit_doc_bank`, `lead_time_loading`, `status`, `nilai_mata_uang`, `dateC`) VALUES
(5, 'XDVDNI20180831A01', '3', 'FERRONICKEL', NULL, ' 124.55', '10000', 'CHENJIAGANG', 'CHINA', NULL, NULL, NULL, NULL, NULL, 'LC0957618005417', NULL, NULL, 'Complate', '$', '2018-10-31'),
(6, '12', '3', '12', NULL, ' 12', '1231', '1221', '12', NULL, NULL, NULL, NULL, NULL, '312', NULL, NULL, 'On Going', '$', '2018-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `eksport`
--

CREATE TABLE `eksport` (
  `id_eksport` int(11) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `quantity_loading` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eksport`
--

INSERT INTO `eksport` (`id_eksport`, `invoice_no`, `quantity_loading`) VALUES
(1, '003B/VDNI-SE-FN/XII/17', 100),
(2, '003B/VDNI-SE-FN/XII/17', 100),
(3, '003B/VDNI-SE-FN/XII/17', 1000.1),
(4, '003B/VDNI-SE-FN/XII/17', 600),
(5, '003B/VDNI-SE-FN/XII/17', 1000),
(6, '003B/VDNI-SE-FN/XII/17', 100),
(7, '003B/VDNI-SE-FN/XII/17', 10930.709),
(8, '003B/VDNI-SE-FN/XII/17', 40000),
(9, '003B/VDNI-SE-FN/XII/17', 200),
(10, '003B/VDNI-SE-FN/XII/17', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `master_shipper`
--

CREATE TABLE `master_shipper` (
  `id_shipper` int(11) NOT NULL,
  `name_shipper` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_shipper`
--

INSERT INTO `master_shipper` (`id_shipper`, `name_shipper`) VALUES
(1, 'FUJIAN XINGDA IMPORT & EXPORT TRADING CO. LTD');

-- --------------------------------------------------------

--
-- Table structure for table `not_longer`
--

CREATE TABLE `not_longer` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start` varchar(100) NOT NULL,
  `finish` varchar(100) NOT NULL,
  `smelter` varchar(100) NOT NULL,
  `plat` varchar(100) NOT NULL,
  `bruto` varchar(100) NOT NULL,
  `tara` varchar(100) NOT NULL,
  `netto` varchar(100) NOT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `r_admin_sistem`
--

CREATE TABLE `r_admin_sistem` (
  `id_r_admin_sistem` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_sistem` int(11) NOT NULL,
  `hak_akses` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `r_admin_sistem`
--

INSERT INTO `r_admin_sistem` (`id_r_admin_sistem`, `id_admin`, `id_sistem`, `hak_akses`) VALUES
(63, 26, 2, 'F'),
(69, 28, 3, 'F'),
(87, 31, 1, 'F'),
(88, 31, 2, 'F'),
(89, 31, 3, 'F'),
(90, 31, 4, 'F'),
(103, 27, 1, 'F'),
(104, 27, 2, 'F'),
(105, 27, 3, 'F'),
(106, 27, 4, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `sistem`
--

CREATE TABLE `sistem` (
  `id_sistem` int(11) NOT NULL,
  `sistem` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sistem`
--

INSERT INTO `sistem` (`id_sistem`, `sistem`) VALUES
(1, 'ORE'),
(2, 'COAL'),
(3, 'LIMESTONE');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id_site` int(11) NOT NULL,
  `date` date NOT NULL,
  `start` varchar(100) NOT NULL,
  `finish` varchar(100) NOT NULL,
  `wajan` int(100) NOT NULL,
  `smelter` varchar(100) NOT NULL,
  `plat` varchar(100) NOT NULL,
  `bruto` varchar(100) NOT NULL,
  `tara` varchar(100) NOT NULL,
  `netto` varchar(100) NOT NULL,
  `dateAcuan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id_site`, `date`, `start`, `finish`, `wajan`, `smelter`, `plat`, `bruto`, `tara`, `netto`, `dateAcuan`) VALUES
(1065, '2018-06-27', '1:03', '1:08', 4, '708', '47', '56.06', '15.94', '40.12', '2018-06-26'),
(1066, '2018-06-27', '1:21', '1:08', 4, '708', '47', '50.68', '15.94', '34.74', '2018-06-26'),
(1067, '2018-06-27', '1:29', '1:33', 5, '655', 'W47', '53.88', '16.06', '37.82', '2018-06-26'),
(1068, '2018-06-27', '2:11', '1:08', 3, '555', '47', '37.94', '15.94', '22', '2018-06-26'),
(1069, '2018-06-27', '2:31', '1:33', 5, '655', 'W47', '57.38', '16.06', '41.32', '2018-06-26'),
(1070, '2018-06-27', '2:42', '1:08', 3, '555', '47', '44.84', '15.94', '28.9', '2018-06-26'),
(1071, '2018-06-27', '2:53', '2:02', 1, '329', '41', '54.6', '16.46', '38.14', '2018-06-26'),
(1072, '2018-06-27', '3:03', '1:08', 2, '620', '47', '62.06', '15.94', '46.12', '2018-06-26'),
(1073, '2018-06-27', '3:09', '1:33', 7, '134', 'W47', '58.04', '16.06', '41.98', '2018-06-26'),
(1074, '2018-06-27', '3:17', '2:02', 7, '134', '41', '22.48', '16.46', '6.02', '2018-06-26'),
(1075, '2018-06-27', '3:37', '1:08', 8, '22', '47', '54.72', '15.94', '38.78', '2018-06-26'),
(1076, '2018-06-27', '3:52', '1:08', 8, '22', '47', '25.86', '15.94', '9.92', '2018-06-26'),
(1077, '2018-06-27', '8:30', '8:57', 4, '709', 'W21', '55.42', '15.8', '39.62', '2018-06-27'),
(1078, '2018-06-27', '8:46', '9:18', 3, '556', 'W04', '51.14', '15.92', '35.22', '2018-06-27'),
(1079, '2018-06-27', '9:29', '9:18', 5, '656', 'W04', '37.56', '15.92', '21.64', '2018-06-27'),
(1080, '2018-06-27', '9:37', '9:48', 2, '621', 'W21', '52.56', '15.8', '36.76', '2018-06-27'),
(1081, '2018-06-27', '9:51', '9:18', 5, '656', 'W04', '39.86', '15.92', '23.94', '2018-06-27'),
(1082, '2018-06-27', '10:00', '9:48', 2, '621', 'W21', '56.6', '15.8', '40.8', '2018-06-27'),
(1083, '2018-06-27', '10:11', '9:18', 2, '621', 'W04', '28.48', '15.92', '12.56', '2018-06-27'),
(1084, '2018-06-27', '14:37', '14:44', 1, '330', '72', '46.18', '16.46', '29.72', '2018-06-27'),
(1085, '2018-06-27', '14:43', '14:49', 1, '330', 'W47', '46.3', '15.88', '30.42', '2018-06-27'),
(1086, '2018-06-27', '15:00', '14:44', 7, '135', '72', '51.28', '16.46', '34.82', '2018-06-27'),
(1087, '2018-06-27', '15:10', '14:49', 7, '135', 'W47', '47.74', '15.88', '31.86', '2018-06-27'),
(1088, '2018-06-27', '15:19', '14:44', 8, '23', '72', '42.48', '16.46', '26.02', '2018-06-27'),
(1089, '2018-06-27', '16:49', '14:44', 2, '622', '72', '55.12', '16.46', '38.66', '2018-06-27'),
(1090, '2018-06-27', '17:14', '14:44', 2, '622', '72', '54.96', '16.46', '38.5', '2018-06-27'),
(1091, '2018-06-27', '17:28', '17:33', 1, '331', 'W47', '46.5', '15.88', '30.62', '2018-06-27'),
(1092, '2018-06-27', '17:35', '17:40', 3, '557', '72', '45.96', '16.46', '29.5', '2018-06-27'),
(1093, '2018-06-27', '17:54', '18:05', 3, '557', '72', '47.02', '16.46', '30.56', '2018-06-27'),
(1094, '2018-06-27', '19:28', '19:34', 4, '710', 'W08', '57', '17', '40', '2018-06-27'),
(1095, '2018-06-27', '19:47', '18:05', 4, '710', '72', '61.12', '16.46', '44.66', '2018-06-27'),
(1096, '2018-06-27', '19:49', '19:54', 5, '657', 'W08', '44.06', '17', '27.06', '2018-06-27'),
(1097, '2018-06-27', '20:05', '20:11', 5, '657', '72', '44.02', '16.46', '27.56', '2018-06-27'),
(1098, '2018-06-27', '20:19', '20:23', 8, '24', 'W08', '44.32', '17', '27.32', '2018-06-27'),
(1099, '2018-06-27', '20:33', '20:40', 7, '136', '72', '50.8', '16.46', '34.34', '2018-06-27'),
(1100, '2018-06-29', '1:19', '1:26', 5, '661', '41', '45.66', '16.28', '29.38', '2018-06-28'),
(1101, '2018-06-29', '1:24', '1:29', 7, '140', '51', '43.14', '15.92', '27.22', '2018-06-28'),
(1102, '2018-06-29', '1:41', '1:26', 5, '661', '41', '41.46', '16.28', '25.18', '2018-06-28'),
(1103, '2018-06-29', '1:50', '1:29', 7, '140', '51', '46.82', '15.92', '30.9', '2018-06-28'),
(1104, '2018-06-29', '1:58', '1:26', 3, '561', '41', '40.7', '16.28', '24.42', '2018-06-28'),
(1105, '2018-06-29', '2:08', '1:29', 8, '27', '51', '46.4', '15.92', '30.48', '2018-06-28'),
(1106, '2018-06-29', '2:19', '1:26', 3, '561', '41', '39.36', '16.28', '23.08', '2018-06-28'),
(1107, '2018-06-29', '2:27', '1:29', 8, '28', '51', '42', '15.92', '26.08', '2018-06-28'),
(1108, '2018-06-29', '2:38', '1:26', 4, '714', '41', '57.82', '16.28', '41.54', '2018-06-28'),
(1109, '2018-06-29', '2:50', '1:29', 8, '28', '51', '47.12', '15.92', '31.2', '2018-06-28'),
(1110, '2018-06-29', '3:04', '1:26', 1, '335', '41', '51.66', '16.28', '35.38', '2018-06-28'),
(1111, '2018-06-29', '3:22', '1:29', 8, '28', '51', '43.26', '15.92', '27.34', '2018-06-28'),
(1112, '2018-06-29', '3:30', '1:26', 1, '335', '41', '44.62', '16.28', '28.34', '2018-06-28'),
(1113, '2018-06-29', '3:39', '1:29', 2, '626', '51', '52.36', '15.92', '36.44', '2018-06-28'),
(1114, '2018-06-29', '3:48', '1:26', 2, '626', '41', '46.28', '16.28', '30', '2018-06-28'),
(1115, '2018-06-29', '8:16', '8:20', 4, '715', '72', '50.3', '16.54', '33.76', '2018-06-29'),
(1116, '2018-06-29', '8:34', '8:20', 4, '715', '72', '50.98', '16.54', '34.44', '2018-06-29'),
(1117, '2018-06-29', '8:51', '8:20', 3, '562', '72', '57.04', '16.54', '40.5', '2018-06-29'),
(1118, '2018-06-29', '9:07', '8:20', 5, '662', '72', '39.18', '16.54', '22.64', '2018-06-29'),
(1119, '2018-06-29', '9:22', '8:20', 5, '662', '72', '48.4', '16.54', '31.86', '2018-06-29'),
(1120, '2018-06-29', '9:39', '8:20', 1, '336', '72', '54.58', '16.54', '38.04', '2018-06-29'),
(1121, '2018-06-29', '9:52', '8:20', 2, '627', '72', '46.84', '16.54', '30.3', '2018-06-29'),
(1122, '2018-06-29', '10:18', '10:22', 8, '29', '62', '41.7', '19.14', '22.56', '2018-06-29'),
(1123, '2018-06-29', '10:35', '10:40', 8, '29', '62', '43.06', '19', '24.06', '2018-06-29'),
(1124, '2018-06-29', '10:36', '10:43', 8, '29', '72', '45.7', '16.38', '29.32', '2018-06-29'),
(1125, '2018-06-29', '10:51', '10:55', 7, '141', '62', '45.22', '18.76', '26.46', '2018-06-29'),
(1126, '2018-06-29', '10:53', '11:08', 8, '29', '41', '43.74', '16.18', '27.56', '2018-06-29'),
(1127, '2018-06-29', '16:16', '16:21', 1, '337', '62', '52.26', '19.18', '33.08', '2018-06-29'),
(1128, '2018-06-29', '16:27', '16:34', 1, '337', '72', '51.34', '16.38', '34.96', '2018-06-29'),
(1129, '2018-06-29', '16:36', '16:41', 2, '628', '62', '51.34', '19.04', '32.3', '2018-06-29'),
(1130, '2018-06-29', '16:52', '16:55', 2, '628', '72', '56.48', '16.32', '40.16', '2018-06-29'),
(1131, '2018-06-29', '19:49', '19:54', 5, '663', 'W47', '59.22', '16.04', '43.18', '2018-06-29'),
(1132, '2018-06-29', '19:55', '20:04', 4, '716', '72', '50.42', '16.32', '34.1', '2018-06-29'),
(1133, '2018-06-29', '20:00', '19:54', 4, '716', 'W47', '45.32', '16.04', '29.28', '2018-06-29'),
(1134, '2018-06-29', '20:12', '20:23', 3, '563', '72', '46.78', '16.32', '30.46', '2018-06-29'),
(1135, '2018-06-29', '20:19', '19:54', 3, '563', 'W47', '28.36', '16.04', '12.32', '2018-06-29'),
(1136, '2018-06-29', '20:31', '20:23', 7, '142', '72', '44.28', '16.32', '27.96', '2018-06-29'),
(1137, '2018-06-29', '20:42', '20:58', 7, '142', 'W47', '43.58', '16.04', '27.54', '2018-06-29'),
(1138, '2018-06-29', '20:57', '21:30', 8, '30', '72', '50.76', '16.36', '34.4', '2018-06-29'),
(1139, '2018-06-29', '21:14', '21:18', 8, '30', 'W47', '48.96', '16.12', '32.84', '2018-06-29'),
(1140, '2018-06-29', '21:26', '21:30', 8, '30', '72', '47.5', '16.36', '31.14', '2018-06-29');

-- --------------------------------------------------------

--
-- Table structure for table `specification`
--

CREATE TABLE `specification` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `nm_spec` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specification`
--

INSERT INTO `specification` (`id`, `category`, `nm_spec`) VALUES
(4, '3', 'Tes Remarks'),
(10, '1', 'Calorific Value (ARB)'),
(11, '1', 'Total Moisture (ARB)'),
(12, '2', 'kcal/kg'),
(13, '2', '%');

-- --------------------------------------------------------

--
-- Table structure for table `stockpile`
--

CREATE TABLE `stockpile` (
  `id_stockpile` int(11) NOT NULL,
  `date` date NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockpile`
--

INSERT INTO `stockpile` (`id_stockpile`, `date`, `total`) VALUES
(3, '2018-06-30', 375.22),
(4, '2018-07-01', 977.08),
(5, '2018-07-02', 582.6);

-- --------------------------------------------------------

--
-- Table structure for table `stock_coal`
--

CREATE TABLE `stock_coal` (
  `id_stockCoal` int(11) NOT NULL,
  `id_contract` int(11) NOT NULL,
  `stock` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_coal`
--

INSERT INTO `stock_coal` (`id_stockCoal`, `id_contract`, `stock`, `amount`) VALUES
(4, 4, '55000', '4639800'),
(5, 5, ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_coal`
--

CREATE TABLE `supplier_coal` (
  `id_sup` int(11) NOT NULL,
  `nm_supplier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_coal`
--

INSERT INTO `supplier_coal` (`id_sup`, `nm_supplier`) VALUES
(26, 'GLENCORE INTERNATIONAL AG'),
(27, '  AVRA COMMODITIES PTE LTD '),
(28, ' XIAMEN XIANGYU LOGISTICS GROUP CORPORATION'),
(29, ' PT MAHAKARTA SENTRA ENERGI '),
(30, ' FUJIAN XINGDA IMPORT & EXPORT TRADING CO., LTD ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_adjustment`
--

CREATE TABLE `tb_adjustment` (
  `id_adjustment` int(11) NOT NULL,
  `id_invoice` varchar(10) NOT NULL,
  `quantityA` varchar(10) NOT NULL,
  `price_pctA` varchar(100) NOT NULL,
  `niA` varchar(10) NOT NULL,
  `price_tonnageA` varchar(10) NOT NULL,
  `amountA` varchar(100) NOT NULL,
  `overpayment` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_adjustment`
--

INSERT INTO `tb_adjustment` (`id_adjustment`, `id_invoice`, `quantityA`, `price_pctA`, `niA`, `price_tonnageA`, `amountA`, `overpayment`) VALUES
(17, '6', '12.100', '$. 124,55', '8,15', '$. 1.015,0', '$. 12.282.468,00', '$. 1.997.098,59'),
(18, '8', '12', '¥.12', '12,55', '¥. 150,60', '¥. 1.807,20', '¥. 0,00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hasil_lab`
--

CREATE TABLE `tb_hasil_lab` (
  `id_lab` int(11) NOT NULL,
  `id_stok` varchar(50) NOT NULL,
  `coa_qty` varchar(255) NOT NULL,
  `ni` varchar(50) NOT NULL,
  `fe` varchar(50) NOT NULL,
  `sio` varchar(50) NOT NULL,
  `mgo` varchar(50) NOT NULL,
  `aio` varchar(50) NOT NULL,
  `sio_mgo` varchar(50) NOT NULL,
  `fe_ni` varchar(50) NOT NULL,
  `mc` varchar(50) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `ni_stat` varchar(255) NOT NULL,
  `mc_stat` varchar(255) NOT NULL,
  `sio_mgo_stat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_intertek`
--

CREATE TABLE `tb_intertek` (
  `id_intertek` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `intertek_qty` varchar(255) NOT NULL,
  `ni` varchar(255) NOT NULL,
  `fe` varchar(255) NOT NULL,
  `sio` varchar(255) NOT NULL,
  `mgo` varchar(255) NOT NULL,
  `aio` varchar(255) NOT NULL,
  `sio_mgo` varchar(255) NOT NULL,
  `fe_ni` varchar(255) NOT NULL,
  `mc` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `ni_stat` varchar(255) NOT NULL,
  `mc_stat` varchar(255) NOT NULL,
  `sio_mgo_stat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `id_invoice` int(11) NOT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `id_contract` varchar(5) NOT NULL,
  `shiping_terms` varchar(100) NOT NULL,
  `tt` int(3) NOT NULL,
  `lc` int(3) NOT NULL,
  `nominalTT` varchar(20) NOT NULL,
  `nominalLC` varchar(20) DEFAULT NULL,
  `shipping_method` varchar(30) NOT NULL,
  `vassel_name` varchar(100) NOT NULL,
  `etd_kendari` date DEFAULT NULL,
  `eta_discharge_port` date DEFAULT NULL,
  `lc_no` varchar(20) DEFAULT NULL,
  `ni` varchar(20) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `net_weight` varchar(100) NOT NULL,
  `unit_price` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `peb_no` varchar(20) DEFAULT NULL,
  `nopen_npe` varchar(20) DEFAULT NULL,
  `bill_landing_no` varchar(20) DEFAULT NULL,
  `price_tonnage` varchar(20) DEFAULT NULL,
  `date_paid_adjustment` date DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_invoice`
--

INSERT INTO `tb_invoice` (`id_invoice`, `invoice_no`, `id_contract`, `shiping_terms`, `tt`, `lc`, `nominalTT`, `nominalLC`, `shipping_method`, `vassel_name`, `etd_kendari`, `eta_discharge_port`, `lc_no`, `ni`, `quantity`, `net_weight`, `unit_price`, `amount`, `peb_no`, `nopen_npe`, `bill_landing_no`, `price_tonnage`, `date_paid_adjustment`, `remarks`, `status`) VALUES
(6, '015/VDNI-SE-FN/IX/18', '5', 'FOB', 40, 60, ' 5978400', ' 8301166.59', 'SEA FREIGHT', 'MV TAY SON 1', '2018-10-30', '2018-11-09', NULL, '9,554', '12000.140', '12000140', '1189.95', ' 14279566.59', '110600-000071-201804', '12', 'PSI/VD/20180304', '$.1.189,950', '2018-10-10', '', 'Complate'),
(8, '12', '6', '12', 12, 12, ' 12', ' 12', '12', '12', '1212-12-12', '1212-12-12', NULL, '12,55', '12', '12000.00', ' 150.60', ' 1807.20', '12', '12', '12', '¥.150,600', NULL, '', 'Waiting Paid Date'),
(9, '123', '6', '12', 12, 12, ' 12', '', '12', '12', '0000-00-00', '0000-00-00', NULL, '12', '12', '12000.00', ' 144.00', ' 1728.00', NULL, NULL, NULL, NULL, NULL, NULL, 'Waiting Adjustment');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kontrak_kecil`
--

CREATE TABLE `tb_kontrak_kecil` (
  `id` int(11) NOT NULL,
  `id_kontrak` varchar(255) NOT NULL,
  `supplier` int(11) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `status` enum('AKTIF','NON AKTIF') NOT NULL,
  `seller_agent` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kontrak_kecil`
--

INSERT INTO `tb_kontrak_kecil` (`id`, `id_kontrak`, `supplier`, `quantity`, `status`, `seller_agent`) VALUES
(62, 'KTR2', 2, '5000', 'AKTIF', '1'),
(63, 'KTR2', 3, '3000', 'AKTIF', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_contract`
--

CREATE TABLE `tb_master_contract` (
  `id_kontrak` varchar(255) NOT NULL,
  `seller_agent` int(11) NOT NULL,
  `master_kontrak` varchar(100) NOT NULL,
  `addendum` varchar(50) NOT NULL,
  `sales_kontrak` varchar(80) NOT NULL,
  `swift_lc` varchar(100) NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `terms` varchar(20) NOT NULL,
  `ni` double NOT NULL,
  `ni_usd` varchar(255) NOT NULL,
  `mc` varchar(255) NOT NULL,
  `mc_usd` varchar(255) NOT NULL,
  `sio_mgo` double NOT NULL,
  `sio_usd` varchar(255) NOT NULL,
  `keterangan` text,
  `price_idr` varchar(255) NOT NULL,
  `cif_usd` varchar(255) NOT NULL,
  `dap_usd` varchar(255) NOT NULL,
  `cif_sc_usd` varchar(255) NOT NULL,
  `dap_sc_usd` varchar(255) NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `status` enum('AKTIF','NON AKTIF') NOT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `lc_amount` varchar(255) DEFAULT NULL,
  `user` varchar(100) NOT NULL,
  `fe` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_contract`
--

INSERT INTO `tb_master_contract` (`id_kontrak`, `seller_agent`, `master_kontrak`, `addendum`, `sales_kontrak`, `swift_lc`, `tgl_awal`, `tgl_akhir`, `terms`, `ni`, `ni_usd`, `mc`, `mc_usd`, `sio_mgo`, `sio_usd`, `keterangan`, `price_idr`, `cif_usd`, `dap_usd`, `cif_sc_usd`, `dap_sc_usd`, `tgl_dibuat`, `status`, `quantity`, `lc_amount`, `user`, `fe`) VALUES
('KTR2', 1, 'Vdni/Tes/1234', 'Add I', 'TI010291', 'sadnas', '2018-10-14', '2018-10-24', 'CIFDAP', 1.8, '0.5', '30-35', '0.2', 2, '0', '', ' 350000', '29', '30', '30', '31', '2018-10-16', 'AKTIF', '8000', '240000', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_seller`
--

CREATE TABLE `tb_seller` (
  `id_seller` int(11) NOT NULL,
  `nama_seller` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_seller`
--

INSERT INTO `tb_seller` (`id_seller`, `nama_seller`) VALUES
(1, 'PT DUA DELAPAN RESOURCES LTD'),
(2, 'PT MINSOURCES INTERNATIONAL'),
(3, 'PT MAKMUR LESTARI PRIMATAMA'),
(4, 'PT BUMANIK');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stok`
--

CREATE TABLE `tb_stok` (
  `id_stok` int(11) NOT NULL,
  `master_kontrak` varchar(255) NOT NULL,
  `stok` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_stok`
--

INSERT INTO `tb_stok` (`id_stok`, `master_kontrak`, `stok`) VALUES
(1, 'KTR1', '10000'),
(2, 'KTR2', '8000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stok_adj`
--

CREATE TABLE `tb_stok_adj` (
  `id_adj` int(11) NOT NULL,
  `id_sm` int(11) NOT NULL,
  `jml_stok` varchar(100) NOT NULL,
  `tgl_input` date NOT NULL,
  `user` varchar(255) NOT NULL,
  `sisa_stok` varchar(255) NOT NULL,
  `master_kontrak` varchar(255) NOT NULL,
  `seller_agent` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `nama_tongkang` varchar(255) NOT NULL,
  `tgl_arr` datetime NOT NULL,
  `comm_disch` datetime NOT NULL,
  `compl_disch` datetime NOT NULL,
  `tgl_dep` datetime NOT NULL,
  `price_afterCoa` varchar(255) NOT NULL,
  `price_intertek` varchar(255) NOT NULL,
  `voyage_number` varchar(255) NOT NULL,
  `bl_number` varchar(255) NOT NULL,
  `intertek_qty` varchar(255) NOT NULL,
  `ni_i` varchar(255) NOT NULL,
  `fe_i` varchar(255) NOT NULL,
  `sio_i` varchar(255) NOT NULL,
  `mgo_i` varchar(255) NOT NULL,
  `aio_i` varchar(255) NOT NULL,
  `sio_mgo_i` varchar(255) NOT NULL,
  `fe_ni_i` varchar(255) NOT NULL,
  `mc_i` varchar(255) NOT NULL,
  `picture_i` varchar(255) NOT NULL,
  `ni_stat_i` varchar(255) NOT NULL,
  `mc_stat_i` varchar(255) NOT NULL,
  `sio_mgoStat_i` varchar(255) NOT NULL,
  `coa_qty` varchar(255) NOT NULL,
  `ni_c` varchar(255) NOT NULL,
  `fe_c` varchar(255) NOT NULL,
  `sio_c` varchar(255) NOT NULL,
  `mgo_c` varchar(255) NOT NULL,
  `aio_c` varchar(255) NOT NULL,
  `sio_mgo_c` varchar(255) NOT NULL,
  `fe_ni_c` varchar(255) NOT NULL,
  `mc_c` varchar(255) NOT NULL,
  `picture_c` varchar(255) NOT NULL,
  `ni_stat_c` varchar(255) NOT NULL,
  `mc_stat_c` varchar(255) NOT NULL,
  `sio_mgoStat_c` varchar(255) NOT NULL,
  `fin` int(11) NOT NULL,
  `sk` int(11) NOT NULL,
  `gm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_stok_masuk`
--

CREATE TABLE `tb_stok_masuk` (
  `id_sm` int(11) NOT NULL,
  `tgl_input` date NOT NULL,
  `jml_stok` int(100) NOT NULL,
  `sisa_stok` int(100) NOT NULL,
  `user` varchar(90) NOT NULL,
  `master_kontrak` varchar(100) NOT NULL,
  `seller_agent` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `nama_tongkang` varchar(255) NOT NULL,
  `tgl_arr` datetime NOT NULL,
  `tgl_dep` datetime NOT NULL,
  `comm_disch` datetime NOT NULL,
  `compl_disch` datetime NOT NULL,
  `price_afterCoa` varchar(255) NOT NULL,
  `price_intertek` varchar(255) NOT NULL,
  `voyage_number` varchar(255) NOT NULL,
  `bl_number` varchar(255) NOT NULL,
  `stat_lab` int(11) DEFAULT NULL,
  `coa_qty` varchar(255) DEFAULT NULL,
  `intertek_qty` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id_supplier`, `nama_supplier`) VALUES
(1, 'PT Anugrah Sukses Mining'),
(2, 'PT Minsources International'),
(3, 'PT Adhi Kartiko Pratama'),
(4, 'PT Sinar Jaya Sultra Utama'),
(5, 'PT Kabaena Kromit Prathama'),
(6, 'PT Total Prima Indonesia'),
(7, 'PT Baula Petra Buana'),
(8, 'PT Cinta Jaya'),
(9, 'PT Konutara Sejati'),
(10, 'PT Elit Kharisma Utama');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(10) NOT NULL,
  `level` varchar(20) NOT NULL,
  `sistem` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `level`, `sistem`) VALUES
(1, 'admin', 'admin', 'admin', 'ore'),
(2, 'coral', 'admin', 'admin', 'coral');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id_terms` int(11) NOT NULL,
  `nm_terms` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id_terms`, `nm_terms`) VALUES
(5, 'CIF '),
(6, 'CFR'),
(7, 'FOB'),
(8, 'FREIGHT CHARTER');

-- --------------------------------------------------------

--
-- Table structure for table `total_npi`
--

CREATE TABLE `total_npi` (
  `id_total_npi` int(11) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_npi`
--

INSERT INTO `total_npi` (`id_total_npi`, `total`) VALUES
(1, 1759.9);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_contract` varchar(100) NOT NULL,
  `id_invoice` varchar(20) NOT NULL,
  `barge` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `trip` varchar(100) NOT NULL,
  `startLoading` date NOT NULL,
  `complateLoading` date NOT NULL,
  `finalDraught` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_contract`, `id_invoice`, `barge`, `quantity`, `trip`, `startLoading`, `complateLoading`, `finalDraught`) VALUES
(2, '5', '6', 'TAURUS', '100', '10', '2018-09-09', '2018-09-11', NULL),
(3, '5', '6', 'SOEKAWATI', '75', '8', '2018-09-12', '2018-09-13', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_master`
--
ALTER TABLE `admin_master`
  ADD PRIMARY KEY (`id_adminM`);

--
-- Indexes for table `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`id_buy`);

--
-- Indexes for table `buyer_agent`
--
ALTER TABLE `buyer_agent`
  ADD PRIMARY KEY (`id_buyer`);

--
-- Indexes for table `coal_contract`
--
ALTER TABLE `coal_contract`
  ADD PRIMARY KEY (`id_contract`),
  ADD KEY `id_buyer` (`id_buyer`),
  ADD KEY `id_sup` (`id_sup`),
  ADD KEY `id_terms` (`id_terms`);

--
-- Indexes for table `coal_discharge`
--
ALTER TABLE `coal_discharge`
  ADD PRIMARY KEY (`id_disch`),
  ADD KEY `id_coalSch` (`id_coalSch`);

--
-- Indexes for table `coal_schedule`
--
ALTER TABLE `coal_schedule`
  ADD PRIMARY KEY (`id_coalSch`),
  ADD KEY `id_contract` (`id_contract`);

--
-- Indexes for table `coal_spec`
--
ALTER TABLE `coal_spec`
  ADD PRIMARY KEY (`id_spec`),
  ADD KEY `id_contract` (`id_contract`),
  ADD KEY `id_remarks` (`id_remarks`),
  ADD KEY `coal_spec_ibfk_4` (`id_satuan`),
  ADD KEY `id_parameter` (`id_parameter`);

--
-- Indexes for table `contract_export`
--
ALTER TABLE `contract_export`
  ADD PRIMARY KEY (`id_contract`);

--
-- Indexes for table `eksport`
--
ALTER TABLE `eksport`
  ADD PRIMARY KEY (`id_eksport`);

--
-- Indexes for table `master_shipper`
--
ALTER TABLE `master_shipper`
  ADD PRIMARY KEY (`id_shipper`);

--
-- Indexes for table `not_longer`
--
ALTER TABLE `not_longer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r_admin_sistem`
--
ALTER TABLE `r_admin_sistem`
  ADD PRIMARY KEY (`id_r_admin_sistem`);

--
-- Indexes for table `sistem`
--
ALTER TABLE `sistem`
  ADD PRIMARY KEY (`id_sistem`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id_site`);

--
-- Indexes for table `specification`
--
ALTER TABLE `specification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockpile`
--
ALTER TABLE `stockpile`
  ADD PRIMARY KEY (`id_stockpile`);

--
-- Indexes for table `stock_coal`
--
ALTER TABLE `stock_coal`
  ADD PRIMARY KEY (`id_stockCoal`),
  ADD KEY `id_contract` (`id_contract`);

--
-- Indexes for table `supplier_coal`
--
ALTER TABLE `supplier_coal`
  ADD PRIMARY KEY (`id_sup`);

--
-- Indexes for table `tb_adjustment`
--
ALTER TABLE `tb_adjustment`
  ADD PRIMARY KEY (`id_adjustment`);

--
-- Indexes for table `tb_hasil_lab`
--
ALTER TABLE `tb_hasil_lab`
  ADD PRIMARY KEY (`id_lab`);

--
-- Indexes for table `tb_intertek`
--
ALTER TABLE `tb_intertek`
  ADD PRIMARY KEY (`id_intertek`);

--
-- Indexes for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indexes for table `tb_kontrak_kecil`
--
ALTER TABLE `tb_kontrak_kecil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_master_contract`
--
ALTER TABLE `tb_master_contract`
  ADD PRIMARY KEY (`id_kontrak`);

--
-- Indexes for table `tb_seller`
--
ALTER TABLE `tb_seller`
  ADD PRIMARY KEY (`id_seller`);

--
-- Indexes for table `tb_stok`
--
ALTER TABLE `tb_stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `tb_stok_adj`
--
ALTER TABLE `tb_stok_adj`
  ADD PRIMARY KEY (`id_adj`);

--
-- Indexes for table `tb_stok_masuk`
--
ALTER TABLE `tb_stok_masuk`
  ADD PRIMARY KEY (`id_sm`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id_terms`);

--
-- Indexes for table `total_npi`
--
ALTER TABLE `total_npi`
  ADD PRIMARY KEY (`id_total_npi`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `admin_master`
--
ALTER TABLE `admin_master`
  MODIFY `id_adminM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buyer`
--
ALTER TABLE `buyer`
  MODIFY `id_buy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buyer_agent`
--
ALTER TABLE `buyer_agent`
  MODIFY `id_buyer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coal_contract`
--
ALTER TABLE `coal_contract`
  MODIFY `id_contract` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `coal_discharge`
--
ALTER TABLE `coal_discharge`
  MODIFY `id_disch` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coal_schedule`
--
ALTER TABLE `coal_schedule`
  MODIFY `id_coalSch` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coal_spec`
--
ALTER TABLE `coal_spec`
  MODIFY `id_spec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contract_export`
--
ALTER TABLE `contract_export`
  MODIFY `id_contract` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `eksport`
--
ALTER TABLE `eksport`
  MODIFY `id_eksport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `master_shipper`
--
ALTER TABLE `master_shipper`
  MODIFY `id_shipper` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `not_longer`
--
ALTER TABLE `not_longer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r_admin_sistem`
--
ALTER TABLE `r_admin_sistem`
  MODIFY `id_r_admin_sistem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `sistem`
--
ALTER TABLE `sistem`
  MODIFY `id_sistem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id_site` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1141;

--
-- AUTO_INCREMENT for table `specification`
--
ALTER TABLE `specification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stockpile`
--
ALTER TABLE `stockpile`
  MODIFY `id_stockpile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_coal`
--
ALTER TABLE `stock_coal`
  MODIFY `id_stockCoal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `supplier_coal`
--
ALTER TABLE `supplier_coal`
  MODIFY `id_sup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_adjustment`
--
ALTER TABLE `tb_adjustment`
  MODIFY `id_adjustment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tb_hasil_lab`
--
ALTER TABLE `tb_hasil_lab`
  MODIFY `id_lab` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_intertek`
--
ALTER TABLE `tb_intertek`
  MODIFY `id_intertek` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  MODIFY `id_invoice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_kontrak_kecil`
--
ALTER TABLE `tb_kontrak_kecil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tb_seller`
--
ALTER TABLE `tb_seller`
  MODIFY `id_seller` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_stok`
--
ALTER TABLE `tb_stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_stok_adj`
--
ALTER TABLE `tb_stok_adj`
  MODIFY `id_adj` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_stok_masuk`
--
ALTER TABLE `tb_stok_masuk`
  MODIFY `id_sm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id_terms` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `total_npi`
--
ALTER TABLE `total_npi`
  MODIFY `id_total_npi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coal_contract`
--
ALTER TABLE `coal_contract`
  ADD CONSTRAINT `coal_contract_ibfk_1` FOREIGN KEY (`id_buyer`) REFERENCES `buyer_agent` (`id_buyer`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coal_contract_ibfk_2` FOREIGN KEY (`id_sup`) REFERENCES `supplier_coal` (`id_sup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coal_contract_ibfk_3` FOREIGN KEY (`id_terms`) REFERENCES `terms` (`id_terms`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coal_discharge`
--
ALTER TABLE `coal_discharge`
  ADD CONSTRAINT `coal_discharge_ibfk_1` FOREIGN KEY (`id_coalSch`) REFERENCES `coal_schedule` (`id_coalSch`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coal_schedule`
--
ALTER TABLE `coal_schedule`
  ADD CONSTRAINT `coal_schedule_ibfk_1` FOREIGN KEY (`id_contract`) REFERENCES `coal_contract` (`id_contract`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coal_spec`
--
ALTER TABLE `coal_spec`
  ADD CONSTRAINT `coal_spec_ibfk_1` FOREIGN KEY (`id_contract`) REFERENCES `coal_contract` (`id_contract`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coal_spec_ibfk_2` FOREIGN KEY (`id_parameter`) REFERENCES `specification` (`id`);

--
-- Constraints for table `stock_coal`
--
ALTER TABLE `stock_coal`
  ADD CONSTRAINT `stock_coal_ibfk_1` FOREIGN KEY (`id_contract`) REFERENCES `coal_contract` (`id_contract`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
