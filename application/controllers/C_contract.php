<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_contract extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
  		parent::__construct();
      $sistem = $this->session->userdata('sistem');
      if(empty($sistem['ORE'])){
        if ($this->session->userdata('status') == "login") {
          redirect(base_url("admin/admin"));
        }else {
          redirect(base_url("admin/login_admin"));
        }

      }
	}
	
	public function index()
	{
		$this->db->select('*');
		$this->db->from('coal_contract');
		$this->db->join('supplier_coal', 'supplier_coal.id_sup = coal_contract.id_sup');
		$this->db->join('buyer_agent', 'buyer_agent.id_buyer = coal_contract.id_buyer');
		$this->db->join('terms', 'terms.id_terms = coal_contract.id_terms');
		$this->db->join('buyer', 'buyer.id_buy = coal_contract.buyer');
		$data['coal_contract'] = $this->db->get();
		$data['content'] = 'coal/data_contract';
		$this->load->view('coal/index',$data);
	}

	public function input_contract()
	{
		$data['supplier']        = $this->db->get('supplier_coal');
		$data['buyer']       	 = $this->db->get('buyer_agent');
		$data['terms']           = $this->db->get('terms');
		$data['buy']           = $this->db->get('buyer');
		$data['buyer']       	 = $this->db->get('buyer_agent');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		$data['content']		 = 'coal/contract';
		$this->load->view('coal/index',$data);
	}

	public function insert_contract()
	{
		$contract_no 	= $this->input->post('contract_no');
		$sales_purchase = $this->input->post('sales_purcahse');
		$supplier 		= $this->input->post('supplier');
		$buyer 			= $this->input->post('buyer');
		$buyer_agent 	= $this->input->post('buyer_agent');
		$lc_no 			= $this->input->post('lc_no');
		$qty_lc 		= $this->input->post('qty_lc');
		$amount_lc 		= $this->input->post('amount_lc');
		$term    		= $this->input->post('term_of_contract');
		$port_of_landing = $this->input->post('port_of_landing');
		$port_of_disch 	 = $this->input->post('port_of_discharge');
		$bl 			= $this->input->post('bl');
		$fob_priceSup = $this->input->post('fob_priceSup');
		$freightSup 	 = $this->input->post('freightSup');
		$fcc_priceSup 	 = $this->input->post('fcc_priceSup');
		$fob_priceBuy = $this->input->post('fob_priceBuy');
		$freightBuy 	 = $this->input->post('freightBuy');
		$fcc_priceBuy 	 = $this->input->post('fcc_priceBuy');
		$demurage = $this->input->post('demurage');
		$despatch 	 = $this->input->post('despatch');
		$discharging 	 = $this->input->post('discharging');
		$payment_method 	 = $this->input->post('payment_method');
		$sat_des 	 = $this->input->post('sat_des');
		$sat_dem	 = $this->input->post('sat_dem');

		$data = array(
			'contract_no' => $contract_no,
			'sales_purchase' => $sales_purchase,
			'id_sup' => $supplier,
			'buyer' => $buyer,
			'id_buyer' => $buyer_agent,
			'lc_no' => $lc_no,
			'quantity_lc' => $qty_lc,
			'amount_lc' => $amount_lc,
			'id_terms' => $term,
			'port_of_landing' => $port_of_landing,
			'port_of_discharge' => $port_of_disch,
			'bl' => $bl,
			'fob_sup' => $fob_priceSup,
			'freight_sup' => $freightSup,
			'cfr_priceSup' => $fcc_priceSup,
			'fob_buyer' => $fob_priceBuy,
			'freight_buyer' => $freightBuy,
			'cfr_priceBuyer' => $fcc_priceBuy,
			'demurage' => $demurage,
			'dispatch' => $despatch,
			'discharging' => $discharging,
			'payment_method' => $payment_method,
			'sat_des' => $sat_des,
			'sat_dem' => $sat_dem
			);
		$where = array(
			'contract_no' => $contract_no );
		$this->load->model('coal/m_contract');
		$cek = $this->m_contract->cek_data("coal_contract",$where);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal_kontrak', 'stat' => '1' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('coal_contract',$data);
		   $header =$this->db->insert_id();
		   $v1 =$this->input->post('typical');
		   
		   while(list($key,$value)=each($v1))	
				{
				    $coa        = $this->input->post("coa[$key]");
					$typical	= $this->input->post("typical[$key]");
					$coa1		= $this->input->post("coa1[$key]");
					$coa2		= $this->input->post("remarks[$key]");
					$coa3		= $this->input->post("rejection[$key]");
					$data2      = array('id_parameter'=>$coa,
								 'id_contract'=>$header,
								 'typical'=>$typical,
								 'id_satuan'=>$coa1,
				  				 'id_remarks'=>$coa2,
				  				 'rejection'=>$coa3);
				    $this->db->insert('coal_spec',$data2);			 
				}
		   $stock = array('id_contract' => $header, 'stock' => $qty_lc, 'amount' => $amount_lc );
		   $this->db->insert('stock_coal',$stock);
		   $aksi  = array('aksi' => 'sukses_kontrak', 'stat' => '1' );
		   $this->load->view('coal/sukses', $aksi);
		   }
		}

		public function view_contract($id)
		{
			$this->db->select('*');
			$this->db->from('coal_contract');
			$this->db->join('supplier_coal', 'supplier_coal.id_sup = coal_contract.id_sup');
			$this->db->join('buyer_agent', 'buyer_agent.id_buyer = coal_contract.id_buyer');
			$this->db->join('terms', 'terms.id_terms = coal_contract.id_terms');
			$this->db->join('buyer', 'buyer.id_buy = coal_contract.buyer');
			$this->db->where('coal_contract.id_contract', $id);
			$data['coal_contract'] = $this->db->get();
			$this->db->select('*');
			$this->db->from('coal_spec');
			$this->db->join('specification', 'specification.id = coal_spec.id_parameter');
			$this->db->where('coal_spec.id_contract', $id);
			$data['specification'] = $this->db->get();
			$this->db->where('id_contract', $id);
		    $data['data']    = $this->db->get('coal_schedule');
		    $where = array('id_coalSch' => $id );
			$data['content'] = 'coal/view_contract';
			$this->load->view('coal/index',$data);
		}

		public function input_schedule($id)
		{
			$this->db->where('id_contract', $id);
		    $data['data']    = $this->db->get('coal_contract');
			$data['content'] = 'coal/input_schedule';
			$this->load->view('coal/index',$data);
		}

		public function hapus_contract($id)
		{
			$this->db->where('id_contract', $id);
    		$this->db->delete('coal_contract');
    		$this->db->where('id_contract', $id);
    		$this->db->delete('coal_spec');
    		$this->db->where('id_contract', $id);
    		$this->db->delete('stock_coal');
    		$aksi = array('aksi' => 'hapus_kontrak', 'stat' => '1' );
			$this->load->view('coal/sukses', $aksi);
		}

		public function edit_contract($id)
	{
		$data['supplier']        = $this->db->get('supplier_coal');
		$data['buyer']       	 = $this->db->get('buyer_agent');
		$data['terms']           = $this->db->get('terms');
		$data['buy']       	 = $this->db->get('buyer');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		$this->db->select('*');
		$this->db->from('coal_contract');
		$this->db->join('supplier_coal', 'supplier_coal.id_sup = coal_contract.id_sup');
		$this->db->join('buyer_agent', 'buyer_agent.id_buyer = coal_contract.id_buyer');
		$this->db->join('terms', 'terms.id_terms = coal_contract.id_terms');
		$this->db->join('buyer', 'buyer.id_buy = coal_contract.buyer');
		$this->db->where('coal_contract.id_contract', $id);
		$data['data'] = $this->db->get();
    	$this->db->from('coal_spec');
		$this->db->join('specification', 'specification.id = coal_spec.id_parameter');
		$this->db->where('coal_spec.id_contract', $id);
		$data['specification'] = $this->db->get();
    	$data['content']		 = 'coal/edit_contract';
		$this->load->view('coal/index',$data);

	}

	public function hapus_spec($id_spec, $id_contract)
		{
    		$this->db->where('id_spec', $id_spec);
    		$this->db->delete('coal_spec');
    		$aksi = array('aksi' => 'hapus_spec', 'stat' => $id_contract );
			$this->load->view('coal/sukses', $aksi);
		}

	public function edit_contractAction()
	{
		$contract_no 	= $this->input->post('contract_no');
		$sales_purchase = $this->input->post('sales_purcahse');
		$supplier 		= $this->input->post('supplier');
		$buyer 			= $this->input->post('buyer');
		$buyer_agent 	= $this->input->post('buyer_agent');
		$lc_no 			= $this->input->post('lc_no');
		$qty_lc 		= $this->input->post('qty_lc');
		$amount_lc 		= $this->input->post('amount_lc');
		$term    		= $this->input->post('term_of_contract');
		$port_of_landing = $this->input->post('port_of_landing');
		$port_of_disch 	 = $this->input->post('port_of_discharge');
		$bl 			= $this->input->post('bl');
		$fob_priceSup = $this->input->post('fob_priceSup');
		$freightSup 	 = $this->input->post('freightSup');
		$fcc_priceSup 	 = $this->input->post('fcc_priceSup');
		$fob_priceBuy = $this->input->post('fob_priceBuy');
		$freightBuy 	 = $this->input->post('freightBuy');
		$fcc_priceBuy 	 = $this->input->post('fcc_priceBuy');
		$demurage = $this->input->post('demurage');
		$despatch 	 = $this->input->post('despatch');
		$discharging 	 = $this->input->post('discharging');
		$payment_method 	 = $this->input->post('payment_method');
		$sat_des 	 = $this->input->post('sat_des');
		$sat_dem	 = $this->input->post('sat_dem');
		$id_contract = $this->input->post('id_contract');
		$contract_noLama = $this->input->post('contract_noLama');
		if ($contract_no == $contract_noLama) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$data = array(
			'contract_no' => $contract_no,
			'sales_purchase' => $sales_purchase,
			'id_sup' => $supplier,
			'buyer' => $buyer,
			'id_buyer' => $buyer_agent,
			'lc_no' => $lc_no,
			'quantity_lc' => $qty_lc,
			'amount_lc' => $amount_lc,
			'id_terms' => $term,
			'port_of_landing' => $port_of_landing,
			'port_of_discharge' => $port_of_disch,
			'bl' => $bl,
			'fob_sup' => $fob_priceSup,
			'freight_sup' => $freightSup,
			'cfr_priceSup' => $fcc_priceSup,
			'fob_buyer' => $fob_priceBuy,
			'freight_buyer' => $freightBuy,
			'cfr_priceBuyer' => $fcc_priceBuy,
			'demurage' => $demurage,
			'dispatch' => $despatch,
			'discharging' => $discharging,
			'payment_method' => $payment_method,
			'sat_des' => $sat_des,
			'sat_dem' => $sat_dem
			);
		$where = array(
			'contract_no' => $contract_no );
		$this->load->model('coal/m_contract');
		$cek = $this->m_contract->cek_data("coal_contract",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id_contract', $id_contract);
		   $this->db->update('coal_contract',$data);
		   $v1 =$this->input->post('typical');
		   while(list($key,$value)=each($v1)) 		 
				{
				    $coa        = $this->input->post("coa[$key]");
					$typical	= $this->input->post("typical[$key]");
					$coa1		= $this->input->post("coa1[$key]");
					$coa2		= $this->input->post("remarks[$key]");
					$coa3		= $this->input->post("rejection[$key]");
					$id_spec	= $this->input->post("id_spec[$key]");
					$data2      = array('id_parameter'=>$coa,
								 'id_contract'=>$id_contract,
								 'typical'=>$typical,
								 'id_satuan'=>$coa1,
				  				 'id_remarks'=>$coa2,
				  				 'rejection'=>$coa3 );
					$this->db->where('id_spec', $id_spec);
    		        $this->db->delete('coal_spec');
				    $this->db->insert('coal_spec', $data2);			 
				}

		   $aksi = array('aksi' => 'update_kontrak', 'stat' => $cek->num_rows() );
		   $stock = array('stock' => $qty_lc, 'amount' => $amount_lc );
		   $this->db->where('id_contract', $id_contract);
		   $this->db->update('stock_coal',$stock);
		   $this->load->view('coal/sukses', $aksi);
		} else {
		    $aksi = array('aksi' => 'gagal_kontrak', 'stat' => '1' );
			$this->load->view('coal/alert', $aksi);

		   }
		}
}
