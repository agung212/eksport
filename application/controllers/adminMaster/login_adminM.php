<?php

class Login_adminM extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('admin_master/m_adminM');

	}

	function index(){
		$this->load->view('login_admin/v_adminM');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password
			);
		$cek = $this->m_adminM->cek_login("admin_master",$where)->num_rows();
		if($cek > 0){
			$data_session = array(
				'nama' => $username,
				'status' => "login",
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("adminMaster/adminM"));

		}else{
			echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}


	public function show_reload()
	{
		$this->load->view('reload');
	}

	function reload(){
		$query = $this->db->get('admin');
		if ($query->num_rows() == 0) {
    		echo "data kosong";
		} else {
			foreach ($query->result() as $row)
			{
        echo $row->username;

			}
    }

	}
}
