<?php

class AdminM extends CI_Controller{

	function __construct(){
		parent::__construct();
			//memeriksa apakah pengguna diizinkan masuk kedalam website
		if($this->session->userdata('name') != "admin"){
			if ($this->session->userdata('status') == "login") {
				redirect(base_url("admin/admin"));
			}else {
				redirect(base_url("admin/login_admin"));
			}
		}
	}

	function index(){
		//mengambil data admin dan menampilkanya pada halaman utama admin
		$data['admin'] = $this->db->get('admin')->result();
		$this->load->view('admin_master/v_adminM',$data);
	}

	public function f_admin(){
		//menampilkan halaman form create user
		$this->load->view('admin_master/V_Fadmin');
	}

	public function Register_admin(){
		// set aturan form
		$this->form_validation->set_message('is_unique', 'The %s is already taken');
		$this->form_validation->set_rules('name', 'NAME','required');
		$this->form_validation->set_message('matches', 'The PASSWORD field does not match the CONFIRM PASSWORD field');
    $this->form_validation->set_rules('username', 'USERNAME','required|is_unique[admin.username]');
    $this->form_validation->set_rules('password','PASSWORD','required|min_length[8]|max_length[16]');
    $this->form_validation->set_rules('password_conf','PASSWORD','required|matches[password]');
    if($this->form_validation->run() == FALSE) {
			//jika form tidak sesuai aturan maka dikembalikan pada halaman nya dengan data error
    	$this->load->view('admin_master/V_Fadmin');
    }else{
			//jika data sesuai aturan maka data disimpan pada variable dan ditambahkan kedalam database
      $data['nama']   = $this->input->post('name');
      $data['username'] = $this->input->post('username');
      $data['password'] = md5($this->input->post('password'));
			$data['level'] = $this->input->post('level');
			$this->db->insert('admin',$data);
			//mengunakan fungsi last insert id untuk menambahkan data pda form r_sistem_admin
			$insert_id = $this->db->insert_id();
			//cek jika kondisi apakah sistem yang dapat diakses dan menyimpannya dalam database
			if ($this->input->post('ORE') != "") {
				if ($this->input->post('ORE') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
				}
			}
			if ($this->input->post('COAL') != "") {
				if ($this->input->post('COAL') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
				}
			}
			if ($this->input->post('LIMESTONE') != "") {
				if ($this->input->post('LIMESTONE') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
				}
			}
			if ($this->input->post('EKSPOR') != "") {
				if ($this->input->post('EKSPOR') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
				}
			}
			if ($this->input->post('SOLAR') != "") {
				if ($this->input->post('SOLAR') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $insert_id,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
				}
			}
			//mengirimkan pesan suskses
			echo $this->session->set_flashdata('sukses',
        "<div class='alert alert-block alert-success'>
        	<button type='button' class='close' data-dismiss='alert'>
          	<i class='ace-icon fa fa-times'></i>
          </button>
          <p>
            <strong>
              <i class='ace-icon fa fa-check'></i>
                Success your data added!
          	</strong>
          </p>
        </div>");

      redirect(base_url('adminMaster/adminM'));
  	}
	}

	public function F_changePassword($id_admin){
		//menampilkan form change password beserta data yang diambil
		$data['admin'] = $this->db->get_where('admin',['id_admin'=>$id_admin])->row();
		$this->load->view('admin_master/V_ChangePassword',$data);
	}

	public function ChangePassword(){
		//set rule form
		 $id_admin = $this->input->post('id_admin');
		 $this->form_validation->set_message('matches', 'The PASSWORD field does not match the CONFIRM PASSWORD field');
		 $this->form_validation->set_rules('password','PASSWORD','required|min_length[8]|max_length[16]');
		 $this->form_validation->set_rules('password_conf','PASSWORD','required|matches[password]');

		 if($this->form_validation->run() == FALSE) {
			 // jika data tidak sesuai format
			 $data['admin'] = $this->db->get_where('admin',['id_admin'=>$id_admin])->row();
			 $this->load->view('admin_master/V_ChangePassword',$data);
		 }else{
			 //jika data sesuai format
				$new_password = md5($this->input->post('password'));
				$this->db->where('id_admin',$id_admin)->update('admin',['password'=>$new_password]);
				echo $this->session->set_flashdata('sukses',
					"<div class='alert alert-block alert-success'>
							<button type='button' class='close' data-dismiss='alert'>
									<i class='ace-icon fa fa-times'></i>
							</button>
							<p>
								<strong>
									<i class='ace-icon fa fa-check'></i>
										Your password has been changed
								</strong>
							</p>
					</div>");
				redirect(base_url('adminMaster/adminM'));
		}
	}

	public function F_UAdmin($id_admin){
		//mengambil data admin dan data sistem yang disimpan
		$data['admin'] = $this->db->get_where('admin',['id_admin'=>$id_admin])->row();
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->join('r_admin_sistem', 'r_admin_sistem.id_admin = admin.id_admin');
		$this->db->where('r_admin_sistem.id_admin',$id_admin);
		$sistem = $this->db->get()->result();
		//inisiasi data kosong dari variable sistem
		$data['ORE'] = "";
		$data['COAL'] = "";
		$data['LIMESTONE'] = "";
		$data['EKSPOR'] = "";
		$data['SOLAR'] = "";
		//mengambil data sistem berdasarkan id user di table r_sistem_admin
		foreach ($sistem as $key => $value) {
			if ($value->id_sistem == 1) {
				if ($value->hak_akses == "F") {
					$data['ORE'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['ORE'] = "R";
				}
			}elseif ($value->id_sistem == 2) {
				if ($value->hak_akses == "F") {
					$data['COAL'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['COAL'] = "R";
				}
			}elseif ($value->id_sistem == 3) {
				if ($value->hak_akses == "F") {
					$data['LIMESTONE'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['LIMESTONE'] = "R";
				}
			}elseif ($value->id_sistem == 4) {
				if ($value->hak_akses == "F") {
					$data['EKSPOR'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['EKSPOR'] = "R";
				}
			}elseif ($value->id_sistem == 5) {
				if ($value->hak_akses == "F") {
					$data['SOLAR'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['SOLAR'] = "R";
				}
			}


		}
		$this->load->view('admin_master/V_UAdmin',$data);
	}

	public function UpdateAdmin(){
		//set rule untuk username
		$this->form_validation->set_message('is_unique', 'The %s is already taken');
		$this->form_validation->set_rules('name', 'NAME','required');
		$this->form_validation->set_rules('username', 'USERNAME','required|is_unique[admin.username]');
		//get username lama,yang nantinya akan digunakan sebagai pembanding
		$id_admin  =    $this->input->post('id_admin');
		$old_username = $this->db->get_where('admin',['id_admin'=>$id_admin])->row()->username;
		//memeriksa apakah usernma yang lama sama dengan username yang baru
		print_r($this->form_validation->run() == FALSE);
		if($old_username != $this->input->post('username')) {
			//jika berbeda maka akan di cek apakah sudah sesuai dengan rule yang dibuat
			if ($this->form_validation->run() == FALSE) {
				//get data dari databse untuk ditampilkan kembali pada form dan menunjukan bahwa username tidak valid
				$data['admin'] = $this->db->get_where('admin',['id_admin'=>$id_admin])->row();
				$this->db->select('*');
				$this->db->from('admin');
				$this->db->join('r_admin_sistem', 'r_admin_sistem.id_admin = admin.id_admin');
				$this->db->where('r_admin_sistem.id_admin',$id_admin);
				$sistem = $this->db->get()->result();
				//inisiasi data kosong dari variable sistem
				$data['ORE'] = "";
				$data['COAL'] = "";
				$data['LIMESTONE'] = "";
				$data['EKSPOR'] = "";
				$data['SOLAR'] = "";
				$data['wrong_username'] = $this->input->post('username');
				//mengambil data sistem berdasarkan id user di table r_sistem_admin
				foreach ($sistem as $key => $value) {
					if ($value->id_sistem == 1) {
						if ($value->hak_akses == "F") {
							$data['ORE'] = "F";
						}elseif ($value->hak_akses == "R") {
							$data['ORE'] = "R";
						}
					}elseif ($value->id_sistem == 2) {
						if ($value->hak_akses == "F") {
							$data['COAL'] = "F";
						}elseif ($value->hak_akses == "R") {
							$data['COAL'] = "R";
						}
					}elseif ($value->id_sistem == 3) {
						if ($value->hak_akses == "F") {
							$data['LIMESTONE'] = "F";
						}elseif ($value->hak_akses == "R") {
							$data['LIMESTONE'] = "R";
						}
					}
					elseif ($value->id_sistem == 4) {
						if ($value->hak_akses == "F") {
							$data['EKSPOR'] = "F";
						}elseif ($value->hak_akses == "R") {
							$data['EKSPOR'] = "R";
						}
					}elseif ($value->id_sistem == 5) {
						if ($value->hak_akses == "F") {
							$data['SOLAR'] = "F";
						}elseif ($value->hak_akses == "R") {
							$data['SOLAR'] = "R";
						}
					}
				}
				$this->load->view('admin_master/V_UAdmin',$data);
			}else {
				//jika semua kondisi memenuhi maka data akan di update
				$data['nama']   =    $this->input->post('name');
				$data['username'] =    $this->input->post('username');

				$data['level'] =    $this->input->post('level');
				$this->db->where('id_admin',$id_admin)->update('admin',$data);
				//menghapusan dilakukan pada table r_sistem_admin untuk mengatasi keasalahan pada saat men-update
				$this->db->where('id_admin',$id_admin);
				$this->db->delete('r_admin_sistem');
				//memasukan data yang sudah diperbarui
				if ($this->input->post('ORE') != "") {
					if ($this->input->post('ORE') == "F") {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
					}else {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
					}
				}
				if ($this->input->post('COAL') != "") {
					if ($this->input->post('COAL') == "F") {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
					}else {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
					}
				}

				if ($this->input->post('LIMESTONE') != "") {
					if ($this->input->post('LIMESTONE') == "F") {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
					}else {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
					}
				}

				if ($this->input->post('EKSPOR') != "") {
					if ($this->input->post('EKSPOR') == "F") {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
					}else {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
					}
				}

				if ($this->input->post('SOLAR') != "") {
					if ($this->input->post('SOLAR') == "F") {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
					}else {
						$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
					}
				}

				//pesan yang memberi tahu bahwa proses update berhasil
				echo $this->session->set_flashdata('sukses',
					"<div class='alert alert-block alert-success'>
						<button type='button' class='close' data-dismiss='alert'>
							<i class='ace-icon fa fa-times'></i>
								</button>
									<p>
										<strong>
											<i class='ace-icon fa fa-check'></i>
												Success Your Data Change
											</strong>
									</p>
						</div>");
				redirect(base_url('adminMaster/adminM'));
			}
		}else{
			//jika semua kondisi memenuhi maka data akan di update
			$data['nama']   =    $this->input->post('name');
			$data['username'] =    $this->input->post('username');

			$data['level'] =    $this->input->post('level');
			$this->db->where('id_admin',$id_admin)->update('admin',$data);
			//menghapusan dilakukan pada table r_sistem_admin untuk mengatasi keasalahan pada saat men-update
			$this->db->where('id_admin',$id_admin);
			$this->db->delete('r_admin_sistem');
			//memasukan data yang sudah diperbarui
			if ($this->input->post('ORE') != "") {
				if ($this->input->post('ORE') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '1','hak_akses'=> $this->input->post('ORE')]);
				}
			}
			if ($this->input->post('COAL') != "") {
				if ($this->input->post('COAL') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '2','hak_akses'=> $this->input->post('COAL')]);
				}
			}

			if ($this->input->post('LIMESTONE') != "") {
				if ($this->input->post('LIMESTONE') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '3','hak_akses'=> $this->input->post('LIMESTONE')]);
				}
			}

			if ($this->input->post('EKSPOR') != "") {
				if ($this->input->post('EKSPOR') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '4','hak_akses'=> $this->input->post('EKSPOR')]);
				}
			}

			if ($this->input->post('SOLAR') != "") {
				if ($this->input->post('SOLAR') == "F") {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
				}else {
					$this->db->insert('r_admin_sistem',['id_admin'=> $id_admin,'id_sistem'=> '5','hak_akses'=> $this->input->post('SOLAR')]);
				}
			}

			//pesan yang memberi tahu bahwa proses update berhasil
			echo $this->session->set_flashdata('sukses',
				"<div class='alert alert-block alert-success'>
					<button type='button' class='close' data-dismiss='alert'>
						<i class='ace-icon fa fa-times'></i>
							</button>
								<p>
									<strong>
										<i class='ace-icon fa fa-check'></i>
											Success Your Data Change
										</strong>
								</p>
					</div>");
			redirect(base_url('adminMaster/adminM'));
		}
	}

	public function DeleteAdmin($id_admin){
		//menghapus data admin/user
	 $this->db->where('id_admin',$id_admin);
	 $this->db->delete('r_admin_sistem');
	 //mengahpus data r_sistem_admin berdasarkan id
	 $this->db->where('id_admin',$id_admin);
	 $this->db->delete('admin');
	 redirect(base_url('adminMaster/adminM'));
	}

	public function DetailAdmin($id_admin){
		// mengabil data dari table admin berdasarkan id
		$data['admin'] = $this->db->get_where('admin',['id_admin'=>$id_admin])->row();
		//mengambil data dari table r_sistem_admin berdasarkan id
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->join('r_admin_sistem', 'r_admin_sistem.id_admin = admin.id_admin');
		$this->db->where('r_admin_sistem.id_admin',$id_admin);
		$sistem = $this->db->get()->result();
		//inisiasi awal bahawa variable sistem
		$data['ORE'] = "";
		$data['COAL'] = "";
		$data['LIMESTONE'] = "";
		$data['EKSPOR'] = "";
		$data['SOLAR'] = "";
		//memasukan data sistem ke variable sistem yang telah disediakan
		foreach ($sistem as $key => $value) {
			if ($value->id_sistem == 1) {
				if ($value->hak_akses == "F") {
					$data['ORE'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['ORE'] = "R";
				}
			}elseif ($value->id_sistem == 2) {
				if ($value->hak_akses == "F") {
					$data['COAL'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['COAL'] = "R";
				}
			}elseif ($value->id_sistem == 3) {
				if ($value->hak_akses == "F") {
					$data['LIMESTONE'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['LIMESTONE'] = "R";
				}
			}elseif ($value->id_sistem == 4) {
				if ($value->hak_akses == "F") {
					$data['EKSPOR'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['EKSPOR'] = "R";
				}
			}elseif ($value->id_sistem == 5) {
				if ($value->hak_akses == "F") {
					$data['SOLAR'] = "F";
				}elseif ($value->hak_akses == "R") {
					$data['SOLAR'] = "R";
				}
			}
		}
		$this->load->view('admin_master/detail_admin',$data);
	}
}
