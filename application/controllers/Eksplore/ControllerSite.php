<?php

class ControllerSite extends CI_Controller{
  private $filename = "import_data"; // Kita tentukan nama filenya
	function __construct(){
		parent::__construct();

    $sistem = $this->session->userdata('sistem');
    //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
    if(empty($sistem['EKSPOR'])){
      //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
      if ($this->session->userdata('status') == "login") {
        //jika teridentifikasi sebagai admin/user
        redirect(base_url("admin/admin"));
      }else {
        //tidak teridentifikasi sebagai siapapun
        redirect(base_url("admin/login_admin"));
      }
    }

    $this->load->model('Eksplore/SiteModel');
	}

	function index(){
    $data['site'] = $this->SiteModel->view();
    $data['smelter'] = "";
    $data['tgl_awal'] = "";
    $data['tgl_akhir'] = "";
		$this->load->view('ViewEksplor/ViewEksplor',$data);
	}
   public function FilterIndex()
   {
     $data['smelter'] = $this->input->post('smelter');
     $data['tgl_awal'] = $this->input->post('tgl_awal');
     $data['tgl_akhir'] = $this->input->post('tgl_akhir');
     if (!empty($data['smelter'])) {
       $this->db->where('wajan',$data['smelter']);
     }
     if (!empty($data['tgl_awal'])) {
       $this->db->where('date >=', $data['tgl_awal']);
   		 $this->db->where('date <=', $data['tgl_akhir']);
     }

     $data['site'] = $this->db->get('site')->result();

     if (isset($_POST['excel'])) {
       $excel = array( 'title' => 'Laporan Excel | Laporan Harian',
            'site' => $data['site']);
       $this->load->view('ViewEksplor/eksportExcel/eksportLaporanHarian',$excel);
     }

 		$this->load->view('ViewEksplor/ViewEksplor',$data);
   }

   public function resetUpload()
   {
     $this->db->empty_table('not_longer');
     redirect("Eksplore/ControllerSite/View_preview");
   }

   public function export_excel()
   {

     // $this->db->select_max('wajan');
     // $max_smelter = $this->db->get('site')->result();




     $smelter = $this->input->post('smelter');
     $tgl_awal = $this->input->post('tgl_awal');
     $tgl_akhir = $this->input->post('tgl_akhir');
     print_r($smelter);
     if (!empty($smelter)) {
       $this->db->where('wajan',$smelter);
     }
     if (!empty($tgl_awal)) {
       $this->db->where('date >=', $tgl_awal);
   		 $this->db->where('date <=', $tgl_akhir);
     }

     $data['site'] = $this->db->get('site')->result();
     $data['smelter'] = $smelter;
     $data['tgl_awal'] = $tgl_awal;
     $data['tgl_akhir'] = $tgl_akhir;


       $excel = array( 'title' => 'Laporan Excel | Laporan Harian',
            'site' => $data['site']);
       $this->load->view('ViewEksplor/eksportExcel/eksportLaporanHarian',$excel);

   }

  public function form(){
		$data = array(); // Buat variabel $data sebagai array

		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->SiteModel->upload_file($this->filename);

			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('ViewEksplor/FormImportExcel', $data);
	}

	public function import(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data

				$pisah = explode("-", $row['E']);
				array_push($data, array(
					'date'=>str_replace(".","-",$row['B']), // Insert data nis dari kolom A di excel
					'start'=>$row['C'], // Insert data nama dari kolom B di excel
					'finish'=>$row['D'], // Insert data jenis kelamin dari kolom C di excel
					'wajan'=>$pisah[0],
					'smelter'=>$pisah[1], // Insert data alamat dari kolom D di excel
					'plat'=>$row['F'], // Insert data nis dari kolom A di excel
					'bruto'=>$row['G'], // Insert data nama dari kolom B di excel
					'tara'=>$row['H'], // Insert data jenis kelamin dari kolom C di excel
					'netto'=>$row['I'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->SiteModel->insert_multiple($data);

		redirect("Eksplore/ControllerSite"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

  public function First()
  {
    $upload = $this->SiteModel->upload_file($this->filename);
    redirect("Eksplore/ControllerSite/priview");
  }

  public function Priview(){
		// Load plugin PHPExcel nya

    // print_r($upload);
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport

			if($numrow > 1){
        $pisah = explode("-",$row['E']);
				$cek = $this->db->get_where('site',['date'=>$row['B'],'start'=>$row['C'],'finish'=>$row['D'],'wajan'=>$pisah[0],'smelter'=>$pisah[1]])->num_rows();
        if ($cek>0) {
          $status = "sama";
        }else {
          $status = "beda";
        }
        array_push($data, array(
					'date'=>$row['B'], // Insert data nis dari kolom A di excel
					'start'=>$row['C'], // Insert data nama dari kolom B di excel
					'finish'=>$row['D'], // Insert data jenis kelamin dari kolom C di excel
					'smelter'=>$row['E'], // Insert data alamat dari kolom D di excel
					'plat'=>$row['F'], // Insert data nis dari kolom A di excel
					'bruto'=>$row['G'], // Insert data nama dari kolom B di excel
					'tara'=>$row['H'], // Insert data jenis kelamin dari kolom C di excel
					'netto'=>$row['I'],
          'status'=>$status
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->SiteModel->insert_multiple_preview($data);

		redirect("Eksplore/ControllerSite/View_preview"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

  public function View_preview()
  {
    $data['site'] = $this->SiteModel->View_preview()->result();
    $total_value = $this->SiteModel->View_preview()->num_rows();
    $data['valid'] = $this->db->get_where('not_longer',['status'=> "sama"])->num_rows();



    $data['allSame'] = TRUE;
    $data['half'] = FALSE;
    if (  $data['valid']>0) {


      if (!$total_value == 0) {
        echo $this->session->set_flashdata('sukses',
          "<div class='alert bg-pink alert-dismissible'>
          	<button type='button' class='close' data-dismiss='alert'>
            	<i class='ace-icon fa fa-times'></i>
            </button>
            <p>
              <strong>
                <i class='ace-icon fa fa-check'></i>
                  <h3><b><center>All Data That You Entered is Already in the Database.<br> Please Click The Reset Button and Enter Another Data!!</center></b></h3>
            	</strong>
            </p>
          </div>");
      }

    }elseif(empty($data['site'])) {
      $data['allSame'] = TRUE;
    }else {
      $data['allSame'] = FALSE;
    }
    // elseif($total_value != $data['valid'] && $total_value != 0) {
    //
    //   if (!$total_value != $data['valid'] && !$data['valid'] == 0) {
    //     $data['half'] = TRUE;
    //     echo $this->session->set_flashdata('sukses',
    //       "<div class='alert bg-pink alert-dismissible'>
    //       	<button type='button' class='close' data-dismiss='alert'>
    //         	<i class='ace-icon fa fa-times'></i>
    //         </button>
    //         <p>
    //           <strong>
    //             <i class='ace-icon fa fa-check'></i>
    //               <h3><b><center>some of the data that you input is already in the database and some are not.<br> if you want to enter some data that is not in the database, click partially upload</center></b></h3>
    //         	</strong>
    //         </p>
    //       </div>");
    //   }
    // }
    // else {
    //   $data['allSame'] = TRUE;
    // }
    // print_r($data['allSame']);
    $this->load->view('ViewEksplor/Preview',$data);
  }
  public function Save_Report($status)
  {
    if ($status == "full") {
        $site = $this->SiteModel->View_preview();
    }else {
      $site = $this->db->get_where('not_longer',['status'=> "beda"]);
    }
    $valueSebelum['total'] = 0;
    $valueSetelah['total'] = 0;
    $cek = "";
    $oneDate = 0;
    $data = array();
    $i = 0;
    $j = 0;
    foreach ($site->result() as $row) {
      $tanggal = str_replace(".","-",$row->date);
      $pisah = explode("-", $row->smelter);
  		if (sizeof($pisah) == 3) {
  			$pisah[1] = $pisah[2];
  		}
      $datetime = new DateTime($tanggal." ".$row->finish);

      $dateStart = new DateTime($tanggal." 07:00");
      // $dateFinish = new DateTime($tanggal." 07:00");
      // $dateFinish->modify('+1 day');

      if ($datetime < $dateStart) {
        //dimasukan pada date sebelumnya
        $datetime = $datetime->modify('-1 day');
        $dateacuan = $datetime->format('Y-m-d');

        $valueSebelum['total'] = $valueSebelum['total'] + $row->netto;
        $valueSebelum['date'] = $row->date;
        $i++;
        // echo "masuk sebelumnya ".$dateacuan;
        // echo "<br>";
      }else {
        $dateacuan = $datetime->format('Y-m-d');
        $valueSetelah['total'] = $valueSetelah['total'] + $row->netto;
        $valueSetelah['date'] = $row->date;
        $j++;
      }
      if ($cek == $dateacuan) {
        $oneDate++;
        $acuansend = $dateacuan;
      }
      $cek = $dateacuan;

      // $dateacuan = new DateTime($tanggal." 07:00");
      // $dateacuan->modify('-1 day');
      //
      // if ($datetime >= $dateacuan && $datetime <= $dateStart) {
      //   if ($datetime >= $dateStart && $datetime <= $dateFinish) {
      //       echo "data".$datetime->format('Y-m-d H:i:s');
      //   }else {
      //     echo "<br>";
      //     echo "start".$dateStart->format('Y-m-d H:i:s');
      //     echo "<br>";
      //     echo "data".$datetime->format('Y-m-d H:i:s');
      //     echo "<br>";
      //     echo "finish".$dateFinish->format('Y-m-d H:i:s');
      //   }
      //
      // }

      array_push($data, array(
        'date'=>$tanggal, // Insert data nis dari kolom A di excel
        'start'=>$row->start, // Insert data nama dari kolom B di excel
        'finish'=>$row->finish, // Insert data jenis kelamin dari kolom C di excel
        'wajan'=>$pisah[0],
        'smelter'=>$pisah[1], // Insert data alamat dari kolom D di excel
        'plat'=>$row->plat, // Insert data nis dari kolom A di excel
        'bruto'=>$row->bruto, // Insert data nama dari kolom B di excel
        'tara'=>$row->tara, // Insert data jenis kelamin dari kolom C di excel
        'netto'=>$row->netto,
        'dateAcuan'=>$dateacuan
      ));





    }
    // print_r($value);



    $total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
    $sum = $total_npi->total + $valueSebelum['total'] + $valueSetelah['total'];
    $this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);
    // print_r($site->num_rows()-1 == $oneDate);
    if ($site->num_rows()-1 == $oneDate) {

      $value['date'] = $acuansend;
      $value['total'] = $valueSebelum['total'] + $valueSetelah['total'];

      // print_r($value['date']." ".$value['total']);
      $query  = $this->db->get_where('Stockpile',['date'=>$value['date']]);
      // print_r($value);
      if ($query->num_rows() > 0) {
        $total = $query->row();
        $new_total = $value['total'] + $total->total;
        $this->db->where('date',$value['date'])->update('Stockpile',['total'=>$new_total]);
        // echo "1 sebelumnya";
      }else {
        $this->db->insert('Stockpile',$value);
        // echo "2 sebelumnya";
      }
    }else {
      $dateSebelum = new DateTime($valueSetelah['date']);
      $dateSebelum->modify('-1 day');
      // $valueSetelah['date'] = $dateSebelum->format('Y-m-d');
      $query  = $this->db->get_where('Stockpile',['date'=>$dateSebelum->format('Y-m-d')]);

        if ($query->num_rows() > 0) {

          $total = $query->row();
          $new_total = $valueSebelum['total'] + $total->total;
          $this->db->where('date',$dateSebelum->format('Y-m-d'))->update('Stockpile',['total'=>$new_total]);
          // echo "1 Setelah";
        }else {
          $valueSebelum['date'] = $dateSebelum->format('Y-m-d');

          $this->db->insert('Stockpile',$valueSebelum);
          // echo "2 Setelah";
        }

        $query  = $this->db->get_where('Stockpile',['date'=>$valueSetelah['date']]);

          if ($query->num_rows() > 0) {
            $total = $query->row();
            $new_total = $valueSetelah['total'] + $total->total;
            $this->db->where('date',$valueSetelah['date'])->update('Stockpile',['total'=>$new_total]);
            // echo "1 Setelah";
          }else {
            $this->db->insert('Stockpile',$valueSetelah);
            // echo "2 Setelah";
          }
    }

    // if (!empty($valueSetelah['total'])) {
    //   // code...
    //   $query  = $this->db->get_where('Stockpile',['date'=>$valueSetelah['date']]);
    //   if ($query->num_rows() > 0) {
    //     $total = $query->row();
    //     $new_total = $valueSetelah['total'] + $total->total;
    //     $this->db->where('date',$valueSetelah['date'])->update('Stockpile',['total'=>$new_total]);
    //     // echo "1 Setelah";
    //   }else {
    //     $this->db->insert('Stockpile',$valueSetelah);
    //     // echo "2 Setelah";
    //   }
    // }


    $this->db->empty_table('not_longer');
    $this->SiteModel->insert_multiple($data);
    //save inform
    $value['comment_subject'] ="A New Stockpile Has Been Added";
    date_default_timezone_set("Asia/Jakarta");
    // $d=strtotime("today");
    $value['comment_text'] = date("Y-m-d h:i:s");
    $this->db->insert('comments',$value);
    redirect("Eksplore/ControllerSite/Stockpile");
  }

  public function Stockpile()
  {
    $data['tgl_awal'] = "";
    $data['tgl_akhir'] = "";
    if (isset($_POST['filter'])) {
      $data['tgl_awal'] = $this->input->post('tgl_awal');
      $data['tgl_akhir'] = $this->input->post('tgl_akhir');
      $this->db->where('date >=', $data['tgl_awal']);
      $this->db->where('date <=', $data['tgl_akhir']);

    }
    $data['site'] = $this->db->get('Stockpile')->result();
    $data['filter'] = 0;
    foreach ($data['site'] as $key => $value) {
      $data['filter'] = $data['filter'] + $value->total;
    }
    $data['total'] =  $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
    $this->db->select_sum('netto');
    $data['total_npi'] = $this->db->get('site')->row();
		$this->load->view('ViewEksplor/ViewStockpile',$data);

  }

  public function DetailStockpile($date)
  {
    $data['site'] = $this->db->get_where('site',['dateAcuan'=> $date])->result();
    // print_r($data['site']);
		$this->load->view('ViewEksplor/DetailStockpile',$data);
  }

  public function F_USite($id_site){
		//mengambil data admin dan data sistem yang disimpan
		$data['site'] = $this->db->get_where('not_longer',['id'=>$id_site])->row();
		//inisiasi data kosong dari variable sistem
		$this->load->view('ViewEksplor/update',$data);
	}
  public function DeleteUpload($id)
  {
    $this->db->where('id',$id);
    $this->db->delete('not_longer');
    redirect(base_url('/Eksplore/ControllerSite/View_preview'));
  }

  public function DeleteStockpile($date)
  {
    $this->db->where('date',$date);
    $this->db->delete('Stockpile');

    $this->db->where('date',$date);
    $this->db->delete('site');
    redirect(base_url('/Eksplore/ControllerSite/Stockpile'));
  }

  public function Update()
  {
    $id = $this->input->post('id');
    $data['date'] = $this->input->post('date');
    // print_r($data['date']);
    $data['start'] = $this->input->post('start');
    $data['finish'] = $this->input->post('finish');
    $data['smelter'] = $this->input->post('smelter');
    $data['plat'] = $this->input->post('plat');
    $data['bruto'] = $this->input->post('bruto');
    $data['Tara'] = $this->input->post('tara');
    $data['Netto'] = $this->input->post('netto');
    $this->db->where('id',$id)->update('not_longer',$data);
    redirect("Eksplore/ControllerSite/View_preview");
  }

  public function getStokpile()
  {

    // $query = $this->db->query("SELECT wajan,
    //   SUM(netto) AS total_netto,
    //   SUM(CASE WHEN date = '2018-06-30' THEN netto ELSE NULL END) AS satu,
    //   SUM(CASE WHEN date = '2018-06-29' THEN netto ELSE NULL END) AS dua,
    //   FROM site group by wajan asc
    // ");
    // $a = $query->result();
    // foreach ($a as $key => $value) {
    //     echo "<br>";
    //     echo $value->satu;
      $data =  $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
      echo number_format($data->total,2)." Ton";

  }
  public function getNPI()
  {
      $this->db->select_sum('netto');
      $data = $this->db->get('site')->row();
      //$data =  $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
      echo number_format($data->netto,2)." Ton";

  }

  public function example($value='')
  {

    	$this->load->view('ViewEksplor/example');
  }

  public function notif()
  {
    if(isset($_POST["view"])){
        if($_POST["view"] != '')
        {
          $update_query = $this->db->query('UPDATE comments SET comment_status=1 WHERE comment_status=0');

        }
        $query = $this->db->query('SELECT * FROM comments ORDER BY comment_id DESC LIMIT 5');
        // $result = mysqli_query($connect, $query);
        $output = '';

        if($query->num_rows() > 0){
          foreach ($query->result() as $key => $value) {
            $output .= '
                <li>
                 <a href="#">
                  <strong>'.$value->comment_subject.'</strong><br />
                  <small><em>'.$value->comment_text.'</em></small>
                 </a>
                </li>
                <li class="divider"></li>
            ';
          }
          // while($row = mysqli_fetch_array($query)){
          //
          // }
      }else{
        $output .= '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
      }

      $query_1 = $this->db->query('SELECT * FROM comments WHERE comment_status=0');
      //$result_1 = mysqli_query($connect, $query_1);
      $count = $query_1->num_rows();
      $data = array(
        'notification'   => $output,
        'unseen_notification' => $count
      );
      //print_r($output);
       echo  json_encode($data);

    }
  }

  public function insert()
  {


       $value['comment_subject'] = $this->input->post('subject');
       $value['comment_text'] = $this->input->post('comment');
       $this->db->insert('comments',$value);
       //$query = $this->db->query('INSERT INTO comments(comment_subject, comment_text)VALUES ('$subject', '$comment')');
  }

  public function eksportExcelStockpile()
  {
    $smelter = $this->input->post('smelter');
    $tgl_awal = $this->input->post('tgl_awalE');
    $tgl_akhir =$this->input->post('tgl_akhirE');

    $this->db->select('*');
    $this->db->from('site');
    //kondisi
    if (!(empty($smelter))) {
      $this->db->where('wajan',$smelter);
    }

    if (!empty($tgl_awal)) {
      $this->db->where('date >=', $tgl_awal);
       $this->db->where('date <=', $tgl_akhir);
    }
    //$this->db->where('contract_export.id_contract',$id_contract);
    $data['site'] = $this->db->get()->result();

    $data['bruto'] = "0";
    foreach ($data['site'] as $key => $value) {
      $data['bruto'] += $value->bruto;
    }

    $data['tara'] = "0";
    foreach ($data['site'] as $key => $value) {
      $data['tara'] += $value->tara;
    }

    $data['netto'] = "0";
    foreach ($data['site'] as $key => $value) {
      $data['netto'] += $value->netto;
    }

    $data['tgl_awal'] = date("j F Y",strtotime($tgl_awal));
    $data['tgl_akhir'] = date("j F Y",strtotime($tgl_akhir));
    $data['smelter'] = $smelter;
    $data['title'] = "DAILY STOCK (".$data['tgl_awal']." - ".$data['tgl_akhir'].")";

  $this->load->view('ViewEksplor/eksportExcel/eksportLaporanHarian',$data);

  }

}
