<?php

class ControllerShipper extends CI_Controller{

	function __construct(){
		parent::__construct();

		$sistem = $this->session->userdata('sistem');
		//menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
		if(empty($sistem['EKSPOR'])){
			//menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
			if ($this->session->userdata('status') == "login") {
				//jika teridentifikasi sebagai admin/user
				redirect(base_url("admin/admin"));
			}else {
				//tidak teridentifikasi sebagai siapapun
				redirect(base_url("admin/login_admin"));
			}
		}

	}

	function index(){
		$data['shipper'] = $this->db->get('master_shipper')->result();
		$this->load->view('ViewEksplor/shipper/FormShipper',$data);
	}
  public function SaveShipper()
  {
    $shipper['name_shipper'] = $this->input->post('nameShipper');
    $cek = $this->db->get_where('master_shipper',['name_shipper'=> $shipper['name_shipper']])->num_rows();
    if ($cek<1) {
      $this->db->insert('master_shipper',$shipper);
			echo $this->session->set_flashdata('sukses',
        "<div class='alert bg-green alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert'>
            <i class='ace-icon fa fa-times'></i>
          </button>
          <p>
            <strong>
              <i class='ace-icon fa fa-check'></i>
                <h3><b><center>successfully added data</center></b></h3>
            </strong>
          </p>
        </div>");
      redirect("Eksplore/ControllerShipper");
    }else {
      echo $this->session->set_flashdata('sukses',
        "<div class='alert bg-pink alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert'>
            <i class='ace-icon fa fa-times'></i>
          </button>
          <p>
            <strong>
              <i class='ace-icon fa fa-check'></i>
                <h3><b><center>the name of the shipper you entered already exists!!</center></b></h3>
            </strong>
          </p>
        </div>");
      redirect("Eksplore/ControllerShipper");
    }

  }

	public function DeleteShipper($id_shipper)
	{
		$this->db->where('id_shipper',$id_shipper);
 	 	$this->db->delete('master_shipper');
		echo $this->session->set_flashdata('sukses',
			"<div class='alert bg-pink alert-dismissible'>
				<button type='button' class='close' data-dismiss='alert'>
					<i class='ace-icon fa fa-times'></i>
				</button>
				<p>
					<strong>
						<i class='ace-icon fa fa-check'></i>
							<h3><b><center>Data has been Deleted</center></b></h3>
					</strong>
				</p>
			</div>");
		redirect("Eksplore/ControllerShipper");
	}

	public function formEditShipper($id_shipper)
	{
		$data['shipper'] = $this->db->get_where('master_shipper',['id_shipper'=> $id_shipper])->row();
		$this->load->view('ViewEksplor/shipper/EditShipper',$data);
	}

	public function updateShipper()
	{
		$data['name_shipper'] = $this->input->post('nameShipper');
		$cek = $this->db->get_where('master_shipper',['name_shipper'=> $data['name_shipper']])->num_rows();
		if ($cek<1) {
			$this->db->where('id_shipper',$this->input->post('id_shipper'))->update('master_shipper',$data);
			echo $this->session->set_flashdata('sukses',
        "<div class='alert bg-green alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert'>
            <i class='ace-icon fa fa-times'></i>
          </button>
          <p>
            <strong>
              <i class='ace-icon fa fa-check'></i>
                <h3><b><center>successfully Update data</center></b></h3>
            </strong>
          </p>
        </div>");
      redirect("Eksplore/ControllerShipper");
		}else {
			echo $this->session->set_flashdata('sukses',
				"<div class='alert bg-pink alert-dismissible'>
					<button type='button' class='close' data-dismiss='alert'>
						<i class='ace-icon fa fa-times'></i>
					</button>
					<p>
						<strong>
							<i class='ace-icon fa fa-check'></i>
								<h3><b><center>the name of the shipper you entered already exists</center></b></h3>
						</strong>
					</p>
				</div>");
			redirect("Eksplore/ControllerShipper");
		}
	}

}
