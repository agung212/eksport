<?php

class ControllerEksport extends CI_Controller{

	function __construct(){
		parent::__construct();

		$sistem = $this->session->userdata('sistem');
		//menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
		if(empty($sistem['EKSPOR'])){
			//menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
			if ($this->session->userdata('status') == "login") {
				//jika teridentifikasi sebagai admin/user
				redirect(base_url("admin/admin"));
			}else {
				//tidak teridentifikasi sebagai siapapun
				redirect(base_url("admin/login_admin"));
			}
		}
		$this->load->model('Eksplore/EksportModel');
	}

	function index(){
    $data['contract'] = "";
		$this->load->view('ViewEksplor/Eksport/findContrac',$data);

	}

	public function FindLoading()
	{
		$no_invoice = $this->input->post('no_invoice');

		$this->db->select('*');
		$this->db->from('tb_invoice');
		$this->db->join('contract_export', 'contract_export.id_contract = tb_invoice.id_contract');
		$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
		//$this->db->where('contract_export.id_contract',$id_contract);
		$this->db->where('tb_invoice.invoice_no',$no_invoice);
		$data['contract'] = $this->db->get()->row();
		$status = $this->db->get_where('tb_invoice',['invoice_no'=> $no_invoice])->row()->status;
		$id_invoice = $data['contract']->id_invoice;
		$id_contract = $data['contract']->id_contract;

		if (!empty($status)) {
			if (($status == "Complate")||($status == "Waiting Paid Date")) {
				redirect("Eksplore/ControllerContract/formPaid/$id_invoice");
			} else {
				redirect("Eksplore/ControllerEksport/FormContract/$id_invoice/$id_contract");
			}
		} else {
			echo $this->session->set_flashdata('sukses',
					"<div class='alert bg-pink alert-dismissible'>
						<button type='button' class='close' data-dismiss='alert'>
							<i class='ace-icon fa fa-times'></i>
						</button>
						<p>
							<strong>
								<i class='ace-icon fa fa-check'></i>
									<h3><b><center>No Invoice Not Found</center></b></h3>
							</strong>
						</p>
					</div>");
				redirect("Eksplore/ControllerEksport");
		}




	}

  public function FormContract($id_invoice, $id_contract)
  {

		$this->db->select('*');
		$this->db->from('contract_export');
		$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
		$this->db->join('tb_invoice', 'tb_invoice.id_contract = contract_export.id_contract');
		$this->db->where('contract_export.id_contract',$id_contract);
		$this->db->where('tb_invoice.id_invoice',$id_invoice);
		$data['contract'] = $this->db->get()->row();


		// $data['contract'] = $this->db->get_where('contract_export',['invoice_no'=>$noInvoice])->row();
		//
    $this->load->view('ViewEksplor/Eksport/FormEksport',$data);
  }
  public function SaveEksport()
  {
		print_r($this->input->post('Barge'));
		print_r($this->input->post('trip'));
		// $ignore = array('$','.' );
		// $stok_loading = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity_loading')));
    // $data = array(
    //   'invoice_no' =>  $this->input->post('invoice_no'),
    //   'quantity_loading' => $stok_loading
    // );
		//
		// $total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
		//
		// if ($total_npi->total > $stok_loading) {
		// 	$sum = $total_npi->total - $stok_loading;
		// 	$this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);
	  //   $this->db->insert('eksport',$data);
		// 	redirect("Eksplore/ControllerSite/Stockpile");
		// }else {
		// 	echo $this->session->set_flashdata('sukses',
		// 		"<div class='alert bg-pink alert-dismissible'>
		// 			<button type='button' class='close' data-dismiss='alert'>
		// 				<i class='ace-icon fa fa-times'></i>
		// 			</button>
		// 			<p>
		// 				<strong>
		// 					<i class='ace-icon fa fa-check'></i>
		// 						<h3><b><center>Stockpile Not Enough</center></b></h3>
		// 				</strong>
		// 			</p>
		// 		</div>");
		// 	redirect("Eksplore/ControllerEksport");
		// }
  }

	function dataTransaksi(){
        // $data=$this->m_barang->barang_list();
				$id = $this->input->post('id');
				$data  = $this->db->get_where('transaksi',['id_invoice'=> $id])->result();
				// print_r($data);
        echo json_encode($data);
    }

    function get_loading(){
			$id = $this->input->post('id');
			$data  = $this->db->get_where('transaksi',['id_transaksi'=> $id])->row();
      echo json_encode($data);
    }

    function saveLoading(){
					$ignore = array('$','.' );
					$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
					$total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();

					if ($total_npi->total > $value['quantity']) {
						$value['barge'] =  $this->input->post('barge');
						$value['trip'] =  $this->input->post('trip');
						$value['startLoading'] = $this->input->post('startLoading');
						$value['complateLoading'] =  $this->input->post('complateLoading');
						// $value['finalDraught'] = $this->input->post('finalDraught');
						$value['id_contract'] =  $this->input->post('id');
						$value['id_invoice'] =  $this->input->post('id_invoice');
						$sum = $total_npi->total - $value['quantity'];
						$this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);
						$this->db->insert('transaksi',$value);
						$data = "cukup";
					}else {
						$data = "tidak cukup";
					}

        echo json_encode($data);
    }

    function update_loading(){
			$value['id_transaksi'] =  $this->input->post('id');
			$ignore = array('$','.' );
			$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
			$total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
			$quantityBefore = $this->db->get_where('transaksi',['id_transaksi'=>$value['id_transaksi']])->row();
			$total_npi = $total_npi->total + $quantityBefore->quantity;

			if ($total_npi > $value['quantity']) {
				$value['barge'] =  $this->input->post('barge');
				$value['trip'] =  $this->input->post('trip');
				$value['startLoading'] = $this->input->post('startLoading');
				$value['complateLoading'] =  $this->input->post('complateLoading');
				$value['finalDraught'] = $this->input->post('finalDraught');

				$sum = $total_npi - $value['quantity'];
				$this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);

				$this->db->where('id_transaksi',$value['id_transaksi'])->update('transaksi',$value);
				$data = "cukup";
			}else {
				$data = "tidak cukup";
			}
        echo json_encode($data);
    }

    function deleteLoading(){

			$quantity =  $this->db->get_where('transaksi',['id_transaksi'=>$this->input->post('kode')])->row();
			$total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
			$sum = $total_npi->total + $quantity->quantity;
			$this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);

				$this->db->where('id_transaksi',$this->input->post('kode'));
		 	 	$data = $this->db->delete('transaksi');
        echo json_encode($data);
    }

		public function FinalAdjustment($id_contract, $id_invoice)
		{
			$this->db->select('*');
			$this->db->from('contract_export');
			$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
			$this->db->join('tb_invoice', 'tb_invoice.id_contract = contract_export.id_contract');
			$this->db->where('contract_export.id_contract',$id_contract);
			$this->db->where('tb_invoice.id_invoice',$id_invoice);
			$data['contract'] = $this->db->get()->row();



			$this->db->where('id_contract',$id_contract);
			$this->db->where('id_invoice',$id_invoice);
			$this->db->select_sum('quantity');
			$data['quantity'] = $this->db->get('transaksi')->row();

			$this->load->view('ViewEksplor/Eksport/adjustment',$data);
		}

		public function saveAdjustment()
		{
			$id_invoice = $this->input->post('id_invoice');
			$id_contract = $this->input->post('id_contract');

			$dataInvoice = array('peb_no' => $this->input->post('peb_no'),
														'nopen_npe'=> $this->input->post('nopen_npe'),
														'bill_landing_no' => $this->input->post('bill_landing_no'),
														'price_tonnage' => $this->input->post('price_tonnage'),

														'remarks' => $this->input->post('remarks'),
														'status' => "Waiting Paid Date",
		 	);

			$ignore = array('.' );
			$dataAdjustment = array('id_invoice' => $id_invoice,
															'quantityA' => str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity2'))),
															'niA' => $this->input->post('ni2'),
															'price_tonnageA' => $this->input->post('price_tonnage2'),
															'price_pctA' => $this->input->post('price_pctA'),
															'amountA' => $this->input->post('amount2'),
															'overpayment' => $this->input->post('overpayment'),
			);

			$this->db->where('id_invoice',$id_invoice)->update('tb_invoice',$dataInvoice);
			$this->db->insert('tb_adjustment',$dataAdjustment);
			$hasil = $this->EksportModel->upload_file($id_invoice);
			redirect("Eksplore/ControllerContract/FormInvoice/$id_contract");
		}

		public function changeStatus($id_contract,$id_invoice)
		{
			$this->db->where('id_invoice',$id_invoice)->update('tb_invoice',['status'=>"Waiting Adjustment"]);
			redirect("Eksplore/ControllerContract/FormInvoice/$id_contract");
		}

		public function eksportExcelVassel($invoice)
		{
			$data['loading']  = $this->db->get_where('transaksi',['id_invoice'=> $invoice])->result();
			$invoiceData = $this->db->get_where('tb_invoice',['id_invoice'=> $invoice]);
			$data['invoice']  = $invoiceData->row();
			$data['total']  = $this->db->get_where('transaksi',['id_invoice'=> $invoice])->num_rows();

			$this->db->select_sum('quantity');
	    $data['total_quantity'] = $this->db->get_where('transaksi',['id_invoice'=> $invoice])->row();

			$this->db->select_sum('trip');
	    $data['total_trip'] = $this->db->get_where('transaksi',['id_invoice'=> $invoice])->row();

			$data['title'] = 'Laporan Excel | Laporan Harian';
			$this->load->view('ViewEksplor/eksportExcel/eksportLoading',$data);
		}

}
