<?php

class ControllerContract extends CI_Controller{

	function __construct(){
		parent::__construct();

		$sistem = $this->session->userdata('sistem');
		//menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
		if(empty($sistem['EKSPOR'])){
			//menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
			if ($this->session->userdata('status') == "login") {
				//jika teridentifikasi sebagai admin/user
				redirect(base_url("admin/admin"));
			}else {
				//tidak teridentifikasi sebagai siapapun
				redirect(base_url("admin/login_admin"));
			}
		}

    $this->load->model('Eksplore/ContractModel');

	}

	function index(){
		//$data['contract'] = $this->db->get('contract_export')->result();
		$this->db->select('*');
		$this->db->from('contract_export');
		$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');

		$data['shipperF'] = "";
		$data['status'] = "";
		$data['tgl_awal'] = "";
		$data['tgl_akhir'] = "";


		if (isset($_POST['filter'])) {
			$data['shipperF'] = $this->input->post('name_shipper2');
			$data['status'] = $this->input->post('status');
			if (!(empty($data['shipperF']))) {
				$this->db->where('contract_export.name_shiper',$data['shipperF']);
			}
			if (!(empty($data['status']))) {
				$this->db->where('contract_export.status',$data['status']);
			}

			$data['tgl_awal'] = $this->input->post('tgl_awal');
      $data['tgl_akhir'] = $this->input->post('tgl_akhir');
      if (!empty($data['tgl_awal'])) {
        $this->db->where('contract_export.dateC >=', $data['tgl_awal']);
    		 $this->db->where('contract_export.dateC <=', $data['tgl_akhir']);
      }
		}
		$data['contract'] = $this->db->get()->result();
		$data['shipper'] = $this->db->get('master_shipper')->result();
		 //print_r($data['shipper']);
		 foreach ($data['contract'] as $value) {
			 $cek = $this->db->get_where('tb_invoice',['id_contract'=> $value->id_contract])->num_rows();
			 if ($cek != 0) {
				 $value->temporary = "ada";
			 }
		 }
		$this->load->view('ViewEksplor/Contract/listContract',$data);
	}

  public function FormContract()
  {
		$data['shipper'] = $this->db->get('master_shipper')->result();
    $this->load->view('ViewEksplor/contract/FormContract',$data);
  }
  public function SaveContract()
  {
		$no_contract = $this->input->post('contract_no');
		$cek = $this->db->get_where('contract_export',['no_contract'=> $no_contract])->num_rows();
		$money = $this->input->post('uang');
		if ($cek == 0) {
			$ignore = array($money,'.' );
			$data = array(
				'no_contract' =>  $this->input->post('contract_no'),
				'name_shiper' => $this->input->post('name_shipper'),
				'description_goods' => $this->input->post('decription_goods'),
				'price_pct' =>     str_replace(',','.',str_replace($ignore,"",$this->input->post('price_ni'))),
				'quantityC' =>     str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity'))),
				'discharge_name' => $this->input->post('discharge_name'),
				'export_country' => $this->input->post('export_country'),
				'LC_no' =>        $this->input->post('LC_no'),
				'nilai_mata_uang' => $this->input->post('uang'),
				'dateC' => date("y.m.d"),
				'status' =>        "On Going"


			);
			$this->ContractModel->insert_contract($data);
			$insert_id = $this->db->insert_id();
			$this->FormInvoice($insert_id);
			//redirect("Eksplore/ControllerContract/listContract");

		}else {
			echo $this->session->set_flashdata('sukses',
				"<div class='alert bg-pink alert-dismissible'>
					<button type='button' class='close' data-dismiss='alert'>
						<i class='ace-icon fa fa-times'></i>
					</button>
					<p>
						<strong>
							<i class='ace-icon fa fa-check'></i>
								<h3><b><center>the contract number that you entered is registered</center></b></h3>
						</strong>
					</p>
				</div>");
			redirect("Eksplore/ControllerContract/FormContract");
		}
  }

	public function FormUpdate($id_contract)
	{
		$data['contract'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row();
		$data['shipper'] = $this->db->get('master_shipper')->result();
		$this->load->view('ViewEksplor/contract/EditContract',$data);
	}

	public function updateContract()
	{
		// $no_contract = $this->input->post('contract_no');
		// $cek = $this->db->get_where('contract_export',['no_contract'=> $no_contract])->num_rows();
		// if ($cek == 0) {
		$money = $this->db->get_where('contract_export',['no_contract'=> $this->input->post('contract_no')])->row()->nilai_mata_uang;
			$ignore = array($money,'.' );
			$data = array(
				'no_contract' =>  $this->input->post('contract_no'),
				'name_shiper' => $this->input->post('name_shipper'),
				'description_goods' => $this->input->post('decription_goods'),
				'price_pct' =>     str_replace(',','.',str_replace($ignore,"",$this->input->post('price_ni'))),
				'quantityC' =>     str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity'))),
				'discharge_name' => $this->input->post('discharge_name'),
				'export_country' => $this->input->post('export_country'),
				'nilai_mata_uang' => $this->input->post('uang'),
				'LC_no' =>        $this->input->post('LC_no'),
			);
			$this->db->where('id_contract',$this->input->post('id_contract'))->update('contract_export',$data);
			redirect("Eksplore/ControllerContract");
	}

	public function FormInvoice($id)
	{

		$this->db->select('*');
		$this->db->from('contract_export');
		$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
		$this->db->where('id_contract',$id);
		$data['contract'] = $this->db->get()->row();
		$invoice = $this->db->get_where('tb_invoice',['id_contract'=> $id]);
		$countS = $this->db->where('id_contract', $id)->group_start()->or_where('status', "Complate")->or_where('status', "Waiting Paid Date")->group_end()->get('tb_invoice')->num_rows();
		// print_r($countS);
		$countI = $invoice->num_rows();
		if ($countS == $countI) {
			$data['status'] = "sama";
		} else {
			$data['status'] = "beda";
		}

		// print_r($countS." ".$countI);
		$data["invoice"]  = $invoice->result();
		// $data['contract'] = $this->db->get_where('contract_export',['invoice_no'=>$data['contract']->invoice_no])->row();

		$data['contract']->price_pct = str_replace(".",",",$data['contract']->price_pct);
		$this->load->view('ViewEksplor/contract/listInvoice',$data);
		// print_r($money);
	}

	function dataInvoice(){
        // $data=$this->m_barang->barang_list();
				$id = $this->input->post('id');
				$data  = $this->db->get_where('tb_invoice',['id_contract'=> $id])->result();
				// print_r($data);
				foreach ($data as $value) {
					$value->amount = number_format($value->amount);
					$value->quantity = number_format($value->quantity);
				}
        echo json_encode($data);
    }

    function get_invoice(){
			$id = $this->input->post('id');
			$data  = $this->db->get_where('tb_invoice',['id_invoice'=> $id])->row();
      echo json_encode($data);
    }

    // function saveInvoice(){
		// 			$ignore = array('$','.' );
		// 			$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
		// 			// $total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
		// 			//
		// 			// if ($total_npi->total > $value['quantity']) {
		// 				$value['invoice_no'] =  $this->input->post('no_invoice');
		// 				$value['shiping_terms'] =  $this->input->post('shiping_terms');
		// 				$value['tt'] = $this->input->post('tt');
		// 				$value['lc'] =  $this->input->post('lc');
		// 				$value['nominalTT'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalTT')));
		// 				$value['nominalLC'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalLC')));
		// 				$value['shipping_method'] = $this->input->post('shipping_method');
		// 				$value['vassel_name'] =  $this->input->post('vassel_name');
		// 				$value['etd_kendari'] =  $this->input->post('etd_kendari');
		// 				$value['eta_discharge_port'] = $this->input->post('eta_discharge_port');
		// 				$value['lc_no'] =  $this->input->post('lc_no');
		// 				$value['ni'] = $this->input->post('ni');
		// 				$value['net_weight'] =  $this->input->post('net_weight');
		// 				$value['amount'] = $this->input->post('amount');
		// 				$value['unit_price'] =  $this->input->post('unit_price');
		// 				$value['status'] =  "no_adjust";
		// 				$value['id_contract'] = $this->input->post('id');
		//
		//
		// 				// $value['id_contract'] =  $this->input->post('id');
		// 				// $sum = $total_npi->total - $value['quantity'];
		// 				// $this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);
		// 			$data =	$this->db->insert('tb_invoice',$value);
		// 				// $data = "cukup";
		// 			// }else {
		// 			// 	$data = "tidak cukup";
		// 			// }
		//
		// 			// redirect("Eksplore/ControllerContract/FormInvoice/".$this->input->post('id'));
    //     echo json_encode($data);
    // }

    function update_Invoice(){
				$ignore = array('$','.' );
				$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
				// $total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
				//
				// if ($total_npi->total > $value['quantity']) {
					$value['invoice_no'] =  $this->input->post('no_invoice');
					$value['shiping_terms'] =  $this->input->post('shiping_terms');
					$value['tt'] = $this->input->post('tt');
					$value['lc'] =  $this->input->post('lc');
					$value['shipping_method'] = $this->input->post('shipping_method');
					$value['vassel_name'] =  $this->input->post('vassel_name');
					$value['etd_kendari'] =  $this->input->post('etd_kendari');
					$value['eta_discharge_port'] = $this->input->post('eta_discharge_port');
					$value['lc_no'] =  $this->input->post('lc_no');
					$value['ni'] = $this->input->post('ni');
					$value['net_weight'] =  $this->input->post('net_weight');
					$value['amount'] = $this->input->post('amount');
					$value['unit_price'] =  $this->input->post('unit_price');

					$value['status'] =  "no_adjust";
					$id_invoice = $this->input->post('id');


					// $value['id_contract'] =  $this->input->post('id');
					// $sum = $total_npi->total - $value['quantity'];
					// $this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);

				$data =	$this->db->where('id_invoice',$id_invoice)->update('tb_invoice',$value);

         echo json_encode($data);
    }

    function deleteInvoice(){

			$quantity =  $this->db->get_where('transaksi',['id_transaksi'=>$this->input->post('kode')])->row();
			$total_npi  = $this->db->get_where('total_npi',['id_total_npi'=>"1"])->row();
			$sum = $total_npi->total + $quantity->quantity;
			$this->db->where('id_total_npi',"1")->update('total_npi',['total'=>$sum]);

				$this->db->where('id_transaksi',$this->input->post('kode'));
		 	 	$data = $this->db->delete('transaksi');
        echo json_encode($data);
    }

		public function DeleteContract($id_contract)
		{
			$id = $this->db->get_where('tb_invoice',['id_contract'=>$id_contract])->result();
			$this->db->where('id_contract',$id_contract);
	 	 	$this->db->delete('contract_export');
			$this->db->where('id_contract',$id_contract);
			$this->db->delete('tb_invoice');
			$this->db->where('id_contract',$id_contract);
			$this->db->delete('transaksi');
			$this->load->helper("file");


			foreach ($id as $value) {
				$this->db->where('id_invoice',$value->id_invoice);
				$this->db->delete('tb_adjustment');
			}



			echo $this->session->set_flashdata('sukses',
				"<div class='alert bg-pink alert-dismissible'>
					<button type='button' class='close' data-dismiss='alert'>
						<i class='ace-icon fa fa-times'></i>
					</button>
					<p>
						<strong>
							<i class='ace-icon fa fa-check'></i>
								<h3><b><center>Data has been Deleted</center></b></h3>
						</strong>
					</p>
				</div>");
			redirect("Eksplore/ControllerContract");
		}

		function deleteInvoiceContract(){

			$this->db->where('id_invoice',$this->input->post('kode'));
			$this->db->delete('tb_invoice');
			$this->db->where('id_invoice',$this->input->post('kode'));
			$this->db->delete('tb_adjustment');
			$this->db->where('id_invoice',$this->input->post('kode'));
			$data = $this->db->delete('transaksi');


        echo json_encode($data);
    }

		public function detailAdjustment()
		{
			$id_invoice =  $this->input->post('id_invoice');
			$id_contract = $this->input->post('id_contract');

			$this->db->select('*');
			$this->db->from('contract_export');
			$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
			$this->db->join('tb_invoice', 'tb_invoice.id_contract = contract_export.id_contract');
			$this->db->where('contract_export.id_contract',$id_contract);
			$this->db->where('tb_invoice.id_invoice',$id_invoice);
			$data['contract'] = $this->db->get()->row();

			if ($data['contract']->status == "no_adjust") {
				echo "Lakukan Adjustment Terlebih Dahulu";
			} else {
				$data['adjustment']  = $this->db->get_where('tb_adjustment',['id_invoice'=>$id_invoice])->row();
				$this->db->where('id_contract',$id_contract);
				$this->db->where('id_invoice',$id_invoice);
				$this->db->select_sum('quantity');
				$data['quantity'] = $this->db->get('transaksi')->row();

				$this->load->view('ViewEksplor/contract/detailAdjustment',$data);
			}


		}

		function get_(){
			$id = $this->input->post('id');
			$data  = $this->db->get_where('transaksi',['id_transaksi'=> $id])->row();
			echo json_encode($data);
		}

		public function addInvoice($id_contract)
		{
			$data['pct'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row()->price_pct;
			$data['id_contract'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row()->id_contract;
			$data['uang'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row()->nilai_mata_uang;
			$this->load->view('ViewEksplor/contract/FormInvoice',$data);
		}

		public function editInvoice($id_invoice, $id_contract)
		{
			$data['pct'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row()->price_pct;
			$data['invoice']  = $this->db->get_where('tb_invoice',['id_invoice'=> $id_invoice])->row();
			$data['uang'] = $this->db->get_where('contract_export',['id_contract'=> $id_contract])->row()->nilai_mata_uang;
			$this->load->view('ViewEksplor/contract/editInvoice',$data);
		}

		public function updateInvoice()
		{
			$money = $this->db->get_where('contract_export',['id_contract'=> $this->input->post('id_contract')])->row()->nilai_mata_uang;
			$ignore = array($money,'.' );
			$value['invoice_no'] =  $this->input->post('invoice_no');
			$value['shiping_terms'] =  $this->input->post('shipping_terms');
			$value['tt'] = str_replace("%","",$this->input->post('tt'));
			$value['lc'] =  str_replace("%","",$this->input->post('lc'));
			$value['nominalTT'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalTT')));
			$value['nominalLC'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalLC')));
			$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
			$value['shipping_method'] = $this->input->post('shipping_method');
			$value['vassel_name'] =  $this->input->post('vassel_name');
			$value['etd_kendari'] =  $this->input->post('etd_kendari');
			$value['eta_discharge_port'] = $this->input->post('eta_discharge_port');
			$value['peb_no'] =  $this->input->post('peb_no');
			$value['pebProcess'] =  $this->input->post('pebProcess');
			$value['nopen_npe'] = $this->input->post('nopen_npe');
			$value['bill_landing_no'] = $this->input->post('bill_landing_no');
			$value['lc_no'] =  $this->input->post('lc_no');
			$value['ni'] = $this->input->post('ni');
			$value['net_weight'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('net_weight')));;
			$value['amount'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('amount')));
			$value['price_tonnage'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('unit_price')));
			$value['submitBank'] = $this->input->post('submitBank');
			$value['leadLoading'] =  $this->input->post('leadLoading');
			$value['startLoading'] = $this->input->post('startLoading');
			$value['CompleteLoading'] =  $this->input->post('CompleteLoading');
			$value['status'] =  "Waiting Loading";
			$value['remarks'] = $this->input->post('remarks');
			$id_invoice = $this->input->post('id_invoice');

			$this->db->where('id_invoice',$id_invoice)->update('tb_invoice',$value);
			redirect("Eksplore/ControllerContract/FormInvoice/".$this->input->post('id_contract'));
		}

		public function saveInvoice()
		{


					$cek  = $this->db->get_where('tb_invoice',['invoice_no'=>$this->input->post('invoice_no')])->num_rows();
					$money = $this->db->get_where('contract_export',['id_contract'=> $this->input->post('id_contract')])->row()->nilai_mata_uang;
					$ignore = array($money,'.' );
					// print_r($cek);
					if ($cek < 1) {
						$value['invoice_no'] =  $this->input->post('invoice_no');
						$value['shiping_terms'] =  $this->input->post('shipping_terms');
						$value['tt'] = $this->input->post('tt');
						$value['lc'] =  $this->input->post('lc');
						$value['nominalTT'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalTT')));
						$value['nominalLC'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('nominalLC')));
						$value['quantity'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('quantity')));
						$value['shipping_method'] = $this->input->post('shipping_method');
						$value['vassel_name'] =  $this->input->post('vassel_name');
						$value['etd_kendari'] =  $this->input->post('etd_kendari');
						$value['eta_discharge_port'] = $this->input->post('eta_discharge_port');
						$value['peb_no'] =  $this->input->post('peb_no');
						$value['pebProcess'] =  $this->input->post('pebProcess');
						$value['nopen_npe'] = $this->input->post('nopen_npe');
						$value['bill_landing_no'] = $this->input->post('bill_landing_no');
						$value['ni'] = $this->input->post('ni');
						$value['net_weight'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('net_weight')));;
						$value['amount'] = str_replace(',','.',str_replace($ignore,"",$this->input->post('amount')));
						$value['price_tonnage'] =  str_replace(',','.',str_replace($ignore,"",$this->input->post('unit_price')));
						$value['startLoading'] = $this->input->post('startLoading');
						$value['CompleteLoading'] =  $this->input->post('CompleteLoading');
						$value['submitBank'] = $this->input->post('submitBank');
						$value['leadLoading'] =  $this->input->post('leadLoading');
						$value['status'] =  "Waiting Loading";
						$value['id_contract'] = $this->input->post('id_contract');
						$value['remarks'] = $this->input->post('remarks');
						$data =	$this->db->insert('tb_invoice',$value);

						redirect("Eksplore/ControllerContract/FormInvoice/".$this->input->post('id_contract'));
					}else {
						echo $this->session->set_flashdata('sukses',
							"<div class='alert bg-pink alert-dismissible'>
								<button type='button' class='close' data-dismiss='alert'>
									<i class='ace-icon fa fa-times'></i>
								</button>
								<p>
									<strong>
										<i class='ace-icon fa fa-check'></i>
											<h3><b><center>>the contract number that you entered is registered</center></b></h3>
									</strong>
								</p>
							</div>");
							redirect("Eksplore/ControllerContract/FormInvoice/".$this->input->post('id_contract'));
					}

		}
		public function formPaid($id_invoice)
		{
			$this->db->select('*');
			$this->db->from('tb_invoice');
			$this->db->join('contract_export', 'contract_export.id_contract = tb_invoice.id_contract');
			$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');
			//$this->db->where('contract_export.id_contract',$id_contract);
			$this->db->where('tb_invoice.id_invoice',$id_invoice);
			$data['contract'] = $this->db->get()->row();
			// print_r($data['contract']);
			$id = $this->input->post('id');
			$data['loading']  = $this->db->get_where('transaksi',['id_invoice'=> $data['contract']->id_invoice])->result();
			$data['adjustment']  = $this->db->get_where('tb_adjustment',['id_invoice'=>$data['contract']->id_invoice])->row();
			$this->load->view('ViewEksplor/contract/addPaidDate',$data);
		}

		function updatePaid(){
			$id_invoice = $this->input->post('id_invoice');
			$id_contract = $this->input->post('id_contract');

			$date = $this->input->post('date');
			$data  = $this->db->where('id_invoice',$id_invoice)->update('tb_invoice',['date_paid_adjustment'=>$date,'status'=>"Complate"]);
			echo json_encode($data);
		}

		public function changeContract($id_contract)
		{
			$this->db->where('id_contract',$id_contract)->update('contract_export',['status'=>"Complete"]);
			redirect("Eksplore/ControllerContract");
		}

		public function eksportExcelContract()
		{

			$shipper = $this->input->post('shipper');
			$status = $this->input->post('statusE');
			$tgl_awal = $this->input->post('tgl_awalE');
			$tgl_akhir =$this->input->post('tgl_akhirE');

			$data['tgl_awal'] = date("j F Y",strtotime($tgl_awal));
			$data['tgl_akhir'] = date("j F Y",strtotime($tgl_akhir));

			$this->db->select('*');
			$this->db->from('tb_invoice');
			$this->db->join('contract_export', 'contract_export.id_contract = tb_invoice.id_contract');
			$this->db->join('master_shipper', 'master_shipper.id_shipper = contract_export.name_shiper');

			if (isset($_POST['excelA'])) {
				$this->db->join('tb_adjustment', 'tb_adjustment.id_invoice = tb_invoice.id_invoice');
				//kondisi
				if (!(empty($shipper))) {
					$this->db->where('contract_export.name_shiper',$shipper);
				}
				if (!(empty($status))) {
					$this->db->where('contract_export.status',$status);
				}


	      if (!empty($tgl_awal)) {
	        $this->db->where('contract_export.dateC >=', $tgl_awal);
	    		 $this->db->where('contract_export.dateC <=', $tgl_akhir);
	      }
				//$this->db->where('contract_export.id_contract',$id_contract);
				$data['contract'] = $this->db->get()->result();
				$data['title'] = "TRACKING FINAL ADJUSTMENT (".$data['tgl_awal']." - ".$data['tgl_akhir'].")";
				$this->load->view('ViewEksplor/eksportExcel/eksportContract',$data);
			} else {
				if (!(empty($shipper))) {
					$this->db->where('contract_export.name_shiper',$shipper);
				}
				if (!(empty($status))) {
					$this->db->where('contract_export.status',$status);
				}
	      if (!empty($tgl_awal)) {
	        $this->db->where('contract_export.dateC >=', $tgl_awal);
	    		 $this->db->where('contract_export.dateC <=', $tgl_akhir);
	      }
				$data['contract'] = $this->db->get()->result();
				$data['title'] = "TRACKING FINAL ADJUSTMENT (".$data['tgl_awal']." - ".$data['tgl_akhir'].")";
				$this->load->view('ViewEksplor/eksportExcel/eksportTracking',$data);
				// print_r($data['contract']);
			}



		}

}
