<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* BY ROby
*/
class C_report extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        $this->load->model('ore/M_stok');
        $this->load->model('ore/M_supplier');
        $this->load->model('ore/M_master_kontrak');
        $this->load->model('ore/M_laporan');
        $this->auth->cek_auth(); //--> ambil auth dari library

        //--> hak akses
        $hak_akses = $this->session->userdata('sis');
        if($hak_akses!='ore')
        {
            echo "<script>alert('Anda tidak berhak mengakses halaman ini!!');</script>";
            redirect('login','refresh');
            exit();
        }
	}

	 public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $this->M_stok->get_stok(),
        //'kontrak_kecil' => $this->M_master_kontrak->all_kontrak_kecil(),
        //'contract' => $this->M_master_kontrak->get_row_kontrak($id_kontrak),
        'title'     => 'Log Report | Ore '
        );

        $this->load->view('v_ore/report/log_report', $data);
    }

    public function detail_report($id_kontrak, $id_sm)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_kontrak;
        $kontrak = $this->M_laporan->get_row_laporanStok($id);
        

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'stok'      => $this->M_stok->get_row_stok_remain($id),
        'supplier' => $this->M_master_kontrak->get_kontrak_kecil($id),
        'coa_qty'      => $this->M_stok->get_row_coa_remain($id_sm),
        'title'     => 'Report Log | Ore '
        );

       
        
        $this->load->view('v_ore/report/detail_report', $data);
    }

    public function detail_lab_report($id_sm)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_sm;
        

        $hasil_lab = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'intertek' => $this->M_stok->get_row_intertek($id),
        'title'     => 'Stok Detail | Ore '
        );

        $this->load->view('v_ore/stok/detail_labresult', $hasil_lab);
    }


    public function update_status($id_kontrak){
         //--> jika form submit
        if($this->input->post('id_kontrak'))
        {
            $this->M_master_kontrak->update_status($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-danger'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='material-icons'>close</i>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>warning</i>
                                   Your data is Non-Aktif!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

    public function addendum($id_kontrak)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_kontrak;
        $kontrak = $this->M_master_kontrak->get_row_kontrak($id);
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'tgl'         => $tgl,
        'seller' => $this->M_seller->get_all_seller(),
        'supplier2' => $this->M_supplier->get_all_supplier(),
        'supplier' => $this->M_master_kontrak->get_kontrak_kecil_row($id),
        'title'     => 'Contract | Ore '
        );

        $this->load->view('v_ore/ore_contract/addendum', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $this->M_master_kontrak->addendum_update($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='ace-icon fa fa-times'></i>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Sukser your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

     public function edit($id_kontrak)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_kontrak;
        $kontrak = $this->M_master_kontrak->get_row_kontrak($id);
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'tgl'         => $tgl,
        'seller' => $this->M_seller->get_all_seller(),
        'supplier2' => $this->M_supplier->get_all_supplier(),
        'supplier' => $this->M_master_kontrak->get_kontrak_kecil_row($id),
        'title'     => 'Contract | Ore '
        );

        $this->load->view('v_ore/ore_contract/edit', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $this->M_master_kontrak->update_kontrak($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='ace-icon fa fa-times'></i>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Sukser your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

     public function tambah_kontrak()
    {
    	$get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');


		$data = array(
				'user'    	 => $get_akun,
        		'sistem'	  	 => $get_akun,
                'tgl'         => $tgl,
                //'id_kontrak' => $this->M_master_kontrak->get_id_max(),
                'seller' => $this->M_seller->get_all_seller(),
                'supplier' => $this->M_supplier->get_all_supplier(),
        		'title'			 => 'New Ore Contract',
			);

		// $this->load->model('M_pengguna','id_user');
		// $data['id_user'] = $this->id_user->buat_kode();
		

		$this->load->view('v_ore/ore_contract/form_tambah_contract', $data);

		//--> jika form submit
		if($this->input->post('submit'))
		{
            $this->M_master_kontrak->insert_kontrak();

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='ace-icon fa fa-times'></i>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Sukser your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

    public function hapus_data($id_kontrak)
    {
        $id = $id_kontrak;
        $this->M_master_kontrak->hapus_data($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_contract');
    }

}