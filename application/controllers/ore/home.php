<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
       // $this->load->model('ore/M_seller');
        //$this->auth->cek_auth(); //--> ambil auth dari library

        //--> hak akses
        $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
    }

    public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        
        'title'     => 'Home [ADMIN] | Ore '
        );

        $this->load->view('v_ore/home', $data);
    }
}