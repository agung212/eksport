<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* BY ROby
*/
class C_stok extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        $this->load->model('ore/M_supplier');
         $this->load->model('ore/M_stok');
        $this->load->model('ore/M_master_kontrak');
        //$this->auth->cek_auth(); //--> ambil auth dari library

        $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
	}

	 public function index()
    {
        $get_akun = $this->session->userdata('name');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'stok' => $this->M_stok->get_all_stok(),
        'hasil_lab' => $this->M_stok->get_all_lab(),
        'title'     => 'Stock | Ore '
        );

        $this->load->view('v_ore/stok/list_stok', $data);
    }

    public function adj_stok($id_sm)
    {   
        $get_akun = $this->session->userdata('name');

        $id = $id_sm;
        
        $get_stok = $this->M_stok->get_row_stok($id);
        $id_supp = $get_stok->supplier;

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_stok,
        'stok' => $this->M_stok->get_row_stok($id_sm),
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'intertek' => $this->M_stok->get_row_intertek($id),
        'seller' => $this->M_stok->get_seller(),
        'title'     => 'Stok Adjustment | Ore ',
        'supplier' => $this->get_supplier_edit($id_supp),
        'periode' => $this->get_periode_edit($id_supp)
        );

        $this->load->view('v_ore/stok/adjust_stok', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $this->M_stok->adj_stok($id);

            //--> Tampilkan notifikasi berhasil ubah
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert bg-green alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Adjustmen success!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok_adj');
        }
        
    }

    // public function get_supplier()
    // {
    //     $id = $this->input->post('id');
    //     echo $this->M_stok->get_supplier($id);
    // }

    function get_supplier(){
            $id = $this->input->post('seller_agent');

            $supplier = $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
                                          ->where('status', 'aktif')
                                          ->where('seller_agent', $id)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->result();

            $html = "<option value=''>- Select Supplier -</option>";
            foreach ($supplier as $value) {
                $a = "selected";
                $html .= "<option value='".$value->supplier."'>".$value->nama_supplier."</option>";
            }
            $callback = array('data_supplier'=>$html);
            echo json_encode($callback);
        }

     function get_supplier_edit($id_supp){
            //$id = $this->input->post('seller_agent');

            return $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
                                          ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
                                          ->where('tb_kontrak_kecil.status', 'aktif')
                                          ->where('supplier', $id_supp)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->result();

            // $html = "<option value=''>- Select Supplier -</option>";
            // foreach ($supplier as $value) {
            //     $a = "selected";
            //     if ($value->supplier == $id) {
            //        $a = "selected";
            //     } else {
            //         $a = "";
            //     }
                
            //     $html .= "<option ".$a." value='".$value->supplier."'>".$value->nama_supplier."</option>";
            // }
            // $callback = array('data_supplier'=>$html);
            // // echo json_encode($callback);
            // return $callback;
        }

    function get_periode(){
            $id = $this->input->post('supplier');

            $supplier = $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
                                          ->where('tb_kontrak_kecil.status', 'aktif')
                                          ->where('supplier', $id)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->row();

            $html = "<option value='".$supplier->id_kontrak."'>".$supplier->tgl_awal." s/d ".$supplier->tgl_akhir."</option>";
            
            $callback = array('data_periode'=>$html);
            echo json_encode($callback);
        }

    function get_periode_edit($id_supp){

            return $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
                                          ->where('tb_kontrak_kecil.status', 'aktif')
                                          ->where('supplier', $id_supp)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->result();

            // $html = "<option value=''>- Periode Shipment -</option>";
            // foreach ($supplier as $value) {
            //     $a = "selected";
            //     if ($value->supplier == $id) {
            //        $a = "selected";
            //     } else {
            //         $a = "";
            //     }
                
            //     $html .= "<option ".$a." value='".$value->id_kontrak."'>".$value->tgl_awal." s/d ".$value->tgl_akhir."</option>";
            // }
            // $callback = array('data_periode'=>$html);
            // // echo json_encode($callback);
            // return $callback;

            // $html = "<option value='".$supplier->id_kontrak."'>".$supplier->tgl_awal."-".$supplier->tgl_akhir."</option>";
            
            // $callback = array('data_periode'=>$html);
            // echo json_encode($callback);
        }

    function get_id(){
            $id = $this->input->post('supplier');

            $supplier = $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
                                          ->where('tb_kontrak_kecil.status', 'aktif')
                                          ->where('supplier', $id)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->row();

                $html = "<option value='".$supplier->id."'>".$supplier->id."</option>";
            
            $callback = array('data_id'=>$html);
            echo json_encode($callback);
        }

    function get_id_edit($id_supp){
            //$id = $this->input->post('supplier');

            return $this->db->select('*')
                                          ->from('tb_kontrak_kecil')
                                          ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
                                          ->where('tb_kontrak_kecil.status', 'aktif')
                                          ->where('supplier', $id_supp)
                                          ->order_by('supplier', 'asc')       
                                          ->get()->result();

            // $html = "<option value=''>- Select Id -</option>";

           
            //     $a = "selected";
            //     foreach ($supplier as $value) {
            //     $a = "selected";
            //     if ($value->id_kontrak == $id_ktr && $value->supplier == $id_supp) {
            //        $a = "";
            //     } else {
            //         $a = "selected";
            //     }
                
            //        $html .= "<option ".$a." value='".$value->id."'>".$value->id."</option>";
            //    }
            
            // $callback = array('data_id'=>$html);
            // return $callback;
        }


     public function tambah_stok(){

    	$get_akun = $this->session->userdata('name');
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');


		$data = array(
				'user'    	 => $get_akun,
        		'sistem'	  	 => $get_akun,
                'tgl'         => $tgl,
                
                'seller' => $this->M_stok->get_seller(),
                'supplier' => $this->M_stok->get_seller(),
        		'title'			 => 'New Stock',
			);

		
		$this->load->view('v_ore/stok/form_tambah_stok', $data);

		//--> jika form submit
		if($this->input->post('submit'))
        {

            $stok = $this->input->post('voyage_number');
            $id_agent = $this->input->post('id_agent');
            $id_kontrak = $this->input->post('master_kontrak');
            $id = $this->input->post('id');
            $id_supplier = $this->input->post('id_supplier');
            $jml_stok = $this->input->post('jml_stok');

            $quantity = $this->db->get_where('tb_kontrak_kecil', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier, 'status'=>"AKTIF"])->row();

            $qty = $this->db->get_where('tb_master_contract', ['id_kontrak' => $master_kontrak])->row();

            $qty_stok = $this->db->get_where('tb_stok', ['master_kontrak' => $id_kontrak])->row();

            $cek_masuk = $this->db->get_where('tb_stok_masuk', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier])->row();
            $max = $quantity->quantity + ($quantity->quantity*0.1);
            $min = $quantity->quantity - ($quantity->quantity*0.1);

            $ba = ($quantity->quantity*0.1);
            $bts_ats = $ba;
            $bts_bw = -$ba;

            $ba2 = ($qty->quantity*0.1);
            $bts_ats2 = $ba2;
            $bts_bw2 = -$ba2;
            
            $sisa_stok = $quantity->st - $jml_stok;

            $sisa_stok2 = $qty_stok->stok - $jml_stok;

            $cek = $this->M_stok->cek_stok($stok);
            if ($cek <1) {

                if (!($max <= $jml_stok) || ($max == $jml_stok)) {
                    if (($jml_stok >= $min) && ($jml_stok <= $max)) {
                        echo "non aktif";
                        $this->M_stok->insert_stok();
                        $this->M_stok->update_status($id_supplier, $id, $id_agent, $sisa_stok);
                        //buat fungsi untuk update status, karna isi bagian yang membuat status nonaktif

                    } elseif($sisa_stok > $bts_ats ) {

                        $this->M_stok->insert_stok();
                        $this->M_stok->update_ktr_kecil($id_supplier, $id, $id_agent, $sisa_stok);
                        $this->M_stok->update_stok_qty($id_kontrak, $sisa_stok2);

                    } elseif($sisa_stok < $bts_ats)  {

                        $this->M_stok->insert_stok();
                        $this->M_stok->update_status($id_supplier, $id, $id_agent, $sisa_stok);
                        $this->M_stok->update_stok_qty($id_kontrak, $sisa_stok2);
                        //$this->M_stok->update_ktr_stat($id_kontrak, $sisa_stok2);

                    } 
                    echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-success'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Success your data added!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_stok');
                    
                }else{
                    echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-danger'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Stok Terlalu Besar, Stok tidak mencukupi!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_stok');
                }

                

                //--> Tampilkan notifikasi berhasil
                
            } else {
                echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-danger'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Voyage Number You Entered Already Exists!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_stok');
            }


        }
    }

    public function hasil_lab($id_sm)
    {
        $get_akun = $this->session->userdata('name');
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');


        $data = array(
                'user'       => $get_akun,
                'sistem'         => $get_akun,
                'tgl'         => $tgl,
                'stok' => $this->M_stok->get_row_stok($id_sm),
                'seller' => $this->M_stok->get_seller(),
                'supplier' => $this->M_stok->get_seller(),
                'title'          => 'Lab Result | Stock',
            );

        
        $this->load->view('v_ore/stok/hasil_lab', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {

            $this->M_stok->insert_hasil_lab($id_sm);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Succes your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok');
        }
    }

    public function edit_stok($id_sm)
    {

        $get_akun = $this->session->userdata('name');

        $id = $id_sm;
        $get_stok = $this->M_stok->get_row_stok($id);
        $id_ktr = $get_stok->master_kontrak;
        $id_supp = $get_stok->supplier;
        //print_r($this->get_supplier_edit($get_stok->supplier));
        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_stok,
        'stok' => $this->M_stok->get_row_stok($id_sm),
        'hasil_lab' => $this->M_stok->get_row_coa($id_sm),
        'intertek' => $this->M_stok->get_row_intertek($id_sm),
        'seller' => $this->M_stok->get_seller(),
        'title'     => 'Stok Edit | Ore ',
        'id' => $this->get_id_edit($id_supp),
        'supplier' => $this->get_supplier_edit($id_supp),
        'periode' => $this->get_periode_edit($id_supp)
        );

       // print_r($data['html']);

        $this->load->view('v_ore/stok/form_edit', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            //$this->M_stok->update_stok_sm($id);

            $stok = $this->input->post('voyage_number');
            $id_agent = $this->input->post('seller_agent');
            $id_kontrak = $this->input->post('master_kontrak');
            $id = $this->input->post('id');
            $id_supplier = $this->input->post('supplier');
            $jml_stok = $this->input->post('jml_stok');

           // print_r($id);

            $quantity = $this->db->get_where('tb_kontrak_kecil', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier, 'status'=>"AKTIF"])->row();

            $qty = $this->db->get_where('tb_master_contract', ['id_kontrak' => $id_kontrak])->row();

            $qty_stok = $this->db->get_where('tb_stok', ['master_kontrak' => $id_kontrak])->row();

           // $cek_masuk = $this->db->get_where('tb_stok_masuk', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier])->row();
            $max = $quantity->quantity + ($quantity->quantity*0.1);
            $min = $quantity->quantity - ($quantity->quantity*0.1);

            $ba = ($quantity->quantity*0.1);
            $bts_ats = $ba;
            $bts_bw = -$ba;

            $ba2 = ($qty->quantity*0.1);
            $bts_ats2 = $ba2;
            $bts_bw2 = -$ba2;
            
            $sisa_stok = $quantity->quantity - $jml_stok;

            $sisa_stok2 = $qty->quantity - $jml_stok;

            //$id = $quantity->id;
            //$sisa_stok3 = $quantity->st - $jml_stok;

             if (!($max <= $jml_stok) || ($max == $jml_stok)) {
                    if (($jml_stok >= $min) && ($jml_stok <= $max)) {
                        echo "non aktif";
                        $this->M_stok->update_stok_sm($id_sm);
                        $this->M_stok->update_status($id_supplier, $id, $id_agent, $sisa_stok);
                        //buat fungsi untuk update status, karna isi bagian yang membuat status nonaktif

                    } elseif($sisa_stok > $bts_ats ) {

                        $this->M_stok->update_stok_sm($id_sm);
                        $this->M_stok->update_ktr_kecil($id_supplier, $id, $id_agent, $sisa_stok);
                        $this->M_stok->update_stok_qty($id_kontrak, $sisa_stok2);

                    } elseif($sisa_stok < $bts_ats)  {

                        $this->M_stok->update_stok_sm($id_sm);
                        $this->M_stok->update_status($id_supplier, $id, $id_agent, $sisa_stok);
                        $this->M_stok->update_stok_qty($id_kontrak, $sisa_stok2);
                        //$this->M_stok->update_ktr_stat($id_kontrak, $sisa_stok2);

                    } 
                    echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-success'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Success your data added!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_stok');
                    
                }else{
                    echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-danger'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Stok Terlalu Besar, Stok tidak mencukupi!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_stok');
                }

                

                //--> Tampilkan notifikasi berhasil
                
            
                

            //--> Tampilkan notifikasi berhasil ubah
           
         }
    }

    public function get_sm()
    {
        $get_akun = $this->session->userdata('name');

        $id = $this->input->post('id');


        $stok = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,

        'stok' => $this->M_stok->get_row_stok($id),
        );

                // print_r($hasil_lab['hasil_lab']->ni);
                echo json_encode($stok);
        // $this->load->view('v_ore/stok/detail_labresult', $hasil_lab);
    }

    public function edit_hasil_lab($id)
    {

        $get_akun = $this->session->userdata('name');

        //$id = $id_sm;
        $get_stok = $this->M_stok->get_row_stok($id);
        //print_r($this->get_supplier_edit($get_stok->supplier));
        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_stok,
        'stok' => $this->M_stok->get_row_stok($id),
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'intertek' => $this->M_stok->get_row_intertek($id),
        'seller' => $this->M_stok->get_seller(),
        'title'     => 'Stok Edit | Ore ',
        'html' => $this->get_supplier_edit($get_stok->supplier),
        'periode' => $this->get_periode_edit($get_stok->supplier)
        );

        $this->load->view('v_ore/stok/form_edit_hasil_lab', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {   
            //$pdf_up = $this->input->post('in_dokumen');
            $nmdokin = str_replace(' ', '_', $_FILES['in_dokumen']['name']);
            $tempatdokin = './assets/dokumen_intertek/'. $nmdokin;
            $ukuran = $_FILES['in_dokumen']['size'];
            
            print_r($ukuran);
            if($ukuran < 8044070)
            {
                $this->M_stok->update_hasil_lab($id);
            }else{
                   echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-danger'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    file size is too large!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok');
            }
            
            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Success your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok');
        }
    }

    public function hapus_data()
    {   
        $id_kontrak = $this->input->post('id_kontrak');
        $seller = $this->input->post('seller');
        $supplier = $this->input->post('supplier');
        $qty = $this->input->post('quantity');
        $id = $this->input->post('id_sm');

        $quantity = $this->db->get_where('tb_kontrak_kecil', ['seller_agent' => $seller, 'supplier'=> $supplier, 'status'=>"AKTIF"])->row();

        $qty_hps = $quantity->st + $qty;

        $stok = $this->db->get_where('tb_stok', ['master_kontrak'=>$id_kontrak])->row();

        $stok_hps = $stok->stok + $qty;

        //print_r($stok_hps);

        
        $this->M_stok->hapus_data($id, $qty_hps,  $seller, $supplier, $stok_hps, $id_kontrak);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_stok');
    }

    public function hapus_hasil_lab($id)
    {   

        $this->M_stok->hapus_lab($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_stok');
    }

    public function detail_lab($id_sm)
    {
        $get_akun = $this->session->userdata('name');

        $id = $id_sm;
        $get_stok = $this->M_stok->get_row_stok($id);

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_stok,
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'intertek' => $this->M_stok->get_row_intertek($id),
        'title'     => 'Stok Detail | Ore '
        );

        $this->load->view('v_ore/stok/detail_lab', $data);
    }

    function download_intertek($file){
        //print_r($file);
        force_download('assets/dokumen_intertek/'.$file, NULL);
    }

    function download_coa($file){
        //print_r($file);
        force_download('assets/dokumen_coa/'.$file, NULL);
    }

    function view_pdf($id) {
        $id= array('pdf' => $id );
        $this->load->view('v_ore/stok/view_pdf', $id);
    }
}