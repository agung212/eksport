<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_supplier extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_supplier');
       // $this->auth->cek_auth(); //--> ambil auth dari library

       $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
    }

    public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'supplier' => $this->M_supplier->get_all_supplier(),
        'title'     => 'Supplier | Ore '
        );

        $this->load->view('v_ore/supplier/list_supplier', $data);
    }

    public function tambah_supplier()
    {

        $nama_supplier = $this->input->post('nama_supplier');
        $cek = $this->M_supplier->cek_supplier($nama_supplier);

      if ($cek<1) {
        $this->M_supplier->insert_supplier();

        //--> Tampilkan notifikasi berhasil
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-success'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Sukser your data added!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_supplier');
      } else {
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                The Data You Entered Already Exists!!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_supplier');
      }
    }

    public function hapus_data($id_supplier)
    {
        $id = $id_supplier;
        $this->M_supplier->hapus_data($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_supplier');
    }

    public function edit_supplier($id_supplier)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_supplier;
        $get_supplier = $this->M_supplier->get_row_supplier($id);

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_supplier,
        'seller' => $this->M_supplier->get_all_supplier(),
        'title'     => 'Supplier | Ore '
        );

        $this->load->view('v_ore/supplier/form_edit', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $nama_supplier = $this->input->post('nama_supplier');
            $cek = $this->M_supplier->cek_supplier($nama_supplier);

          if ($cek<1) {
                $this->M_supplier->update_supplier($id);

                //--> Tampilkan notifikasi berhasil ubah
                echo $this->session->set_flashdata('sukses',
                         "<div class='alert alert-block alert-success'>
                                <button type='button' class='close' data-dismiss='alert'>
                                    <i class='ace-icon fa fa-times'></i>
                                </button>

                                <p>
                                    <strong>
                                        <i class='ace-icon fa fa-check'></i>
                                        Edit success!
                                    </strong>
                                </p>
                            </div>"
                    );

                redirect('ore/C_supplier');
            } else {
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                The Data You Entered Already Exists!!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_supplier');
            }
        }
    }
}
