<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* BY ROby
*/
class C_stok_adj extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        $this->load->model('ore/M_supplier');
         $this->load->model('ore/M_stok');
         $this->load->model('ore/M_stok_adj');
        $this->load->model('ore/M_master_kontrak');
        //$this->auth->cek_auth(); //--> ambil auth dari library

       $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
	}

	 public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'adj' => $this->M_stok_adj->get_all_adj(),
        'adj_notif' => $this->M_stok_adj->get_all_adj(),
        'hasil_lab' => $this->M_stok->get_all_lab(),
        'title'     => 'Stock | Ore ',
        'jml_notif' => $this->M_stok_adj->jml_notif(),
        'jml_notif_fin' => $this->M_stok_adj->jml_notif_fin(),
        'jml_notif_fin2' => $this->M_stok_adj->jml_notif_fin2(),
        'jml_notif_fin3' => $this->M_stok_adj->jml_notif_fin3()
        );

        $this->load->view('v_ore/adj/list_adj', $data);
    }

    public function detail_adj($id, $id_sm)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'jml_notif' => $this->M_stok_adj->jml_notif(),
        'jml_notif_fin' => $this->M_stok_adj->jml_notif_fin(),
        'jml_notif_fin2' => $this->M_stok_adj->jml_notif_fin2(),
        'adj_notif' => $this->M_stok_adj->get_all_adj(),
        'adj'       => $this->M_stok_adj->get_row_adj($id),
        'sm'        => $this->M_stok_adj->get_row_sm($id_sm),
        'coa'        => $this->M_stok_adj->get_row_coa($id_sm),
        'intertek'        => $this->M_stok_adj->get_row_intertek($id_sm),
        'title'     => 'Stok Adjustmen | Ore '
        );

        //print_r($data);
        $this->load->view('v_ore/adj/detail_adj', $data);
    }

    public function update_status($id_adj){
         //--> jika form submit
        if($id=$this->input->post('id_adj'))
        {
            $this->M_stok_adj->update_status($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>done</i>
                                   Adjustmen Stock Accepted!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok_adj');
        }
    }

    public function update_sk($id_adj){
         //--> jika form submit
        if($id=$this->input->post('id_adj'))
        {
            $this->M_stok_adj->update_sk($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>done</i>
                                   Adjustmen Stock Accepted!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok_adj');
        }
    }

    public function update_gm($id_adj){
         //--> jika form submit
        if($id=$this->input->post('id_adj'))
        {   
            $id_sm =$this->input->post('id_sm');

            $this->M_stok_adj->update_gm($id, $id_sm);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>done</i>
                                   Adjustmen Stock Accepted!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok_adj');
        }
    }

    public function reject($id_adj){
         //--> jika form submit
        if($id=$this->input->post('id_adj'))
        {
            $this->M_stok_adj->reject($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-danger'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>warning</i>
                                   Adjustmen Stock Rejected!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_stok_adj');
        }
    }
}