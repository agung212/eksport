<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* BY ROby
*/
class C_report extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        $this->load->model('ore/M_stok');
        $this->load->model('ore/M_supplier');
        $this->load->model('ore/M_master_kontrak');
        $this->load->model('ore/M_laporan');
        //$this->auth->cek_auth(); //--> ambil auth dari library

        $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
	}

	 public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $this->M_stok->get_stok(),
        //'kontrak_kecil' => $this->M_master_kontrak->all_kontrak_kecil(),
        //'contract' => $this->M_master_kontrak->get_row_kontrak($id_kontrak),
        'title'     => 'Log Report | Ore '
        );

        $this->load->view('v_ore/report/log_report', $data);
    }

    public function detail_report($id_kontrak, $id_sm)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_kontrak;
        $kontrak = $this->M_laporan->get_row_laporanStok($id);


        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'stok'      => $this->M_stok->get_row_stok_remain($id),
        'supplier' => $this->M_master_kontrak->get_kontrak_kecil($id),
        'coa_qty'      => $this->M_stok->get_row_coa_remain($id_sm),
        'title'     => 'Report Log | Ore '
        );



        $this->load->view('v_ore/report/detail_report', $data);
    }

    public function detail_lab_report()
    {
        

        $id = $this->input->post('id');


        $hasil_lab = array(

        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'intertek' => $this->M_stok->get_row_intertek($id),
        'title'     => 'Stok Detail | Ore '
        );

				// print_r($hasil_lab['hasil_lab']->ni);
				//echo json_encode($hasil_lab);
        $this->load->view('v_ore/report/detail_labresult', $hasil_lab);
        //print_r($id);

    }

    public function detail_coa_price()
    {

        $id = $this->input->post('id');


        $coa_price = array(

        'hasil_lab' => $this->M_stok->get_row_coa($id),
        'stok'      => $this->M_stok->get_row_stok($id),
        'title'     => 'Stok Detail | Ore '
        );

       
        $this->load->view('v_ore/report/detail_coa', $coa_price);
    }

    public function detail_in_price()
    {

        $id = $this->input->post('id');


        $in_price = array(

        'intertek' => $this->M_stok->get_row_intertek($id),
        'stok'      => $this->M_stok->get_row_stok($id),
        'title'     => 'Stok Detail | Ore '
        );

        //print_r($in_price);
        $this->load->view('v_ore/report/detail_intertek', $in_price);

    }


    public function update_status($id_kontrak){
         //--> jika form submit
        if($this->input->post('id_kontrak'))
        {
            $this->M_master_kontrak->update_status($id);

            //--> Tampilkan notifikasi berhasil
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-danger'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='material-icons'>close</i>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>warning</i>
                                   Your data is Non-Aktif!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

    

    public function hapus_data($id_kontrak)
    {
        $id = $id_kontrak;
        $this->M_master_kontrak->hapus_data($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_contract');
    }

    function filter_report(){
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $search_by = $this->input->post('search');

        if ($search_by == 1) {
            $data = array(
                        'user'      => $get_akun,
                        'sistem'     => $get_akun,
                        'kontrak' => $this->M_laporan->get_timearr(),
                        'title'     => 'Report'
                        );

            //print_r($data);
            $this->load->view('v_ore/report/report_detail', $data);
        }
        elseif ($search_by == 2) {
            //Comm Disch
             $data = array(
                        'user'      => $get_akun,
                        'sistem'     => $get_akun,
                        'kontrak' => $this->M_laporan->get_comm_disch(),
                        'title'     => 'Report'
                        );

            //print_r($data);
            $this->load->view('v_ore/report/report_detail', $data);
        }
         elseif ($search_by == 3) {
            //Comm Disch
             $data = array(
                        'user'      => $get_akun,
                        'sistem'     => $get_akun,
                        'kontrak' => $this->M_laporan->get_comp_disch(),
                        'title'     => 'Report'
                        );

            //print_r($data);
            $this->load->view('v_ore/report/report_detail', $data);
        }
        elseif ($search_by == 4) {
            //Comm Disch
             $data = array(
                        'user'      => $get_akun,
                        'sistem'     => $get_akun,
                        'kontrak' => $this->M_laporan->get_timedep(),
                        'title'     => 'Report'
                        );

            //print_r($data);
            $this->load->view('v_ore/report/report_detail', $data);
        }
        elseif ($search_by == 5) {
            //Comm Disch
             $data = array(
                        'user'      => $get_akun,
                        'sistem'     => $get_akun,
                        'kontrak' => $this->M_laporan->get_input(),
                        'title'     => 'Report'
                        );

            //print_r($data);
            $this->load->view('v_ore/report/report_detail', $data);
        }
    }

}
