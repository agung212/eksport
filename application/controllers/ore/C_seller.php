<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_seller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        //$this->auth->cek_auth(); //--> ambil auth dari library

        //--> hak akses
        $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
    }

    public function index()
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'seller' => $this->M_seller->get_all_seller(),
        'title'     => 'Seller [ADMIN] | Ore '
        );

        $this->load->view('v_ore/seller/list_seller', $data);
    }

    public function tambah_seller()
    {
        $nama_seller = $this->input->post('nama_seller');

        $cek = $this->M_seller->cek_seller($nama_seller); 

        if($cek < 1) {

            $this->M_seller->insert_seller();

            //--> Tampilkan notifikasi berhasil
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='ace-icon fa fa-times'></i>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Sukser your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_seller');
      }else {
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger' data-type='success'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                The Data You Entered Already Exists!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_seller');
      }
    }

    public function hapus_data($id_seller)
    {
        $id = $id_seller;
        $this->M_seller->hapus_data($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_seller');
    }

    public function edit_seller($id_seller)
    {
        $get_akun = $this->model_login->get_user($this->session->userdata('username'), $this->session->userdata('sis'));

        $id = $id_seller;
        $get_seller = $this->M_seller->get_row_seller($id);

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'data' => $get_seller,
        'seller' => $this->M_seller->get_all_seller(),
        'title'     => 'Seller [ADMIN] | Ore '
        );

        $this->load->view('v_ore/seller/form_edit', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {   
            $nama_seller = $this->input->post('nama_seller');

            $cek = $this->M_seller->cek_seller($nama_seller);

        if($cek < 1) {
            $this->M_seller->update_seller($id);

            //--> Tampilkan notifikasi berhasil ubah
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <i class='ace-icon fa fa-times'></i>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Edit success!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_seller');
        }else {
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger' data-type='success'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <i class='ace-icon fa fa-times'></i>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                The Data You Entered Already Exists!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_seller');
      }
  }
    }
}
