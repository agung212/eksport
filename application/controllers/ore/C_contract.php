<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* BY ROby
*/
class C_contract extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('model_login');
        $this->load->model('ore/M_seller');
        $this->load->model('ore/M_supplier');
        $this->load->model('ore/M_master_kontrak');
        //$this->auth->cek_auth(); //--> ambil auth dari library

        //--> hak akses
        $sistem = $this->session->userdata('sistem');
      //menunjukan apakah pengguna sistem diperbolehkan mengakses halaman ore atau tidak
      if(empty($sistem['ORE'])){
        //menunjukan bahawa jika pengguna sudah login tapi tidak diberi izin untuk menggunakan sistem maka akan dialihkan pada halaman utamanya
        if ($this->session->userdata('status') == "login") {
          //jika teridentifikasi sebagai admin/user
          redirect(base_url("admin/admin"));
        }else {
          //tidak teridentifikasi sebagai siapapun
          redirect(base_url("admin/login_admin"));
        }
      }
	}

	 public function index()
    {
        $get_akun = $this->session->userdata('name');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $this->M_master_kontrak->get_all_kontrak(),
        //'kontrak_kecil' => $this->M_master_kontrak->all_kontrak_kecil(),
        //'contract' => $this->M_master_kontrak->get_row_kontrak($id_kontrak),
        'title'     => 'Contract | Ore '
        );

        $this->load->view('v_ore/ore_contract/list_ore_contract', $data);
    }

    
    public function detail_kontrak($id_kontrak)
    {
        $get_akun = $this->session->userdata('name');

        $id = $id_kontrak;
        $kontrak = $this->M_master_kontrak->get_row_kontrak($id);
        

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'stok'      => $this->M_master_kontrak->get_row_stok($id),
        'supplier' => $this->M_master_kontrak->get_kontrak_kecil($id),
        'title'     => 'Contract | Ore '
        );

        $this->load->view('v_ore/ore_contract/detail_ore_contract', $data);
    }

    public function update_status($id_kontrak){
         //--> jika form submit
        if($id=$this->input->post('id_kontrak'))
        {
            $this->M_master_kontrak->update_status($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-danger'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='material-icons'>warning</i>
                                   Your data is Non-Aktif!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

    public function addendum($id_kontrak)
    {
        $get_akun = $this->session->userdata('name');
        
        $id = $id_kontrak;
        $kontrak = $this->M_master_kontrak->get_row_kontrak($id);
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'id_kontrak' => $this->M_master_kontrak->get_id_max(),
        'tgl'         => $tgl,
        'seller' => $this->M_seller->get_all_seller(),
        'supplier' => $this->M_supplier->get_all_supplier(),
        'supplier2' => $this->M_master_kontrak->get_kontrak_kecil_row($id),
        'title'     => 'Contract | Ore '
        );


        //print_r($jml_sm);

        $this->load->view('v_ore/ore_contract/addendum', $data);

        // $qty_sisa = $this->db->get_where('tb_kontrak_kecil', ['id_kontrak' => $id_kontrak])->result();
        // $st[]=0;

        // foreach ($qty_sisa as $q) {
        //     $st[] = $q->quantity - $q->st; 
        // }

        // $j=1;
        // while ( $j <= 2) {
        //     $s[] = $st[$j];
           

        // $j++;
        // }
        
        // //insert Supplier
        // $jum_supplier = $this->input->post('jml_supplier')-1;
        // $supplier = $this->input->post('supplier');

        // $jum_quantity = $this->input->post('jml_quantity')-1;
        // $quantity = $this->input->post('quantity');


        // if($jum_supplier < $jum_quantity)
        // {
        //     $jml = $jum_quantity;
        // }
        // elseif($jum_supplier > $jum_quantity)
        // {
        //     $jml = $jum_supplier;
        // }
        // else
        // {
        //     $jml = $jum_supplier;
        // }

        // $jml_qty=0;

        // $i=0;
        // while($i < $jml)
        // {
        //     //--> tambah sub_tim
        //     $data_supplier = array(
        //             'id_kontrak' => $this->input->post('id_kontrak'),
        //             'seller_agent'  => $this->input->post('seller_agent'),
        //             'supplier'    => $supplier[$i],
        //             'quantity'    => $quantity[$i],
        //             'st'    => $quantity[$i] - $s[$i],
        //             'status'    => $this->input->post('status')
    
        //         );

        //     $jml_qty = $jml_qty + $quantity[$i];

        //     print_r($data_supplier);
        //     //$this->db->insert('tb_kontrak_kecil', $data_supplier);

        //     $i++;
        // }
        //print_r($st[1]);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $this->M_master_kontrak->addendum_update($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Success your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

     public function edit($id_kontrak)
    {
        $get_akun = $this->session->userdata('name');

        $id = $id_kontrak;
        $kontrak = $this->M_master_kontrak->get_row_kontrak($id);
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');

        $data = array(
        'user'      => $get_akun,
        'sistem'     => $get_akun,
        'kontrak' => $kontrak,
        'tgl'         => $tgl,
        'seller' => $this->M_seller->get_all_seller(),
        'supplier' => $this->M_supplier->get_all_supplier(),
        'supplier2' => $this->M_master_kontrak->get_kontrak_kecil_row($id),
        'title'     => 'Contract | Ore '
        );

        $this->load->view('v_ore/ore_contract/form_edit_kontrak', $data);

        //--> jika form submit
        if($this->input->post('submit'))
        {
            $this->M_master_kontrak->update_kontrak($id);

            //--> Tampilkan notifikasi berhasil 
            echo $this->session->set_flashdata('sukses',
                     "<div class='alert alert-block alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>
                                <span aria-hidden='true'>×</span>
                            </button>

                            <p>
                                <strong>
                                    <i class='ace-icon fa fa-check'></i>
                                    Success your data added!
                                </strong>
                            </p>
                        </div>"
                );

            redirect('ore/C_contract');
        }
    }

     public function tambah_kontrak()
    {
    	$get_akun = $this->session->userdata('name');
        $tgl = date('d')." ".get_nama_bulan(date('m'))." ". date('Y');


		$data = array(
				'user'    	 => $get_akun,
        		'sistem'	  	 => $get_akun,
                'tgl'         => $tgl,
                'id_kontrak' => $this->M_master_kontrak->get_id_max(),
                'seller' => $this->M_seller->get_all_seller(),
                'supplier' => $this->M_supplier->get_all_supplier(),
        		'title'			 => 'New Ore Contract',
			);

		// $this->load->model('M_pengguna','id_user');
		// $data['id_user'] = $this->id_user->buat_kode();
		

		$this->load->view('v_ore/ore_contract/form_tambah_contract', $data);

		//--> jika form submit
		if($this->input->post('submit'))
        {
            $master_kontrak = $this->input->post('master_kontrak');
            $cek = $this->M_master_kontrak->cek_kontrak($master_kontrak);

            if ($cek<1) {
                $this->M_master_kontrak->insert_kontrak();
                //--> Tampilkan notifikasi berhasil
                echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-success'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Success your data added!
                                                        </strong>
                                                </p>
                                        </div>"
                        );

                redirect('ore/C_contract');
            } else {
                echo $this->session->set_flashdata('sukses',
                                 "<div class='alert alert-block alert-danger'>
                                                <button type='button' class='close' data-dismiss='alert'>
                                                        <span aria-hidden='true'>×</span>
                                                </button>

                                                <p>
                                                        <strong>
                                                                <i class='ace-icon fa fa-check'></i>
                                                                Master Contract You Entered Already Exists!
                                                        </strong>
                                                </p>
                                        </div>"
                        );
                    redirect('ore/C_contract');
            }



        }
    }

    public function hapus_data($id_kontrak)
    {
        $id = $id_kontrak;
        $this->M_master_kontrak->hapus_data($id);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <span aria-hidden='true'>×</span>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_contract');
    }

     public function hapus_supplier($id, $id_kontrak)
    {
        $id_hps = $id;
        $id2 = $id_kontrak;
        $this->M_master_kontrak->hapus_supplier($id_hps);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <span aria-hidden='true'>×</span>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_contract/edit/'.$id2);
    }

    public function hapus_supplier_add($id, $id_kontrak)
    {
        $id_hps = $id;
        $id2 = $id_kontrak;
        $this->M_master_kontrak->hapus_supplier($id_hps);

        //--> Tampilkan notifikasi berhasil hapus
        echo $this->session->set_flashdata('sukses',
                 "<div class='alert alert-block alert-danger'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <span aria-hidden='true'>×</span>
                        </button>

                        <p>
                            <strong>
                                <i class='ace-icon fa fa-check'></i>
                                Your data has ben delete!
                            </strong>
                        </p>
                    </div>"
            );

        redirect('ore/C_contract/addendum/'.$id2);
    }

    
}