<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_coal_schedule extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
  		parent::__construct();
      $sistem = $this->session->userdata('sistem');
      if(empty($sistem['ORE'])){
        if ($this->session->userdata('status') == "login") {
          redirect(base_url("admin/admin"));
        }else {
          redirect(base_url("admin/login_admin"));
        }

      }
	}
	public function index()
	{
		$data['data']    = $this->db->get('coal_contract');
		$data['content'] = 'coal/input_schedule1';
		$this->load->view('coal/index',$data);
	}
	public function insert_schedule()
	{ 
		$id_contract = $this->input->post('id_contract');
		$material = $this->input->post('material');
		$type = $this->input->post('type');
		$vessel = $this->input->post('vessel');
		$laycan_loading = $this->input->post('laycan_loading');
		$laycan_loadingEnd = $this->input->post('laycan_loadingEnd');
		$eta_morosi = $this->input->post('eta_morosi');
		$estimate_total = $this->input->post('estimate_total');
		$final_price1 = $this->input->post('final_price1');
		$final_price2 = $this->input->post('final_price2');
		$premium_1 = $final_price1 * $estimate_total;
		$premium_2 = $final_price2 * $estimate_total;
		$data = array(
			'id_contract' => $id_contract,
			'material' => $material,
			'laycan_loading' => $laycan_loading,
			'laycan_end' => $laycan_loadingEnd,
			'eta' => $eta_morosi,			
			'type' => $type,
			'eta_total' => $estimate_total,
			'vessel_name' => $vessel,
			'estimate_daily' => $final_price1,
			'minumum_stock' => $final_price2,
			'premium_1' => $premium_1,
			'premium_2' => $premium_2
			);
	     $this->db->insert('coal_schedule',$data);
		 $aksi = array('aksi' => 'sukses_schedule', 'id_contract' => $id_contract, 'waktu' => $laycan_loading );
		 $this->load->view('coal/sukses', $aksi);
	}


    public function edit_schedule($id = NULL) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $data['data_contract']    = $this->db->get('coal_contract');
		   $this->db->select('*');
		   $this->db->from('coal_schedule');
		   $this->db->join('coal_contract', 'coal_contract.id_contract = coal_schedule.id_contract');
		   $this->db->where('coal_schedule.id_coalSch', $id);
		   $data['data'] = $this->db->get();
		   $data['content'] = 'coal/edit_schedule'; 
		   $this->load->view('coal/index', $data);
    }

    public function edit_scheduleAction()
	{ 
		$id_contract = $this->input->post('id_contract');
		$material = $this->input->post('material');
		$type = $this->input->post('type');
		$vessel = $this->input->post('vessel');
		$laycan_loading = $this->input->post('laycan_loading');
		$laycan_loadingEnd = $this->input->post('laycan_loadingEnd');
		$eta_morosi = $this->input->post('eta_morosi');
		$estimate_total = $this->input->post('estimate_total');
		$final_price1 = $this->input->post('final_price1');
		$final_price2 = $this->input->post('final_price2');
		$premium_1 = $final_price1 * $estimate_total;
		$premium_2 = $final_price2 * $estimate_total;

		
		$id_coalSch = $this->input->post('id_coalSch');
		$data = array(
			'id_contract' => $id_contract,
			'material' => $material,
			'laycan_loading' => $laycan_loading,
			'laycan_end' => $laycan_loadingEnd,
			'eta' => $eta_morosi,
			'type' => $type,
			'eta_total' => $estimate_total,
			'vessel_name' => $vessel,
			'estimate_daily' => $final_price1,
			'minumum_stock' => $final_price2,
			'premium_1' => $premium_1,
			'premium_2' => $premium_2
			);
	     $this->db->where('id_coalSch', $id_coalSch);
		 $this->db->update('coal_schedule',$data);
		 $aksi = array('aksi' => 'update_schedule', 'stat' => '1', 'id_contract' => $id_contract);
		 $this->load->view('coal/sukses', $aksi);
	}

	 public function del_schedule($id, $id_contract) {
    		$this->db->where('id_coalSch', $id);
    		$this->db->delete('coal_schedule');
    		$aksi = array('aksi' => 'hapus_schedule', 'stat' => '1', 'id_contract' => $id_contract );
			$this->load->view('coal/sukses', $aksi);
    }
}
