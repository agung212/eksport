<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_input_discharge extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
  		parent::__construct();
      $sistem = $this->session->userdata('sistem');
      if(empty($sistem['ORE'])){
        if ($this->session->userdata('status') == "login") {
          redirect(base_url("admin/admin"));
        }else {
          redirect(base_url("admin/login_admin"));
        }

      }
	}

	public function index()
	{
		$data['content']     = 'coal/data_discharge';
		$this->db->select('*');
		$this->db->from('coal_discharge');
		$this->db->join('coal_schedule', 'coal_schedule.id_coalSch = coal_discharge.id_coalSch');
		$data['data'] = $this->db->get();
		$this->load->view('coal/index1', $data);
		
	}

	public function listKota(){
		// Ambil data ID Provinsi yang dikirim via ajax post
		$id  = $this->input->post('id');
		
		$this->load->model('coal/KotaModel');
		$cek = $this->KotaModel->ViewByProvinsi($id);
		echo "<option value=''>-- Please Select -- </option>";
		foreach ($cek as $key) {
		echo "<option value='".$key->id_coalSch."\/".$key->id_contract."\/".$key->vessel_name."'>".$key->vessel_name."</option>";
		}
	}

	public function insert_contract()
	{
		$type	 		= $this->input->post('type');
		$vessel_name 	= $this->input->post('vessel_name');
		$voyage_no 		= $this->input->post('voyage_number');
		$bl_number 		= $this->input->post('bl_number');
		$bl_qty 		= $this->input->post('bl_qty');
		$ta 			= $this->input->post('ta');
		$comm_disch 	= $this->input->post('comm_disch');
		$comp_disch 	= $this->input->post('comp_disch');
		$td    			= $this->input->post('td');
		$pecah			= explode("\/", $vessel_name);
		$id_coalSch		= $pecah[0];
		$id_contract	= $pecah[1];
		$vessel			= $pecah[2];
		

		$data = array(
			'id_coalSch' => $id_coalSch,
			'voyage_no'	=> $voyage_no,
			'bl_qty' => $bl_qty,
			'bl_no'	=> $bl_number,
			'ta' => $ta,
			'comm_disch' => $comm_disch,
			'comp_disch' => $comp_disch,
			'td' => $td
		);
		$this->load->model('coal/m_discharge');
		$cek = $this->m_discharge->ViewByProvinsi($id_contract);
		foreach ($cek as $key) {
		$min_stock = $key->stock * 0.10;
		$bw = -$min_stock;
		$st = $key->stock - $bl_qty;
		}
		if ($st >= $bw) {
		$this->db->insert('coal_discharge',$data);
		$stock = array('stock' => $st);
		$this->db->where('id_contract', $id_contract);
		$this->db->update('stock_coal',$stock);

		$status = array('status' => '1');
		$this->db->where('id_coalSch', $id_coalSch);
		$this->db->update('coal_schedule',$status);

		$aksi  = array('aksi' => 'sukses_disc', 'stat' => '1' );
		$this->load->view('coal/sukses', $aksi);
		} else {
			$aksi  = array('aksi' => 'gagal_disc', 'stat' => '1' );
			$this->load->view('coal/alert', $aksi);
		}
	}

	public function del_disch($id_coalSch, $id_disch, $bl_qty) {
			$this->load->model('coal/m_discharge');
			$cek1 = $this->m_discharge->coal_schedule($id_coalSch);
			foreach ($cek1 as $key) {
			$id_contract = $key->id_contract;
			}
			$cek2 = $this->m_discharge->ViewByProvinsi($id_contract);
			foreach ($cek2 as $key2) {
			$st = $key2->stock + $bl_qty;
			}
			$stock = array('stock' => $st);
			$this->db->where('id_contract', $id_contract);
			$this->db->update('stock_coal',$stock);

			$status = array('status' => '0');
			$this->db->where('id_coalSch', $id_coalSch);
			$this->db->update('coal_schedule',$status);

    		$this->db->where('id_disch', $id_disch);
    		$this->db->delete('coal_discharge');
    		$aksi = array('aksi' => 'hapus_disch', 'stat' => '2' );
			$this->load->view('coal/sukses', $aksi);
    }

    public function edit_disch($id_coalSch, $id_disch, $bl_qty) {
    	 $this->db->select('*');
		 $this->db->from('coal_discharge');
		 $this->db->join('coal_schedule', 'coal_schedule.id_coalSch  = coal_discharge.id_coalSch');
		 $this->db->join('coal_contract', 'coal_contract.id_contract = coal_schedule.id_contract');
		 $this->db->where('coal_discharge.id_coalSch', $id_coalSch);
		 $data['edit'] = $this->db->get();
		 $data['content'] = 'coal/edit_discharge';
		 $this->db->select('*');
		 $this->db->from('coal_discharge');
		 $this->db->join('coal_schedule', 'coal_schedule.id_coalSch = coal_discharge.id_coalSch');
		 $data['data'] = $this->db->get();
		 $this->load->view('coal/index1',$data);

    }

     public function edit_discharge() {
		$type	 		= $this->input->post('type');
		$vessel_name 	= $this->input->post('vessel_name');
		$voyage_no 		= $this->input->post('voyage_number');
		$bl_number 		= $this->input->post('bl_number');
		$bl_qty 		= $this->input->post('bl_qty');
		$ta 			= $this->input->post('ta');
		$comm_disch 	= $this->input->post('comm_disch');
		$comp_disch 	= $this->input->post('comp_disch');
		$td    			= $this->input->post('td');
		$id_disch    	= $this->input->post('id_disch');
		$id_contractLm  = $this->input->post('id_contractLm');
		$bl_qtyLm  		= $this->input->post('bl_qtyLm');
		$id_disch    	= $this->input->post('id_disch');
		$id_coalSchLm   = $this->input->post('id_coalSch');
		$pecah			= explode("\/", $vessel_name);
		$id_coalSch		= $pecah[0];
		$id_contract	= $pecah[1];
		$vessel			= $pecah[2];
		

		$data = array(
			'id_coalSch' => $id_coalSch,
			'voyage_no'	=> $voyage_no,
			'bl_qty' => $bl_qty,
			'bl_no'	=> $bl_number,
			'ta' => $ta,
			'comm_disch' => $comm_disch,
			'comp_disch' => $comp_disch,
			'td' => $td
		);
		$this->load->model('coal/m_discharge');
		$cek = $this->m_discharge->ViewByProvinsi($id_contractLm);
		foreach ($cek as $key) {
		$st1 = $key->stock + $bl_qtyLm;
		}
		$stock1 = array('stock' => $st1);
		$this->db->where('id_contract', $id_contractLm);
		$this->db->update('stock_coal',$stock1);

		$cek = $this->m_discharge->ViewByProvinsi($id_contract);
		foreach ($cek as $key) {
		$min_stock = $key->stock * 0.10;
		$bw = -$min_stock;
		$st = $key->stock - $bl_qty;
		}

		if($st >= $bw) {
		$status = array('status' => '0');
		$this->db->where('id_coalSch', $id_coalSchLm);
		$this->db->update('coal_schedule',$status);
		$this->db->where('id_disch', $id_disch);
		$this->db->update('coal_discharge',$data);
		
		$stock = array('stock' => $st);
		$this->db->where('id_contract', $id_contract);
		$this->db->update('stock_coal',$stock);
		$status = array('status' => '1');
		$this->db->where('id_coalSch', $id_coalSch);
		$this->db->update('coal_schedule',$status);
		$aksi  = array('aksi' => 'update_disc', 'stat' => '1' );
		$this->load->view('coal/sukses', $aksi);

		} else {
			$cek = $this->m_discharge->ViewByProvinsi($id_contractLm);
			foreach ($cek as $key) {
			$st1 = $key->stock - $bl_qtyLm;
			}
			$stock1 = array('stock' => $st1);
			$this->db->where('id_contract', $id_contractLm);
			$this->db->update('stock_coal',$stock1);
			$aksi  = array('aksi' => 'gagal_disc', 'stat' => '1' );
			$this->load->view('coal/alert', $aksi);
		}
    }
}