<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_setting extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
  		parent::__construct();
      $sistem = $this->session->userdata('sistem');
      if(empty($sistem['ORE'])){
        if ($this->session->userdata('status') == "login") {
          redirect(base_url("admin/admin"));
        }else {
          redirect(base_url("admin/login_admin"));
        }

      }
	}

	public function index($stat = NULL)
	{
		if (empty($stat)) {
			$status = '1';
		} else {
			$status = $stat;
		}
		$data['content']     = 'coal/setting';
		$data['data']        = $this->db->get('supplier_coal');
		$data['buyer']       = $this->db->get('buyer_agent');
		$data['terms']       = $this->db->get('terms');
		$data['buy']       = $this->db->get('buyer');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		$data['status']      = array('status' => $status );
		$this->load->view('coal/index', $data);
		
	}

	public function insert_seller()
	{
		 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$supplier = $this->input->post('nm_supplier');
		$data = array(
			'nm_supplier' => $supplier
			);
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_data("supplier_coal",$data);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal', 'stat' => '1' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('supplier_coal',$data);
		   $aksi = array('aksi' => 'sukses', 'stat' => '1' );
		   $this->load->view('coal/sukses', $aksi);
		}
	}

    public function del_supplier($id_sup = NULL) {
    		$this->db->where('id_sup', $id_sup);
    		$this->db->delete('supplier_coal');
    		$aksi = array('aksi' => 'hapus', 'stat' => '1' );
			$this->load->view('coal/sukses', $aksi);
    	}

    public function edit_supplier($id_sup = NULL) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $this->db->where('id_sup', $id_sup);
		   $data['data']        = $this->db->get('supplier_coal');
		   $data['content']     = 'coal/update_supplier'; 
		   $data['normal']      = $this->db->get('supplier_coal');
		   $data['buyer']       = $this->db->get('buyer_agent');
		   $data['terms']       = $this->db->get('terms');
		   $data['buy']       = $this->db->get('buyer');
		  $this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		   $data['status']      = array('status' => '1' );
		   $this->load->view('coal/index', $data);
    }
    public function edit_supplierAction() {
    	 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$supplier = $this->input->post('nm_supplier');
		$nm_supL = $this->input->post('nm_supplier_lm');
		if ($supplier == $nm_supL) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$id_sup = $this->input->post('id_sup');
		$data = array(
			'nm_supplier' => $supplier,
			'id_sup'=> $id_sup

			);
		$where = array('nm_supplier' => $supplier );
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_data("supplier_coal",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id_sup', $id_sup);
		   $this->db->update('supplier_coal',$data);
		   $aksi = array('aksi' => 'update', 'stat' => '1');
		   $this->load->view('coal/sukses', $aksi);
		} else {
		   $aksi = array('aksi' => 'gagal', 'stat' => '1');
		   $this->load->view('coal/alert', $aksi);
		}
    }


    public function insert_buyer()
	{
		 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$buyer = $this->input->post('buyer_agent');
		$data = array(
			'nm_buyer' => $buyer
			);
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_buyer("buyer_agent",$data);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal', 'stat' => '2' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('buyer_agent',$data);
		   $aksi = array('aksi' => 'sukses', 'stat' => '2' );
		   $this->load->view('coal/sukses', $aksi);
		}
	}

    public function del_buyer($id_buyer = NULL) {
    		$this->db->where('id_buyer', $id_buyer);
    		$this->db->delete('buyer_agent');
    		$aksi = array('aksi' => 'hapus', 'stat' => '2' );
			$this->load->view('coal/sukses', $aksi);
    	}

    public function edit_buyer($id_buyer = NULL) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $this->db->where('id_buyer', $id_buyer);
		   $data['data']        = $this->db->get('buyer_agent');
		   $data['content']     = 'coal/update_supplier'; 
		   $data['normal']      = $this->db->get('supplier_coal');
		   $data['buyer']       = $this->db->get('buyer_agent');
		   $data['terms']       = $this->db->get('terms');
		   $data['buy']       = $this->db->get('buyer');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		   $data['status']      = array('status' => '2' );
		   $this->load->view('coal/index', $data);
    }

    public function edit_buyerAction() {
    	 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$nm_buyer = $this->input->post('buyer_agent');
		$nm_buyer_lm = $this->input->post('buyer_agent_lm');
		if ($nm_buyer == $nm_buyer_lm) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$id_buyer = $this->input->post('id_buyer');
		$data = array(
			'nm_buyer' => $nm_buyer,
			'id_buyer'=> $id_buyer
			);
		$where = array('nm_buyer' => $nm_buyer );
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_buyer("buyer_agent",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id_buyer', $id_buyer);
		   $this->db->update('buyer_agent',$data);
		   $aksi = array('aksi' => 'update', 'stat' => '2');
		   $this->load->view('coal/sukses', $aksi);
		} else {
		   $aksi = array('aksi' => 'gagal', 'stat' => '2');
		   $this->load->view('coal/alert', $aksi);
		}
    }

    public function insert_buy()
	{
		 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$buyer = $this->input->post('buyer');
		$data = array(
			'nm_buy' => $buyer
			);
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_buyer("buyer",$data);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal', 'stat' => '5' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('buyer',$data);
		   $aksi = array('aksi' => 'sukses', 'stat' => '5' );
		   $this->load->view('coal/sukses', $aksi);
		}
	}

    public function del_buy($id_buyer = NULL) {
    		$this->db->where('id_buy', $id_buyer);
    		$this->db->delete('buyer');
    		$aksi = array('aksi' => 'hapus', 'stat' => '5' );
			$this->load->view('coal/sukses', $aksi);
    	}

    public function edit_buy($id_buyer = NULL) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $this->db->where('id_buy', $id_buyer);
		   $data['data']        = $this->db->get('buyer');
		   $data['content']     = 'coal/update_supplier'; 
		   $data['normal']      = $this->db->get('supplier_coal');
		   $data['buyer']       = $this->db->get('buyer_agent');
		   $data['terms']       = $this->db->get('terms');
		   $data['buy']       = $this->db->get('buyer');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		   $data['status']      = array('status' => '5' );
		   $this->load->view('coal/index', $data);
    }

    public function edit_buyAction() {
    	 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$nm_buyer = $this->input->post('buyer');
		$nm_buyer_lm = $this->input->post('buyer_lm');
		if ($nm_buyer == $nm_buyer_lm) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$id_buyer = $this->input->post('id_buy');
		$data = array(
			'nm_buy' => $nm_buyer,
			'id_buy'=> $id_buyer
			);
		$where = array('nm_buy' => $nm_buyer );
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_buyer("buyer",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id_buy', $id_buyer);
		   $this->db->update('buyer',$data);
		   $aksi = array('aksi' => 'update', 'stat' => '5');
		   $this->load->view('coal/sukses', $aksi);
		} else {
		   $aksi = array('aksi' => 'gagal', 'stat' => '5');
		   $this->load->view('coal/alert', $aksi);
		}
    }

    public function insert_terms()
	{
		 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$terms = $this->input->post('terms');
		$data = array(
			'nm_terms' => $terms
			);
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_terms("terms",$data);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal', 'stat' => '3' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('terms',$data);
		   $aksi = array('aksi' => 'sukses', 'stat' => '3' );
		   $this->load->view('coal/sukses', $aksi);
		}
	}

    public function del_terms($id_terms = NULL) {
    		$this->db->where('id_terms', $id_terms);
    		$this->db->delete('terms');
    		$aksi = array('aksi' => 'hapus', 'stat' => '3' );
			$this->load->view('coal/sukses', $aksi);
    	}

    public function edit_terms($id_terms = NULL) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $this->db->where('id_terms', $id_terms);
		   $data['data']        = $this->db->get('terms');
		   $data['content']     = 'coal/update_supplier'; 
		   $data['normal']      = $this->db->get('supplier_coal');
		   $data['buyer']       = $this->db->get('buyer_agent');
		   $data['terms']       = $this->db->get('terms');
		   $data['buy']       = $this->db->get('buyer');
		$this->db->where('category', 1);
		$data['parameter']   = $this->db->get('specification');
		$this->db->where('category', 2);
		$data['satuan']   = $this->db->get('specification');
		$this->db->where('category', 3);
		$data['remarks']   = $this->db->get('specification');
		   $data['status']      = array('status' => '3' );
		   $this->load->view('coal/index', $data);
    }
    
    public function edit_termsAction() {
    	 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$terms = $this->input->post('terms');
		$terms_lm = $this->input->post('terms_lm');
		if ($terms == $terms_lm) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$id_terms = $this->input->post('id_terms');
		$data = array(
			'id_terms' => $id_terms,
			'nm_terms'=> $terms
		);
		$where = array('nm_terms' => $terms );
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_terms("terms",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id_terms', $id_terms);
		   $this->db->update('terms',$data);
		   $aksi = array('aksi' => 'update', 'stat' => '3');
		   $this->load->view('coal/sukses', $aksi);
		} else {
		   $aksi = array('aksi' => 'gagal', 'stat' => '3');
		   $this->load->view('coal/alert', $aksi);
		}
    }

     public function insert_specification()
	{
		 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$category = $this->input->post('category');
		$specification = $this->input->post('specification');
		$data = array(
			'category' => $category,
			'nm_spec' => $specification
			);
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_terms("specification",$data);
		if($cek->num_rows() > 0){
			$aksi = array('aksi' => 'gagal', 'stat' => '4' );
			$this->load->view('coal/alert', $aksi);
		} else {
		   $this->db->insert('specification',$data);
		   $aksi = array('aksi' => 'sukses', 'stat' => '4' );
		   $this->load->view('coal/sukses', $aksi);
		}
	}

 public function del_specification($category, $id_specification)
	{
    		$this->db->where('id', $id_specification);
    		$this->db->delete('specification');
    		$aksi = array('aksi' => 'hapus', 'stat' => '4' );
			$this->load->view('coal/sukses', $aksi);
	}

public function edit_specification($category, $id_specification) {
    	   $this->load->helper(array('form', 'url'));
		   $this->load->library('form_validation');
		   $this->db->where('id', $id_specification);
		   $data['data']        = $this->db->get('specification');
		   $data['content']     = 'coal/update_supplier'; 
		   $data['normal']      = $this->db->get('supplier_coal');
		   $data['buyer']       = $this->db->get('buyer_agent');
		   $data['terms']       = $this->db->get('terms');
		   $data['buy']         = $this->db->get('buyer');
		   $this->db->where('category', 1);
		   $data['parameter']   = $this->db->get('specification');
		   $this->db->where('category', 2);
		   $data['satuan']   = $this->db->get('specification');
		   $this->db->where('category', 3);
		   $data['remarks']   = $this->db->get('specification');
		   $data['status']      = array('status' => '4' );
		   $this->load->view('coal/index', $data);
    }

  public function edit_specificationAction() {
    	 $this->load->helper(array('form', 'url'));
		 $this->load->library('form_validation');

		$id = $this->input->post('id');
		$category =  $this->input->post('category');
		$category_lm = $this->input->post('category_lm');
		$specification_lm = $this->input->post('specification_lm');
		$specification = $this->input->post('specification');
		if (($specification == $specification_lm) && ($category == $category_lm)) {
			$batas = 1;
		} else {
			$batas = 0;
		}
		$data = array(
			'id' => $id,
			'category' => $category,
			'nm_spec'=> $specification
		);
		$where = array('category' => $category,
			'nm_spec'=> $specification );
		$this->load->model('coal/m_setting');
		$cek = $this->m_setting->cek_terms("specification",$where);
		if($cek->num_rows() == $batas){
		   $this->db->where('id', $id);
		   $this->db->update('specification',$data);
		   $aksi = array('aksi' => 'update', 'stat' => '4');
		   $this->load->view('coal/sukses', $aksi);
		} else {
		   $aksi = array('aksi' => 'gagal', 'stat' => '4');
		   $this->load->view('coal/alert', $aksi);
		}
    }
}
