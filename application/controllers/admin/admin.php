<?php

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();

			if($this->session->userdata('status') != "login"){
				redirect(base_url("admin/login_admin"));
			}
	}

	function index(){
		//	print_r($this->session->userdata('username'));
		 $this->load->view('admin/Home_admin');
	}


	function Logout(){
		$this->session->sess_destroy();
		redirect(base_url('adminMaster/Login_adminM'));
	}

}
