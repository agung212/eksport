<?php

class Login_admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('admin/M_admin'); //memanggil model admin
	}

	function index(){
		$session = $this->session->userdata('status');//ambil data dari session status
		$akun = $this->session->userdata('nama');
		if ($session != "login"){//memerikasa jika kondisi tidak login maka data seasson dari empty diambil dan sesson empty dihapus
			$sub_data['empty']     = $this->session->userdata('empty');
			$this->session->unset_userdata('empty');
			//mengirim data empty dan menampilkan pesan error
			$this->load->view('login_admin/v_admin',$sub_data);
		}
		//digunakan jika pengguna mengakses halaman login tetapi sudah login sebelumnya
		if ($akun == "admin" && $session == "login") {
			//admin akan dialihkan pada menu utamanya
			redirect(base_url("adminMaster/adminM"));
		}elseif($akun != "admin" && $session == "login"){
			//users akan dialihkan pada menu utamanya
			redirect(base_url("admin/admin"));
		}
	}

	function aksi_login(){
		//menarima data dari form dan menyimpannya dalam variable
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array('username' => $username,'password' => md5($password));
		//memeriksa kondisi jika login sebagai admin dan user

		switch ($username) {
    case "admin":
				//memeriksa apakah data username dan password admin sudah benar atau tidak
        $cek = $this->M_admin->cek_login("admin_master",$where);
				$name = $cek->row()->nama;
				//hail dari cek adalah angka, jika $cek lebih dari 0 maka password dan username sesuai
				if ($cek->num_rows()>0) {
					//membuat data masukan untuk session dan me-redirect kehalaman utama admin
					$sistem['ORE'] = "F";
					$sistem['COAL'] = "F";
					$sistem['LIMESTONE'] = "F";
					$sistem['EKSPOR'] = "F";
					$sistem['SOLAR'] = "F";
					$data_session = array('name' => $name,'username' => $username,'status' => "login",'sistem' => $sistem);
					$this->session->set_userdata($data_session);
					// print_r($data_session);
					redirect(base_url("adminMaster/adminM"));
				}else {
					//jika data disesui atau tidak ditemukan maka akan di kembalikan pada halaman admin dan mengirimkan pesan error
					$empty="
		          <div class='alert alert-danger'>
		            <h4><i class='fa fa-warning'></i> Peringatan!</h4> Username atau password kosong. <br/> Harap isi username atau password.
		          </div>";
		      $this->session->set_userdata('empty',$empty);
		      redirect('admin/login_admin');
				}
        break;
    default:
					//memeriksa username dan password user dengan mengembalikan nilai lebih dari 1 jika sesuai
		      $cek = $this->M_admin->cek_login("admin",$where)->num_rows();
					if($cek > 0){
						//mengambil id admin yang sesuai dengan username yang terlogin
						$id_admin= $this->db->get_where('admin',['username'=>$username])->row();
						$name = $id_admin->nama;
						//mengambil data sistem yang dapat diakses oleh user berdasarkan id_admin, hasil pengembalian dalam bentuk array
						$this->db->select('*');
						$this->db->from('admin');
						$this->db->join('r_admin_sistem', 'r_admin_sistem.id_admin = admin.id_admin');
						$this->db->where('r_admin_sistem.id_admin',$id_admin->id_admin);
						$query = $this->db->get()->result();
						//pengambilan data sistem yang sudah diambil dan disimpan pada variable khusus
						foreach ($query as $key => $value) {
							if ($value->id_sistem == 1) {
								$sistem['ORE'] = $value->hak_akses;
							}elseif ($value->id_sistem == 2) {
								$sistem['COAL'] = $value->hak_akses;
							}elseif ($value->id_sistem == 3) {
								$sistem['LIMESTONE'] = $value->hak_akses;
							}elseif ($value->id_sistem == 4) {
								$sistem['EKSPOR'] = $value->hak_akses;
							}elseif ($value->id_sistem == 5) {
								$sistem['SOLAR'] = $value->hak_akses;
							}
						}
							//membuat session dengan data yang sudah disiapkan dan me-redirect ke halaman user
							$data_session = array('name' => $name,'username' => $username,'status' => "login",'sistem' => $sistem ,'level' => $id_admin->level);
							$this->session->set_userdata($data_session);
							redirect(base_url("admin/admin"));
						}else{
							//jika usernam dan password tidak sesuai
							$empty="
				          <div class='alert alert-danger'>
				            <h4><i class='fa fa-warning'></i> Peringatan!</h4> Username atau password salah/tidak terdaftar.
				          </div>";
				      $this->session->set_userdata('empty',$empty);
				      redirect('admin/login_admin');
						}
        break;
			}
	}

	function logout(){
		//mengahpus semua session
		$this->session->sess_destroy();
		redirect(base_url('admin/login_admin'));
	}

}
