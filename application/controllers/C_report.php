<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_report extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
  		parent::__construct();
      $sistem = $this->session->userdata('sistem');
      if(empty($sistem['ORE'])){
        if ($this->session->userdata('status') == "login") {
          redirect(base_url("admin/admin"));
        }else {
          redirect(base_url("admin/login_admin"));
        }

      }
	}
	
	public function index()
	{
		$this->db->select('*');
		$this->db->from('coal_contract');
		$this->db->join('supplier_coal', 'supplier_coal.id_sup = coal_contract.id_sup');
		$this->db->join('buyer_agent', 'buyer_agent.id_buyer = coal_contract.id_buyer');
		$this->db->join('terms', 'terms.id_terms = coal_contract.id_terms');
		$data['coal_contract'] = $this->db->get();
		$data['content'] = 'coal/report';
		$this->load->view('coal/index',$data);
	}


	public function detail_report($id)
	{
			$this->db->select('*');
			$this->db->from('coal_contract');
			$this->db->join('supplier_coal', 'supplier_coal.id_sup = coal_contract.id_sup');
			$this->db->join('buyer_agent', 'buyer_agent.id_buyer = coal_contract.id_buyer');
			$this->db->join('terms', 'terms.id_terms = coal_contract.id_terms');
			$this->db->where('coal_contract.id_contract', $id);
			$data['coal_contract'] = $this->db->get();
			$this->db->select('*');
			$this->db->from('coal_discharge');
			$this->db->join('coal_schedule', 'coal_schedule.id_coalSch = coal_discharge.id_coalSch');
			$this->db->join('coal_contract', 'coal_contract.id_contract = coal_schedule.id_contract');
			$this->db->where('coal_schedule.id_contract', $id);
			$data['discharging'] = $this->db->get();
			$this->db->where('id_contract', $id);
		    $data['data']    = $this->db->get('coal_schedule');
		    $where = array('id_coalSch' => $id );
			$data['content'] = 'coal/detail_report';
			$this->load->view('coal/index',$data);
	}
}
