<!DOCTYPE html>
<html>

<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>

<body class="theme-red">
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <div class="card">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-top:7%;margin-left:22%;">
        <div class="info-box-2 bg-blue">
        <div class="icon">
            <i class="material-icons">info</i>
        </div>
        <div class="content">
            <div class="text">Stockpile Today</div>
            <div class="number count-to"  id="awe" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo number_format($total->total,2); ?> Ton</div>
        </div>
        </div>
</div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-top:7%;margin-left:0%;">
    <div class="info-box-2 bg-orange">
    <div class="icon">
        <i class="material-icons">info</i>
    </div>
    <div class="content">
        <div class="text">Total NPI Production</div>
        <div class="number count-to" id="awe3" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo number_format($total_npi->netto,2); ?> Ton</div>
    </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-top:7%;margin-left:0%;">
    <div class="info-box-2 bg-light-green">
    <div class="icon">
        <i class="material-icons">info</i>
    </div>
    <div class="content">
        <div class="text">Stockpile Filter</div>
        <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo number_format($filter,2); ?> Ton</div>
    </div>
    </div>
  </div>
  </div>

  <section class="content">
      <div class="container-fluid">
          <?= $this->session->flashdata("sukses"); ?>
          <!-- Basic Table -->

          <div class="row clearfix">
                <div class="card" style="height:150px;margin-top:5%;">
                  <div class="header">
                      <h2>
                          FILTER TABLE
                      </h2>

                  </div>
                  <form class="" action="<?php echo base_url(); ?>Eksplore/ControllerSite/Stockpile" method="post">

                  <div class="col-lg-3 col-md-6 col-xs-6" style="margin-top:2%;margin-left:0%;">
                                      <b>Filter by Date</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                Start
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" value="<?php echo $tgl_awal; ?>" name="tgl_awal" placeholder="Ex: 30/07/2016">
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6 col-xs-6" style="margin-top:2%;margin-left:1%;">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                Finish
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" name="tgl_akhir" value="<?php echo $tgl_akhir; ?>" placeholder="Ex: 30/07/2016">
                                            </div>

                                        </div>
                                        </div>

                                        <div class="col-lg-1 col-md-6 col-xs-6" style="margin-top:2%;margin-left:-1%;">
                                        <br>
                                        <div class="input-group">
                                          <div class="col-md-2">
                                            <button type="submit" name="filter" class="btn btn-primary waves-effect">Filter</button>
                                          </div>
                                          <div class="col-md-2" style="margin-left:30%;">
                                            <a href="<?php echo base_url(); ?>Eksplore/ControllerSite/Stockpile"><button type="btn" name="reset" class="btn btn-primary waves-effect">Reset</button></a>

                                          </div>
                                        </div>
                                        </div>
                      </form>

                </div>
                  <div class="card">
                      <div class="header">
                          <h2>
                              TABLE STOCKPILE
                          </h2>
                          <ul class="header-dropdown m-r--5">
                            <a href="<?php echo base_url(); ?>Eksplore/ControllerSite/View_preview"><button type="button" class="btn btn-primary waves-effect">IMPORT EXCEL</button></a>

                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                  <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Action</th>

                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Action</th>

                                      </tr>
                                  </tfoot>
                                  <tbody>
                                    <?php foreach ($site as $key => $data): ?>
                                      <tr>
                                      <td><a href="<?php echo base_url("Eksplore/ControllerSite/DetailStockpile/$data->date");?>"><?php echo $data->date; ?></td>
                                      <td><?php echo $data->total; ?></td>
                                      <td> <a href="<?php echo base_url("Eksplore/ControllerSite/DeleteStockpile/$data->date");?>"><i class="material-icons">delete</i></a></td>

                                    </tr>
                                    <?php endforeach; ?>


                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Basic Table -->

          <!-- Konfirmasi Hapus Data -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to delete this data?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                      <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /konfirmasi -->

          <!-- #END# With Material Design Colors -->
      </div>
  </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>

<script type="text/javascript">
  var auto_refresh = setInterval(
  function ()
  {
  $('#awe').load(<?php base_url(); ?>'getStokpile');
  $('#awe3').load(<?php base_url(); ?>'getNPI');
}, 10000); // refresh every 10000 milliseconds



</script>
