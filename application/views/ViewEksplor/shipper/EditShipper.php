<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>
<body class="theme-red">

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM SHIPPER
                            </h2>
                        </div>
                        <div class="body">


                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Add Name Of Shipper
                                        </b>

                                    </div>

                                    <form action="<?php echo base_url(); ?>Eksplore/ControllerShipper/updateShipper"  method="post">
                                    <div class="body">

                                            <label>Name of Shipper</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                  <input type="hidden" name="id_shipper" value="<?php echo $shipper->id_shipper; ?>">
                                                    <input type="text"  name="nameShipper" value="<?php echo $shipper->name_shipper; ?>" class="form-control" placeholder="Insert Name of Shipper">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>

                                            &nbsp; &nbsp;

                                            <button class="btn btn-danger m-t-15" type="reset">
                                                Cancel
                                            </button>

                                    </div>
                                    </div>
                                    </form>



                            <!-- isi -->


                            </div>




                        </div>
                    </div>
                    </div>

                    <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
<?php
//--> include data footer
$this->load->view('ViewEksplor/layoutEksplor/foot');

?>

<!-- inline scripts related to this page -->
<script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'ControllerShipper/DeleteShipper/' +id;
        });
        //.notifikasi hapus

      });
    </script>
</html>
