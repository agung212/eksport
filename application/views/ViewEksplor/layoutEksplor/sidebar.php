<body class="theme-red">
    <!-- Page Loader -->
<?php $sistem = $this->session->userdata('sistem'); ?>
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../../index.html">Ekspor NPI VDNI</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <!-- <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-envelope" style="font-size:18px;"></span></a>
                     <ul class="dropdown-menu notif"></ul>
                    </li> -->
                    <!-- #END# Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count label label-pill label-danger count"></span>
                        </a>
                        <ul class="dropdown-menu notif">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 254px;"><ul class="menu" style="overflow: hidden; width: auto; height: 254px;">
                                  <ul class="dropdown-menu notif"></ul>


                                </ul><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.5); width: 4px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 0px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);" class=" waves-effect waves-block">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>

                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="">
                                    <li>

                                            <h4>
                                                Stockpile Today
                                                <small></small>

                                            </h4>

                                                <div class="b" id="ada"></div>


                                    </li>
                                    <li>

                                            <h4>
                                                Total NPI Production

                                            </h4>
                                            <div class="b" id="ada2"></div>

                                    </li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                            <i class="material-icons">view_headline</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">SISTEM INFORMASI</li>
                            <li class="body">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="menu" style="overflow: hidden; width: auto; height: 254px;">
                                    <li <?php echo (empty($sistem['ORE']))?'hidden':'' ?>>
                                        <a href="<?php echo base_url(); ?>ORE/C_ORE" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">business</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>ORE</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i><?php echo ($sistem['ORE'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['COAL']))?'hidden':'' ?>>
                                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>COAL</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i><?php echo ($sistem['COAL'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['LIMESTONE']))?'hidden':'' ?>>
                                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>LIMESTONE</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['LIMESTONE'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['EKSPOR']))?'hidden':'' ?>>
                                        <a href="<?php echo base_url(); ?>/Eksplore/ControllerSite" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>EKSPOR</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['EKSPOR'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['SOLAR']))?'hidden':'' ?>>
                                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>SOLAR</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['SOLAR'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>

                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url(); ?>assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
                    <div class="email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENU UTAMA</li>



                      <li>
                        <a href="<?= site_url('Eksplore/ControllerSite'); ?>" title="Tracking Tools">
                          <i class="material-icons">note_add</i>
                            <span>Input Daily Stock</span>
                        </a>
                      </li>
                      <li>
                          <a href="<?= site_url('Eksplore/ControllerSite/Stockpile'); ?>" title="Home">
                            <i class="material-icons">network_cell</i>
                              <span>Stockpile</span>
                          </a>
                      </li>


                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                          <i class="material-icons">tv</i>
                          <span>Menu Ekspor</span>
                        </a>
                        <ul class="ml-menu">
                          <li>
                            <a href="<?= site_url('Eksplore/ControllerContract/FormContract'); ?>" title="Tracking Tools">
                                <span>Tracking Tool</span>
                            </a>
                          </li>
                          <li>
                              <a href="<?= site_url('Eksplore/ControllerContract'); ?>" title="Home">
                                  <span>Tracking Tool Report</span>
                              </a>
                          </li>
                          <li>
                              <a href="<?= site_url('Eksplore/ControllerEksport'); ?>" title="Home">
                                  <span>Loading Ekspor</span>
                              </a>
                          </li>
                      </ul>
                    </li>
                    <!-- <li>
                        <a href="<?= site_url('Eksplore/ControllerEksport'); ?>" title="Home">
                            <i class="material-icons">directions_boat</i>
                            <span>Eksport</span>
                        </a>
                    </li> -->
                  <li>
                      <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings</i>
                        <span>Setting</span>
                      </a>
                      <ul class="ml-menu">
                        <li>
                          <a href="<?= site_url('Eksplore/ControllerShipper'); ?>">Master Shipper</a>
                        </li>
                    </ul>
                  </li>

                  <li>
                      <a href="<?php echo base_url(); ?>admin/login_admin/Logout">
                          <i class="material-icons">input</i>
                          <span>Logout</span>
                      </a>
                  </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside  class="right-sidebar">

            <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                    <input type="hidden" id="base" name="" value="<?php echo base_url(); ?>">

        </aside>
        <!-- #END# Right Sidebar -->
    </section>
    <script type="text/javascript">
    var base = document.getElementById('base').value;
      var auto_refresh = setInterval(
      function ()
      {
      $('#ada').load(base+'/Eksplore/ControllerSite/getStokpile');
    }, 1000); // refresh every 10000 milliseconds

    var auto_refresh = setInterval(
    function ()
    {
    $('#ada2').load(base+'Eksplore/ControllerSite/getNPI');
  }, 1000);

  $(document).ready(function(){

   function load_unseen_notification(view = '')
   {
    $.ajax({
     url:base+"/Eksplore/ControllerSite/notif",
     method:"POST",
     data:{view:view},
     dataType:"json",
     success:function(data)
     {
      $('.notif').html(data.notification);
      if(data.unseen_notification > 0)
      {
       $('.count').html(data.unseen_notification);
      }
     }
    });
   }

   load_unseen_notification();


   $(document).on('click', '.dropdown-toggle', function(){
    $('.count').html('');
    load_unseen_notification('yes');
   });

   setInterval(function(){
    load_unseen_notification();;
   }, 5000);

  });

    </script>
