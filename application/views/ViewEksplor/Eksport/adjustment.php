<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">
<?php $uang = $contract->nilai_mata_uang; ?>
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                  <div class="body">
                              <div class="card">
                                  <div class="header bg-blue">
                                      <b>
                                          Detail Contract
                                      </b>

                                  </div>

                              <div class="body">
                                  <div class="row">
                                  <div class="col-sm-12">
                                <form role="form" action="<?php echo base_url(); ?>Eksplore/ControllerEksport/saveAdjustment" method="post" enctype="multipart/form-data">
                                  <input type="hidden" name="id_invoice" value="<?php echo $contract->id_invoice; ?>">
                                  <input type="hidden" name="id_contract" value="<?php echo $contract->id_contract; ?>">
                                  <input type="hidden" name="uang" id="uang" value="<?php echo $uang; ?>">
                                    <div class="col-md-6">
                                      <b>Contract NO</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="contract_no" class="form-control" readonly required value="<?php echo $contract->no_contract; ?>" placeholder="Insert Contract No EX: XDPT20171016A01">
                                        </div>
                                      </div>

                                    </div>

                                    <div class="col-md-6">
                                      <b>Invoice NO</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="invoice_no" class="form-control" readonly required value="<?php echo $contract->invoice_no; ?>"  placeholder="Insert Contract No EX: XDPT20171016A01">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <b>Name Of Shipper</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="name_shiper" readonly class="form-control" required value="<?php echo $contract->name_shipper; ?>"  placeholder="Insert Contract No EX: XDPT20171016A01">
                                        </div>
                                      </div>

                                    </div>

                                    <div class="col-md-6">
                                      <b>PEB No</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="peb_no" class="form-control" readonly value="<?php echo $contract->peb_no; ?>"  placeholder="Insert PEB NO">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <b>NOPEN NPE <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="nopen_npe" value="<?php echo $contract->nopen_npe; ?>"  class="form-control" readonly  placeholder="Insert NOPEN NPE">
                                        </div>
                                      </div>

                                    </div>

                                    <div class="col-md-6">
                                      <b>Decription Of Goods</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="decription_goods" readonly class="form-control" required value="<?php echo $contract->description_goods; ?>" placeholder="Insert Contract No EX: XDPT20171016A01">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <b>Vassel Name</b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="vassel_name" readonly class="form-control" required value="<?php echo $contract->vassel_name; ?>"  placeholder="Insert Contract No EX: XDPT20171016A01">
                                        </div>
                                      </div>

                                    </div>

                                    <div class="col-md-6">
                                      <b>Bill Landing No <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="bill_landing_no" readonly value="<?php echo $contract->bill_landing_no; ?>"  class="form-control" required  placeholder="Insert Bill Landing No">
                                        </div>
                                      </div>
                                    </div>


                              </div>

                              </div>
                          </div>
                      </div><!-- end card -->


                      <input type="hidden" name="id_stok" class="form-control" value="10">
                      <input type="hidden" name="id_sm" class="form-control" value="10">
                       <input type="hidden" name="stat_lab" class="form-control" value="1" readonly="true">
                      <div class="row clearfix">
                          <div class="col-sm-6">
                              <div class="card">
                                  <div class="header bg-red">
                                      <b>
                                          SURVEYOR RESULT
                                      </b>
                                  </div>

                                  <div class="body">


                                          <div class="form-group">
                                              <label>NI Content (PCT)</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly name="ni" class="form-control" value="<?php echo $contract->ni; ?>" placeholder="Enter Fe" readonly>
                                                  </div>
                                          </div>

                                          <div class="form-group">
                                              <label>Quantity (MT)</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly name="quantity" value="<?php echo str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($contract->quantity,3)))); ?>" class="form-control" placeholder="Enter Ni" required="">
                                                  </div>
                                          </div>
                                          <div class="form-group">
                                              <label>Price Per PCT Ni (1%/MT) (USD)</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly name="price_pct" readonly class="form-control" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($contract->price_pct,3)))); ?>" placeholder="Enter SiO2" required="">
                                                  </div>
                                          </div>
                                          <div class="form-group">
                                              <label>Price Per Tonnage (USD)</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($contract->unit_price,3)))); ?>" name="price_tonnage" class="form-control" placeholder="Insert Price Per Tonnage" required="">
                                                  </div>
                                          </div>
                                          <div class="form-group">
                                             <label>Amount</label>
                                                 <div class="form-line">
                                                     <input type="text" readonly id="amount" name="amount" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($contract->amount,3)))); ?>" class="form-control" placeholder="" required="">
                                                 </div>
                                         </div>

                              </div>
                          </div>
                      </div> <!-- end card -->

                          <div class="col-sm-6">
                          <div class="card">
                                  <div class="header bg-red">
                                      <b>
                                        FINAL ADJUSTMENT
                                      </b>

                                  </div>

                              <div class="body">

                                          <div class="form-group">
                                              <label>Ni Content (PCT) <font color="red">*</font></label>
                                                  <div class="form-line">
                                                      <input type="text" name="ni2" id="ni2" class="form-control price" placeholder="Insert Ni Content (PCT)" required="">
                                                  </div>
                                          </div>
                                          <div class="form-group">
                                                      <label>Quantity (MT) <font color="red">*</font></label>
                                                          <div class="form-line">
                                                              <input type="text"   name="quantity2" id="quantity2" class="form-control price2"  placeholder="Enter Quantity" required="">
                                                          </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label>Price Per PCT Ni (1%/MT) (USD)</label>
                                                          <div class="form-line">
                                                              <input type="text" readonly name="price_pctA"  id="price_pct2" class="form-control price" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($contract->price_pct,3)))); ?>" placeholder="Insert Price Per PCT Ni (1%/MT) (USD)" required="">
                                                          </div>
                                                  </div>
                                          <div class="form-group">
                                              <label>Price Per Tonnage (USD)</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly name="price_tonnage2" id="price_tonnage2" class="form-control price2" placeholder="Insert Price Per Tonnage" required="">
                                                  </div>
                                          </div>
                                          <div class="form-group">
                                              <label>Amount</label>
                                                  <div class="form-line">
                                                      <input type="text" readonly name="amount2"  id="amount2" class="form-control overpaymentAuto" placeholder="Insert Amount" required="">
                                                  </div>
                                          </div>



                              </div>
                          </div>

                          </div>
                          </div> <!-- end card -->

                          <div class="card">
                              <div class="header bg-blue">
                                  <b>
                                      Detail Contract
                                  </b>

                              </div>

                          <div class="body">
                              <div class="row">
                              <div class="col-sm-12">



                                <div class="col-md-6">
                                  <b>Overpayment (USD)</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="overpayment" readonly id="overpayment" class="form-control" required  placeholder="Insert Overpayment">
                                    </div>
                                  </div>

                                </div>

                                <div class="col-md-6">
                                  <b>Discharge Port Name</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="discharge_name" readonly class="form-control" required value="<?php echo $contract->discharge_name; ?>" placeholder="Insert Contract No EX: XDPT20171016A01">
                                    </div>
                                  </div>

                                </div>


                                <div class="col-md-6">
                                  <b>Export Country</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="export_country" readonly class="form-control" required value="<?php echo $contract->export_country; ?>" placeholder="Insert Contract No EX: XDPT20171016A01">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-6">
                                  <b>ETD Kendari</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="etd_kendari" readonly class="form-control" required value="<?php echo $contract->etd_kendari; ?>"  placeholder="Insert Contract No EX: XDPT20171016A01">
                                    </div>
                                  </div>

                                </div>

                                <div class="col-md-6">
                                  <b>L/C NO</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="contract_no" required readonly class="form-control" required value="<?php echo $contract->LC_no; ?>" placeholder="Insert Contract No EX: XDPT20171016A01">
                                    </div>
                                  </div>
                                </div>



                                <div class="col-md-6">
                                  <b>Remarks</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="text" name="remarks" class="form-control"   placeholder="Insert Remarks">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-6">
                                  <b>File Certificate of Quality</b>
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard</i>
                                    </span>
                                    <div class="form-line focused">
                                      <input type="file" name="file" class="form-control" required  placeholder="Insert Remarks">

                                    </div>
                                    <label for="">: Max File 8MB</label>
                                  </div>
                                  <input class="btn btn-info full-right" type="submit" name="submit" value="Submit">

                                </div>



                      </div>
                  </div>

                          </form>
                          </div>
                </div>
                </div>
            </div>
    </section>


</body>
<?php
//--> include data footer
$this->load->view('ViewEksplor/layoutEksplor/foot');

?>

<!-- inline scripts related to this page -->
        <script type="text/javascript">
        var uang = document.getElementById('uang').value;

        function myFunction() {
          var x = document.getElementById("amount").value;
          var awe = document.getElementById("amount2").value;
          var aw2 = document.getElementById("quantity2").value;
          console.log(aw2);
          console.log(awe);
          x = x.replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");
          awe = awe.replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");


          var sum = (parseFloat(x) - (parseFloat(awe))).toFixed(2);
          var minus = "$.-";
          if (sum<0) {
            var result = minus.concat(formatRupiah(sum.replace(".",",")));
            result = result.concat(" (UNDERPAYMET)")
          }else {
            var result = formatRupiah(sum.replace(".",","),uang+". ");
          }
          document.getElementById("overpayment").value =  result;
        }

        $('.price').keyup(function () {

            // initialize the sum (total price) to zero
            var sum = 1;

            // we use jQuery each() to loop through all the textbox with 'price' class
            // and compute the sum for each loop
            $('.price').each(function() {
              var value = $(this).val().replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");
                sum = sum * Number(value);
            });
            sum = sum.toFixed(2);
            sum = sum.toString().replace(".",",");
            // set the computed value to 'totalPrice' textbox
              var result = formatRupiah(sum.toString().replace(".",","),uang+". ");

            $('#price_tonnage2').val(result);

        });

        $('.price2').keyup(function () {

            // initialize the sum (total price) to zero
            var sum = 1;

            // we use jQuery each() to loop through all the textbox with 'price' class
            // and compute the sum for each loop
            $('.price2').each(function() {

              var value = $(this).val().replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");

                sum = sum * Number(value);

            });
            sum2 = sum.toFixed(2);
            sum2 = sum2.toString().replace(".",",");
            var result = formatRupiah(sum2.toString().replace(".",","),uang+". ");

            // set the computed value to 'totalPrice' textbox

            $('#amount2').val(result);

            var x = document.getElementById("amount").value;
            var awe = document.getElementById("amount2").value;
            var aw2 = document.getElementById("quantity2").value;

            x = x.replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");
            result = result.replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");


            var sum = (parseFloat(x) - (parseFloat(result))).toFixed(2);
            var minus = uang+".-";
            if (sum<0) {
              var result = minus.concat(formatRupiah(sum.replace(".",",")));
              result = result.concat(" (UNDERPAYMET)")
            }else {
              var result = formatRupiah(sum.replace(".",","),uang+". ");
            }
            document.getElementById("overpayment").value =  result;

        });

        // $('.overpaymentAuto').keyup(function () {
        //
        //     // initialize the sum (total price) to zero
        //     var sum = 1;
        //
        //     // we use jQuery each() to loop through all the textbox with 'price' class
        //     // and compute the sum for each loop
        //     $('.overpaymentAuto').each(function() {
        //       var value = $(this).val().replace("$","").replace(".","").replace(",",".");
        //         sum -= Number(value);
        //     });
        //
        //     // set the computed value to 'totalPrice' textbox
        //     $('#overpayment').val(sum);
        //
        // });



      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'C_seller/hapus_data/' +id;
        });
        //.notifikasi hapus

      });
    </script>
</html>

<script type="text/javascript">
var overpayment = document.getElementById('overpayment');
overpayment.addEventListener('keyup', function(e)
{
  overpayment.value = formatRupiah(this.value,uang+".");
});
var quantity2 = document.getElementById('quantity2');
quantity2.addEventListener('keyup', function(e)
{
  quantity2.value = formatRupiah(this.value);
});
var amount2 = document.getElementById('amount2');
amount2.addEventListener('keyup', function(e)
{
  amount2.value = formatRupiah(this.value);
});
var ni2 = document.getElementById('ni2');
ni2.addEventListener('keyup', function(e)
{
  ni2.value = formatRupiah(this.value);
});
var price_pct2 = document.getElementById('price_pct2');
price_pct2.addEventListener('keyup', function(e)
{
  price_pct2.value = formatRupiah(this.value,uang+".");
});
var price_tonnage2 = document.getElementById('price_tonnage2');
price_tonnage2.addEventListener('keyup', function(e)
{
  price_tonnage2.value = formatRupiah(this.value,uang+".");
});


var finalDraught = document.getElementById('finalDraught');
finalDraught.addEventListener('keyup', function(e)
{
  finalDraught.value = formatRupiah(this.value);
});

var finalDraught2 = document.getElementById('finalDraught2');
finalDraught2.addEventListener('keyup', function(e)
{
  finalDraught2.value = formatRupiah(this.value);
});



/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split	= number_string.split(','),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 188 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>

<!--  -->
