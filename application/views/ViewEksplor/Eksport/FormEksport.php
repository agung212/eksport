<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">
  <?php $uang = $contract->nilai_mata_uang; ?>
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM EKSPORT
                            </h2>
                        </div>
                        <div class="body">

                            <!-- isi -->
                            <div class="row">

                              <div class="col-sm-6">

                                  <dl id="dt-list-1" class="dl-horizontal">
                                  <table width="100%" border="1">
                                      <tr>
                                          <td class="b"> Contract NO  </td>
                                          <td colspan='3'><?php echo (!empty($contract))?$contract->no_contract:""; ?></td>
                                      </tr>
                                      <tr>

                                          <tr>
                                              <td class="b"> Name Of Shipper  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->name_shipper:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Decription Goods   </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->description_goods:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Price PCT  </td>
                                              <td colspan='3'><?php echo (!empty($contract))? $uang.".".str_replace(".",",",$contract->price_pct):""; ?></td>
                                          </tr>



                                          <tr>
                                              <td class="b">Discharge Name  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->discharge_name:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b"> Quantity Contract</td>
                                              <td colspan='3'><?php echo (!empty($contract))?str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($contract->quantityC,3)))):""; ?></td>
                                          </tr>
                                          <tr>
                                              <td class="b"> Eksport Country</td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->export_country:""; ?></td>
                                          </tr>
                                          <tr>
                                              <td class="b"> L/C No</td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->LC_no:""; ?></td>
                                          </tr>
                                      </tr>

                                  </table>
                              </dl>

                            </div>

                            <div class="col-sm-6">

                                <dl id="dt-list-1" class="dl-horizontal">
                                <table width="100%" border="1">
                                    <tr>
                                        <tr>
                                            <td class="b"> Invoice NO  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->invoice_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> Shipping of Terms  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->shiping_terms:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b" rowspan="3"> Payment Terms  </td>
                                            <tr>
                                              <td class="b" width="10%">TT</td>
                                              <td><?php echo (!empty($contract))?$contract->tt:""; ?>%</td>
                                              <td><?php echo (!empty($contract))?$uang.". ".number_format($contract->nominalTT,3):""; ?></td>
                                            </tr>
                                            <tr>
                                              <td class="b">LC</td>
                                              <td><?php echo (!empty($contract))?$contract->lc:""; ?>%</td>
                                              <td><?php echo (!empty($contract))?$uang.". ".number_format($contract->nominalTT,3):""; ?></td>
                                            </tr>
                                        </tr>
                                        <tr>
                                            <td class="b"> Shipping Method  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->shipping_method:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b">Vassel Name </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->vassel_name:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> Quantity Invoice</td>
                                            <td colspan='3'><?php echo (!empty($contract))?str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($contract->quantity,3)))):""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> ETD Kendari</td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->etd_kendari:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> ETA Discharge Port </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->eta_discharge_port:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> PEB NO  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->peb_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> NOPEN NPE   </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->nopen_npe:""; ?></td>
                                        </tr>

                                        <tr>
                                            <td class="b">Price Tonnage </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->price_tonnage:""; ?></td>
                                        </tr>

                                        <tr>
                                            <td class="b">Bill Landing No </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->bill_landing_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> Remarks </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->remarks:""; ?></td>
                                        </tr>

                                    </tr>

                                </table>
                                <div class="col-md-2" style="margin-top:5%;margin-left:-3%;">
                                    <a href="<?php echo base_url(); ?>Eksplore/ControllerEksport/changeStatus/<?php echo $contract->id_contract."/".$contract->id_invoice; ?>"
                                       class="delete-row red btn btn-danger <?php echo ((empty($contract->LC_no))||($contract->etd_kendari=="0000-00-00")||($contract->eta_discharge_port=="0000-00-00"))?" disabled":""; ?> ">
                                      <i class="material-icons">remove_red_eye</i>
                                      <span>FINAL ADJUSTMENT</span>

                                    </a>
                                    </div>
                            </dl>

                          </div>
                      </div>
                  </div>
              </div>

                                <input type="hidden" id="id_contract" name="id_contract" value="<?php echo (!empty($contract))?$contract->id_contract:""; ?>">
                                <input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo (!empty($contract))?$contract->id_invoice:""; ?>">

                                <input type="hidden" name="uang" id="uang" value="<?php echo $uang; ?>">

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <?= $this->session->flashdata("sukses"); ?>
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        FORM EKSPORT
                                    </h2>
                                </div>
                                <div class="body">


                                  <div >
                                    <button type="btn" class="btn bg-light-blue waves-effect" data-toggle="modal" data-target="#ModalaAdd">Add Loading</button>
                                  </div>
                                  <br>
                                    <div class="table-responsive">
                                      <div id="reload">
                                        <table id="mydata" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                            <thead>
                                                <tr>
                                                  <th>No</th>
                                                  <th>Barge</th>
                                                  <th>Quantity</th>
                                                  <th>Trip</th>
                                                  <th>Start Loading</th>
                                                  <th>Complate Loading</th>
                                                  <!-- <th>Final Draught</th> -->
                                                  <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody id="show_data">


                                            </tbody>
                                        </table>
                                      </div>
                                      </div>

                                </div>
                              </div>

                              </div>
                    </div>
                    </div>

                    <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
                        <!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Vassel</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <b>Barge</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="text" name="barge" id="barge"  class="form-control"  placeholder="Insert Barge">
                      </div>
                    </div>

                    <b>Quantity</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="text" name="quantity" id="quantity"  class="form-control"  placeholder="Insert Quantity">
                      </div>
                    </div>

                    <b>Trip</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="text" name="trip" id="trip" class="form-control"  placeholder="Insert Trip">
                      </div>
                    </div>

                    <b>Start Loading</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="date" name="startLoading" id="startLoading" class="form-control"  placeholder="Insert Start Loading">
                      </div>
                    </div>

                    <b>Complate Loading</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="date" name="complateLoading" id="complateLoading"  class="form-control"  placeholder="Insert Complate Loading">
                      </div>
                    </div>

                    <!-- <b>Final Draught</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">keyboard</i>
                      </span>
                      <div class="form-line focused">
                        <input type="text" name="finalDraught" id="finalDraught"  class="form-control"  placeholder="Insert Final Draught">
                      </div>
                    </div> -->

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_simpan">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL ADD

        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalaEdit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Edit Loading</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                  <b>Barge</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="text" name="barge2" id="barge2"  class="form-control"  placeholder="Insert Barge">
                    </div>
                  </div>

                  <b>Quantity</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="text" name="quantity2" id="quantity2"  class="form-control"  placeholder="Insert Quantity">
                    </div>
                  </div>

                  <b>Trip</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="text" name="trip2" id="trip2" class="form-control"  placeholder="Insert Trip">
                    </div>
                  </div>

                  <b>Start Loading</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="date" name="startLoading2" id="startLoading2" class="form-control"  placeholder="Insert Start Loading">
                    </div>
                  </div>

                  <b>Complate Loading</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="date" name="complateLoading2" id="complateLoading2"  class="form-control"  placeholder="Insert Complate Loading">
                    </div>
                  </div>

                  <!-- <b>Final Draught</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="text" name="finalDraught2" id="finalDraught2"  class="form-control"  placeholder="Insert Final Draught">
                    </div>
                  </div> -->

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_update">Update</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL EDIT-->

        <!--MODAL HAPUS-->
        <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete Loading</h4>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">

                            <input type="hidden" name="kode" id="textkode" value="">
                            <div class="alert alert-warning"><p>Are you sure you want to delete this data?</p></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!--END MODAL HAPUS -->

        <!--MODAL HAPUS-->
        <div class="modal fade" id="ModalStok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h4 class="modal-title" id="myModalLabel">Peringatan</h4>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">

                            <input type="hidden" name="kode" id="textkode" value="">
                            <div class="alert alert-warning"><p>Stok Tidak Cukup</p></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!--END MODAL HAPUS -->
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
<?php
//--> include data footer
$this->load->view('ViewEksplor/layoutEksplor/foot');

?>

<!-- inline scripts related to this page -->
        <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'C_seller/hapus_data/' +id;
        });
        //.notifikasi hapus

      });
    </script>
</html>

<script type="text/javascript">


var quantity = document.getElementById('quantity');
quantity.addEventListener('keyup', function(e)
{
  quantity.value = formatRupiah(this.value);
});


/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split	= number_string.split(','),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 188 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>

<!--  -->
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data_barang();	//pemanggilan fungsi tampil barang.

		$('#mydata').dataTable();

		//fungsi tampil barang
		function tampil_data_barang(){
        var id =$('#id_invoice').val();
        // console.log(id);
		    $.ajax({
		        type  : 'post',
		        url   : '<?php echo base_url()?>Eksplore/ControllerEksport/dataTransaksi',

		        dataType : 'json',
            data : {id:id},
		        success : function(data){
              // console.log(data);
		            var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<tr>'+
                          '<td>'+(i+1)+'</td>'+
		                  		'<td>'+data[i].barge+'</td>'+
                          '<td>'+data[i].quantity+'</td>'+
                          '<td>'+data[i].trip+'</td>'+
                          '<td>'+data[i].startLoading+'</td>'+
                          '<td>'+data[i].complateLoading+'</td>'+
                          // '<td>'+data[i].finalDraught+'</td>'+
		                        '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="green btn btn-warning btn-xs item_edit" data="'+data[i].id_transaksi+'"><i class="material-icons">edit</i></a>'+' '+
                                    '<a href="javascript:;" class="delete-row red btn btn-danger btn-xs item_hapus" data="'+data[i].id_transaksi+'"><i class="material-icons">delete</i></a>'+
                                '</td>'+
		                        '</tr>';
		            }

		            $('#show_data').html(html);
		        }

		    });
		}

		//GET UPDATE
		$('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "post",
                url  : "<?php echo base_url()?>Eksplore/ControllerEksport/get_loading",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(id_transaksi,barge, quantity, trip, startLoading, complateLoading){
                    	$('#ModalaEdit').modal('show');
                      $('[name="barge2"]').val(data.barge);
                      $('[name="quantity2"]').val(data.quantity);
                      $('[name="trip2"]').val(data.trip);
                      $('[name="startLoading2"]').val(data.startLoading);
                      $('[name="complateLoading2"]').val(data.complateLoading);
                      // $('[name="finalDraught2"]').val(data.finalDraught);
                      $('[name="kode"]').val(data.id_transaksi);
            		});
                }
            });
            return false;
        });


		//GET HAPUS
		$('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });

		//Simpan Barang
		$('#btn_simpan').on('click',function(){

            var barge=$('#barge').val();
            var quantity=$('#quantity').val();
            var trip=$('#trip').val();
            var startLoading=$('#startLoading').val();
            var complateLoading=$('#complateLoading').val();
            // var finalDraught=$('#finalDraught').val();
            var id=$('#id_contract').val();
            var id_invoice=$('#id_invoice').val();

            $.ajax({
                type : "post",
                url  : '<?php echo base_url()?>Eksplore/ControllerEksport/saveLoading',
                dataType : "json",
                data : {'barge':barge ,'quantity':quantity, 'trip':trip, 'startLoading':startLoading,
                  'complateLoading':complateLoading,  'id_invoice':id_invoice, 'id':id},

                success: function(data){
                    if (data == "cukup") {
                      $('#mydata').dataTable();
                      //edit ini

                    }else {
                      $('#ModalStok').modal('show');
                    }
                    $('[name="barge"]').val("");
                    $('[name="quantity"]').val("");
                    $('[name="trip"]').val("");
                    $('[name="startLoading"]').val("");
                    $('[name="complateLoading"]').val("");
                    // $('[name="finalDraught"]').val("");
                    $('#ModalaAdd').modal('hide');
                    tampil_data_barang();

                }
            });
            return false;
        });

        //Update Barang
		$('#btn_update').on('click',function(){
            var barge=$('#barge2').val();
            var quantity=$('#quantity2').val();
            var trip=$('#trip2').val();
            var startLoading=$('#startLoading2').val();
            var complateLoading=$('#complateLoading2').val();
            // var finalDraught=$('#finalDraught2').val();
            var id=$('#textkode').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Eksplore/ControllerEksport/update_loading",
                dataType : "JSON",
                data : {'barge':barge ,'quantity':quantity, 'trip':trip, 'startLoading':startLoading,
                  'complateLoading':complateLoading, 'id':id},
                success: function(data){
                  if (data == "cukup") {
                    $('#mydata').dataTable();
                    //edit ini
                  }else {
                    $('#ModalStok').modal('show');
                  }
                  $('#ModalaEdit').modal('hide');
                  $('[name="barge2"]').val("");
                  $('[name="quantity2"]').val("");
                  $('[name="trip2"]').val("");
                  $('[name="startLoading2"]').val("");
                  $('[name="complateLoading2"]').val("");
                  // $('[name="finalDraught2"]').val("");
                  tampil_data_barang();
                }
            });
            return false;
        });

        //Hapus Barang
        $('#btn_hapus').on('click',function(){
            var kode=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url()?>Eksplore/ControllerEksport/deleteLoading",
            dataType : "JSON",
                    data : {kode: kode},
                    success: function(data){
                            $('#ModalHapus').modal('hide');
                            tampil_data_barang();
                    }
                });
                return false;
            });

	});

</script>
