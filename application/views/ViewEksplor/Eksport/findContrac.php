<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>
<body class="theme-red">

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                FIND INVOICE
                            </h2>
                        </div>
                        <div class="body">


                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Input Nomer Invoice
                                        </b>

                                    </div>

                                    <form action="<?php echo base_url(); ?>Eksplore/ControllerEksport/FindLoading"  method="post">
                                    <div class="body">

                                            <label>Invoice No</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="no_invoice" class="form-control" placeholder="Insert Invoice No Ex: 003B/VDNI-SE-FN/XII/17">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Find</button>

                                            &nbsp; &nbsp;

                                            <button class="btn btn-danger m-t-15" type="reset">
                                                Cancel
                                            </button>

                                    </div>
                                    </div>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
      </div>

    </section>
  </div>


</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
