<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
  </head>
  <?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <body class="theme-red ls-closed">

      <section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="margin-left:15%;">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Form Update Daily Stock
                            </h2>

                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                  <?php echo form_open('Eksplore/ControllerSite/Update');?>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label class="">Date</label>
                                          <input type="date" name="date" readonly class="form-control" value="<?php echo $site->date; ?>" required>
                                      </div>
                                      <p><font color="red"><?php echo form_error('name'); ?></font></p>
                                  </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label class="">Start</label>
                                            <input type="text" name="start" class="form-control" value="<?php echo $site->start; ?>" required>
                                        </div>

                                        <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label class="">Finish</label>
                                            <input type="text" name="finish" class="form-control" value="<?php echo $site->finish; ?>" required>
                                        </div>
                                        <p><font color="red"><?php echo form_error('name'); ?></font></p>
                                    </div>
                                      <div class="form-group">
                                          <div class="form-line">
                                              <label class="">smelter</label>
                                              <input type="text" name="smelter" class="form-control" value="<?php echo $site->smelter; ?>" required>
                                          </div>

                                          <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                      </div>
                                      <div class="form-group">
                                          <div class="form-line">
                                              <label class="">Plat</label>
                                              <input type="text" name="plat" class="form-control" value="<?php echo $site->plat; ?>" required>
                                          </div>
                                          <p><font color="red"><?php echo form_error('name'); ?></font></p>
                                      </div>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="">Bruto</label>
                                                <input type="text" name="bruto" class="form-control" value="<?php echo $site->bruto; ?>" required>
                                            </div>

                                            <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="">Tara</label>
                                                <input type="text" name="tara" class="form-control" value="<?php echo $site->tara; ?>" required>
                                            </div>
                                            <p><font color="red"><?php echo form_error('name'); ?></font></p>
                                        </div>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <label class="">netto</label>
                                                  <input type="text" name="netto" class="form-control" value="<?php echo $site->netto; ?>" required>
                                              </div>

                                              <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                          </div>
                                    <input type="hidden" name="id" value="<?php echo $site->id; ?>">


                                    <div class="row">
                                        <div class="col-lg-2 col-md-10 col-sm-8 col-xs-7" style="margin-top:3%;">
                                            <button class="btn btn-block bg-pink waves-effect" type="submit">Change</button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Textarea -->

        </div>
      </section>
  </body>
</html>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
