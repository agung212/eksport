<?php

 header("Content-type: application/vnd-ms-excel");

 header("Content-Disposition: attachment; filename=$title.xls");

 header("Pragma: no-cache");

 header("Expires: 0");

 ?>
<table>
  <tr>
    <th colspan="25"></th>
  </tr>
  <tr>
    <th colspan="25">FINAL ADJUSTEMENT</th>
  </tr>
  <tr>
    <th colspan="25">PT. VIRTUE DRAGON NICKEL INDUSTRY</th>
  </tr>
  <tr>
    <th colspan="25">PERIODE <?php echo strtoupper($tgl_awal)." - ".strtoupper($tgl_akhir); ?> </th>
  </tr>
  <tr>
    <th colspan="25"></th>
  </tr>
</table>


 <table border="1" width="100%">

      <thead>

           <tr align="center">

                <th rowspan="2" bgcolor="#FFFF00">No</th>
                <th rowspan="2" bgcolor="#FFFF00">CONTRACT NO</th>
                <th rowspan="2" bgcolor="#FFFF00">INVOICE NO</th>
                <th rowspan="2" bgcolor="#FFFF00">NAME OF BUYER</th>
                <th rowspan="2" bgcolor="#FFFF00">PEB NO</th>
                <th rowspan="2" bgcolor="#FFFF00">NOPEN NPE</th>
                <th rowspan="2" bgcolor="#FFFF00">DESCRIPTION OF GOODS</th>
                <th rowspan="2" bgcolor="#FFFF00">VESSEL NAME</th>
                <th rowspan="2" bgcolor="#FFFF00">BILL OF LADING NO</th>
                <th rowspan="2" bgcolor="#FFD700">QUANTITY (MT)</th>
                <th rowspan="2" bgcolor="#FFD700">Ni CONTENT (PCT)</th>
                <th rowspan="2" bgcolor="#FFD700">PRICE PER PCT Ni (1%/MT)</th>
                <th rowspan="2" bgcolor="#FFD700">PRICE PER TONNAGE</th>
                <th rowspan="2" bgcolor="#FFD700">AMOUNT</th>
                <th colspan="5" bgcolor="##1E90FF">FINAL ADJUSTMENT</th>
                <th rowspan="2" bgcolor="#FFFF00">DISCHARGE PORT NAME</th>
                <th rowspan="2" bgcolor="#FFFF00">EXPORT COUNTRY</th>
                <th rowspan="2" bgcolor="#FFFF00">ETD KENDARI</th>
                <th rowspan="2" bgcolor="#FFFF00">L/C NO</th>
                <th rowspan="2" bgcolor="#FFFF00">FINAL ADJUSTMENT PAID DATE</th>
                <th rowspan="2" bgcolor="#FFFF00">REMARKS</th>
           </tr>
           <tr align="center">
             <th bgcolor="##1E90FF">QUANTITY (MT)</th>
             <th bgcolor="##1E90FF">Ni CONTENT (PCT)</th>
             <th bgcolor="##1E90FF">PRICE PER TONNAGE (USD)</th>
             <th bgcolor="##1E90FF">AMOUNT (USD)</th>
             <th bgcolor="##1E90FF">OVERPAYMENT <font color="red"> (UNDERPAYMET) </font></th>
           </tr>

      </thead>

      <tbody>
        <?php foreach ($contract as $key => $value): ?>
          <tr>
            <td align="center"><?php echo $key+1; ?></td>
            <td align="center"><?php echo $value->no_contract; ?></td>
            <td align="center"><?php echo $value->invoice_no; ?></td>
            <td align="center"><?php echo $value->name_shipper; ?></td>
            <td align="center"><?php echo $value->peb_no; ?></td>
            <td align="center"><?php echo "'".$value->nopen_npe; ?></td>
            <td align="center"><?php echo $value->description_goods; ?></td>
            <td align="center"><?php echo $value->vassel_name; ?></td>
            <td align="center"><?php echo $value->bill_landing_no; ?></td>
            <td align="center"><?php echo $value->quantity; ?></td>
            <td align="center"><?php echo $value->ni; ?></td>
            <td align="center"><?php echo $value->price_pct; ?></td>
            <td align="right"><?php echo  utf8_decode ( $value->price_tonnage ); ?></td>
            <td align="right"><?php echo $value->amount; ?></td>
            <td align="center"><?php echo $value->quantityA; ?></td>
            <td align="center"><?php echo $value->niA; ?></td>
            <td align="right"><?php echo  utf8_decode ( $value->price_tonnageA ); ?></td>
            <td align="right"><?php echo utf8_decode ( $value->amountA ) ; ?></td>
            <td align="right"><?php echo utf8_decode ( $value->overpayment ) ; ?></td>
            <td align="center"><?php echo $value->discharge_name; ?></td>
            <td align="center"><?php echo $value->export_country; ?></td>
            <td align="center"><?php echo $value->etd_kendari; ?></td>
            <td align="center"><?php echo $value->LC_no; ?></td>
            <td align="center"><?php echo $value->date_paid_adjustment; ?></td>
            <td align="center"><?php echo $value->remarks; ?></td>
          </tr>
        <?php endforeach; ?>

      </tbody>

 </table>
