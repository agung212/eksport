<?php

 header("Content-type: application/vnd-ms-excel");

 header("Content-Disposition: attachment; filename=$title.xls");

 header("Pragma: no-cache");

 header("Expires: 0");

 ?>

 <table>
   <tr>
     <th colspan="9"></th>
   </tr>
   <tr>
     <th colspan="9">KEGIATAN PEMUATAN TONGKANG	</th>
   </tr>
   <tr>
     <th colspan="9">PT. VIRTUE DRAGON NICKEL INDUSTRY</th>
   </tr>
   <tr>
     <th colspan="9">NO INVOICE : <?php echo strtoupper($invoice->invoice_no); ?> </th>
   </tr>
   <tr>
     <th colspan="9"></th>
   </tr>
 </table>

 <table border="1" width="100%">

      <thead>

           <tr align="center">

                <th rowspan="2" bgcolor="#FFFF00">No</th>
                <th rowspan="2" bgcolor="#FFFF00">VESSEL NAME</th>
                <th rowspan="2" bgcolor="#FFFF00">BARGE</th>
                <th rowspan="2" bgcolor="#FFFF00">QUATITY (MT)</th>
                <th rowspan="2" bgcolor="#FFFF00">TRIP</th>
                <th rowspan="2" bgcolor="#FFFF00">STAR LODING</th>
                <th rowspan="2" bgcolor="#FFFF00">COMPLETE LOADING</th>
                <th rowspan="2" bgcolor="#FFFF00">FINAL DRAUGHT (LS)</th>
                <th rowspan="2" bgcolor="#FFFF00">REMARKS</th>

           </tr>

      </thead>

      <tbody>
        <tr>
          <tr align="center">
            <td rowspan="<?php echo $total+1; ?>">1</td>
            <td rowspan="<?php echo $total+1; ?>"><?php echo $invoice->vassel_name; ?></td>

            <?php foreach ($loading as $key => $value): ?>
                <?php if ($key == "0"): ?>
                  <td><?php echo $value->barge; ?></td>
                  <td><?php echo $value->quantity; ?></td>
                  <td><?php echo $value->trip; ?></td>
                  <td><?php echo $value->startLoading; ?></td>
                  <td><?php echo $value->complateLoading; ?></td>
                  <td valign="middle" rowspan="<?php echo $total+1; ?>"><?php echo $invoice->quantity; ?> MT</td>
                  <td><?php echo $invoice->remarks; ?></td>
                </tr>
                <?php else: ?>
                  <td align="center"><?php echo $value->barge; ?></td>
                  <td align="center"><?php echo $value->quantity; ?></td>
                  <td align="center"><?php echo $value->trip; ?></td>
                  <td align="center"><?php echo $value->startLoading; ?></td>
                  <td align="center"><?php echo $value->complateLoading; ?></td>
                  <td align="center"><?php echo $invoice->remarks; ?></td>
                <?php endif; ?>
            <?php endforeach; ?>


          <tr >
            <td align="right" bgcolor="#ADFF2F">total</td>
            <td align="center" bgcolor="#ADFF2F"><?php echo $total_quantity->quantity; ?></td>
            <td align="center" bgcolor="#ADFF2F"><?php echo $total_trip->trip; ?></td>
            <td></td>
            <td></td>
          </tr>
        </tr>




      </tbody>

 </table>
