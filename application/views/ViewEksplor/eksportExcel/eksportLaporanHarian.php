<?php

 header("Content-type: application/vnd-ms-excel");

 header("Content-Disposition: attachment; filename=$title.xls");

 header("Pragma: no-cache");

 header("Expires: 0");

 ?>

 <table>
   <tr>
     <th width="25%" colspan="8"></th>
   </tr>
   <tr>
     <th width="25%" colspan="8">DAILY STOCK	</th>
   </tr>
   <tr>
     <th width="25%" colspan="8">PT. VIRTUE DRAGON NICKEL INDUSTRY</th>
   </tr>
   <tr>
     <th width="25%" colspan="8">SMELTER : <?php echo $smelter; ?> </th>
   </tr>
   <tr>
     <th width="25%" colspan="8">PERIODE <?php echo strtoupper($tgl_awal)." - ".strtoupper($tgl_akhir); ?> </th>
   </tr>
   <tr>
     <th width="25%" colspan="8"></th>
   </tr>
 </table>

 <table border="1" width="100%">

      <thead>

           <tr align="center">

                <th width="100%" bgcolor="#FFFF00">Date</th>
                <th bgcolor="#FFFF00">Start</th>
                <th bgcolor="#FFFF00">Finish</th>
                <th bgcolor="#FFFF00">Smelter</th>
                <th bgcolor="#FFFF00">Plat</th>
                <th bgcolor="#FFFF00">Bruto</th>
                <th bgcolor="#FFFF00">Tara</th>
                <th bgcolor="#FFFF00">Netto</th>

           </tr>

      </thead>

      <tbody>
         <?php foreach ($site as $key => $data): ?>
           <tr align="center">
             <td><?php echo $data->date; ?></td>
             <td><?php echo $data->start; ?></td>
             <td><?php echo $data->finish; ?></td>
             <td><?php echo (strlen($data->smelter) == "2")?$data->wajan."--".$data->smelter:$data->wajan."-".$data->smelter; ?></td>
             <td><?php echo $data->plat; ?></td>
             <td><?php echo $data->bruto; ?></td>
             <td><?php echo $data->tara; ?></td>
             <td><?php echo $data->netto; ?></td>
           </tr>
         <?php endforeach; ?>
            <tr align="center">
                <td bgcolor="#ADFF2F"  colspan="5">TOTAL</td>
                <td bgcolor="#ADFF2F"><?php echo $bruto; ?></td>
                <td bgcolor="#ADFF2F"><?php echo $tara; ?></td>
                <td bgcolor="#ADFF2F"><?php echo $netto; ?></td>
            </tr>
      </tbody>

 </table>
