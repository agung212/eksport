<?php

 header("Content-type: application/vnd-ms-excel");

 header("Content-Disposition: attachment; filename=$title.xls");

 header("Pragma: no-cache");

 header("Expires: 0");

 ?>
<table>
  <tr>
    <th colspan="25"></th>
  </tr>
  <tr>
    <th colspan="25">TRACKING TOOLS EXPORT</th>
  </tr>
  <tr>
    <th colspan="25">PT. VIRTUE DRAGON NICKEL INDUSTRY</th>
  </tr>
  <tr>
    <th colspan="25">PERIODE <?php echo strtoupper($tgl_awal)." - ".strtoupper($tgl_akhir); ?> </th>
  </tr>
  <tr>
    <th colspan="25"></th>
  </tr>
</table>


 <table border="1" width="100%">

      <thead>

           <tr align="center">

                <th rowspan="2" bgcolor="#FFFF00">No</th>
                <th rowspan="2" bgcolor="#FFFF00">CONTRACT NO</th>
                <th rowspan="2" bgcolor="#FFFF00">INVOICE NO</th>
                <th rowspan="2" bgcolor="#FFFF00">NAME OF BUYER</th>
                <th rowspan="2" bgcolor="#FFFF00">PEB NO</th>
                <th rowspan="2" bgcolor="#FFFF00">NOPEN NPE</th>
                <th rowspan="2" bgcolor="#FFFF00">DESCRIPTION OF GOODS</th>
                <th rowspan="2" bgcolor="#FFD700">Ni CONTENT (PCT)</th>
                <th rowspan="2" bgcolor="#FFD700">PRICE PER PCT Ni (1%/MT)</th>
                <th rowspan="2" bgcolor="#FFD700">QUANTITY (MT)</th>
                <th rowspan="2" bgcolor="#FFD700">PRICE PER TONNAGE</th>
                <th rowspan="2" bgcolor="#FFD700">AMOUNT</th>
                <th rowspan="2" bgcolor="#FFFF00">BILL OF LADING NO</th>
                <th rowspan="2" bgcolor="#FFFF00">VESSEL NAME</th>
                <th rowspan="2" bgcolor="#FFFF00">DISCHARGE PORT NAME</th>
                <th rowspan="2" bgcolor="#FFFF00">EXPORT COUNTRY</th>
                <th rowspan="2" bgcolor="#FFFF00">ETD KENDARI</th>
                <th rowspan="2" bgcolor="#FFFF00">ETA DISCHARGE PORT</th>
                <th rowspan="2" bgcolor="#FFFF00">START LOADING</th>
                <th rowspan="2" bgcolor="#FFFF00">COMPLETE LOADING</th>
                <th rowspan="2" bgcolor="#FFFF00">PEB PROCESS</th>
                <th rowspan="2" bgcolor="#FFFF00">L/C NO</th>
                <th rowspan="2" bgcolor="#FFFF00">SUBMIT DOCUMENT TO BANK</th>
                <th rowspan="2" bgcolor="#FFFF00">LEAD TIME LOADING (DAYS)</th>
                <th rowspan="2" bgcolor="#FFFF00">REMARKS</th>
           </tr>

      </thead>

      <tbody>
        <tr>

        </tr>
        <?php foreach ($contract as $key => $value): ?>
          <tr>
            <td align="center"><?php echo $key+1; ?></td>
            <td align="center"><?php echo $value->no_contract; ?></td>
            <td align="center"><?php echo $value->invoice_no; ?></td>
            <td align="center"><?php echo $value->name_shipper; ?></td>
            <td align="center"><?php echo $value->peb_no; ?></td>
            <td align="center"><?php echo "'".$value->nopen_npe; ?></td>
            <td align="center"><?php echo $value->description_goods; ?></td>
            <td align="center"><?php echo $value->ni; ?></td>
            <td align="center"><?php echo $value->price_pct; ?></td>
            <td align="center"><?php echo $value->quantity; ?></td>
            <td align="right"><?php echo  utf8_decode ( $value->price_tonnage ); ?></td>
            <td align="right"><?php echo $value->amount; ?></td>
            <td align="center"><?php echo $value->bill_landing_no; ?></td>
            <td align="center"><?php echo $value->vassel_name; ?></td>
            <td align="center"><?php echo $value->discharge_name; ?></td>
            <td align="center"><?php echo $value->export_country; ?></td>
            <td align="center"><?php echo date("j-F-Y",strtotime($value->etd_kendari)); ?></td>
            <td align="center"><?php echo date("j-F-Y",strtotime($value->eta_discharge_port)); ?></td>
            <td align="center"><?php echo date("j-F-Y",strtotime($value->startLoading)); ?></td>
            <td align="center"><?php echo date("j-F-Y",strtotime($value->completeLoading)); ?></td>
            <td align="center"><?php echo date("j-F-Y",strtotime($value->pebProcess)); ?></td>
            <td align="center"><?php echo $value->LC_no; ?></td>
            <td align="center"> <?php echo date("j-F-Y",strtotime($value->submitBank)); ?></td>
            <td align="center"><?php echo $value->leadLoading; ?></td>
            <td align="center"><?php echo $value->remarks; ?></td>
          </tr>
        <?php endforeach; ?>

      </tbody>

 </table>
