<!DOCTYPE html>
<html>
<style media="screen">
.upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>
<body class="theme-red">
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <section class="content">
      <div class="container-fluid">
          <?= $this->session->flashdata("sukses"); ?>
          <!-- Basic Table -->

          <div class="row clearfix">
                <div class="card" style="height:150px;">
                  <div class="card">
                      <div class="header">
                          <h2>
                              FORM IMPORT EXCEL
                          </h2>
                          <ul class="header-dropdown m-r--5">
                            <a href="<?php echo base_url('excel/format.xlsx'); ?>"><button type="button" class="btn btn-primary waves-effect">DOWNLOAD FORMAT</button></a>
                          </ul>
                      </div>
                      <div class="body">
                          <form method="post" action="<?php echo base_url(); ?>Eksplore/ControllerSite/First" enctype="multipart/form-data">
                              <div class="row clearfix">

                                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                    <div class="upload-btn-wrapper">
                                      <button type="button" <?php echo (!empty($site))?"disabled":""; ?> class="btn btn-primary btn-lg m-l-15 waves-effect">Upload File</button>
                                      <input type="file" name="file">
                                      <button type="submit" name="preview" <?php echo (!empty($site))?"disabled":""; ?> class="btn btn-primary btn-lg m-l-15 waves-effect">Priview</button>
                                      <a href="<?php echo base_url('Eksplore/ControllerSite/resetUpload'); ?>"><button type="button" <?php echo (empty($site))?"disabled":""; ?> class="btn btn-primary btn-lg m-l-15 waves-effect">Reset</button></a>
                                    </div>
                                  </div>
                              </div>
                          </form>
                        </div>
                      </div>
                </div>
                  <div class="card">
                      <div class="header">
                          <h2>
                              PRIVIEW LAPORAN HARIAN
                          </h2>
                          <ul class="header-dropdown m-r--5">
                            <a href="<?php echo base_url(); ?>Eksplore/ControllerSite/Save_Report/full"><button <?php echo ($allSame)?"disabled":""; ?> type="button" class="btn btn-primary waves-effect">SAVE</button></a>
                            <!-- <a href="<?php echo base_url(); ?>Eksplore/ControllerSite/Save_Report/sebagian"><button <?php echo (!$half)?"disabled":""; ?> type="button" class="btn btn-primary waves-effect">partially upload</button></a> -->

                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table id="table" class="table ">
                                  <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Smelter</th>
                                        <th>Plat</th>
                                        <th>Bruto</th>
                                        <th>Tara</th>
                                        <th>Netto</th>
                                        <th>Action</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                        <th>Date</th>
                                    		<th>Start</th>
                                    		<th>Finish</th>
                                    		<th>Smelter</th>
                                    		<th>Plat</th>
                                    		<th>Bruto</th>
                                    		<th>Tara</th>
                                    		<th>Netto</th>
                                        <th>Action</th>
                                      </tr>
                                  </tfoot>
                                  <tbody>

                                    <?php foreach ($site as $key => $data): ?>
                                      <tr bgcolor="<?php echo ($data->status == "sama")?"#FF00000":""; ?>">
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->date; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->start; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->finish; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->smelter; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->plat; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->bruto; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->tara; ?></font></td>
                                        <td><font color="<?php echo ($data->status == "sama")?"white":""; ?>"><?php echo $data->netto; ?></font></td>
                                        <td><a href="<?php echo base_url("Eksplore/ControllerSite/F_USite/$data->id"); ?>"><button type='button' class='btn btn-primary waves-effect'>Update</button></a>
                                        <a href="<?php echo base_url("Eksplore/ControllerSite/DeleteUpload/$data->id"); ?>"><button type='button' class='btn btn-primary waves-effect'>Delete</button></a></td>
                                      </tr>
                                    <?php endforeach; ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Basic Table -->

          <!-- Konfirmasi Hapus Data -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to delete this data?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                      <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /konfirmasi -->

          <!-- #END# With Material Design Colors -->
      </div>
  </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
