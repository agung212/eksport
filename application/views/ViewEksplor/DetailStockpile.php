<!DOCTYPE html>
<html>

<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>

<body class="theme-red">
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <section class="content">
      <div class="container-fluid">
          <?= $this->session->flashdata("sukses"); ?>
          <!-- Basic Table -->

          <div class="row clearfix">
                <div class="card" style="height:150px;">
                  <div class="header">
                      <h2>
                          FILTER TABLE
                      </h2>

                  </div>
                  <form class="" action="<?php echo base_url(); ?>Eksplore/ControllerSite/FilterIndex" method="post">


                  <div class="col-lg-2 col-md-6 col-xs-6" style="margin-top:2%;">
                                      <b>Filter By Name</b>
                                        <div class="input-group">
                                            <div class="form-line">
                                                <select class="" name="smelter">
                                                  <option disabled selected value="">Selet Smelter</option>
                                                  <?php for ($i=1; $i <16 ; $i++) {?>
                                                    <option <?php echo ($smelter == $i)?" selected ":""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>";
                                                  <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                  <div class="col-lg-3 col-md-6 col-xs-6" style="margin-top:2%;margin-left:15%;">
                                      <b>Filter by Date</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                Start
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" value="<?php echo $tgl_awal; ?>" name="tgl_awal" placeholder="Ex: 30/07/2016">
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6 col-xs-6" style="margin-top:2%;margin-left:1%;">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                Finish
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" name="tgl_akhir" value="<?php echo $tgl_akhir; ?>" placeholder="Ex: 30/07/2016">
                                            </div>

                                        </div>
                                        </div>

                                        <div class="col-lg-1 col-md-6 col-xs-6" style="margin-top:2%;margin-left:-1%;">
                                        <br>
                                        <div class="input-group">
                                          <div class="form-line">
                                            <button type="submit" class="btn btn-primary waves-effect">Filter</button>
                                          </div>
                                        </div>
                                        </div>
                      </form>

                </div>
                  <div class="card">
                      <div class="header">
                          <h2>
                              TABLE LAPORAN HARIAN
                          </h2>
                          <ul class="header-dropdown m-r--5">
                            <a href="<?php echo base_url(); ?>Eksplore/ControllerSite/View_preview"><button type="button" class="btn btn-primary waves-effect">IMPORT EXCEL</button></a>

                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                  <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Smelter</th>
                                        <th>Plat</th>
                                        <th>Bruto</th>
                                        <th>Tara</th>
                                        <th>Netto</th>
                                        <th>Action</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                        <th>Date</th>
                                    		<th>Start</th>
                                    		<th>Finish</th>
                                    		<th>Smelter</th>
                                    		<th>Plat</th>
                                    		<th>Bruto</th>
                                    		<th>Tara</th>
                                    		<th>Netto</th>
                                        <th>Action</th>
                                      </tr>
                                  </tfoot>
                                  <tbody>
                                    <?php
                                  	if( ! empty($site)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
                                  		foreach($site as $data){ // Lakukan looping pada variabel siswa dari controller
                                  			echo "<tr>";
                                  			echo "<td>".$data->date."</td>";
                                  			echo "<td>".$data->start."</td>";
                                  			echo "<td>".$data->finish."</td>";
                                  			echo "<td>".$data->wajan."-".$data->smelter."</td>";
                                  			echo "<td>".$data->plat."</td>";
                                  			echo "<td>".$data->bruto."</td>";
                                  			echo "<td>".$data->tara."</td>";
                                  			echo "<td>".$data->netto."</td>";
                                        echo "<td><a><button type='button' class='btn btn-primary waves-effect'>Update</button></a></td>";
                                  			echo "</tr>";
                                  		}
                                  	}
                                  	?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Basic Table -->

          <!-- Konfirmasi Hapus Data -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to delete this data?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                      <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /konfirmasi -->

          <!-- #END# With Material Design Colors -->
      </div>
  </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
