ViewEksplor/layoutEksplor<!DOCTYPE html>
<html>
<style media="screen">
#shipper{
 width:1000px;
}

#shipper option{
 width:100px;
}
</style>
<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
<body class="theme-red ls-closed">
    <!-- Page Loader -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left:0%;">
                    <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header bg-green">
                            <h2>
                                NEW EKSPLOR CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                          <div class="row clearfix">


                                    <form role="form" action="<?php echo base_url(); ?>Eksplore/ControllerContract/updateInvoice" method="post">
                                    <div class="body">

                                    <input type="hidden" id="id_kontrak" name="id_contract" value="<?php echo $invoice->id_contract; ?>" class="form-control">
                                    <input type="hidden" name="price_pct" class="price3" value="<?php echo str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($pct,2)))); ?>">
                                    <input type="hidden"  name="id_invoice" value="<?php echo $invoice->id_invoice; ?>" class="form-control">
                                    <input type="hidden" name="uang" id="uang" value="<?php echo $uang; ?>">

                                    <div style="margin-top:-5%;" class="input-group header bg-red">
                                      <h2 ><center> PAY ATTENTION </center></h2>
                                      <br>
                                      <font color="White">*: must be filled</font>
                                    </div>

                                    <div class="col-md-6">
                                      <b>No Invoice <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="invoice_no" id="invoice_no"  class="form-control" value="<?php echo $invoice->invoice_no; ?>"  placeholder="Insert No Invoice">
                                        </div>
                                      </div>

                                    </div>
                                    <div class="col-md-6">
                                      <b>Shipping Terms <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="shipping_terms" id="shipping_terms"  class="form-control" value="<?php echo $invoice->shiping_terms; ?>"  placeholder="Insert Shipping shipping_terms">
                                        </div>
                                      </div>

                                    </div>

                                    <b>Payment Terms</b>
                                    <div class="input-group">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-6">
                                          <b>TT <font color="red">*</font></b>
                                          <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                            <div class="form-line focused">
                                              <input type="text" name="tt" id="tt" class="form-control" value="<?php echo $invoice->tt."%"; ?>"  placeholder="Insert TT">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <b>Nominal TT <font color="red">*</font></b>
                                          <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                            <div class="form-line focused">
                                              <input type="text" name="nominalTT" id="nominalTT" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->nominalTT,3)))); ?>" class="form-control"  placeholder="Insert LC">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <b>LC <font color="red">*</font></b>
                                          <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                            <div class="form-line focused">
                                              <input type="text" name="lc" value="<?php echo $invoice->lc."%"; ?>" id="lc" class="form-control"  placeholder="Insert TT">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <b>Nominal LC</b>
                                          <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="material-icons">keyboard</i>
                                            </span>
                                            <div class="form-line focused">
                                              <input type="text" name="nominalLC" style="<?php echo (empty($invoice->nominalLC))?"background-color: #FFFF33;":""; ?>" value="<?php echo (empty($invoice->nominalLC))?"":$uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->nominalLC,3)))); ?>" id="nominalLC" class="form-control"  placeholder="Insert LC">
                                            </div>
                                          </div>
                                        </div>
                                      </div>


                                    </div>


                                    <div class="col-md-6">
                                      <b>Shipping Method <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="shipping_method" id="shipping_method" value="<?php echo $invoice->shipping_method; ?>" class="form-control"  placeholder="Insert Shipping Method">
                                        </div>
                                      </div>

                                    </div>
                                    <div class="col-md-6">
                                      <b>Vassel Name <font color="red">*</font></b>
                                      <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="material-icons">keyboard</i>
                                        </span>
                                        <div class="form-line focused">
                                          <input type="text" name="vassel_name" id="vassel_name" value="<?php echo $invoice->vassel_name; ?>" class="form-control"  placeholder="Insert Vassel Name">
                                        </div>
                                      </div>

                                    </div>

                                      <div class="col-md-6">
                                        <b>PEB No <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="peb_no" style="<?php echo (empty($invoice->peb_no))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->peb_no; ?>" class="form-control"   placeholder="Insert PEB NO">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <b>PEB PROCESS <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date" name="pebProcess" style="<?php echo ($invoice->pebProcess == "0000-00-00")?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->pebProcess; ?>" class="form-control"   placeholder="Insert PEB NO">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <b>NOPEN NPE <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="nopen_npe" style="<?php echo (empty($invoice->nopen_npe))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->nopen_npe; ?>" class="form-control"   placeholder="Insert NOPEN NPE">
                                          </div>
                                        </div>

                                      </div>

                                      <div class="col-md-6">
                                        <b>Bill Landing No <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="bill_landing_no" style="<?php echo (empty($invoice->bill_landing_no))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->bill_landing_no; ?>" class="form-control"   placeholder="Insert Bill Landing No">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <b>ETD Kendari <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date" name="etd_kendari" style="<?php echo ($invoice->etd_kendari == "0000-00-00")?"background-color: #FFFF33;":""; ?>" id="etd_kendari" value="<?php echo $invoice->etd_kendari; ?>"  class="form-control"  placeholder="Insert ETD Kendari">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>ETA Discharge Port <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date" name="eta_discharge_port" style="<?php echo ($invoice->eta_discharge_port == "0000-00-00")?"background-color: #FFFF33;":""; ?>" id="eta_discharge_port" value="<?php echo $invoice->eta_discharge_port; ?>"  class="form-control"  placeholder="Insert ETA Discharge Port">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>Start Loading </b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date" name="startLoading" style="<?php echo ($invoice->startLoading == "0000-00-00")?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->startLoading; ?>"   class="form-control"  placeholder="Insert ETD Kendari">
                                          </div>
                                        </div>

                                      </div>


                                      <div class="col-md-6">
                                        <b>Complete Loading</b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date" name="CompleteLoading" style="<?php echo ($invoice->completeLoading == "0000-00-00")?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->completeLoading; ?>"  class="form-control"  placeholder="Insert ETA Discharge Port">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <b>NI content <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="ni" id="ni" class="form-control price3 price" value="<?php echo $invoice->ni; ?>"  placeholder="Insert NI Content">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>Quantity <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="quantity"  id="quantity" value="<?php echo str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->quantity,3)))); ?>" class="form-control price"  placeholder="Insert quantity">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>Net Weight <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text" name="net_weight" id="net_weight" value="<?php echo str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->net_weight,3)))); ?>" class="form-control"  placeholder="Insert Net Weight">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>Unit Price <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line ">
                                            <input type="text" readonly name="unit_price" id="unit_price" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->price_tonnage,3)))); ?>" class="form-control price"  placeholder="Insert Unit Price">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-md-6">
                                        <b>Amount <font color="red">*</font></b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line">
                                            <input type="text" readonly name="amount" id="amount" value="<?php echo $uang.".".str_replace("a",",",str_replace(",",".",str_replace(".","a",number_format($invoice->amount,3)))); ?>" class="form-control"  placeholder="Insert Amount">
                                          </div>
                                        </div>

                                      </div>

                                      <div class="col-md-6">
                                        <b>Submit Document to Bank</b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="date"  name="submitBank" style="<?php echo (empty($invoice->submitBank))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->submitBank; ?>"  class="form-control"  placeholder="Insert Amount">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <b>Lead Time Loading (DAYS)</b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text"  name="leadLoading" style="<?php echo (empty($invoice->leadLoading))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->leadLoading; ?>"  class="form-control"  placeholder="Insert Amount">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <b>remarks</b>
                                        <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">keyboard</i>
                                          </span>
                                          <div class="form-line focused">
                                            <input type="text"  name="remarks" style="<?php echo (empty($invoice->remarks))?"background-color: #FFFF33;":""; ?>" value="<?php echo $invoice->remarks; ?>"  class="form-control"  placeholder="Insert Amount">
                                          </div>
                                        </div>
                                      </div>


                                            <div class="col-md-12">
                                              <button type="submit" class="btn bg-light-blue waves-effect">Save</button>
                                            </div>

                                    </div>
                                    </form>
                                  </div>

                      </div>
                        </div>

                      </div>
                    </div>
                    </div>
                </div>


    </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
<script type="text/javascript">
var uang = document.getElementById('uang').value;

var unit_price = document.getElementById('unit_price');
unit_price.addEventListener('keyup', function(e)
{
  unit_price.value = formatRupiah(this.value);


});
var quantity = document.getElementById('quantity');
quantity.addEventListener('keyup', function(e)
{
  quantity.value = formatRupiah(this.value);

});

// var quantity2 = document.getElementById('quantity2');
// quantity2.addEventListener('keyup', function(e)
// {
//   quantity2.value = formatRupiah(this.value);
// });


// var unit_price2 = document.getElementById('unit_price2');
// unit_price2.addEventListener('keyup', function(e)
// {
//   unit_price2.value = formatRupiah(this.value);
// });

var nominalTT = document.getElementById('nominalTT');
nominalTT.addEventListener('keyup', function(e)
{
  nominalTT.value = formatRupiah(this.value,uang+". ");
});

var nominalLC = document.getElementById('nominalLC');
nominalLC.addEventListener('keyup', function(e)
{
  nominalLC.value = formatRupiah(this.value,uang+". ");
});

/* Fungsi */
$('.price').on('keyup change',function () {

    // initialize the sum (total price) to zero
    var sum = 1;

    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    $('.price').each(function() {

      var value = $(this).val().replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");

        sum = sum * Number(value);
    });
    var result = formatRupiah(sum.toFixed(2).toString().replace(".",","),uang+". ");
    // set the computed value to 'totalPrice' textbox
    var amount = document.getElementById("quantity").value;
    var net_weight = parseFloat(amount.replace(uang,"").replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".")) * 1000;
    var result_net = formatRupiah(net_weight.toFixed(2).toString().replace(".",","));
    $('#amount').val(result);
    $('#net_weight').val(result_net);

});


$('.price3').on('keyup change',function () {

    // initialize the sum (total price) to zero
    var sum = 1;

    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    $('.price3').each(function() {

      var value = $(this).val().replace(".","").replace(".","").replace(".","").replace(".","").replace(".","").replace(",",".");

        sum = sum * Number(value);
    });

    sum = sum.toFixed(2).toString().replace(".",",");

    var result = formatRupiah(sum.toString().replace(".",","),uang+". ");
    // set the computed value to 'totalPrice' textbox
    $('#unit_price').val(result);

});

function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split	= number_string.split(','),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 188 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>
