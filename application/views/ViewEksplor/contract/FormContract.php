<!DOCTYPE html>
<html>
<style media="screen">
#shipper{
 width:1000px;
}

#shipper option{
 width:100px;
}
</style>
<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
<body class="theme-red ls-closed">
    <!-- Page Loader -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left:0%;">
                    <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                NEW EKSPLOR CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                          <div class="row clearfix">


                                    <form role="form" action="<?php echo base_url(); ?>Eksplore/ControllerContract/SaveContract" method="post">
                                    <div class="body">

                                    <input type="hidden" id="id_kontrak" name="id_kontrak" class="form-control">

                                            <div class="col-md-6">
                                              <b>Contract NO</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="contract_no" class="form-control" required  placeholder="Insert Contract No EX: XDPT20171016A01">
                                                </div>
                                              </div>

                                            </div>

                                            <!-- <div class="col-md-6">
                                              <b>Invoice NO</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="invoice_no" class="form-control" required placeholder="Insert Invoice No Ex: 003B/VDNI-SE-FN/XII/17">
                                                </div>
                                              </div>
                                            </div> -->

                                            <div class="col-md-6" >
                                              <b>Name Of Shipper</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">local_shipping</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <select name="name_shipper" required class="show-tick" data-live-search="true">
                                                    <option selected disabled value="">Select Shipper</option>
                                                      <?php foreach ($shipper as $value): ?>
                                                        <option value="<?php echo $value->id_shipper; ?>"><?php echo $value->name_shipper; ?></option>

                                                      <?php endforeach; ?>
                                                  </select>
                                                  <!-- <input type="text" name="name_shipper" class="form-control" placeholder="Insert Name Of Shipper"> -->
                                                </div>
                                              </div>
                                            </div>



                                            <div class="col-md-6" style="margin-left:0.05%;">
                                              <b>Decription of Goods</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="decription_goods" required class="form-control" placeholder="Insert Decription Of Goods">
                                                </div>
                                              </div>
                                            </div>



                                            <div class="col-md-6" style="margin-left:-0.05%;">
                                              <b>Price Per PCT Ni (1%/MT)</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">attach_money</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" id="Price" required onkeydown="return limitCharacter(event);" name="price_ni" class="form-control" placeholder="Insert Price Per PCT Ni (1%/MT)(USD) " >
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-6">
                                              <b>Quantity</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="quantity" id="quantity" required  onkeydown="return limitCharacter(event);" class="form-control" placeholder="Insert Quantity">
                                                </div>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <b>Discharge Port Name</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                  <div class="form-line focused">
                                                      <input type="text" name="discharge_name" required class="form-control" placeholder="Insert Discharge Port Name">
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="col-md-6">
                                              <b>Export Country</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="export_country" required class="form-control" placeholder="Insert Export Country">
                                                </div>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <b>L/C No</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <input type="text" name="LC_no" required class="form-control" placeholder="Insert L/C No Ex: LC35401B702649">
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-6" >
                                              <b>Currency Value</b>
                                              <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">keyboard</i>
                                                </span>
                                                <div class="form-line focused">
                                                  <select name="uang" required class="show-tick" data-live-search="true">
                                                    <option selected disabled value="">Currency Value</option>
                                                    <option value="$">DOllar ($.)</option>
                                                    <option value="¥">Yen (¥.)</option>
                                                  </select>
                                                  <!-- <input type="text" name="name_shipper" class="form-control" placeholder="Insert Name Of Shipper"> -->
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <button type="submit" class="btn bg-light-blue waves-effect">Save</button>
                                            </div>

                                    </div>
                                    </form>
                                  </div>

                      </div>
                        </div>

                      </div>
                    </div>
                    </div>
                </div>


    </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
<script type="text/javascript">
/* Dengan Rupiah */
var quantity = document.getElementById('quantity');
quantity.addEventListener('keyup', function(e)
{
  quantity.value = formatRupiah(this.value);
});
var price = document.getElementById('Price');
price.addEventListener('keyup', function(e)
{
  price.value = formatRupiah(this.value);
});



/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split	= number_string.split(','),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 188 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>
