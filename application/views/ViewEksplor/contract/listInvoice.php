<!DOCTYPE html>
<html>
<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
    th, td {
    text-align: left;
}

</style>
<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header">
                          <div class="">
                            <h2>
                                FORM EKSPORT
                            </h2>
                          </div>

                            <div class="" style="margin-left:85%;margin-top:-2%;">
                              <a href="<?php echo base_url(); ?>Eksplore/ControllerContract/changeContract/<?php echo $contract->id_contract; ?>"><button type="btn" <?php echo ((empty($invoice))||($contract->status == "Complete")||($status =="beda"))?"disabled":""; ?> class="btn bg-light-blue waves-effect">Complete Contract</button></a>
                            </div>
                        </div>
                        <div class="body">

                            <!-- isi -->
                            <div class="row">

                              <div class="col-sm-6">

                                  <dl id="dt-list-1" class="dl-horizontal">
                                  <table width="100%" border="1">
                                      <tr>
                                          <td class="b"> Contract NO  </td>
                                          <td colspan='3'><?php echo (!empty($contract))?$contract->no_contract:""; ?></td>
                                      </tr>
                                      <tr>

                                          <tr>
                                              <td class="b"> Name Of Shipper  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->name_shipper:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Decription Goods   </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->description_goods:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Price PCT  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->nilai_mata_uang.".".str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format( floatval($contract->price_pct),2)))):""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Quantity </td>
                                              <td colspan='3'><?php echo (!empty($contract))?str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($contract->quantityC,2)))):""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Discharge Name  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->discharge_name:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b"> ETD Kendari  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->export_country:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b"> L/C No </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->LC_no:""; ?></td>
                                          </tr>


                                      </tr>

                                  </table>
                              </dl>

                            </div>

                            

                      </div>
                  </div>
              </div>

                                <input type="hidden" id="id_contract" name="id_contract" value="<?php echo (!empty($contract))?$contract->id_contract:""; ?>">







                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="card">
                                <div class="header">
                                    <h2>
                                        List Invoice
                                    </h2>
                                </div>
                                <div class="body">


                                  <div >
                                    <a href="<?php echo base_url(); ?>Eksplore/ControllerContract/addInvoice/<?php echo $contract->id_contract; ?>"><button type="btn" <?php echo ($contract->status == "Complete")?"disabled":""; ?> class="btn bg-light-blue waves-effect">Add Invoice</button></a>
                                    <!-- <button type="btn" class="btn bg-light-blue waves-effect" data-toggle="modal" data-target="#ModalaAdd">Add Invoice</button> -->
                                  </div>
                                  <br>
                                    <div class="table-responsive">
                                      <div id="reload">
                                        <table id="mydata" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                            <thead>
                                              <tr>
                                                <th rowspan="">No</th>
                                                <th rowspan="">No Invoice</th>

                                                <!-- <th colspan="2">Payment Terms</th> -->
                                                <!-- <th rowspan="2">Shipping Method</th> -->
                                                <th rowspan="">Vassel Name</th>
                                                <!-- <th rowspan="2">ETD Kendari</th>
                                                <th rowspan="2">ETA Discharge Port</th> -->
                                                <!-- <th rowspan="2">L/C No</th> -->
                                                <th rowspan="">Ni Conten</th>
                                                <th rowspan="">Quantity</th>
                                                <!-- <th rowspan="2">Net Weight</th>
                                                <th rowspan="2">Unit Price</th> -->
                                                <th rowspan="">Amount</th>
                                                <th rowspan="">Date Paid Adjustment</th>
                                                <th rowspan="">Status</th>
                                                <th width="18%">Action</th>
                                              </tr>
                                                <!-- <tr>
                                                  <th>TT</th>
                                                  <th>TC</th>
                                                </tr> -->
                                            </thead>

                                            <tbody id="show_data">
                                              <?php foreach ($invoice as $key => $value): ?>
                                                <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td>
                                                  <?php if (($value->status == "Waiting Paid Date") || ($value->status == "Complate")): ?>
                                                      <a  target="_blank" href="<?php echo base_url(); ?>Eksplore/ControllerContract/formPaid/<?php echo $value->id_invoice; ?>"><?php echo $value->invoice_no; ?></td>

                                                  <?php else: ?>
                                                    <a  target="_blank" href="<?php echo base_url(); ?>Eksplore/ControllerEksport/FormContract/<?php echo $value->id_invoice."/".$contract->id_contract; ?>"><?php echo $value->invoice_no; ?></td>

                                                  <?php endif; ?>

                                                <td><?php echo $value->vassel_name; ?></td>
                                                <td><?php echo $value->ni; ?></td>
                                                <td><?php echo str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($value->quantity,3)))); ?></td>
                                                <td><?php echo $contract->nilai_mata_uang.".".str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($value->amount,3)))); ?></td>
                                                <td><?php echo $value->date_paid_adjustment; ?></td>
                                                <td><?php echo $value->status; ?></td>
                                                <td>
                                                  <?php if ($value->status == "Waiting Adjustment"): ?>
                                                    <a href="<?php echo base_url(); ?>Eksplore/ControllerEksport/FinalAdjustment/<?php echo $value->id_contract."/".$value->id_invoice; ?>" class="delete-row red btn btn-danger">
                                                      <i class="material-icons">remove_red_eye</i>
                                                      <span>FINAL ADJUSTMENT</span>

                                                    </a>
                                                  <?php endif; ?>
                                                  <?php if ($value->status == "Waiting Loading"): ?>
                                                    <a href="<?php echo base_url(); ?>Eksplore/ControllerContract/editInvoice/<?php echo $value->id_invoice."/".$contract->id_contract; ?>" style="" class="green btn btn-warning item_edit" data="<?php echo $value->id_invoice; ?>"><i class="material-icons">edit</i></a>
                                                    <a href="javascript:;" class="delete-row red btn btn-danger item_hapus" data="<?php echo $value->id_invoice; ?>"><i class="material-icons">delete</i></a>
                                                  <?php endif; ?>
                                                    <?php if ($value->status == "Waiting Paid Date"): ?>
                                                      <a href="#" id="detail-row" class="detail-row green btn btn-default"  data-toggle="modal" data-target="#myModal" data-id="<?php echo $value->id_invoice; ?>" data-id2="<?php echo $value->id_contract; ?>" ><i class="material-icons">remove_red_eye</i></a>
                                                      <a href="#" id="detail-row" class="detail-row green btn btn-default paid"   data-id="<?php echo $value->id_invoice; ?>" data-id2="<?php echo $value->id_contract; ?>" ><i class="material-icons">add</i></a>
                                                    </td>
                                                    <?php endif; ?>
                                                    <?php if ($value->status == "Complate"): ?>
                                                      <a href="#" id="detail-row" class="detail-row green btn btn-default"  data-toggle="modal" data-target="#myModal" data-id="<?php echo $value->id_invoice; ?>" data-id2="<?php echo $value->id_contract; ?>" ><i class="material-icons">remove_red_eye</i></a>
                                                    <?php endif; ?>
                                                </tr>
                                              <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                      </div>
                                      </div>

                                </div>
                              </div>

                              </div>
                    </div>
                    </div>
                    <input type="hidden" name="price_pct" class=" price3" value="<?php echo $contract->price_pct; ?>">
                    <!-- Konfirmasi Hapus Data -->
                    <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h2 class="modal-title header">FINAL ADJUSTMENT</h2>
                            </div>
                            <div class="modal-body">
                                <div class="fetched-data"></div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Keluar</button>
                            </div>
                        </div>
                    </div>
                </div>
                        <!-- /konfirmasi -->
                        <!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Add Paid Date Adjustment</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                  <b>Paid Date</b>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard</i>
                    </span>
                    <div class="form-line focused">
                      <input type="date" name="paid_date" id="paid_date"  class="form-control"  placeholder="Insert Amount">
                    </div>
                  </div>

              </div>
              <input type="hidden" name="kode1" id="textkode1" value="">
              <input type="hidden" name="kode2" id="textkode2" value="">
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_update">Update</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL EDIT-->

        <!-- MODAL adjustment -->

                </div>
            </div>
            <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button"  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Loading</h4>
                        </div>
                        <form class="form-horizontal">
                        <div class="modal-body">

                                <input type="hidden" name="kode" id="textkode" value="">
                                <div class="alert alert-warning"><p>Are you sure you want to delete this data?</p></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
<?php
//--> include data footer
$this->load->view('ViewEksplor/layoutEksplor/foot');

?>

<!-- inline scripts related to this page -->
        <script type="text/javascript">


      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'C_seller/hapus_data/' +id;
        });
        //.notifikasi hapus

      });

      $(document).on('click', '#detail-row', function (e) {
              e.preventDefault();
              id_invoice = $(this).data('id');
              id_contract = $(this).data('id2');
              //menggunakan fungsi ajax untuk pengambilan data
              $.ajax({
                  type : 'post',
                  url : '<?php echo base_url()?>Eksplore/ControllerContract/detailAdjustment',
                  data :  {id_invoice:id_invoice , id_contract:id_contract},
                  success : function(data){
                  $('.fetched-data').html(data);//menampilkan data ke dalam modal
                  }
              });
           });
    </script>
</html>

<script type="text/javascript">
// var unit_price = document.getElementById('unit_price');
// unit_price.addEventListener('keyup', function(e)
// {
//   unit_price.value = formatRupiah(this.value);
//
//
// });
// var quantity = document.getElementById('quantity');
// quantity.addEventListener('keyup', function(e)
// {
//   quantity.value = formatRupiah(this.value);
//
// });
//
// var quantity2 = document.getElementById('quantity2');
// quantity2.addEventListener('keyup', function(e)
// {
//   quantity2.value = formatRupiah(this.value);
// });
//
//
// var unit_price2 = document.getElementById('unit_price2');
// unit_price2.addEventListener('keyup', function(e)
// {
//   unit_price2.value = formatRupiah(this.value);
// });
//
// var nominalTT = document.getElementById('nominalTT');
// nominalTT.addEventListener('keyup', function(e)
// {
//   nominalTT.value = formatRupiah(this.value,"$. ");
// });
//
// var nominalLC = document.getElementById('nominalLC');
// nominalLC.addEventListener('keyup', function(e)
// {
//   nominalLC.value = formatRupiah(this.value,"$. ");
// });
//
// /* Fungsi */
// $('.price').keyup(function () {
//
//     // initialize the sum (total price) to zero
//     var sum = 1;
//
//     // we use jQuery each() to loop through all the textbox with 'price' class
//     // and compute the sum for each loop
//     $('.price').each(function() {
//       var value = $(this).val().replace(".","").replace(",",".");
//         sum = sum * Number(value);
//     });
//
//     // set the computed value to 'totalPrice' textbox
//     $('#amount').val(sum);
//
// });
//
// $('.price2').keyup(function () {
//
//     // initialize the sum (total price) to zero
//     var sum = 1;
//
//     // we use jQuery each() to loop through all the textbox with 'price' class
//     // and compute the sum for each loop
//     $('.price2').each(function() {
//       var value = $(this).val().replace(".","").replace(",",".");
//         sum = sum * Number(value);
//     });
//
//     // set the computed value to 'totalPrice' textbox
//     $('#amount2').val(sum);
//
// });
//
// $('.price3').keyup(function () {
//
//     // initialize the sum (total price) to zero
//     var sum = 1;
//
//     // we use jQuery each() to loop through all the textbox with 'price' class
//     // and compute the sum for each loop
//     $('.price3').each(function() {
//       var value = $(this).val().replace(".","").replace(",",".");
//         sum = sum * Number(value);
//     });
//     sum = sum.toString().replace(".",",");
//     // set the computed value to 'totalPrice' textbox
//     $('#unit_price').val(sum);
//
// });
//
// function formatRupiah(bilangan, prefix)
// {
//
//   var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
//     split	= number_string.split(','),
//     sisa 	= split[0].length % 3,
//     rupiah 	= split[0].substr(0, sisa),
//     ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
//
//   if (ribuan) {
//     separator = sisa ? '.' : '';
//     rupiah += separator + ribuan.join('.');
//   }
//
//   rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
//   return prefix == undefined ? rupiah : (rupiah ? '$. ' + rupiah : '');
// }
//
// function limitCharacter(event)
// {
//   key = event.which || event.keyCode;
//   if ( key != 188 // Comma
//      && key != 8 // Backspace
//      && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
//      && (key < 48 || key > 57) // Non digit
//      // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
//     )
//   {
//     event.preventDefault();
//     return false;
//   }
// }
// </script>
//
// <!--  -->
// <script type="text/javascript">
	$(document).ready(function(){
//
//
//
// 	//	tampil_data_barang();	//pemanggilan fungsi tampil barang.
//
//     var table = $('#mydata').DataTable();
//
//
// 		//fungsi tampil barang
// 		// function tampil_data_barang(){
//     //     var id =$('#id_contract').val();
//     //     // console.log(id);
// 		//     $.ajax({
// 		//         type  : 'post',
// 		//         url   : '<?php echo base_url()?>Eksplore/ControllerContract/dataInvoice',
//     //
// 		//         dataType : 'json',
//     //         data : {id:id},
// 		//         success : function(data){
//     //           // console.log(data);
// 		//             var html = '';
// 		//             var i;
// 		//             for(i=0; i<data.length; i++){
// 		//                 html += '<tr>'+
//     //                       '<td>'+(i+1)+'</td>'+
// 		//                   		'<td><a href="<?php echo base_url(); ?>Eksplore/ControllerEksport/FormContract/'+data[i].id_invoice+'/'+data[i].id_contract+'">'+data[i].invoice_no+'</td>'+
//     //                       '<td>'+data[i].shiping_terms+'</td>'+
//     //                       // '<td>$.'+data[i].nominalTT+'('+data[i].tt+'%)</td>'+
//     //                       // '<td>$.'+data[i].nominalLC+'('+data[i].lc+'%)</td>'+
//     //                       // '<td>'+data[i].shipping_method+'</td>'+
//     //                       '<td>'+data[i].vassel_name+'</td>'+
//     //                       // '<td>'+data[i].etd_kendari+'</td>'+
//     //                       // '<td>'+data[i].eta_discharge_port+'</td>'+
//     //                       // '<td>'+data[i].lc_no+'</td>'+
//     //                       '<td>'+data[i].ni+'</td>'+
//     //                       '<td>'+data[i].quantity+'</td>'+
//     //                       // '<td>'+data[i].net_weight+'</td>'+
//     //                       // '<td>'+data[i].unit_price+'</td>'+
//     //                       '<td>'+data[i].amount+'</td>'+
// 		//                         '<td style="align:center;">'+
//     //                                 '<a href="javascript:;" class="green btn btn-warning  item_edit" data="'+data[i].id_invoice+'"><i class="material-icons">edit</i></a>'+
//     //                                 '<a href="javascript:;" class="delete-row red btn btn-danger  item_hapus" data="'+data[i].id_invoice+'"><i class="material-icons">delete</i></a>'+
//     //                                 '<a href="#" id="detail-row" class="detail-row green btn btn-default"  data-toggle="modal" data-target="#myModal" data-id="'+data[i].id_invoice+'" data-id2="'+data[i].id_contract+'" ><i class="material-icons">remove_red_eye</i></a>'+
//     //                             '</td>'+
// 		//                         '</tr>';
// 		//             }
//     //
// 		//             $('#show_data').html(html);
// 		//         }
//     //
// 		//     });
// 		// }
//
// 		//GET UPDATE
// 		$('#show_data').on('click','.item_edit',function(){
//             var id=$(this).attr('data');
//             $.ajax({
//                 type : "post",
//                 url  : "<?php echo base_url()?>Eksplore/ControllerContract/get_invoice",
//                 dataType : "JSON",
//                 data : {id:id},
//                 success: function(data){
//                 	$.each(data,function(invoice_no,shipping_terms, tt, lc, shipping_method, vassel_name, etd_kendari
//                   ,eta_discharge_port, lc_no, ni, quantity, net_weight, unit_price){
//                     	$('#ModalaEdit').modal('show');
//
//                       $('[name="invoice_no2"]').val(data.invoice_no);
//                       $('[name="shipping_terms2"]').val(data.shipping_terms);
//                       $('[name="tt2"]').val(data.tt);
//                       $('[name="lc2"]').val(data.lc);
//                       $('[name="shipping_method2"]').val(data.shipping_method);
//                       $('[name="vassel_name2"]').val(data.vassel_name);
//                       $('[name="etd_kendari2"]').val(data.etd_kendari);
//                       $('[name="eta_discharge_port2"]').val(data.eta_discharge_port);
//                       $('[name="lc_no2"]').val(data.lc_no);
//                       $('[name="ni2"]').val(data.ni);
//                       $('[name="quantity2"]').val(data.quantity);
//                       $('[name="net_weight2"]').val(data.net_weight);
//                       $('[name="unit_price2"]').val(data.unit_price);
//                       $('[name="amount2"]').val(data.amount);
//             		});
//                 }
//             });
//             return false;
//         });
//
//         //GET adjustment
//     		$('#show_data').on('click','.adjustment',function(){
//                 var id=$(this).attr('data');
//                 $.ajax({
//                     type : "post",
//                     url  : "<?php echo base_url()?>Eksplore/ControllerEksport/get_loading",
//                     dataType : "JSON",
//                     data : {id:id},
//                     success: function(data){
//                           html += '<tr><td>sda</td></try>';
//                         	$('#ModalaAdjustment').modal('show');
//                           $('#awe').html(html);
//
//
//                     }
//                 });
//                 return false;
//             });
//
//
// 		//GET HAPUS
		$('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });

    $('#show_data').on('click','.paid',function(){
            var id_invoice=$(this).data('id');
            var id_contract=$(this).data('id2');
            $('#ModalaAdd').modal('show');
            $('[name="kode1"]').val(id_invoice);
            $('[name="kode2"]').val(id_contract);

        });

// 		//Simpan Barang
// 		$('#btn_simpan').on('click',function(){
//
//             var no_invoice=$('#invoice_no').val();
//             var shiping_terms=$('#shipping_terms').val();
//             var tt=$('#tt').val();
//             var lc=$('#lc').val();
//             var shipping_method=$('#shipping_method').val();
//             var vassel_name=$('#vassel_name').val();
//             var etd_kendari=$('#etd_kendari').val();
//             var eta_discharge_port=$('#eta_discharge_port').val();
//             var lc_no=$('#lc_no').val();
//             var ni=$('#ni').val();
//             var quantity=$('#quantity').val();
//             var net_weight=$('#net_weight').val();
//             var unit_price=$('#unit_price').val();
//             var amount=$('#amount').val();
//
//             var id=$('#id_contract').val();
//             $.ajax({
//                 type : "post",
//                 url  : '<?php echo base_url()?>Eksplore/ControllerContract/saveInvoice',
//                 dataType : "json",
//                 data : {'no_invoice':no_invoice ,'shiping_terms':shiping_terms, 'tt':tt, 'lc':lc,
//                   'shipping_method':shipping_method, 'vassel_name':vassel_name,
//                   'etd_kendari':etd_kendari, 'eta_discharge_port':eta_discharge_port,
//                   'lc_no':lc_no, 'ni':ni,
//                   'quantity':quantity, 'net_weight':net_weight,
//                   'amount':amount, 'unit_price':unit_price, 'id':id},
//
//                 success: function(data){
//
//                     //   $('#mydata').dataTable();
//                     //
//                     // $('[name="invoice_no"]').val("");
//                     // $('[name="shipping_terms"]').val("");
//                     // $('[name="tt"]').val("");
//                     // $('[name="lc"]').val("");
//                     // $('[name="shipping_method"]').val("");
//                     // $('[name="vassel_name"]').val("");
//                     // $('[name="etd_kendari"]').val("");
//                     // $('[name="eta_discharge_port"]').val("");
//                     // $('[name="lc_no"]').val("");
//                     // $('[name="ni"]').val("");
//                     // $('[name="quantity"]').val("");
//                     // $('[name="net_weight"]').val("");
//                     // $('[name="unit_price"]').val("");
//                     // $('[name="amount"]').val("");
//                     //
//                     //
//                     // $('#ModalaAdd').modal('hide');
//                     // tampil_data_barang();
//                      location.reload();
//                 }
//             });
//             return false;
//         });





//         //Update Barang
		$('#btn_update').on('click',function(){

            var date=$('#paid_date').val();
            var id_invoice=$('#textkode1').val();
            var id_contract=$('#textkode2').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Eksplore/ControllerContract/updatePaid",
                dataType : "JSON",
                data : {'id_invoice':id_invoice ,'id_contract':id_contract, 'date':date},
                success: function(data){
                  location.reload();

                  //   $('#mydata').dataTable();
                  //   //edit ini
                  //
                  // $('[name="invoice_no"]').val("");
                  // $('[name="shipping_terms"]').val("");
                  // $('[name="tt"]').val("");
                  // $('[name="lc"]').val("");
                  // $('[name="shipping_method"]').val("");
                  // $('[name="vassel_name"]').val("");
                  // $('[name="etd_kendari"]').val("");
                  // $('[name="eta_discharge_port"]').val("");
                  // $('[name="lc_no"]').val("");
                  // $('[name="ni"]').val("");
                  // $('[name="quantity"]').val("");
                  // $('[name="net_weight"]').val("");
                  // $('[name="unit_price"]').val("");
                  // $('[name="amount"]').val("");
                  // $('#ModalaEdit').modal('hide');
                  // tampil_data_barang();
                }
            });
            return false;
        });
//
//         //Hapus Barang
        $('#btn_hapus').on('click',function(){
            var kode=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url()?>Eksplore/ControllerContract/deleteInvoiceContract",
            dataType : "JSON",
                    data : {kode: kode},
                    success: function(data){
                            // $('#ModalHapus').modal('hide');
                            // tampil_data_barang();
                            location.reload();
                    }
                });
                return false;
            });
//
//
	});

</script>
