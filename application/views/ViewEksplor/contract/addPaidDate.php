<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('ViewEksplor/layoutEksplor/head');
//--> include data sidebar
$this->load->view('ViewEksplor/layoutEksplor/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= $this->session->flashdata("sukses"); ?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                Detail Contract
                            </h2>
                        </div>
                        <div class="body">

                            <!-- isi -->
                            <div class="row">

                              <div class="col-sm-6">

                                  <dl id="dt-list-1" class="dl-horizontal">
                                  <table width="100%" border="1">
                                      <tr>
                                          <td class="b"> Contract NO  </td>
                                          <td colspan='3'><?php echo (!empty($contract))?$contract->no_contract:""; ?></td>
                                      </tr>
                                      <tr>

                                          <tr>
                                              <td class="b"> Name Of Shipper  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->name_shipper:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Decription Goods   </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->description_goods:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b">Price PCT  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->price_pct:""; ?></td>
                                          </tr>



                                          <tr>
                                              <td class="b">Discharge Name  </td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->discharge_name:""; ?></td>
                                          </tr>

                                          <tr>
                                              <td class="b"> Quantity Contract</td>
                                              <td colspan='3'><?php echo (!empty($contract))?number_format($contract->quantityC,3):""; ?></td>
                                          </tr>
                                          <tr>
                                              <td class="b"> Eksport Country</td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->export_country:""; ?></td>
                                          </tr>
                                          <tr>
                                              <td class="b"> L/C No</td>
                                              <td colspan='3'><?php echo (!empty($contract))?$contract->LC_no:""; ?></td>
                                          </tr>
                                      </tr>

                                  </table>
                              </dl>

                            </div>

                            <div class="col-sm-6">

                                <dl id="dt-list-1" class="dl-horizontal">
                                <table width="100%" border="1">
                                    <tr>
                                        <tr>
                                            <td class="b"> Invoice NO  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->invoice_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> Shipping of Terms  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->shiping_terms:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b" rowspan="3"> Payment Terms  </td>
                                            <tr>
                                              <td class="b" width="10%">TT</td>
                                              <td><?php echo (!empty($contract))?$contract->tt:""; ?>%</td>
                                              <td><?php echo (!empty($contract))?$contract->nilai_mata_uang.". ".number_format($contract->nominalTT,3):""; ?></td>
                                            </tr>
                                            <tr>
                                              <td class="b">LC</td>
                                              <td><?php echo (!empty($contract))?$contract->lc:""; ?>%</td>
                                              <td><?php echo (!empty($contract))?$contract->nilai_mata_uang.". ".number_format($contract->nominalTT,3):""; ?></td>
                                            </tr>
                                        </tr>
                                        <tr>
                                            <td class="b"> Shipping Method  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->shipping_method:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b">Vassel Name </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->vassel_name:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> ETD Kendari</td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->etd_kendari:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> ETA Discharge Port </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->eta_discharge_port:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> PEB NO  </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->peb_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> NOPEN NPE   </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->nopen_npe:""; ?></td>
                                        </tr>

                                        <tr>
                                            <td class="b">Price Tonnage </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->price_tonnage:""; ?></td>
                                        </tr>

                                        <tr>
                                            <td class="b">Bill Landing No </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->bill_landing_no:""; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="b"> Remarks </td>
                                            <td colspan='3'><?php echo (!empty($contract))?$contract->remarks:""; ?></td>
                                        </tr>

                                    </tr>

                                </table>

                            </dl>

                          </div>
                      </div>
                  </div>
              </div>

                                <input type="hidden" id="id_contract" name="id_contract" value="<?php echo (!empty($contract))?$contract->id_contract:""; ?>">
                                <input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo (!empty($contract))?$contract->id_invoice:""; ?>">


                                <div class="card">
                                    <div class="header">
                                      <div class="">
                                        <h2>
                                            Detail Loading
                                        </h2>
                                      </div>
                                        <div class="" style="margin-left:87%;margin-top:-2%;">
                                          <a href="<?php echo base_url(); ?>Eksplore/ControllerEksport/eksportExcelVassel/<?php echo $contract->id_invoice; ?>"><button type="submit" name="excel" class="btn btn-primary waves-effect">EKSPORT EXCEL</button></a>
                                        </div>
                                    </div>
                                    <div class="body">

                                        <!-- isi -->
                                        <div class="row">
                                          <div class="col-md-12">

                                            <div class="table-responsive">
                                              <div id="reload">
                                                <table id="mydata" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                  <thead>
                                                    <tr>
                                                      <th rowspan="2">No</th>
                                                      <th rowspan="2">Barge</th>
                                                      <th rowspan="2">Quantity</th>
                                                      <th rowspan="">Trip</th>
                                                      <th rowspan="2">Start Loading</th>
                                                      <th rowspan="2">Complate Loading</th>
                                                      <th rowspan="2">Final Draught</th>
                                                    </tr>

                                                  </thead>
                                                  <tfoot>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Barge</th>
                                                      <th>Quantity</th>
                                                      <th>Trip</th>
                                                      <th>Start Loading</th>
                                                      <th>Complate Loading</th>
                                                      <th>Final Draught</th>
                                                    </tr>
                                                  </tfoot>
                                                  <tbody id="show_data">
                                                    <?php foreach ($loading as $key => $value): ?>
                                                      <tr>
                                                        <td><?php echo $key+1; ?></td>
                                                        <td><?php echo $value->barge; ?></td>
                                                        <td><?php echo $value->quantity; ?></td>
                                                        <td><?php echo $value->trip; ?></td>
                                                        <td><?php echo $value->startLoading; ?></td>
                                                        <td><?php echo $value->complateLoading; ?></td>
                                                        <td><?php echo $contract->quantity; ?></td>

                                                      </tr>
                                                    <?php endforeach; ?>

                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                          </div>
                                  </div>
                                </div>
                                </div>



                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            Detail Adjustment
                                        </h2>
                                    </div>
                                    <div class="body">

                                        <!-- isi -->
                                        <div class="row">

                                          <div class="col-sm-6">

                                              <dl id="dt-list-1" class="dl-horizontal">
                                              <table width="100%" border="1">
                                                  <tr>
                                                      <td colspan="2" class="b"> <center> SURVEYOR RESULT</center></td>

                                                  </tr>
                                                  <tr>
                                                      <td class="b"> NI Content PCT  </td>
                                                      <td colspan='3'><?php echo (!empty($contract))?$contract->ni:""; ?></td>
                                                  </tr>
                                                  <tr>

                                                      <tr>
                                                          <td class="b"> Price Per PCT Ni (1%/MT) (USD)  </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$contract->price_pct:""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Quantity (MT)</td>
                                                          <td colspan='3'><?php echo (!empty($contract))?str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($contract->quantity,3)))):""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Price Per Tonnage (USD)  </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$contract->price_tonnage:""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Amount </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$contract->nilai_mata_uang.".".str_replace("a",".",str_replace(".",",",str_replace(",","a",number_format($contract->amount,3)))):""; ?></td>
                                                      </tr>


                                                  </tr>

                                              </table>
                                          </dl>
                                          <table width="100%" border="1">
                                            <tr>
                                              <td>OVERPAYMENT (UNDERPAYMET) (USD)</td>
                                              <td><?php echo (!empty($adjustment))?$adjustment->overpayment:""; ?></td>
                                            </tr>
                                          </table>
                                        </div>

                                        <div class="col-sm-6">

                                            <dl id="dt-list-1" class="dl-horizontal">
                                            <table width="100%" border="1">
                                                <tr>
                                                  <tr>
                                                      <td colspan="2" class="b"> <center> FINAL ADJUSTMENT</center></td>

                                                  </tr>
                                                  <tr>
                                                      <td class="b"> NI Content PCT  </td>
                                                      <td colspan='3'><?php echo (!empty($contract))?$adjustment->niA:""; ?></td>
                                                  </tr>
                                                  <tr>

                                                      <tr>
                                                          <td class="b"> Price Per PCT Ni (1%/MT) (USD)  </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$adjustment->price_pctA:""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Quantity (MT)</td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$adjustment->quantityA:""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Price Per Tonnage (USD)  </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$adjustment->price_tonnageA:""; ?></td>
                                                      </tr>

                                                      <tr>
                                                          <td class="b">Amount </td>
                                                          <td colspan='3'><?php echo (!empty($contract))?$adjustment->amountA:""; ?></td>
                                                      </tr>


                                                </tr>

                                            </table>

                                        </dl>
                                        <table width="100%" border="1">
                                          <tr>
                                            <td>DOCUMENT CERTIFICATE OF QUALITY</td>
                                            <td><a href="<?php echo base_url(); ?>pdf/<?php echo "CERTIFICATE_OF_QUALITY_".$contract->id_invoice.".pdf"; ?>"><button type="btn"  class="btn btn-primary waves-effect">DOWNLOAD</button></a></td>
                                          </tr>
                                        </table>
                                      </div>

                                  </div>
                                </div>
                                </div>
                        </div>

                    </div>
                    </div>


        <!--END MODAL HAPUS -->
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
<?php
//--> include data footer
$this->load->view('ViewEksplor/layoutEksplor/foot');

?>

<!-- inline scripts related to this page -->
