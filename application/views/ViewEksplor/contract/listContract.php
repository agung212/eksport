<!DOCTYPE html>
<html>

<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>

<body class="theme-red">
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <section class="content">
      <div class="container-fluid">
          <?= $this->session->flashdata("sukses"); ?>
          <!-- Basic Table -->
          <div class="row clearfix">
            <div class="card" style="height:210px;margin-top:0%;">
              <div class="header">
                  <h2>
                      FILTER TABLE
                  </h2>

              </div>
              <form class="" action="<?php echo base_url(); ?>Eksplore/ControllerContract" method="post">


                  <div class="col-md-12" style="margin-left:0%;">
                    <div class="col-lg-2 col-md-6 col-xs-6" style="margin-top:2%;margin-left:0%;">
                      <b>Name Of Shipper</b>
                      <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">local_shipping</i>
                        </span>
                        <div class="form-line focused">
                          <select name="name_shipper2" class="show-tick" data-live-search="true">
                            <option selected disabled value="">Select Shipper</option>
                              <?php foreach ($shipper as $value): ?>
                                <option <?php echo (!(empty($shipperF)) && ($shipperF == $value->id_shipper))?" selected ":""; ?> value="<?php echo $value->id_shipper; ?>"><?php echo $value->name_shipper; ?></option>

                              <?php endforeach; ?>
                          </select>
                          <!-- <input type="text" name="name_shipper" class="form-control" placeholder="Insert Name Of Shipper"> -->
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-2 col-md-6 col-xs-6" style="margin-top:2%;margin-left:10%;">
                      <b>Status</b>
                      <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">assignment_turned_in</i>
                        </span>
                        <div class="form-line focused">
                          <select name="status" class="show-tick" data-live-search="true">
                            <option selected disabled value="">All</option>
                            <option <?php echo ($status == "On Going")?" selected ":""; ?> value="On Going">On Going</option>
                            <option <?php echo ($status == "Complete")?" selected ":""; ?> value="Complete">Complete</option>

                          </select>
                          <!-- <input type="text" name="name_shipper" class="form-control" placeholder="Insert Name Of Shipper"> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-xs-6" style="margin-top:2%;margin-left:10%;">
                                        <b>Filter by Date</b>
                                          <div class="input-group">
                                              <span class="input-group-addon">
                                                  Start
                                              </span>
                                              <div class="form-line">
                                                  <input type="date" class="form-control date" value="<?php echo $tgl_awal; ?>" name="tgl_awal" placeholder="Ex: 30/07/2016">
                                              </div>
                                          </div>
                                          </div>

                                          <div class="col-lg-2 col-md-6 col-xs-6" style="margin-top:2%;margin-left:5%;">
                                          <br>
                                          <div class="input-group">
                                              <span class="input-group-addon">
                                                  Finish
                                              </span>
                                              <div class="form-line">
                                                  <input type="date" class="form-control date" name="tgl_akhir" value="<?php echo $tgl_akhir; ?>" placeholder="Ex: 30/07/2016">
                                              </div>

                                          </div>
                                          </div>
                                          <div class="col-lg-1 col-md-6 col-xs-6" style="margin-top:-2%;margin-left:1%;">
                                          <br>
                                          <div class="input-group">
                                            <div class="form-line">
                                              <button type="submit" name="filter" class="btn btn-primary waves-effect">Filter</button>
                                            </div>
                                          </div>
                                          </div>
                                          <div class="col-lg-1 col-md-6 col-xs-6" style="margin-top:-2%;margin-left:-2%;">
                                          <br>
                                          <div class="input-group">
                                            <div class="form-line">
                                              <a href="<?php echo base_url(); ?>Eksplore/ControllerContract"><button type="btn"  class="btn btn-primary waves-effect">Reset</button></a>

                                            </div>
                                          </div>
                                          </div>
                </div>

                  </form>

            </div>
                  <div class="card">
                      <div class="header">
                          <h2>
                              TABLE CONTRACT
                          </h2>
                          <form class="" action="<?php echo base_url(); ?>Eksplore/ControllerContract/eksportExcelContract" method="post">
                            <input type="hidden" name="shipper" value="<?php echo $shipperF; ?>">
                            <input type="hidden" name="statusE" value="<?php echo $status; ?>">
                            <input type="hidden" name="tgl_awalE" value="<?php echo $tgl_awal; ?>">
                            <input type="hidden" name="tgl_akhirE" value="<?php echo $tgl_akhir; ?>">
                            <ul class="header-dropdown m-r--5">
                              <li><button type="submit" name="excelB" class="btn btn-primary waves-effect">TRACKING TOOLS EXPORT</button></li>
                              <li><button type="submit" name="excelA" class="btn btn-primary waves-effect">TRACKING FINAL ADJUSTMENT</button></li>

                              <!-- //<a href="<?php echo base_url(); ?>Eksplore/ControllerSite/View_preview"><button type="button" class="btn btn-primary waves-effect">IMPORT EXCEL</button></a> -->
                            </ul>
                          </form>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                  <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>No Contract</th>
                                        <th>Name Of Shipper</th>
                                        <th>Status</th>
                                        <th>Created Date</th>
                                        <th>Action</th>

                                      </tr>
                                  </thead>

                                  <tbody>
                                    <?php foreach ($contract as $key => $data): ?>
                                      <tr>
                                      <td><?php echo $key+1; ?></td>
                                      <td><a href="<?php echo base_url(); ?>Eksplore/ControllerContract/FormInvoice/<?php echo $data->id_contract; ?>"><?php echo $data->no_contract; ?></a></td>

                                      <td><?php echo $data->name_shipper; ?></td>

                                      <td><?php echo $data->status; ?></td>
                                      <td><?php echo $data->dateC; ?></td>
                                      <td><center><a href="<?php echo base_url(); ?>Eksplore/ControllerContract/FormUpdate/<?php echo $data->id_contract; ?>"><button type="update" class="btn btn-primary waves-effect">Update</button>
                                        <a href='#'  id='delete-row' class='delete-row red' title='Delete' data-toggle='modal' data-target='<?php echo (empty($data->temporary))?"#myModal":""; ?>' aria-hidden='true' data-id='<?php echo $data->id_contract; ?>'>
                                        <button type="button" <?php echo (empty($data->temporary))?"":" disabled "; ?> class="btn btn-danger waves-effect"><small>Delete</small></button>
                                      </a></td>
                                      </tr>
                                    <?php endforeach; ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Basic Table -->

          <!-- Konfirmasi Hapus Data -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to delete this data?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                      <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /konfirmasi -->

          <!-- #END# With Material Design Colors -->
      </div>
  </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
<script type="text/javascript">
$(function(){

  //notifikasi hapus
  $(document).on('click', '#delete-row', function(e){
    e.preventDefault();
    id = $(this).data('id');
  });
  $(document).on('click', '#del-row', function(e){
    window.location = 'ControllerContract/DeleteContract/'+id;
  });
  //.notifikasi hapus

});

</script>
</html>
