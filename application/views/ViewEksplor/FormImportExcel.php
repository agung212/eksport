<!DOCTYPE html>
<html>
<style media="screen">
.upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
<head>
<?php $this->load->view('ViewEksplor/layoutEksplor/head'); ?>
</head>

<body class="theme-red">
<?php $this->load->view('ViewEksplor/layoutEksplor/sidebar'); ?>
  <section class="content">
      <div class="container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                          <h2>
                              FORM IMPORT EXCEL
                          </h2>
                          <ul class="header-dropdown m-r--5">
                            <a href="<?php echo base_url('excel/format.xlsx'); ?>"><button type="button" class="btn btn-primary waves-effect">DOWNLOAD FORMAT</button></a>
                          </ul>
                      </div>
                      <div class="body">
                          <form method="post" action="<?php echo base_url(); ?>Eksplore/ControllerSite/First" enctype="multipart/form-data">
                              <div class="row clearfix">

                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <div class="upload-btn-wrapper">
                                      <button type="button" class="btn btn-primary btn-lg m-l-15 waves-effect">Upload File</button>
                                      <input type="file" name="file">
                                      <button type="submit" name="preview" class="btn btn-primary btn-lg m-l-15 waves-effect">Priview</button>
                                    </div>
                                  </div>
                              </div>
                          </form>
                          <?php
                        	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
                        		if(isset($upload_error)){ // Jika proses upload gagal
                        			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
                        			die; // stop skrip
                        		}

                        		// Buat sebuah tag form untuk proses import data ke database
                        		echo "<form method='post' action='".base_url("Eksplore/ControllerSite/import")."'>";

                        		// Buat sebuah div untuk alert validasi kosong
                        		// echo "<div style='color: red;' id='kosong'>
                        		// Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                        		// </div>";

                        		echo "<div class='body'>
                                <div class='table-responsive'>
                                    <table class='table table-bordered table-striped table-hover js-basic-example dataTable'>
                                        <thead>
                                            <tr>
                                              <th>Date</th>
                                              <th>Start</th>
                                              <th>Finish</th>
                                              <th>Smelter</th>
                                              <th>Plat</th>
                                              <th>Bruto</th>
                                              <th>Tara</th>
                                              <th>Netto</th>
                                              <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>Date</th>
                                          		<th>Start</th>
                                          		<th>Finish</th>
                                          		<th>Smelter</th>
                                          		<th>Plat</th>
                                          		<th>Bruto</th>
                                          		<th>Tara</th>
                                          		<th>Netto</th>
                                              <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                        ";

                        		$numrow = 1;
                        		$kosong = 0;

                        		// Lakukan perulangan dari data yang ada di excel
                        		// $sheet adalah variabel yang dikirim dari controller
                        		foreach($sheet as $row){


                        			$date=$row['B']; // Insert data nis dari kolom A di excel
                        			$start=$row['C']; // Insert data nama dari kolom B di excel
                        			$finish=$row['D']; // Insert data jenis kelamin dari kolom C di excel
                        			$smelter=$row['E']; // Insert data alamat dari kolom D di excel
                        			$plat=$row['F']; // Insert data nis dari kolom A di excel
                        			$bruto=$row['G']; // Insert data nama dari kolom B di excel
                        			$tara=$row['H']; // Insert data jenis kelamin dari kolom C di excel
                        			$netto=$row['I'];

                        			// Cek jika semua data tidak diisi
                        			if(empty($date) && empty($start) && empty($finish) && empty($smelter) && empty($plat) && empty($bruto) && empty($tara) && empty($netto))
                        				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                        			// Cek $numrow apakah lebih dari 1
                        			// Artinya karena baris pertama adalah nama-nama kolom
                        			// Jadi dilewat saja, tidak usah diimport
                        			if($numrow > 1){
                        				// Validasi apakah semua data telah diisi
                        				$date_td = ( ! empty($date))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                        				$start_td = ( ! empty($start))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                        				$finish_td = ( ! empty($finish))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
                        				$smelter_td = ( ! empty($smelter))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                        				$plat_td = ( ! empty($plat))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                        				$bruto_td = ( ! empty($bruto))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                        				$tara_td = ( ! empty($tara))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
                        				$netto_td = ( ! empty($netto))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah

                        				// Jika salah satu data ada yang kosong
                        				if(empty($date) or empty($start) or empty($finish) or empty($smelter) or empty($plat) or empty($bruto) or empty($tara) or empty($netto)){
                        					$kosong++; // Tambah 1 variabel $kosong
                        				}

                        				echo "<tr>";
                        				echo "<td".$date_td.">".$date."</td>";
                        				echo "<td".$start_td.">".$start."</td>";
                        				echo "<td".$finish_td.">".$finish."</td>";
                        				echo "<td".$smelter_td.">".$smelter."</td>";
                        				echo "<td".$plat_td.">".$plat."</td>";
                        				echo "<td".$bruto_td.">".$bruto."</td>";
                        				echo "<td".$tara_td.">".$tara."</td>";
                        				echo "<td".$netto_td.">".$netto."</td>";
                                echo "<td><a><button type='button' class='btn btn-primary waves-effect'>Update</button></a></td>";
                        				echo "</tr>";
                        			}

                        			$numrow++; // Tambah 1 setiap kali looping
                        		}

                        		echo "</tbody>
                                  </table>
                                  </div>
                                  </div>";

                        		// Cek apakah variabel kosong lebih dari 0
                        		// Jika lebih dari 0, berarti ada data yang masih kosong
                        		if($kosong > 0){
                        		?>
                        			<script>
                        			$(document).ready(function(){
                        				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                        				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

                        				$("#kosong").show(); // Munculkan alert validasi kosong
                        			});
                        			</script>
                        		<?php
                        		}else{ // Jika semua data sudah diisi
                        			echo "<hr>";

                        			// Buat sebuah tombol untuk mengimport data ke database
                        			echo "<button type='submit' name='import'>Import</button>";
                        			echo "<a href='".base_url("Eksplore/ControllerSite")."'>Cancel</a>";
                        		}

                        		echo "</form>";
                        	}
                        	?>
                      </div>
                  </div>
              </div>
      </div>
  </section>
</body>
<?php $this->load->view('ViewEksplor/layoutEksplor/foot'); ?>
</html>
