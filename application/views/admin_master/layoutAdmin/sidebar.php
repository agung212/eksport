<?php $sistem = $this->session->userdata('sistem'); ?>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?php echo base_url(); ?><?php echo ($this->session->userdata('nama') == "admin")?"adminMaster/adminM":"admin/admin"; ?>">PT.VDNI - ADMIN</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- #END# Tasks -->


                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                        <i class="material-icons">view_headline</i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">SISTEM INFORMASI</li>
                        <li class="body">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="menu" style="overflow: hidden; width: auto; height: 254px;">
                                <li <?php echo (empty($sistem['ORE']))?'hidden':'' ?>>
                                    <a href="<?php echo base_url(); ?>ORE/C_ORE" class=" waves-effect waves-block">
                                        <div class="icon-circle bg-light-green">
                                            <i class="material-icons">business</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>ORE</h4>
                                            <p>
                                                <i class="material-icons">play_for_work</i><?php echo ($sistem['ORE'] == "F")?' FULL':' READ' ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li <?php echo (empty($sistem['COAL']))?'hidden':'' ?>>
                                    <a href="javascript:void(0);" class=" waves-effect waves-block">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">add_shopping_cart</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>COAL</h4>
                                            <p>
                                                <i class="material-icons">play_for_work</i><?php echo ($sistem['COAL'] == "F")?' FULL':' READ' ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li <?php echo (empty($sistem['LIMESTONE']))?'hidden':'' ?>>
                                    <a href="javascript:void(0);" class=" waves-effect waves-block">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">delete_forever</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>LIMESTONE</h4>
                                            <p>
                                                <i class="material-icons">play_for_work</i> <?php echo ($sistem['LIMESTONE'] == "F")?' FULL':' READ' ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li <?php echo (empty($sistem['EKSPOR']))?'hidden':'' ?>>
                                    <a href="<?php echo base_url(); ?>/Eksplore/ControllerSite" class=" waves-effect waves-block">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">delete_forever</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>EKSPOR</h4>
                                            <p>
                                                <i class="material-icons">play_for_work</i> <?php echo ($sistem['EKSPOR'] == "F")?' FULL':' READ' ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li <?php echo (empty($sistem['SOLAR']))?'hidden':'' ?>>
                                    <a href="javascript:void(0);" class=" waves-effect waves-block">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">delete_forever</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>SOLAR</h4>
                                            <p>
                                                <i class="material-icons">play_for_work</i> <?php echo ($sistem['SOLAR'] == "F")?' FULL':' READ' ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>

                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?php echo base_url(); ?>assets/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div style="margin-top:-15%;" class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b><h3><?php echo $this->session->userdata('name'); ?></h3></b></div>
                <!-- <div class="email"><?php echo $this->session->userdata('username'); ?></div> -->
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MENU UTAMA</li>
                <li <?php echo ($this->session->userdata('name')!="admin")?'hidden':'' ?>>
                    <a href="<?php echo base_url(); ?>adminMaster/adminM">
                        <i class="material-icons">work</i>
                        <span>User</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>admin/login_admin/Logout">
                        <i class="material-icons">input</i>
                        <span>Logout</span>
                    </a>
                </li>
        </div>

</section>
        <!-- #Menu -->
