<!DOCTYPE html>
<html>
<?php $this->load->view('admin_master/layoutAdmin/head'); ?>
<?php $this->load->view('admin_master/layoutAdmin/sidebar'); ?>

<body class="theme-red">

    <section class="content">
      <div class="container-fluid">
          <!-- Input -->
          <div class="row clearfix">
            <?= $this->session->flashdata("sukses"); ?>
              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="margin-left:15%;">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Change Password
                            </h2>
                        </div>
                        <div class="body">
                          <?php echo form_open('adminMaster/adminM/ChangePassword');?>

                          <div class="row clearfix">
                              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                  <label>Nama</label>
                              </div>
                              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                  <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" disabled class="form-control" value="<?php echo $admin->nama; ?>">
                                    </div>
                                    <p> <?php echo form_error('name'); ?> </p>
                                  </div>
                              </div>
                          </div>

                          <div class="row clearfix">
                              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                  <label>Username</label>
                              </div>
                              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                  <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="username" disabled class="form-control" value="<?php echo $admin->username; ?>">
                                    </div>
                                    <p> <?php echo form_error('Username'); ?> </p>
                                  </div>
                              </div>
                          </div>

                          <div class="row clearfix">
                              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                  <label>New Password</label>
                              </div>
                              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                  <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="password" class="form-control" placeholder="new Password" required>
                                    </div>
                                    <p><font color="red"><?php echo form_error('password'); ?></font></p>
                                  </div>
                              </div>
                          </div>

                          <div class="row clearfix">
                              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                  <label>Confirm Password</label>
                              </div>
                              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                  <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="password_conf" class="form-control" placeholder="Confirm Password" required>
                                    </div>
                                    <p><font color="red"><?php echo form_error('password_conf'); ?></font></p>

                              </div>
                          </div>

                            <input type="hidden" name="id_admin" value="<?php echo $admin->id_admin; ?>">
                            <div class="row">
                                <div class="col-lg-2 col-md-10 col-sm-8 col-xs-7" style="margin-left:5%;">
                                    <button class="btn btn-block bg-pink waves-effect" type="submit">Change</button>
                                </div>
                            </div>

                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>

          </div>
          <!-- #END# Textarea -->

      </div>
    </section>
<?php $this->load->view('admin_master/layoutAdmin/footer'); ?>
</body>

</html>
