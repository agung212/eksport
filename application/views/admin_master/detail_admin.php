<!DOCTYPE html>
<html>

<head>
<?php $this->load->view('admin_master/layoutAdmin/head'); ?>
</head>
<?php $this->load->view('admin_master/layoutAdmin/sidebar'); ?>
<body class="theme-red ls-closed">

    <section class="content">
      <div class="container-fluid">
          <!-- Input -->
          <div class="row clearfix">

            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="margin-Left:13%;">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Informasi User
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url(); ?>adminMaster/adminM/F_changePassword/<?php echo $admin->id_admin; ?>" class=" waves-effect waves-block">Change Password</a></li>
                                        <li><a href="<?php echo base_url(); ?>adminMaster/adminM/F_UAdmin/<?php echo $admin->id_admin; ?>" class=" waves-effect waves-block">Update</a></li>
                                        <li><a href='#' id='delete-row' class='delete-row red' title='Delete' data-toggle='modal' data-target='#myModal' aria-hidden='true' data-id='<?php echo $admin->id_admin; ?>'>Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form class="form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Nama Admin</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" readonly id="email_address_2" class="form-control" value="<?php echo $admin->nama; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Username</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" readonly id="password_2" class="form-control" value="<?php echo $admin->username; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Level</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                              <select name="level" class="form-control show-tick">
                                                  <option disabled <?php echo ($admin->level == "0")?"selected ":""; ?> value="0">No level</option>
                                                  <option disabled <?php echo ($admin->level == "1")?"selected ":""; ?> value="1">1. Manager</option>
                                                  <option disabled <?php echo ($admin->level == "2")?"selected ":""; ?> value="2">2. Keuangan</option>
                                                  <option disabled <?php echo ($admin->level == "3")?"selected ":""; ?> value="3">3. kadif</option>
                                                  <option disabled <?php echo ($admin->level == "4")?"selected ":""; ?> value="4">4. Admin</option>
                                                  <option disabled <?php echo ($admin->level == "5")?"selected ":""; ?> value="5">5. User</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-left:-1%" class="header">
                                    <h2>
                                        Hak Akses
                                    </h2>

                                </div>

                                <div class="" style="margin-left:5%;margin-top:2%;">
                                  <div class="demo-radio-button">
                                    <label for="radio_1">ORE</label>
                                    <input name="ORE" disabled type="radio" <?php echo ($ORE=="F")?'checked':'' ?> value="F" id="radio_1" >
                                    <label for="radio_1">FULL</label>
                                    <input name="ORE" disabled type="radio" <?php echo ($ORE=="R")?'checked':'' ?> value="R" id="radio_2">
                                    <label for="radio_2">READ</label>
                                    <input name="ORE" disabled type="radio" <?php echo ($ORE=="")?'checked':'' ?> value="" id="radio_7">
                                    <label for="radio_7">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">COAL</label>
                                    <input name="COAL" disabled type="radio" <?php echo ($COAL=="F")?'checked':'' ?> value="F" id="radio_3" >
                                    <label for="radio_3">FULL</label>
                                    <input name="COAL" disabled type="radio" <?php echo ($COAL=="R")?'checked':'' ?> value="R" id="radio_4">
                                    <label for="radio_4">READ</label>
                                    <input name="COAL" disabled type="radio" <?php echo ($COAL=="")?'checked':'' ?> value="" id="radio_5">
                                    <label for="radio_5">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">LIMESTONE</label>
                                    <input name="LIMESTONE" disabled type="radio" <?php echo ($LIMESTONE =="F")?'checked':'' ?> value="F" id="radio_5" >
                                    <label for="radio_5">FULL</label>
                                    <input name="LIMESTONE" disabled type="radio" <?php echo ($LIMESTONE =="R")?'checked':'' ?> value="R" id="radio_6">
                                    <label for="radio_6">READ</label>
                                    <input name="LIMESTONE" disabled type="radio" <?php echo ($LIMESTONE =="")?'checked':'' ?> value="" id="radio_8">
                                    <label for="radio_9">NA</label>
                                  </div>

                                  <div class="demo-radio-button">
                                    <label for="radio_1">EKSPOR</label>
                                    <input name="EKSPOR" type="radio" disabled <?php echo ($EKSPOR =="F")?'checked':'' ?> value="F" id="radio_7" >
                                    <label for="radio_7">FULL</label>
                                    <input name="EKSPOR" type="radio" disabled <?php echo ($EKSPOR =="R")?'checked':'' ?> value="R" id="radio_8">
                                    <label for="radio_8">READ</label>
                                    <input name="EKSPOR" type="radio" disabled <?php echo ($EKSPOR =="")?'checked':'' ?> value="" id="radio_14" >
                                    <label for="radio_14">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">SOLAR</label>
                                    <input name="SOLAR" type="radio" disabled <?php echo ($SOLAR =="F")?'checked':'' ?> value="F" id="radio_9" >
                                    <label for="radio_9">FULL</label>
                                    <input name="SOLAR" type="radio" disabled <?php echo ($SOLAR =="R")?'checked':'' ?> value="R" id="radio_10">
                                    <label for="radio_10">READ</label>
                                    <input name="SOLAR" type="radio" disabled <?php echo ($SOLAR =="")?'checked':'' ?> value="" id="radio_15" >
                                    <label for="radio_15">NA</label>
                                  </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
          </div>
          <!-- #END# Textarea -->

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                </div>
                <div class="modal-body">
                  Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                  <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                </div>
              </div>
            </div>
          </div>

      </div>
    </section>

  <?php $this->load->view('admin_master/layoutAdmin/footer'); ?>
</body>

</html>
<script type="text/javascript">
$(function(){

//notifikasi hapus
$(document).on('click', '#delete-row', function(e){
    e.preventDefault();
    id = $(this).data('id');
});
$(document).on('click', '#del-row', function(e){
    // window.location = 'adminM/DeleteAdmin/' +id;
    window.location.href = "<?php echo base_url(); ?>adminMaster/adminM/DeleteAdmin/"+id;

});
//.notifikasi hapus

});
</script>
