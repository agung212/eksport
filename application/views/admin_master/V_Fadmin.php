<!DOCTYPE html>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<html>
<?php $this->load->view('admin_master/layoutAdmin/head'); ?>
<?php $this->load->view('admin_master/layoutAdmin/sidebar'); ?>
<body class="theme-red ls-closed">
    <section class="content">
      <div class="container-fluid">
          <!-- Input -->
          <div class="row clearfix">
              <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="margin-left:15%;">
                  <div class="card">
                      <div class="header">
                          <h2>
                              Form New Admin
                          </h2>

                      </div>
                      <div class="body">
                          <div class="row clearfix">
                              <div class="col-sm-12">
                                <?php echo form_open('adminMaster/adminM/Register_admin');?>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" class="form-control" placeholder="Name" required>
                                    </div>
                                    <p><font color="red"><?php echo form_error('name'); ?></font></p>

                                </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                                      </div>
                                      <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="password" name="password" class="form-control" placeholder="Password" required>
                                      </div>
                                      <p><font color="red"><?php echo form_error('password'); ?></font></p>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="password" name="password_conf" class="form-control" placeholder="confirm Password" required>
                                      </div>
                                      <p><font color="red"><?php echo form_error('password_conf'); ?></font></p>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                        <select name="level" class="form-control show-tick">
                                            <option disabled selected>Select Level User</option>
                                            <option value="1">1. Manager</option>
                                            <option value="2">2. Keuangan</option>
                                            <option value="3">3. kadif</option>
                                            <option value="4">4. Admin</option>
                                            <option value="5">5. User</option>
                                        </select>
                                      </div>
                                      <p><font color="red"><?php echo form_error('level'); ?></font></p>
                                  </div>

                                  <div class="demo-radio-button">
                                    <label for="radio_21">ORE</label>
                                    <input name="ORE" type="radio" value="F" id="radio_1" >
                                    <label for="radio_1">FULL</label>
                                    <input name="ORE" type="radio" value="R" id="radio_2">
                                    <label for="radio_2">READ</label>
                                    <input name="ORE" type="radio" value="" id="radio_3" checked>
                                    <label for="radio_3">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_22">COAL</label>
                                    <input name="COAL" type="radio" value="F" id="radio_4" >
                                    <label for="radio_4">FULL</label>
                                    <input name="COAL" type="radio" value="R" id="radio_5">
                                    <label for="radio_5">READ</label>
                                    <input name="COAL" type="radio" value="" id="radio_6" checked>
                                    <label for="radio_6">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_23">LIMESTONE</label>
                                    <input name="LIMESTONE" type="radio" value="F" id="radio_7" >
                                    <label for="radio_7">FULL</label>
                                    <input name="LIMESTONE" type="radio" value="R" id="radio_8">
                                    <label for="radio_8">READ</label>
                                    <input name="LIMESTONE" type="radio" value="" id="radio_9" checked>
                                    <label for="radio_9">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_10">EKSPOR</label>
                                    <input name="EKSPOR" type="radio" value="F" id="radio_10" >
                                    <label for="radio_10">FULL</label>
                                    <input name="EKSPOR" type="radio" value="R" id="radio_11">
                                    <label for="radio_11">READ</label>
                                    <input name="EKSPOR" type="radio" value="" id="radio_12" checked>
                                    <label for="radio_12">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_24">SOLAR</label>
                                    <input name="SOLAR" type="radio" value="F" id="radio_13" >
                                    <label for="radio_13">FULL</label>
                                    <input name="SOLAR" type="radio" value="R" id="radio_14">
                                    <label for="radio_14">READ</label>
                                    <input name="SOLAR" type="radio" value="" id="radio_15" checked>
                                    <label for="radio_15">NA</label>
                                  </div>
                                  <div class="row">
                                      <div class="col-lg-2 col-md-10 col-sm-8 col-xs-7" style="margin-left:0%;margin-top:5%;">
                                          <button class="btn btn-block bg-pink waves-effect" type="submit">Create</button>
                                      </div>
                                  </div>
                                  <?php echo form_close();?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Textarea -->

      </div>
    </section>


</body>
<?php $this->load->view('admin_master/layoutAdmin/footer'); ?>
</html>

<script type="text/javascript">
 $(document).ready(function(){
  $('#username').change(function(){
   var username = $('#username').val();
   if(username != ''){
    $.ajax({
     url: "<?php echo site_url()?>adminMaster/adminM/check_username",
     method: "POST",
     data: {username:username},
     success: function(data){
      $('#message').html(data);
     }
    });
   }
  });
 });
</script>
