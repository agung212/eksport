<!DOCTYPE html>
<html>
<?php $this->load->view('admin_master/layoutAdmin/head'); ?>
<?php $this->load->view('admin_master/layoutAdmin/sidebar'); ?>
<body class="theme-red ls-closed">

    <section class="content">
      <div class="container-fluid">
          <!-- Input -->
          <div class="row clearfix">
              <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="margin-left:15%;">
                  <div class="card">
                      <div class="header">
                          <h2>
                              Form Update Admin
                          </h2>

                      </div>
                      <div class="body">
                          <div class="row clearfix">
                              <div class="col-sm-12">
                                <?php echo form_open('adminMaster/adminM/UpdateAdmin');?>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="">Nama Admin</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $admin->nama; ?>" required>
                                    </div>
                                    <p><font color="red"><?php echo form_error('name'); ?></font></p>
                                </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label class="">Username</label>
                                          <input type="text" name="username" class="form-control" value="<?php echo (empty($wrong_username))?$admin->username:$wrong_username; ?>" required>
                                      </div>

                                      <p><font color="red"><?php echo form_error('username'); ?></font></p>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                        <select name="level" class="form-control show-tick">
                                            <option disabled <?php echo ($admin->level == "0")?"selected ":""; ?> value="0">No level</option>
                                            <option  <?php echo ($admin->level == "1")?"selected ":""; ?> value="1">1. Manager</option>
                                            <option  <?php echo ($admin->level == "2")?"selected ":""; ?> value="2">2. Keuangan</option>
                                            <option  <?php echo ($admin->level == "3")?"selected ":""; ?> value="3">3. kadif</option>
                                            <option  <?php echo ($admin->level == "4")?"selected ":""; ?> value="4">4. Admin</option>
                                            <option  <?php echo ($admin->level == "5")?"selected ":""; ?> value="5">5. User</option>
                                        </select>
                                      </div>
                                      <p><font color="red"><?php echo form_error('level'); ?></font></p>
                                  </div>
                                  <input type="hidden" name="id_admin" value="<?php echo $admin->id_admin; ?>">

                                  <div class="demo-radio-button">
                                    <label for="radio_1">ORE</label>
                                    <input name="ORE" type="radio" <?php echo ($ORE=="F")?'checked':'' ?> value="F" id="radio_1" >
                                    <label for="radio_1">FULL</label>
                                    <input name="ORE" type="radio" <?php echo ($ORE=="R")?'checked':'' ?> value="R" id="radio_2">
                                    <label for="radio_2">READ</label>
                                      <input name="ORE" type="radio" <?php echo ($ORE=="")?'checked':'' ?> value="" id="radio_7">
                                      <label for="radio_7">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">COAL</label>
                                    <input name="COAL" type="radio" <?php echo ($COAL=="F")?'checked':'' ?> value="F" id="radio_3" >
                                    <label for="radio_3">FULL</label>
                                    <input name="COAL" type="radio" <?php echo ($COAL=="R")?'checked':'' ?> value="R" id="radio_4">
                                    <label for="radio_4">READ</label>
                                    <input name="COAL" type="radio" <?php echo ($COAL=="")?'checked':'' ?> value="" id="radio_8">
                                    <label for="radio_8">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">LIMESTONE</label>
                                    <input name="LIMESTONE" type="radio" <?php echo ($LIMESTONE =="F")?'checked':'' ?> value="F" id="radio_5" >
                                    <label for="radio_5">FULL</label>
                                    <input name="LIMESTONE" type="radio" <?php echo ($LIMESTONE =="R")?'checked':'' ?> value="R" id="radio_6">
                                    <label for="radio_6">READ</label>
                                    <input name="LIMESTONE" type="radio" <?php echo ($LIMESTONE =="")?'checked':'' ?> value="" id="radio_9">
                                    <label for="radio_9">NA</label>
                                  </div>

                                  <div class="demo-radio-button">
                                    <label for="radio_1">EKSPOR</label>
                                    <input name="EKSPOR" type="radio" <?php echo ($EKSPOR =="F")?'checked':'' ?> value="F" id="radio_22" >
                                    <label for="radio_22">FULL</label>
                                    <input name="EKSPOR" type="radio" <?php echo ($EKSPOR =="R")?'checked':'' ?> value="R" id="radio_23">
                                    <label for="radio_23">READ</label>
                                    <input name="EKSPOR" type="radio" <?php echo ($EKSPOR =="")?'checked':'' ?> value="" id="radio_14" >
                                    <label for="radio_14">NA</label>
                                  </div>
                                  <div class="demo-radio-button">
                                    <label for="radio_1">SOLAR</label>
                                    <input name="SOLAR" type="radio" <?php echo ($SOLAR =="F")?'checked':'' ?> value="F" id="radio_24" >
                                    <label for="radio_24">FULL</label>
                                    <input name="SOLAR" type="radio" <?php echo ($SOLAR =="R")?'checked':'' ?> value="R" id="radio_10">
                                    <label for="radio_10">READ</label>
                                    <input name="SOLAR" type="radio" <?php echo ($SOLAR =="")?'checked':'' ?> value="" id="radio_15" >
                                    <label for="radio_15">NA</label>
                                  </div>

                                  <div class="row">
                                      <div class="col-lg-2 col-md-10 col-sm-8 col-xs-7" style="margin-top:3%;">
                                          <button class="btn btn-block bg-pink waves-effect" type="submit">Change</button>
                                      </div>
                                  </div>
                                  <?php echo form_close();?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Textarea -->

      </div>
    </section>
</body>
<?php $this->load->view('admin_master/layoutAdmin/footer'); ?>
</html>
