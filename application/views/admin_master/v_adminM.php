<!DOCTYPE html>
<html>
  <?php $this->load->view('admin_master/layoutAdmin/head'); ?>
  <?php $this->load->view('admin_master/layoutAdmin/sidebar'); ?>
<body class="theme-red ls-closed">

    <section class="content">
        <div class="container-fluid">
            <?= $this->session->flashdata("sukses"); ?>
            <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                TABLE USERS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              <a href="<?php echo base_url(); ?>adminMaster/adminM/f_admin"><button type="button" class="btn btn-primary waves-effect">CREATE</button></a>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Username</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Nama</th>
                                          <th>Username</th>
                                          <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($admin as $key => $value) {?>
                                        <tr>

                                            <td>
                                              <a href="<?php echo base_url(); ?>adminMaster/adminM/DetailAdmin/<?php echo $value->id_admin; ?>"><?php echo $value->nama; ?></a>

                                            </td>


                                            <td>
                                            <a href="<?php echo base_url(); ?>adminMaster/adminM/DetailAdmin/<?php echo $value->id_admin; ?>"><?php echo $value->username; ?></a>
                                            </td>
                                        <td>
                                          <div class="button-demo">
                                                  <a href="<?php echo base_url(); ?>adminMaster/adminM/F_changePassword/<?php echo $value->id_admin; ?>"><button type="button" class="btn btn-danger btn-xs waves-effect"><small>Change Password</small></button></a>
                                                  <a href="<?php echo base_url(); ?>adminMaster/adminM/F_UAdmin/<?php echo $value->id_admin; ?>"><button type="button" class="btn btn-danger btn-xs waves-effect"><small>Update</small></button></a>
                                                  <a href='#' id='delete-row' class='delete-row red' title='Delete' data-toggle='modal' data-target='#myModal' aria-hidden='true' data-id='<?php echo $value->id_admin; ?>'>
                                                    <button type="button" class="btn btn-danger btn-xs waves-effect"><small>Delete</small></button>
                                                  </a>
                                          </div>
                                        </td>
                                          </tr>

                                          <?php };?>

                                      </div>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Table -->

            <!-- Konfirmasi Hapus Data -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                      </div>
                      <div class="modal-body">
                        Are you sure you want to delete this data?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                        <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /konfirmasi -->

            <!-- #END# With Material Design Colors -->
        </div>
    </section>
<?php $this->load->view('admin_master/layoutAdmin/footer'); ?>
</body>

</html>
<script type="text/javascript">
$(function(){

//notifikasi hapus
$(document).on('click', '#delete-row', function(e){
    e.preventDefault();
    id = $(this).data('id');
});
$(document).on('click', '#del-row', function(e){
    window.location = 'adminM/DeleteAdmin/' +id;
});
//.notifikasi hapus

});
</script>
