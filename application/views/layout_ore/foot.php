<!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

     <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
       

        $('.select2').css('width','300px').select2({allowClear:false});

        //Jumlah supplier
            $("#alert").hide();
            function tambahElemenAgt() {
              var ke = document.getElementById("ke").value;

              if(ke > 20)
              {
                $("#alert").show();
              }
              else
              {
                var stre;
                    stre = "<div id='arow" + ke + "'>" +
                                "<br><div class='col-md-4'>" +
                                                    "<label></label>" +
                                                     " <select name='supplier[]' class='select2  show-tick'>" +
                                                     "     <option> Pilih Supplier Ke-"+ ke +" </option> <?php" +
                                                                    foreach($supplier as $row)
                                                                    {
                                                                        echo "<option value='$row->id_supplier'> $row->nama_supplier </option>";
                                                                    }
                                                                "?>" +
                                                     "  </select>" +
                                            "</div>" +
                                            "<div class='col-md-6'>" +
                                                 "<label></label>" +
                                                  "<input type='number'  name='quantity[]' requiredaria-required='true' aria-invalid='false' placeholder='Enter Quantity' >" +   
                                           
                                             " <a href='#' onclick='hapusElemen(\"#arow" + ke + "\"); return false;' class='btn btn-sm btn-danger' title='Hapus supplier ke-"+ ke +"'> <i class='material-icons'>remove</i> </a>"+
                                             "</div>" +
                         "</div>";

                $("#form_agt").append(stre);
                ke = (ke-1) + 2;
                document.getElementById("ke").value = ke;

                $("#alert").hide();
              }
            }

            function hapusElemen(ke) {
                $("#alert").hide();
              $(ke).remove();

              var ke2 = document.getElementById("ke").value;
              ke3 = ke2-1;
              document.getElementById("ke").value = ke3;
            }
            //.jumlah supplier

           
      
        function sum() {
              var txtFirstNumberValue = document.getElementById('txt1').value;
              var txtSecondNumberValue = 1;
              var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
              if (!isNaN(result)) {
                 document.getElementById('txt3').value = result;
              }
        }

        function sum2() {
              var txtFirstNumberValue = document.getElementById('txt2').value;
              var txtSecondNumberValue = 1;
              var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
              if (!isNaN(result)) {
                 document.getElementById('txt4').value = result;
              }
        }
</script>
<script type="text/javascript">
/* Dengan Rupiah */
var price = document.getElementById('Price');
price.addEventListener('keyup', function(e)
{
  price.value = formatRupiah(this.value, 'Rp. ');
});

var amount = document.getElementById('amount');
amount.addEventListener('keyup', function(e)
{
  amount.value = formatRupiah(this.value, '$. ');
});


/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split   = number_string.split(','),
    sisa    = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 190 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>

    