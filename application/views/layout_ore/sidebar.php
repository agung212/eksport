    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url(); ?>assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $user; ?></div>
                    <div class="email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            
                            <li><a href="<?php echo base_url(); ?>admin/login_admin/Logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENU UTAMA</li>
                <?php $hak_akses = $this->session->userdata('level');
                    if($hak_akses== 3 || $hak_akses== 2 || $hak_akses== 1) { ?>
                        <li>
                        <a href="<?= site_url('ore/C_stok_adj'); ?>">
                            <i class="material-icons">widgets</i>
                            <span>Adjusment Stock</span>
                        </a>
                    </li>

                    <?php } else{ ?>
                    <li>
                        <a href="<?= site_url('ore/C_contract'); ?>">
                            <i class="material-icons">account_balance_wallet</i>
                            <span>Ore Contract</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('ore/C_stok'); ?>">
                            <i class="material-icons">layers</i>
                            <span>Stock</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= site_url('ore/C_stok_adj'); ?>">
                            <i class="material-icons">widgets</i>
                            <span>Adjusment Stock</span>
                        </a>
                    </li>

                     <li>
                        <a href="<?= site_url('ore/C_report'); ?>">
                            <i class="material-icons">report</i>
                            <span>Report</span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">settings</i>
                            <span>Settings</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?= site_url('ore/C_seller'); ?>" >
                                    <span>Seller</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('ore/C_supplier'); ?>" >
                                    <span>Supplier</span>
                                </a>
                            </li>
                        </ul>
                    </li>                    
                </ul>
                <?php } ?>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside  class="right-sidebar">

            <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
            
        </aside>
        <!-- #END# Right Sidebar -->
    </section>