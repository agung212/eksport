<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> <?= $title; ?> </title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

     <!-- Sweetalert Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(); ?>assets/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-datetimepicker.min.css" media="screen" />

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

     <!-- Multi Select Css -->
    <link href="<?php echo base_url(); ?>assets/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    
    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../../index.html">PT VDNI - ORE PRODUCT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                   
                    <!-- Notifications -->
                    <li class="dropdown ">
                    <?php $hak_akses = $this->session->userdata('level');
                    if($hak_akses== 3) { ?>
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <?php if($jml_notif->jml_stat < 1) { ?>
                            <i class="material-icons">notifications </i>
                        </a>

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <?php } else { ?>
                            <i class="material-icons">notifications</i>
                            <span class="label-count"><?= $jml_notif->jml_stat; ?></span>
                        </a>
                        <?php } ?>  
                   
                        <ul class="dropdown-menu">
                                <li class="header"><?= $jml_notif->jml_stat; ?> Notifications Adjustment</li>
                                <li class="body">
                                    <ul class="menu">

                                     <?php 
                                                //-> jika tidak ada notif baru
                                                if($jml_notif->jml_stat < 1) { 
                                                    echo "<center>Tidak ada Adjustment baru!</center>";
                                                } else{
                                                    foreach ($adj_notif as $row) {
                                                        $id=$row->id_adj;
                                                        $id_sm = $row->id_sm;
                                                        echo "<li><a href='"; echo base_url('ore/C_stok_adj/detail_adj/'.$id.'/'.$id_sm). "'>
                                                        <div class='icon-circle bg-light-green'>
                                                            <i class='material-icons'>person_add</i>
                                                        </div>
                                                        <div class='menu-info'>
                                                            <h4> New Adjustment</h4>
                                                            <p>
                                                                <i class='material-icons'>access_time</i> ".$row->tgl_input; echo "
                                                            </p>
                                                        </div>
                                                    </a></li>";
                                                    }
                                                }
                                    // ?>
                                    
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <?php $hak_akses = $this->session->userdata('level');
                    } elseif($hak_akses== 2) { ?>
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <?php  if($jml_notif_fin->sk == 1 && $jml_notif_fin->fin == 0 ) { ?>
                            <i class="material-icons">notifications </i>
                            <span class="label-count"><?= $jml_notif_fin->jml_fin; ?></span>
                        </a>

                        <?php } else{ ?>
                            <i class="material-icons">notifications</i>
                            <span class="label-count"></span>
                        </a>
                        <?php } ?>
                        <ul class="dropdown-menu">
                                <li class="header">Notifications Adjustment</li>
                                <li class="body">
                                    <ul class="menu">

                                     <?php 
                                                //-> jika tidak ada notif baru
                                                if(($jml_notif_fin->sk == 0) || ($jml_notif_fin->fin == 1) || ($jml_notif_fin->fin == 2)) { 
                                                    echo "<center>Tidak ada Adjustment baru!</center>";
                                                // } elseif ($jml_notif_fin->fin == 1){
                                                //     echo "<center>Tidak ada Adjustment baru!</center>";

                                                } else{
                                                    foreach ($adj_notif as $row) {
                                                        $id=$row->id_adj;
                                                        $id_sm = $row->id_sm;
                                                    //if($row->sk==1){
                                                        echo "<li><a href='"; echo base_url('ore/C_stok_adj/detail_adj/'.$id.'/'.$id_sm). "'>
                                                        <div class='icon-circle bg-light-green'>
                                                            <i class='material-icons'>person_add</i>
                                                        </div>
                                                        <div class='menu-info'>
                                                            <h4> New Adjustment</h4>
                                                            <p>
                                                                <i class='material-icons'>access_time</i> ".$row->tgl_input; echo "
                                                            </p>
                                                        </div>
                                                    </a></li>";
                                                    
                                                    }
                                                }
                                    // ?>
                            
                                </ul>
                            </li>
                        </ul>

                        <?php $hak_akses = $this->session->userdata('level');
                    } elseif($hak_akses== 1) { ?>
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <?php  if($jml_notif_fin->fin == 1 && $jml_notif_fin->gm == 0 ) { ?>
                            <i class="material-icons">notifications </i>
                            <span class="label-count"><?= $jml_notif_fin->jml_fin; ?></span>
                        </a>

                        <?php } else{ ?>
                            <i class="material-icons">notifications</i>
                            <span class="label-count"></span>
                        </a>
                        <?php } ?>
                        <ul class="dropdown-menu">
                                <li class="header">Notifications Adjustment</li>
                                <li class="body">
                                    <ul class="menu">

                                     <?php 
                                                //-> jika tidak ada notif baru
                                                if(($jml_notif_fin->sk == 0) || ($jml_notif_fin->fin == 0) || ($jml_notif_fin->fin == 2) || ($jml_notif_fin->gm == 1) || ($jml_notif_fin->gm == 2)) { 
                                                    echo "<center>Tidak ada Adjustment baru!</center>";
                                                // } elseif ($jml_notif_fin->fin == 1){
                                                //     echo "<center>Tidak ada Adjustment baru!</center>";

                                                } else{
                                                    foreach ($adj_notif as $row) {
                                                        $id=$row->id_adj;
                                                        $id_sm = $row->id_sm;
                                                    //if($row->sk==1){
                                                        echo "<li><a href='"; echo base_url('ore/C_stok_adj/detail_adj/'.$id.'/'.$id_sm). "'>
                                                        <div class='icon-circle bg-light-green'>
                                                            <i class='material-icons'>person_add</i>
                                                        </div>
                                                        <div class='menu-info'>
                                                            <h4> New Adjustment</h4>
                                                            <p>
                                                                <i class='material-icons'>access_time</i> ".$row->tgl_input; echo "
                                                            </p>
                                                        </div>
                                                    </a></li>";
                                                    
                                                    }
                                                }
                                    // ?>
                            
                                </ul>
                            </li>
                        </ul>
                        <?php } ?>
                    </li>
                    
                    <!-- #END# Notifications -->
                   
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</body>
</html>

   