<!DOCTYPE html>
<html>
<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class='material-icons'>assignment</i>
                                    <a href="<?= site_url('ore/C_stok_adj'); ?>"> Adjustment Stock </a>
                                </li>
                                <li class="active"> Detail  Adjustment Stock  </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                                <i class='material-icons'>assignment</i> DETAIL ADJUSTMEN STOCK 
                            </h2>
                        </div>
                        <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                            
                                        <h8>
                                            <i class='material-icons' style="width:20%">send ADJUSTMEN STOCK </i> 
                                        </h8>
                                            
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b"> Seller Agent </td>
                                                <td colspan='2'><?= $adj->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                
                                                <tr>
                                                    <td class="b"> Supplier </td>
                                                    <td colspan='2'><?= $adj->nama_supplier; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Periode Shipment </td>
                                                    <td colspan='2'><?= $adj->tgl_awal; ?> s/d <?= $adj->tgl_akhir; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Tug Boat/Barge </td>
                                                    <td colspan='2'><?= $adj->nama_tongkang; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Voyage Number </td>
                                                    <td colspan='2'><?= $adj->voyage_number; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Quantity </td>
                                                    <td colspan='2'><?= number_format($adj->jml_stok); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">BL Number </td>
                                                    <td colspan='2'><?= $adj->bl_number; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Time Arrived </td>
                                                    <td colspan='2'><?= $adj->tgl_arr; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Commence Discharge </td>
                                                    <td colspan='2'><?= $adj->comm_disch; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Complete Discharge </td>
                                                    <td colspan='2'><?= $adj->compl_disch; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Time Depatured </td>
                                                    <td colspan='2'><?= $adj->tgl_dep; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                    <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b c" colspan="2"> After COA </td>
                                            </tr>
                                            <tr>

                                            <?php if ($adj->coa_qty == $coa->coa_qty) { ?>

                                                <tr>
                                                    <td class="b"> After COA Qty </td>
                                                    <td colspan='2'><?= $adj->coa_qty; ?></td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> After COA Qty </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->coa_qty; ?></td>
                                                </tr>

                                            <?php } ?>

                                             <?php if ($adj->ni_c == $coa->ni) { ?>

                                                <tr>
                                                    <td class="b"> % Ni </td>
                                                    <td colspan='2'><?= $adj->ni_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                 <tr>
                                                    <td class="b" bgcolor="yellow"> % Ni </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->ni_c; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->fe_c == $coa->fe) { ?>

                                                <tr>
                                                    <td class="b"> % Fe </td>
                                                    <td colspan='2'><?= $adj->fe_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Fe </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->fe_c; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->sio_c == $coa->sio) { ?>

                                                <tr>
                                                    <td class="b"> % SiO2 </td>
                                                    <td colspan='2'><?= $adj->sio_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % SiO2 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->sio_c; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->mgo_c == $coa->mgo) { ?>

                                                 <tr>
                                                    <td class="b"> % MgO </td>
                                                    <td colspan='2'><?= $adj->mgo_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % MgO </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->mgo_c; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->aio_c == $coa->aio) { ?>

                                                <tr>
                                                    <td class="b">Al2O3 </td>
                                                    <td colspan='2'><?= $adj->aio_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Al2O3 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->aio_c; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->mc_c == $coa->mc) { ?>

                                                 <tr>
                                                    <td class="b">Mc </td>
                                                    <td colspan='2'><?= $adj->mc_c; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Mc </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->mc_c; ?> %</td>
                                                </tr>
                                            <?php } ?>
                                                 <tr>
                                                    <td class="b">Dokumen </td>
                                                    <td colspan='2'><?= $adj->picture_c; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                     <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b c" colspan="2"> Intertek Lab </td>
                                            </tr>
                                            <tr>
                                                <?php if ($adj->intertek_qty == $intertek->intertek_qty) { ?>

                                                <tr>
                                                    <td class="b"> Intertek Qty </td>
                                                    <td colspan='2'><?= $adj->intertek_qty; ?></td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> Intertek Qty </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->intertek_qty; ?></td>
                                                </tr>

                                                 <?php } ?>

                                                <?php if ($adj->ni_i == $intertek->ni) { ?>

                                                <tr>
                                                    <td class="b"> % Ni </td>
                                                    <td colspan='2'><?= $adj->ni_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Ni </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->ni_i; ?> %</td>
                                                </tr>

                                                <?php } ?>
                                                
                                                <?php if ($adj->fe_i == $intertek->fe) { ?>

                                                <tr>
                                                    <td class="b"> % Fe </td>
                                                    <td colspan='2'><?= $adj->fe_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Fe </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->fe_i; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->sio_i == $intertek->sio) { ?>

                                                <tr>
                                                    <td class="b"> % SiO2 </td>
                                                    <td colspan='2'><?= $adj->sio_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % SiO2 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->sio_i; ?> %</td>
                                                </tr>

                                                 <?php } ?>

                                                 <?php if ($adj->mgo_i == $intertek->mgo) { ?>

                                                 <tr>
                                                    <td class="b"> % MgO </td>
                                                    <td colspan='2'><?= $adj->mgo_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % MgO </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->mgo_i; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->aio_i == $intertek->aio) { ?>

                                                <tr>
                                                    <td class="b">Al2O3 </td>
                                                    <td colspan='2'><?= $adj->aio_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Al2O3 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->aio_i; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->mc_i == $intertek->mc) { ?>

                                                 <tr>
                                                    <td class="b">Mc </td>
                                                    <td colspan='2'><?= $adj->mc_i; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Mc </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $adj->mc_i; ?> %</td>
                                                </tr>

                                                <?php } ?>
                                                 <tr>
                                                    <td class="b">Dokumen </td>
                                                    <td colspan='2'><?= $adj->picture_i; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                     <div class="col-md-6">
                                        <a href="<?= site_url('ore/C_stok_adj'); ?>" class="btn btn-default">
                                            <i class="material-icons">arrow_back</i>
                                             <span>Back</span>
                                        </a>
                                    </div>

                                     <div class="col-md-5">
                                   
                                        <!-- <a href="<?= site_url('ore/C_contract/addendum/'); ?>" class="btn btn-primary waves-effect">
                                            <i class="material-icons">add</i>
                                            <span>Create Addendum</span>
                                        </a> -->
                                   </div>

                                </div>

                                <div class="col-sm-6">
                                        <h8>
                                            <i class='material-icons' style="width:20%">send STOCK MASUK </i> 
                                        </h8>
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b"> Seller Agent </td>
                                                <td colspan='2'><?= $sm->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                
                                                <tr>
                                                    <td class="b"> Supplier </td>
                                                    <td colspan='2'><?= $sm->nama_supplier; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Periode Shipment </td>
                                                    <td colspan='2'><?= $sm->tgl_awal; ?> s/d <?= $adj->tgl_akhir; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Tug Boat/Barge </td>
                                                    <td colspan='2'><?= $sm->nama_tongkang; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Voyage Number </td>
                                                    <td colspan='2'><?= $sm->voyage_number; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Quantity </td>
                                                    <td colspan='2'><?= number_format($sm->jml_stok); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">BL Number </td>
                                                    <td colspan='2'><?= $sm->bl_number; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Time Arrived </td>
                                                    <td colspan='2'><?= $sm->tgl_arr; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Commence Discharge </td>
                                                    <td colspan='2'><?= $sm->comm_disch; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Complete Discharge </td>
                                                    <td colspan='2'><?= $sm->compl_disch; ?></td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Time Depatured </td>
                                                    <td colspan='2'><?= $sm->tgl_dep; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>
                                     
                                     <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b c" colspan="2"> After COA </td>
                                            </tr>
                                            <tr>
                                            
                                            <?php if ($adj->coa_qty == $coa->coa_qty) { ?>

                                                <tr>
                                                    <td class="b"> After COA Qty </td>
                                                    <td colspan='2'><?= $coa->coa_qty; ?></td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> After COA Qty </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->coa_qty; ?></td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->ni_c == $coa->ni) { ?>

                                                <tr>
                                                    <td class="b"> % Ni </td>
                                                    <td colspan='2'><?= $coa->ni; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Ni </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->ni; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->fe_c == $coa->fe) { ?>

                                                <tr>
                                                    <td class="b"> % Fe </td>
                                                    <td colspan='2'><?= $coa->fe; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Fe </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->fe; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->sio_c == $coa->sio) { ?>

                                                <tr>
                                                    <td class="b"> % SiO2 </td>
                                                    <td colspan='2'><?= $coa->sio; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % SiO2 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->sio; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->mgo_c == $coa->mgo) { ?>

                                                <tr>
                                                    <td class="b"> % MgO </td>
                                                    <td colspan='2'><?= $coa->mgo; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                 <tr>
                                                    <td class="b" bgcolor="yellow"> % MgO </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->mgo; ?> %</td>
                                                </tr>

                                             <?php } ?>

                                             <?php if ($adj->aio_c == $coa->aio) { ?>

                                                <tr>
                                                    <td class="b">Al2O3 </td>
                                                    <td colspan='2'><?= $coa->aio; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Al2O3 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->aio; ?> %</td>
                                                </tr>

                                            <?php } ?>

                                            <?php if ($adj->mc_c == $coa->mc) { ?>

                                                 <tr>
                                                    <td class="b">Mc </td>
                                                    <td colspan='2'><?= $coa->mc; ?> %</td>
                                                </tr>

                                            <?php } else{ ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Mc </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $coa->mc; ?> %</td>
                                                </tr>

                                             <?php } ?>
                                                 <tr>
                                                    <td class="b">Dokumen </td>
                                                    <td colspan='2'><?= $coa->picture; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                    <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b c" colspan="2"> Intertek Lab </td>
                                            </tr>
                                            <tr>
                                                <?php if ($adj->intertek_qty == $intertek->intertek_qty) { ?>

                                                <tr>
                                                    <td class="b"> Intertek Qty </td>
                                                    <td colspan='2'><?= $intertek->intertek_qty; ?></td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> Intertek Qty </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->intertek_qty; ?></td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->ni_i == $intertek->ni) { ?>

                                                <tr>
                                                    <td class="b"> % Ni </td>
                                                    <td colspan='2'><?= $intertek->ni; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Ni </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->ni; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->fe_i == $intertek->fe) { ?>

                                                <tr>
                                                    <td class="b"> % Fe </td>
                                                    <td colspan='2'><?= $intertek->fe; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % Fe </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->fe; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->sio_i == $intertek->sio) { ?>

                                                <tr>
                                                    <td class="b"> % SiO2 </td>
                                                    <td colspan='2'><?= $intertek->sio; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % SiO2 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->sio; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->mgo_i == $intertek->mgo) { ?>

                                                 <tr>
                                                    <td class="b"> % MgO </td>
                                                    <td colspan='2'><?= $intertek->mgo; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow"> % MgO </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->mgo; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->aio_i == $intertek->aio) { ?>

                                                <tr>
                                                    <td class="b">Al2O3 </td>
                                                    <td colspan='2'><?= $intertek->aio; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Al2O3 </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->aio; ?> %</td>
                                                </tr>

                                                <?php } ?>

                                                <?php if ($adj->mc_i == $intertek->mc) { ?>

                                                 <tr>
                                                    <td class="b">Mc </td>
                                                    <td colspan='2'><?= $intertek->mc; ?> %</td>
                                                </tr>

                                                <?php } else { ?>

                                                <tr>
                                                    <td class="b" bgcolor="yellow">Mc </td>
                                                    <td colspan='2' bgcolor="yellow"><?= $intertek->mc; ?> %</td>
                                                </tr>

                                                 <?php } ?>

                                                 <tr>
                                                    <td class="b">Dokumen </td>
                                                    <td colspan='2'><?= $intertek->picture; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                    <div>

                                        <!--  <a href='#' id='reject' class='delete-row red btn btn-danger' data-toggle='modal' data-target='#myModal4' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>highlight_off</i>
                                            <span>Rejected </span>
                                            </a>


                                        <a href='#' id='nonaktif' class='delete-row red btn btn-primary' data-toggle='modal' data-target='#myModal1' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>send</i>
                                            <span>Accept </span>
                                            </a> -->

                                       <?php $id = $adj->id_adj;
                                                $hak_akses = $this->session->userdata('level');
                                            if($hak_akses== 3 ) {
                                                if($adj->sk !=1 && $adj->sk !=2){
                                        ?>

                                            <a href='#' id='reject' class='delete-row red btn btn-danger' data-toggle='modal' data-target='#myModal4' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>highlight_off</i>
                                            <span>Rejected </span>
                                            </a>

                                            <a href='#' id='nonaktif' class='delete-row red btn btn-primary' data-toggle='modal' data-target='#myModal1' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>send</i>
                                            <span>Accept Kadiv</span>
                                            </a>
                                            <?php } else{ } ?>

                                        <?php } elseif ($hak_akses== 2 && $adj->sk == 1 ) { 
                                                    if($adj->fin !=1 && $adj->fin !=2){
                                        ?>

                                            <a href='#' id='reject' class='delete-row red btn btn-danger' data-toggle='modal' data-target='#myModal4' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>highlight_off</i>
                                            <span>Rejected </span>
                                            </a>

                                             <a href='#' id='stat_sk' class='delete-row red btn btn-primary' data-toggle='modal' data-target='#myModal2' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>send</i>
                                            <span>Accept Finance</span>
                                            </a>
                                            <?php } else{ } ?>

                                        <?php } elseif ($hak_akses== 1 && $adj->fin == 1) { 
                                                    if($adj->gm !=1 && $adj->gm !=2){
                                            ?>

                                            <a href='#' id='reject' class='delete-row red btn btn-danger' data-toggle='modal' data-target='#myModal4' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>highlight_off</i>
                                            <span>Rejected </span>
                                            </a>

                                            <a href='#' id='stat_gm' class='delete-row red btn btn-primary' data-toggle='modal' data-target='#myModal3' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>send</i>
                                            <span>Accept GM</span>
                                            </a>
                                            <?php } else{ } ?>

                                        <?php } else{
                                            
                                            } ?>
                                    </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

                    

                        <!-- Adjusment Stock -->
                        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form role="form" action="<?= site_url('ore/C_stok_adj/update_sk/'. $adj->id_adj); ?>"  method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                Accepted this adjustment stock! <br/>

                                Are you sure?

                                
                                        <input type="hidden" name="id_adj" value="<?= $adj->id_adj; ?>">
                                        <input type="hidden" name="status" value="1">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>

                                <button type="submit" class="btn btn-primary del-row"><i class='material-icons'>send</i> Accepted </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->


                         <!-- Adjusment Stock -->
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form role="form" action="<?= site_url('ore/C_stok_adj/update_status/'. $adj->id_adj); ?>"  method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                Accepted this adjustment stock! <br/>
                                
                                Are you sure?

                                
                                        <input type="hidden" name="id_adj" value="<?= $adj->id_adj; ?>">
                                        <input type="hidden" name="status" value="1">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="submit" class="btn btn-primary del-row"><i class='material-icons'>send</i> Accepted </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->

                        <!-- Adjusment Stock -->
                        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form role="form" action="<?= site_url('ore/C_stok_adj/update_gm/'. $adj->id_adj); ?>"  method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                Accepted this adjustment stock! <br/>
                                
                                Are you sure?

                                        <input type="hidden" name="id_sm" value="<?= $adj->id_sm; ?>">
                                        <input type="hidden" name="id_adj" value="<?= $adj->id_adj; ?>">
                                        <input type="hidden" name="status" value="1">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="submit" class="btn btn-primary del-row"><i class='material-icons'>send</i> Accepted </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->

                         <!-- Adjusment Stock -->
                        <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form role="form" action="<?= site_url('ore/C_stok_adj/reject/'. $adj->id_adj); ?>"  method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                Rejected this adjustment stock! <br/>
                                
                                Are you sure?

                                
                                        <input type="hidden" name="id_adj" value="<?= $adj->id_adj; ?>">
                                        <input type="hidden" name="status" value="2">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="submit" class="btn btn-danger del-row"><i class='material-icons'>highlight_off</i> Reject </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
   
</body>
<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<!-- inline scripts related to this page -->
    <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'hapus_data/' +id;
        });
        //.notifikasi hapus

         $(document).on('click', '#nonaktif', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });


         $(document).on('click', '#stat_sk', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });

          $(document).on('click', '#stat_gm', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });

           $(document).on('click', '#reject', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });

      });
    </script>
</html>
