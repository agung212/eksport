<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-14 col-md-14 col-sm-14 col-xs-14">
                    <div class="card">
                         <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li class="active">
                                    <i class="material-icons">layers</i>
                                     Adjustment Stock 
                                </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                               Adjustment Stock 
                            </h2>
                        </div>
                        <div class="body">
                        <!-- <a href="<?php echo site_url('ore/C_stok/tambah_stok'); ?>" class="btn btn-primary waves-effect">
                             <i class="material-icons">add</i
                               <span>NEW STOCK</span>
                        </a>
 -->
                        <br>
                        <br>
                        <?= $this->session->flashdata("sukses"); ?>
                        <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead >
                                        <tr>
                                            <th>Seller</th>
                                            <th>Supplier</th>
                                            <th>Tug Boat/Barge</th>
                                            <th>Voyage</th>
                                            <th>BL Qty</th>
                                            <th>TA</th>
                                            <th>Comm Disch</th>
                                            <th>Compl Disch</th>
                                            <th>TD</th>
                                            <th>Status</th>
                                             <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                                foreach ($adj as $row) {

                                                    $id = $row->id_adj;

                                                    $id_sm = $row->id_sm;

                                                    $stok = $row->jml_stok;

                                                    echo "
                                                    <tr>
                                                        <td> $row->nama_seller </td>
                                                        <td> $row->nama_supplier </td>
                                                        <td> $row->nama_tongkang</td>
                                                        <td> $row->voyage_number </td>
                                                        <td> $row->jml_stok </td>
                                                        <td> $row->tgl_arr</td>
                                                        <td> $row->comm_disch</td>
                                                        <td> $row->compl_disch</td>
                                                        <td> $row->tgl_dep</td>
                                                        <td align='center'>"; if ($row->gm == 1) {
                                                          echo "Accepted";
                                                        } elseif (($row->fin == 2)||($row->sk == 2) || ($row->gm == 2)) {
                                                          echo "Rejected";
                                                        } else {
                                                          echo "Not Avaible";
                                                        }
                                                        echo "
                                                          </td>
                                                        <td align='center'> 
                                                         <a href='C_stok_adj/detail_adj/$id/$id_sm' title='Detail Contract' class='btn btn-primary'>
                                                                <i class='material-icons'>reorder</i>
                                                                <span>Detail</span>
                                                            </a> 
                                                       </td>";

                                                        
                                        $no++;
                                    }
                        
                                    ?>
                                    </tbody>
                                </table>                           
                        </div>
                    </div>

                     <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="button" id="dlt-row" class="btn btn-danger dlt-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->


                        <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="fetched-data"></div>
                                </div>
                                <div class="modal-footer">
                                  <a href='C_stok/edit_stok/$id' class='green btn btn-warning' title='Edit Data'>
                                    <i class='material-icons'>edit</i><span>Edit</span>
                                  </a>

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    

   
</body>
<?php
//--> include data footer
$this->load->view('layout/foot');

?>

 <!-- inline scripts related to this page -->
    <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#dlt-row', function(e){
            window.location = 'C_stok/hapus_data/' +id;
        });
        //.notifikasi hapus
        
        $(document).on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : 'C_stok/detail_lab/'+id,
                    data :  'id='+ id,
                    success : function(data){
                    $('.fetched-data').html(data);//menampilkan data ke dalam modal
                    }
                });
             });

        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "<i class='fa fa-angle-left'><i/>",
              sNext     : "<i class='fa fa-angle-right'><i/>"
             }
           }
        });
        //.dattables
      });
    </script>

    <!-- <script type="text/javascript">
    

            $(document).ready(function(){
            $('#myModal').on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : 'C_stok/detail_lab/'+id,
                    data :  'id_sm='+ id,
                    success : function(data){
                    $('.fetched-data').html(data);//menampilkan data ke dalam modal
                    }
                });
             });
        });
  </script> -->

</html>
