<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                SELLER AGENT
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Input Seller
                                        </b>
                                        
                                    </div>

                                    <form role="form" action="<?= site_url('ore/C_seller/edit_seller/'. $data->id_seller); ?>"  method="post">
                                    <div class="body">
                                       
                                            <label>Seller Name</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="hidden" id="id_seller" name="id_seller" class="form-control" placeholder="Enter your seller name" value="<?= $data->id_seller; ?>">

                                                    <input type="text" id="seller_name" name="nama_seller" class="form-control" placeholder="Enter your seller name" value="<?= $data->nama_seller; ?>">
                                                </div>
                                            </div>
                                            <input class="btn btn-info" type="submit" name="submit" value="Save"></input>
                                             &nbsp; &nbsp;
                                            <a href="<?= site_url('ore/C_seller'); ?>" class="btn btn-default">
                                            Kembali
                                          </a>
                                    </div>
                                    </div>
                                    </form>
                                    <?= $this->session->flashdata("sukses"); ?>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   
</body>
<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>


    </script>
</html>
