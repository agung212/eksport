<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">

            
            <div class="card">
              <form  role="form" action="<?php echo base_url(); ?>index.php/ore/C_report/filter_report" method="post">
                    <div class="body">
                        <div class="row">
                            
                            <div class="col-lg-2 form-control-label" style="width:9.666667%">
                                  <label>Search By</label>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="search" data-live-search="true" class="select2" required>
                                            <option selected disabled>Search</option>
                                            <option value="1">Time Arrived</option>
                                            <option value="2">Commence Discharge</option>
                                            <option value="3">Complete Discharge</option>
                                            <option value="4">Time Depatured</option>
                                            <option value="5">Input Date</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 form-control-label" style="width:9.666667%">
                                  <label>Periode :</label>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 ">
                                <div class="input-group">
                                
                                <div class="form-line">
                                    <input type="date" class="form-control date" name="tgl_awal" placeholder="Ex: 30/07/2016" required>
                                </div>
                            </div>
                            </div>

                            <div class="col-lg-2 form-control-label" style="width:9.666667%">
                                  <label>Until :</label>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="input-group">
                                
                                <div class="form-line">
                                    <input type="date" class="form-control date" name="tgl_akhir" placeholder="Ex: 30/07/2016" required>
                                </div>
                            </div>
                            </div>

                            <div class="col-lg-1 form-control-label" style="width:9.666667%">
                            <input class="btn btn-info" type="submit" name="submit" value="Search"></input>
                            </div>
                    </div>
                </div> 
                </form>
            </div><!-- end card -->


                    <div class="card">
                        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                            <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li class="active">
                                    <i class="material-icons">report</i>
                                    Log Report 
                                 </li>
                            </ul><!-- /.breadcrumb -->
                        </div>
                        <div class="header">
                            <h2>
                                REPORT LOG
                            </h2>
                        </div>

                        <div class="body">
                             
                        <?= $this->session->flashdata("sukses"); ?>
                        <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead >
                                        <tr>
                                            
                                            <th rowspan="2">Seller Agent</th> 
                                            <th>Master Contract</th>
                                            <th>Sales Contract</th>
                                            <th rowspan="2">Swift LC No</th>
                                            
                                             <th colspan="2">Periode Shipment</th>
                                             
                                             <th rowspan="2">Addendum</th>
                                             <th rowspan="2">Action</th>
                                        </tr>
                                        <tr>
                                            <th>Supplier-HKTW</th>
                                            <th>HKTW-VDNI</th>
                                            <th>Start</th>
                                            <th>Finish</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php 
                                                foreach ($kontrak as $row) {

                                                    $id = $row->id_kontrak;
                                                    $id_sm = $row->id_sm;

                                                    echo "
                                                    <tr>
                                                       
                                                        <td> $row->nama_seller  </td>
                                                        <td>
                                                            <a href='C_contract/detail_kontrak/$id' >
                                                             $row->master_kontrak
                                                             </a>
                                                        </td>
                                                        <td> $row->sales_kontrak </td>
                                                        <td> $row->swift_lc</td>
                                                        <td> $row->tgl_awal</td>
                                                        <td> $row->tgl_akhir</td>
                                                       
                                                        <td> $row->addendum </td>
                                                        
                                                        <td align='center'>

                                                            
                                                            <a href='C_report/detail_report/$id/$id_sm' title='Detail Contract' class='btn btn-primary'>
                                                                <i class='material-icons'>reorder</i>
                                                                <span>Detail</span>
                                                            </a> 
                                                        </td>";
                                        
                                    }
                                    ?>
                                    </tbody>
                                </table>                           
                        </div>
                    </div>
                </div>
            </div>
    </section>

 <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Detail Ore Contract</h4>
                              </div>
                              <div class="modal-body"> <div class="fetched-data"></div>
                                <table width="100%" border="1">
                                    <tr>
                                        <td class="b"> Seller Agent </td>
                                        <td ></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Master Contract </td>
                                        <td align="justify" ></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Addendum </td>
                                        <td align="justify"></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Sales Contract </td>
                                        <td align="justify"></td>
                                    </tr>
                                </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Ok </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
   
</body>


<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<!-- inline scripts related to this page -->
 <script type="text/javascript">
      $(document).ready(function(){
        $('#myModal2').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : '<?php echo base_url(); ?>index.php/ore/C_contract/detail_kontrak',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });

        //GET UPDATE
        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/barang/get_barang')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(barang_kode, barang_nama, barang_harga){
                        $('#ModalaEdit').modal('show');
                        $('[name="kobar_edit"]').val(data.barang_kode);
                        $('[name="nabar_edit"]').val(data.barang_nama);
                        $('[name="harga_edit"]').val(data.barang_harga);
                    });
                }
            });
            return false;
        });

        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "Previous ",
              sNext     : "Next"
             }
           }
        });
        //.dattables
    });
 </script>
</html>
