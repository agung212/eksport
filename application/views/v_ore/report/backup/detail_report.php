<!DOCTYPE html>
<html>
<?php
//--> include data header
$this->load->view('layout/head');
//--> include data sidebar
$this->load->view('layout/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class='material-icons'>report</i>
                                    <a href="<?= site_url('ore/C_report'); ?>"> Report Log </a>
                                </li>
                                <li class="active"> Detail Log Report </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                                <i class='material-icons'>assignment</i> DETAIL REPORT LOG
                            </h2>
                        </div>
                        <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" >
                                            </tr>
                                                    <td class="b"> Master Contract </td>
                                                    <td colspan='2'>: <?= $kontrak->master_kontrak; ?></td>
                                                <tr>
                                            <tr>
                                                <td class="b"> Seller Agent </td>
                                                <td colspan='2'>: <?= $kontrak->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b"> Supplier </td>
                                                <td>: <?= $kontrak->nama_supplier; ?> (<?= $kontrak->quantity; ?>)</td>
                                            </tr>
                                                
                                                <tr>
                                                    <td class="b"> Begining Price </td>

                                                   <?php  if ($kontrak->cif_usd > $kontrak->dap_usd) {
                                                           echo "<td>: $ $kontrak->cif_usd </td>"; 
                                                        } else {
                                                           echo "<td>: $ $kontrak->dap_usd</td>";
                                                        } ?>
                                                             
                                                </tr>
                                        </table>
                                    </dl>

                                </div>

                                <div class="col-sm-6">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" >

                                            <tr>
                                                <td class="b" width='30%'> Quantity </td>
                                                <td colspan='4'>: <?= number_format($kontrak->quantity); ?></td> 
                                            </tr>

                                                <tr>
                                                    <td class="b"> Remaining Stock</td>
                                                    <?php 
                                                    $remain_qty = 0;
                                                    foreach ($stok as $row) {
                                                        

                                                        if ($row->intertek_qty != 0 ) {
                                                            $remain_qty = $remain_qty + $row->intertek_qty;
                                                        } elseif ($row->coa_qty != 0 ) {
                                                            $remain_qty = $remain_qty + $row->coa_qty;
                                                        } else{
                                                            $remain_qty = $remain_qty + $row->jml_stok;
                                                        }
                                                        
                                                    }
                                                        $remaining_qty = $kontrak->quantity - $remain_qty;
                                                    ?>
                                                    
                                                    <td colspan='4'>: <?= number_format($remaining_qty); ?></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td class="b"> LC Amount </td>
                                                    <td colspan='4'>: $ <?= number_format($kontrak->lc_amount); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Remaining LC </td>
                                                     <?php 
                                                    $remain_lc = 0;
                                                    foreach ($stok as $row) {

                                                    //Perhitungan

                                                    $coa_amount = $row->coa_qty * $row->price_afterCoa;
                                                    $final_amount = $row->intertek_qty * $row->price_intertek;

                                                    if ($coa_amount != 0) {
                                                        $remain_lc = $remain_lc + $coa_amount;
                                                    } else {
                                                        $remain_lc = $remain_lc + $final_amount;
                                                    }
                                                }   
                                                    $remaining_lc = $kontrak->lc_amount -  $remain_lc;

                                                    ?>
                                                    
                                                    <td colspan='4'>: $ <?= number_format($remaining_lc); ?> </td>
                                                     
                                                </tr>

                                        </table>
                                    </dl>
                                </div>
                            </div>
                                 <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead >
                                        <tr>
                                            
                                            <th>Supplier</th>
                                            <th>Bl Qty</th>
                                            <th>Begining Price</th>
                                            <th>After Coa Qty</th>
                                            <th>After Coa Price</th>
                                            <th>After Coa Amount</th>
                                             <th>Final Qty</th>
                                             <th>Final Disch Unit Price</th>
                                             <th>Final Disch Amount</th>
                                             <th>Lab Result</th>
                                             
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $no = 1;
                                                foreach ($stok as $row) {

                                                    //Perhitungan
                                                    $coa_amount = $row->coa_qty * $row->price_afterCoa;
                                                    $final_amount = $row->intertek_qty * $row->price_intertek;

                                                    $id = $row->id_sm;

                                                    echo "
                                                    <tr>
                                                       
                                                        <td> $row->nama_supplier </td>
                                                        <td> $row->jml_stok</td>";
                                                        if ($row->cif_usd > $row->dap_usd) {
                                                            echo "<td>$ $row->cif_usd </td>";
                                                        } else {
                                                            echo "<td>$ $row->dap_usd </td>";
                                                        }
                                                        echo "
                                                        <td> $row->coa_qty </td>
                                                        <td>
                                                            <a href='#' id='detail-coa'  data-toggle='modal' data-target='#myModal2' data-id='$id'>
                                                            $ 0.00 
                                                            </a>
                                                        </td>
                                                        <td>$ "; echo number_format($coa_amount,2); echo "</td>
                                                        <td>$row->intertek_qty </td>
                                                        <td>$ $row->price_intertek </td>
                                                        <td>$ "; echo number_format($final_amount,2); echo "</td>
                                                        
                                                        
                                                        <td align='center'>

                                                            <a href='#' id='detail-row' class='green btn btn-default'  data-toggle='modal' data-target='#myModal' data-id='$id'>
                                                                    <i class='material-icons'>reorder</i>
                                                            </a>
                                                        </td>
                                                        ";
                                        $no++;
                                    }
                                    ?>
                                        
                                    </tbody>
                                </table>

                                <a href="<?= site_url('ore/C_report'); ?>" class="btn btn-default">
                                    <i class="material-icons">arrow_back</i>
                                     <span>Back</span>
                                </a>
                        </div>
                    </div>
                    </div>

                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="fetched-data"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="myModal2" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Detail After Coa Price</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="fetched-data"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
</body>
<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<script type="text/javascript">
        
        $(function(){
        
        $(document).on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                // $.ajax({
                //     type : 'post',
                //     url : 'C_stok/detail_lab/'+id,
                //     data :  'id='+ id,
                //     success : function(ajaxData){
                //     //$('.fetched-data').html(data);//menampilkan data ke dalam modal
                //     $('.fetched-data').html(ajaxData);
                //     $('.fetched-data').modal('show',{backdrop: 'true'});
                //     }
                // });
             });

        $(document).on('click', '#detail-coa', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : 'C_report/detail_lab_report/'+id,
                    data :  'id='+ id,
                    success : function(data){
                    $('.fetched-data').html(data);//menampilkan data ke dalam modal
                    //$('.fetched-data').html(ajaxData);
                    //$('.fetched-data').modal('show',{backdrop: 'true'});
                    }
                });
             });

        // $(document).on('click','.detail-row',function(e){
        //         e.preventDefault();
        //         $("#myModal").modal('show');
        //         $.post('C_stok/detail_lab/',
        //             {id:$(this).attr('data-id')},
        //             function(html){
        //                 $(".modal-body").html(html);
        //             }   
        //         );
        //     });

      });
      
    </script>

<script type="text/javascript">
   $(document).ready(function () {
   $(".detail-row").click(function(e) {
      var id = $(this).attr("id");
       $.ajax({
             url: "<?php echo site_url('ore/C_stok/detail_lab'.$id);?>",
             type: "GET",
             data : {id: id,},
             success: function (ajaxData){
               $("#Modalcoa").html(ajaxData);
               $("#Modalcoa").modal('show',{backdrop: 'true'});
             }
           });
        });
      });
</script>


</html>

 <!-- <div class="modal fade" id="myModal1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                   <div class="fetched-data"></div>
                                   test
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div> -->
<!-- <a href='C_report/detail_report/$id' title='Detail Contract' class='btn btn-primary'>
                                                                <i class='material-icons'>reorder</i>
                                                                <span>Detail</span>
                                                            </a>  -->