<!DOCTYPE html>
<html>
<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class='material-icons'>report</i>
                                    <a href="<?= site_url('ore/C_report'); ?>"> Report Log </a>
                                </li>
                                <li class="active"> Detail Log Report </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                                <i class='material-icons'>assignment</i> DETAIL REPORT LOG
                            </h2>
                        </div>
                        <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">

                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" >
                                            </tr>
                                                    <td class="b"> Master Contract </td>
                                                    <td colspan='2'>: <?= $kontrak->master_kontrak; ?></td>
                                                <tr>
                                            <tr>
                                                <td class="b"> Seller Agent </td>
                                                <td colspan='2'>: <?= $kontrak->nama_seller; ?></td>
                                            </tr>
                                            <?php
                                                    foreach($supplier as $row)
                                                    { ?>
                                                <tr>
                                                    <?php echo "<td class='b' width='30%'>Supplier</td>
                                                                <td >: $row->nama_supplier (".number_format($row->quantity,2).")</td>"; }?>
                                                </tr>

                                                <tr>
                                                    <td class="b"> Begining Price </td>

                                                   <?php  if ($kontrak->cif_usd > $kontrak->dap_usd) {
                                                           echo "<td>: $ $kontrak->cif_usd </td>";
                                                        } else {
                                                           echo "<td>: $ $kontrak->dap_usd</td>";
                                                        } ?>

                                                </tr>
                                        </table>
                                    </dl>

                                </div>

                                <div class="col-sm-6">

                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" >

                                            <tr>
                                                <td class="b" width='30%'> Quantity </td>
                                                <td colspan='4'>: <?= number_format($kontrak->quantity,2); ?></td>
                                            </tr>

                                                <tr>
                                                    <td class="b"> Remaining Stock</td>
                                                    <?php
                                                    $remain_qty = 0;
                                                    foreach ($stok as $row) {


                                                        if ($row->intertek_qty != 0 ) {
                                                            $remain_qty = $remain_qty + $row->intertek_qty;
                                                        } elseif ($row->coa_qty != 0 ) {
                                                            $remain_qty = $remain_qty + $row->coa_qty;
                                                        } else{
                                                            $remain_qty = $remain_qty + $row->jml_stok;
                                                        }

                                                    }
                                                        $remaining_qty = $kontrak->quantity - $remain_qty;
                                                    ?>

                                                    <td colspan='4'>: <?= number_format($remaining_qty,2); ?></td>

                                                </tr>
                                                <tr>
                                                    <td class="b"> LC Amount </td>
                                                    <td colspan='4'>: $ <?= number_format($kontrak->lc_amount,2); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Remaining LC </td>
                                                     <?php
                                                    $remain_lc = 0;
                                                    foreach ($stok as $row) {

                                                    //Perhitungan

                                                    $coa_amount = $row->coa_qty * $row->price_afterCoa;
                                                    $final_amount = $row->intertek_qty * $row->price_intertek;

                                                    if ($coa_amount != 0) {
                                                        $remain_lc = $remain_lc + $coa_amount;
                                                    } else {
                                                        $remain_lc = $remain_lc + $final_amount;
                                                    }
                                                }
                                                    $remaining_lc = $kontrak->lc_amount -  $remain_lc;

                                                    ?>

                                                    <td colspan='4'>: $ <?= number_format($remaining_lc,2); ?> </td>

                                                </tr>

                                        </table>
                                    </dl>
                                </div>
                            </div>
                                 <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead >
                                        <tr>

                                            <th>Supplier</th>
                                            <th>Bl Qty</th>
                                            <th>Begining Price</th>
                                            <th>After Coa Qty</th>
                                            <th>After Coa Price</th>
                                            <th>After Coa Amount</th>
                                             <th>Final Qty</th>
                                             <th>Final Disch Unit Price</th>
                                             <th>Final Disch Amount</th>
                                             <th>Lab Result</th>

                                        </tr>
                                    </thead>
                                    <tbody id="show_data">

                                        <?php $no = 1;
                                                foreach ($stok as $row) {

                                                    //Perhitungan
                                                    $coa_amount = $row->coa_qty * $row->price_afterCoa;
                                                    $final_amount = $row->intertek_qty * $row->price_intertek;

                                                    $id = $row->id_sm;

                                                    echo "
                                                    <tr>

                                                        <td> $row->nama_supplier </td>
                                                        <td>". number_format($row->jml_stok,2)." </td>";
                                                        if ($row->cif_usd > $row->dap_usd) {
                                                            echo "<td>$ $row->cif_usd </td>";
                                                        } else {
                                                            echo "<td>$ $row->dap_usd </td>";
                                                        }
                                                        echo "
                                                        <td>".number_format($row->coa_qty,2). " </td>";
                                                        if ($row->price_afterCoa<0 || $row->price_afterCoa==NULL) {
                                                            echo "<td>
                                                            
                                                            $ 0.00
                                                           

                                                        </td>"; 
                                                         } else{
                                                        echo "
                                                        <td>
                                                            <a href='#' class='detail-coa' id='detail-coa' data-toggle='modal' data-target='#ModalAdd' data-id='$id'>
                                                            $ ". number_format($row->price_afterCoa,2)."
                                                            </a>

                                                        </td>"; }
                                                        echo "<td>$ "; echo number_format($coa_amount,2); echo "</td>
                                                        <td> ".number_format($row->intertek_qty,2)." </td>
                                                        ";
                                                        if ($row->price_intertek<0 || $row->price_intertek==NULL) {
                                                            echo "<td> $ 0.00 </td>"; 
                                                        } else{
                                                        echo "<td>
                                                            <a href='#' class='detail-in' id='detail-in' data-toggle='modal' data-target='#Modalin' data-id='$id'>
                                                                $ ". number_format($row->price_intertek,2)."
                                                            </a>
                                                        </td>"; }
                                                        echo "
                                                        <td>$ "; echo number_format($final_amount,2); echo "</td>

                                                        <td align='center'>";
                                                        if ($row->coa_qty!=NULL) { 
                                                            echo "
                                                            <a href='#' id='detail-row' class='green btn btn-primary'  data-toggle='modal' data-target='#myModal' data-id='$id'>
                                                                     View
                                                            </a>";
                                                        } else{
                                                            echo "Not Aviabel";
                                                        }
                                                        echo "
                                                        </td>
                                                        ";
                                        $no++;
                                    }
                                    ?>

                                    </tbody>
                                </table>

                                <a href="<?= site_url('ore/C_report'); ?>" class="btn btn-default">
                                    <i class="material-icons">arrow_back</i>
                                     <span>Back</span>
                                </a>
                        </div>
                    </div>
                    </div>

                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                    <div id="fetched-data"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="Modalin" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title header" id="myModalLabel">Detail Intertek Price</h3>
                        </div>
                            <div class="modal-body">
                            <div id="data_intertek"></div>
                            </div>
                        </div>
                        </div>
                    </div>


                    <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title header" id="myModalLabel">Detail After Coa Price</h3>
                        </div>
                            <div class="modal-body">
                            <div id="data_coa"></div>
                            </div>
                        </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
</body>
<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<script type="text/javascript">

        $(function(){

        $(document).on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : '<?php echo base_url()?>index.php/ore/C_report/detail_lab_report/'+id,
                    data :  'id='+ id,
                    success : function(hasil_lab){
                    //$('#myModal').modal('show');
                    $('#fetched-data').html(hasil_lab);//menampilkan data ke dalam modal
                    }
                });
             });

        $(document).on('click', '#detail-coa', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : '<?php echo base_url()?>index.php/ore/C_report/detail_coa_price/'+id,
                    data :  'id='+ id,
                    success : function(coa_price){
                    //$('#myModal').modal('show');
                    $('#data_coa').html(coa_price);//menampilkan data ke dalam modal
                    }
                });
             });

        $(document).on('click', '#detail-in', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : '<?php echo base_url()?>index.php/ore/C_report/detail_in_price/'+id,
                    data :  'id='+ id,
                    success : function(in_price){
                    //$('#myModal').modal('show');
                    $('#data_intertek').html(in_price);//menampilkan data ke dalam modal
                    }
                });
             });

             // $(document).on('click','.detail-coa',function(){
             //         var id=$(this).data('id');
             //         $.ajax({
             //             type : "post",
             //             url  : "<?php echo base_url()?>index.php/ore/C_report/detail_lab_report",
             //             dataType : "JSON",
             //             data : {id:id},

             //             success: function(data){
             //             	$.each(data,function(hasil_lab){
             //                console.log(data.hasil_lab.ni);
             //                 	$('#ModalAdd').modal('show');
             //                   $('#ni_coa').val(data.hasil_lab.ni);
             //                   $('#coa_qty').val(data.hasil_lab.coa_qty);



             //         		});
             //             }
             //         });
             //         return false;
             //     });


        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "Previous ",
              sNext     : "Next"
             }
           }
        });
        //.dattables

      });

    </script>



</html>

 <!-- <div class="modal fade" id="myModal1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                   <div class="fetched-data"></div>
                                   test
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div> -->
<!-- <a href='C_report/detail_report/$id' title='Detail Contract' class='btn btn-primary'>
                                                                <i class='material-icons'>reorder</i>
                                                                <span>Detail</span>
                                                            </a>  -->
