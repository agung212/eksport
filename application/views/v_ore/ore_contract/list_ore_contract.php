<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                            <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li class="active">
                                    <i class="material-icons">account_balance_wallet</i>
                                    Ore Contract 
                                 </li>
                            </ul><!-- /.breadcrumb -->
                        </div>
                        <div class="header">
                            <h2>
                                ORE CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                        <a href="<?php echo site_url('ore/C_contract/tambah_kontrak'); ?>" class="btn btn-primary waves-effect">
                             <i class="material-icons">add</i
                               <span>NEW CONTRACT</span>
                        </a>

                        <br>
                        <br>
                        <?= $this->session->flashdata("sukses"); ?>
                        <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="mytable">
                                    <thead >
                                        <tr>
                                            <th>No</th>
                                            <th>Seller Agent</th>
                                            <th>Master Contract</th>
                                            <th>Addendum</th>
                                            <th>Sales Contract</th>
                                            <th>Swift LC No</th>
                                             <th>Status</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $no = 1;
                                                foreach ($kontrak as $row) {

                                                    $id = $row->id_kontrak;

                                                    echo "
                                                    <tr>
                                                        <td> $no </td>
                                                        <td> $row->nama_seller </td>
                                                        <td> $row->master_kontrak</td>
                                                        <td> $row->addendum </td>
                                                        <td> $row->sales_kontrak </td>
                                                        <td> $row->swift_lc</td>
                                                        <td> $row->status</td>
                                                        <td align='center'>

                                                            
                                                            <a href='C_contract/detail_kontrak/$id' title='Detail Contract' class='btn btn-primary'>
                                                                <i class='material-icons'>reorder</i>
                                                                <span>Detail</span>
                                                            </a> 
                                                        </td>";
                                        $no++;
                                    }
                                    ?>
                                    </tbody>
                                </table>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Detail Ore Contract</h4>
                              </div>
                              <div class="modal-body"> <div class="fetched-data"></div>
                                <table width="100%" border="1">
                                    <tr>
                                        <td class="b"> Seller Agent </td>
                                        <td ></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Master Contract </td>
                                        <td align="justify" ></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Addendum </td>
                                        <td align="justify"></td>
                                    </tr>
                                    <tr>
                                        <td class="b"> Sales Contract </td>
                                        <td align="justify"></td>
                                    </tr>
                                </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Ok </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
   
</body>


<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>

<!-- inline scripts related to this page -->
 <script type="text/javascript">
      $(document).ready(function(){
        $('#myModal2').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : '<?php echo base_url(); ?>index.php/ore/C_contract/detail_kontrak',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });

        //GET UPDATE
        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/barang/get_barang')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(barang_kode, barang_nama, barang_harga){
                        $('#ModalaEdit').modal('show');
                        $('[name="kobar_edit"]').val(data.barang_kode);
                        $('[name="nabar_edit"]').val(data.barang_nama);
                        $('[name="harga_edit"]').val(data.barang_harga);
                    });
                }
            });
            return false;
        });

        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "Previous",
              sNext     : "Next"
             }
           }
        });
        //.dattables
    });
 </script>
</html>
