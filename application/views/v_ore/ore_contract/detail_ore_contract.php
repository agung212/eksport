<!DOCTYPE html>
<html>
<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class='material-icons'>assignment</i>
                                    <a href="<?= site_url('ore/C_contract'); ?>"> Ore Contract </a>
                                </li>
                                <li class="active"> Detail Ore Contract </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                                <i class='material-icons'>assignment</i> DETAIL ORE CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b"> Seller Agent </td>
                                                <td colspan='2'><?= $kontrak->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                    foreach($supplier as $row)
                                                    { ?>
                                                <tr>
                                                    <?php echo "<td class='b' width='30%'>Supplier</td>
                                                                <td width='30%'>$row->nama_supplier</td>
                                                                <td width='30%'>".number_format($row->quantity,2)."</td>"; }?>
                                                </tr>
                                                    <td class="b"> Master Contract </td>
                                                    <td colspan='2'><?= $kontrak->master_kontrak; ?></td>
                                                <tr>
                                                    <td class="b"> Addendum </td>
                                                    <td colspan='2'><?= $kontrak->addendum; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Sales Contract </td>
                                                    <td colspan='2'><?= $kontrak->sales_kontrak; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Periode Shipment </td>
                                                    <td colspan='2'><?= $kontrak->tgl_awal; ?> s/d <?= $kontrak->tgl_akhir; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Swift LC No </td>
                                                    <td colspan='2'><?= $kontrak->swift_lc; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">LC Amount </td>
                                                    <td colspan='2'>$ <?= number_format($kontrak->lc_amount,2); ?>  </td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">Quantity </td>
                                                    <td colspan='2'><?= number_format($kontrak->quantity,2); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Contract Status </td>
                                                    <td colspan='2'><?= $kontrak->status; ?></td>
                                                </tr>
                                            </tr>

                                        </table>
                                    </dl>

                                     <div class="col-md-6">
                                        <a href="<?= site_url('ore/C_contract'); ?>" class="btn btn-default">
                                            <i class="material-icons">arrow_back</i>
                                             <span>Back</span>
                                        </a>
                                    </div>

                                     <div class="col-md-5">
                                   
                                        <a href="<?= site_url('ore/C_contract/addendum/'.$kontrak->id_kontrak); ?>" class="btn btn-primary waves-effect">
                                            <i class="material-icons">add</i>
                                            <span>Create Addendum</span>
                                        </a>
                                   </div>

                                </div>

                                <div class="col-sm-6">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td class="b" width='30%'> Terms </td>
                                                <td colspan='4'><?= $kontrak->terms; ?></td>
                                            </tr>

                                                <tr>
                                                    <td class="b"> Price IDR </td>
                                                    <td colspan='4'>Rp. <?= number_format($kontrak->price_idr,2); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Price USD CIF </td>
                                                    <td colspan='4'> $ <?= $kontrak->cif_usd; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Price USD DAP </td>
                                                    <td colspan='4'> $ <?= $kontrak->dap_usd; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Price SCUSD CIF </td>
                                                    <td colspan='4'> $ <?= $kontrak->cif_sc_usd; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Price SCUSD DAP  </td>
                                                    <td colspan='4'> $ <?= $kontrak->dap_sc_usd; ?> </td>
                                                </tr>
                                                <tr>
                                                    <td class="b">NI  </td>
                                                    <td colspan='2'> <?= $kontrak->ni; ?> </td>
                                                    <td class="b">USD  </td>
                                                    <td colspan='2'> $ <?= $kontrak->ni_usd; ?> </td>
                                                </tr>
                                                 <tr>
                                                    <td class="b">MC  </td>
                                                    <td colspan='2'> <?= $kontrak->mc; ?> </td>
                                                    <td class="b">USD  </td>
                                                    <td colspan='2'> $ <?= $kontrak->mc_usd; ?> </td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Sio / Mgo  </td>
                                                    <td colspan='2'> <?= $kontrak->sio_mgo; ?> </td>
                                                    <td class="b">USD  </td>
                                                    <td colspan='2'> $ <?= $kontrak->sio_usd; ?> </td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Keterangan </td>
                                                    <td colspan='4'><?= $kontrak->keterangan; ?></td>
                                                </tr>

                                        </table>
                                    </dl>                                   
                                    
                             <?php if (empty($stok->master_kontrak) != false) { ?>
                                    <div class="col-md-3">
                                         <a href="<?= site_url('ore/C_contract/edit/'.$kontrak->id_kontrak); ?>" class="btn btn-primary waves-effect">
                                            <i class="material-icons">edit</i>
                                            <span>Edit</span>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $id = $kontrak->id_kontrak;
                                        echo "
                                         <a href='#' id='delete-row' class='delete-row red btn btn-danger' title='Delete' data-toggle='modal' data-target='#myModal' aria-hidden='true' data-id='$id'>
                                            <i class='material-icons'>delete</i>
                                            <span>Delete</span>
                                        </a>"; ?>
                                    </div>
                                <?php }else { ?>
                                    <div class="col-md-3">
                                         
                                    </div>
                                <?php } ?>

                                    <div class="col-md-2">
                                        <?php if ($kontrak->status != "NON AKTIF") {  $id = $kontrak->id_kontrak; 
                                            echo "
                                            <a href='#' id='nonaktif' class='delete-row red btn btn-danger' data-toggle='modal' data-target='#myModal1' aria-hidden='true' data-id='$id'>
                                                <i class='material-icons'>remove_red_eye</i>
                                            <span>NON AKTIF</span>
                                            </a>";
                                         }else { 
                                             
                                         } ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

                     <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <a  href="<?= site_url('ore/C_contract/hapus_data/'.$kontrak->id_kontrak); ?>"  class="btn btn-danger del-row"><i class='material-icons'>delete</i> Delete </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->

                        <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form role="form" action="<?= site_url('ore/C_contract/update_status/'. $kontrak->id_kontrak); ?>"  method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                Your contract will be Non-Aktif! <br/>

                                Are you sure?

                                
                                        <input type="hidden" name="id_kontrak" value="<?= $kontrak->id_kontrak; ?>">
                                        <input type="hidden" name="status" value="NON AKTIF">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="submit" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Non Aktif </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->
   
</body>
<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>

<!-- inline scripts related to this page -->
    <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'hapus_data/' +id;
        });
        //.notifikasi hapus

         $(document).on('click', '#nonaktif', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });

      });
    </script>
</html>
