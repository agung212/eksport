<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class='material-icons'>assignment</i>
                                    <a href="<?= site_url('ore/C_contract'); ?>"> Ore Contract </a>
                                </li>
                                <li class="active"> New Ore Contract </li>
                            </ul><!-- /.breadcrumb -->
                        

                        <div class="header">
                            <h2>
                                NEW ORE CONTRACT
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Input Addendum Contract
                                        </b>
                                        
                                    </div>

                                    <form role="form" action="<?= site_url('ore/C_contract/tambah_kontrak/'); ?>"  method="post">
                                    <div class="body">

                                    <input type="hidden" id="id_kontrak" name="id_kontrak" class="form-control" value="<?= $id_kontrak; ?>">
                                    <input type="hidden" name="user" class="form-control" value="<?= $user; ?>">

                                            <div class="form-group">
                                                <label class="control-label no-padding-right"> Tanggal</label>
                                                <div class="form-line">
                                                    <input type="text" name="tgl_dibuat" class="form-control" value="<?= $tgl; ?>" readonly="true" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Master Contract</label>
                                                <div class="form-line">
                                                    <input type="text" id="master_kontrak" name="master_kontrak" class="form-control" placeholder="Enter Master Contract" required>
                                                </div>
                                            </div>

                                            
                                            <div class="form-group">
                                                <p>
                                                    <b>Seller Name</b>
                                                </p>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="seller_agent" data-live-search="true" class="select2" required>
                                                    <option disabled selected>Pilih Seller Agent</option>
                                                    <?php
                                                        foreach($seller as $row)
                                                        {
                                                            echo "<option value='$row->id_seller'> $row->nama_seller </option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            </div>

                                            <!-- <input type="hidden" name="jml_supplier" id="ke" value="2" />
                                            <div class="form-group">
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn btn-xs btn-warning" onclick="tambahElemenAgt(); return false;" title="Tambah Supplier">
                                                        <i class="material-icons">add</i>              
                                                    </button>
                                                </div>
                                            </div> -->

                                            <div class="col-lg-10 col-md-10 col-xs-10">
                                            <label>Supplier Name </label>  
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>Quantity </label>
                                            <div class="form-group">
                                                 <input type="hidden" name="jml_supplier" id="ke" value="2" />
                                                 <button type="button" class="btn btn-xs btn-warning" onclick="tambahElemenAgt(); return false;" title="Tambah Supplier">
                                                        <i class="material-icons">add</i>              
                                                    </button>
                                                <select class="show-tick" name="supplier[]" data-live-search="true" class="select2">
                                                    <option disabled>Pilih Supplier</option>
                                                    <?php
                                                        foreach($supplier as $row)
                                                        {
                                                            echo "<option value='$row->id_supplier'> $row->nama_supplier </option>";
                                                        }
                                                    ?>
                                                </select>
                                             &nbsp;&nbsp;&nbsp;
                                                <input type="number"  name="quantity[]" requiredaria-required="true" aria-invalid="false">
                                                <div id="form_agt"></div>
                                           
                                            </div> 
                                            </div>        

                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Sales Contract</label>
                                                <div class="form-line">
                                                    <input type="text" id="sales_kontrak" name="sales_kontrak" class="form-control" placeholder="Enter Sales Contract" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Swift LC No</label>
                                                <div class="form-line">
                                                    <input type="text" id="swift_lc" name="swift_lc" class="form-control" placeholder="Enter No Swift LC" required>
                                                </div>
                                            </div>
                                            </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <b>Periode Shipment</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" name="tgl_awal" placeholder="Ex: 30/07/2016" required>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="date" class="form-control date" name="tgl_akhir" placeholder="Ex: 30/07/2016" required>
                                            </div>
                                        </div>
                                        </div>

                                       
                                        <div class="input-group">
                                            <label>Terms</label>
                                                <div class="form-line">
                                                <input type="checkbox" name="terms[]" value="CIF" id="md_checkbox_31" class="filled-in chk-col-light-green"/>
                                                <label for="md_checkbox_31">CIF</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" id="md_checkbox_32" name="terms[]" value="DAP" class="filled-in chk-col-light-green" />
                                                <label for="md_checkbox_32">DAP</label>
                                                </div>
                                        </div>
                                        

                                        <div class="form-group">
                                                <label>Addendum</label>
                                                <div class="form-line">
                                                    <input type="text" id="Addendum" name="addendum" class="form-control" value="Add I" readonly="true">
                                                </div>
                                            </div>
                                    </div>
                                    </div>  

                        <!-- Form Guaranteed -->
                        <div class="card">
                                    <div class="header bg-green">
                                        <b>
                                            Guaranteed
                                        </b>
                                    </div>
                                    <div class="body">       
                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Ni</label>
                                                <div class="form-line">
                                                    <input type="text" id="ni" name="ni" class="form-control" placeholder="Enter Ni" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>USD</label>
                                                <div class="form-line">
                                                    <input type="text" id="ni_usd" name="ni_usd" class="form-control" placeholder="Enter Ni USD" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>MC</label>
                                                <div class="form-line">
                                                    <input type="text" id="mc" name="mc" class="form-control" placeholder="Enter MC" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>USD</label>
                                                <div class="form-line">
                                                    <input type="text" id="mc_usd" name="mc_usd" class="form-control" placeholder="Enter MC USD" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>SiO / MgO</label>
                                                <div class="form-line">
                                                    <input type="text" id="sio_mgo" name="sio_mgo" class="form-control" placeholder="Enter SiO / MgO" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>USD</label>
                                                <div class="form-line">
                                                    <input type="text" id="sio_usd" name="sio_usd" class="form-control" placeholder="Enter SiO / MgO USD" required>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <div class="form-line">
                                                    <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Enter Keterangan">
                                                </div>
                                            </div>
                                    </div>
                                    </div>

                                    <!-- Form Price -->
                        <div class="card">
                                    <div class="header bg-blue">
                                        <b>
                                            Price
                                        </b>
                                    </div>
                                    <div class="body">  
                                        <div class="col-md-12">
                                        <b>IDR</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="price_idr" class="form-control credit-card" placeholder="Rp. 1000.000,00" required onkeydown="return limitCharacter(event);" id="Price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <b>USD</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="txt1" name="cif_usd"  onkeyup="sum();" class="form-control money-dollar" placeholder="CIF 00,00 $" required onkeydown="return limitCharacter(event);"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="txt2" name="dap_usd"  onkeyup="sum2();" class="form-control money-dollar" placeholder="CIF 00,00 $" required onkeydown="return limitCharacter(event);"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <b>SC USD</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="txt3" name="cif_sc_usd" class="money-dollar" placeholder="Ex: 99,99 $ CIF" readonly="true" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <input type="hidden" name="status" value="AKTIF"></input>
                                    <div class="col-md-6">
                                        <br>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="txt4" name="dap_sc_usd" class="money-dollar" placeholder="Ex: 99,99 $ DAP" readonly="true" />
                                            </div>
                                        </div>
                                    </div>
                                            <input class="btn btn-info" type="submit" name="submit" value="Save"></input>
                                             &nbsp; &nbsp;
                                            <a href="<?= site_url('ore/C_contract'); ?>" class="btn btn-default">
                                            Kembali
                                          </a>
                                    </div>
                                    </div>
                                    </form>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>

<!-- inline scripts related to this page -->
<script type="text/javascript">
       

        $('.select2').css('width','300px').select2({allowClear:false});

        //Jumlah supplier
            $("#alert").hide();
            function tambahElemenAgt() {
              var ke = document.getElementById("ke").value;

              if(ke > 20)
              {
                $("#alert").show();
              }
              else
              {
                var stre;
                    stre = "<div id='arow" + ke + "'>" +
                                "<br><div class='col-md-4'>" +
                                                    "<label></label>" +
                                                     " <select name='supplier[]' class='select2  show-tick'>" +
                                                     "     <option> Pilih Supplier Ke-"+ ke +" </option> <?php
                                                                    foreach($supplier as $row)
                                                                    {
                                                                        echo "<option value='$row->id_supplier'> $row->nama_supplier </option>";
                                                                    }
                                                                ?>" +
                                                     "  </select>" +
                                            "</div>" +
                                            "<div class='col-md-6'>" +
                                                 "<label></label>" +
                                                  "<input type='number'  name='quantity[]' requiredaria-required='true' aria-invalid='false' placeholder='Enter Quantity' >" +   
                                           
                                             " <a href='#' onclick='hapusElemen(\"#arow" + ke + "\"); return false;' class='btn btn-sm btn-danger' title='Hapus supplier ke-"+ ke +"'> <i class='material-icons'>remove</i> </a>"+
                                             "</div>" +
                         "</div>";

                $("#form_agt").append(stre);
                ke = (ke-1) + 2;
                document.getElementById("ke").value = ke;

                $("#alert").hide();
              }
            }

            function hapusElemen(ke) {
                $("#alert").hide();
              $(ke).remove();

              var ke2 = document.getElementById("ke").value;
              ke3 = ke2-1;
              document.getElementById("ke").value = ke3;
            }
            //.jumlah supplier

           
      
        function sum() {
              var txtFirstNumberValue = document.getElementById('txt1').value;
              var txtSecondNumberValue = 1;
              var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
              if (!isNaN(result)) {
                 document.getElementById('txt3').value = result;
              }
        }

        function sum2() {
              var txtFirstNumberValue = document.getElementById('txt2').value;
              var txtSecondNumberValue = 1;
              var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
              if (!isNaN(result)) {
                 document.getElementById('txt4').value = result;
              }
        }
</script>
<script type="text/javascript">
/* Dengan Rupiah */
var price = document.getElementById('Price');
price.addEventListener('keyup', function(e)
{
  price.value = formatRupiah(this.value, 'Rp. ');
});

var amount = document.getElementById('amount');
amount.addEventListener('keyup', function(e)
{
  amount.value = formatRupiah(this.value, '$. ');
});


/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split   = number_string.split(','),
    sisa    = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 190 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>

</body>

</html>
