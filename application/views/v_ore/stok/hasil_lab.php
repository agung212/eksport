<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                         <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class="material-icons">layers</i>
                                    <a href="<?= site_url('ore/C_stok'); ?>"> Stock </a>
                                </li>
                                <li class="active"><i class="material-icons">functions</i> Lab Result </li>
                            </ul><!-- /.breadcrumb -->
                        <div class="header">
                            <h2>
                                LAB RESULT
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-blue">
                                        <b>
                                            Data Stock
                                        </b>
                                        
                                    </div>

                                <div class="body">
                                    <div class="row">
                                    <div class="col-sm-12">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%">
                                            <tr>
                                                <td class="b" width="30%"> Seller Agent </td>
                                                <td >:  &nbsp; <?= $stok->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                
                                                <tr>
                                                   <td class="b"> Supplier </td>
                                                    <td >:  &nbsp; <?= $stok->nama_supplier; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Tug Boat / Barge </td>
                                                    <td >:  &nbsp; <?= $stok->nama_tongkang; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Bl Quantity </td>
                                                    <td >:  &nbsp; <?= $stok->jml_stok; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Time Arrived </td>
                                                    <td >:  &nbsp; <?= $stok->tgl_arr; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Commence Discharge </td>
                                                    <td >:  &nbsp; <?= $stok->comm_disch; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Complete Discharge </td>
                                                    <td >:  &nbsp; <?= $stok->compl_disch; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Time Depatured </td>
                                                    <td >:  &nbsp; <?= $stok->tgl_dep; ?></td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </dl>
                                </div>

                                </div>
                            </div> 
                        </div><!-- end card -->

                        <form role="form" action="<?= site_url('ore/C_stok/hasil_lab/'.$stok->id_sm); ?>"  method="post" enctype='multipart/form-data'>
                        <input type="hidden" name="id_stok" class="form-control" value="<?= $stok->id_sm; ?>" >
                        <input type="hidden" name="id_sm" class="form-control" value="<?= $stok->id_sm; ?>" >
                         <input type="hidden" name="stat_lab" class="form-control" value="1" readonly="true" >
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            After Coa
                                        </b>
                                    </div>

                                    <div class="body">

                                    <input type="hidden" name="ni_ktr" class="form-control" value="<?= $stok->ni; ?>" >
                                    <input type="hidden" name="ni_usd" class="form-control" value="<?= $stok->ni_usd; ?>" >
                                    <input type="hidden" name="mc_ktr" class="form-control" value="<?= $stok->mc; ?>" >
                                    <input type="hidden" name="mc_usd" class="form-control" value="<?= $stok->mc_usd; ?>" >
                                    <input type="hidden" name="sio_mgo_ktr" class="form-control" value="<?= $stok->sio_mgo; ?>" >
                                    <input type="hidden" name="sio_usd" class="form-control" value="<?= $stok->sio_usd; ?>" >
                                    <input type="hidden" name="cif_usd" class="form-control" value="<?= $stok->cif_usd; ?>" >
                                    <input type="hidden" name="dap_usd" class="form-control" value="<?= $stok->dap_usd; ?>" >
                
                                             <div class="form-group">
                                                <label>After COA Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="coa_qty" class="form-control" placeholder="Enter Coa Quantity" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="ni" class="form-control" placeholder="Enter Ni" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="fe" class="form-control" placeholder="Enter Fe" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="sio" class="form-control" placeholder="Enter SiO2" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mgo" class="form-control" placeholder="Enter MgO" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="aio" class="form-control" placeholder="Enter Al2O3" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mc" class="form-control" placeholder="Enter Mc" required value="0" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="dokumen_coa" class="form-control" accept="application/pdf" >

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>
                                </div>
                            </div>
                        </div> <!-- end card -->

                            <div class="col-sm-6">
                            <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Intertek Lab
                                        </b>
                                        
                                    </div>

                                <div class="body">
                                    <div class="form-group">
                                                <label>Intertek Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="intertek_qty" class="form-control" placeholder="Enter Intertek Quantity" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_ni" class="form-control" placeholder="Enter Ni" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_fe" class="form-control" placeholder="Enter Fe" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_sio" class="form-control" placeholder="Enter SiO2" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mgo" class="form-control" placeholder="Enter MgO" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_aio" class="form-control" placeholder="Enter Al2O3" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mc" class="form-control" placeholder="Enter Mc" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="in_dokumen" class="form-control" accept="application/pdf" >

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>

                                </div>
                            </div>
                                <a href="<?= site_url('ore/C_stok'); ?>" class="btn btn-default">Kembali</a>
                                &nbsp; &nbsp;&nbsp; &nbsp;
                                 <input class="btn btn-info full-right" type="submit" name="submit" value="Submit">
                            </div>
                            </div> <!-- end card -->
                            </form>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<!-- inline scripts related to this page -->
<script type="text/javascript">
       

        $('.select2').css('width','310px').select2({allowClear:false});

        //Jumlah anggota
            $("#alert").hide();
            function tambahElemenAgt() {
              var ke = document.getElementById("ke").value;

              if(ke > 20)
              {
                $("#alert").show();
              }
              else
              {
                var stre;
                    stre = "<p id='arow" + ke + "'>" +
                                 " <br> <select name='supplier[]' class='select2 form-control show-tick form-line'>" +
                                 "     <option> Pilih Supplier Ke-"+ ke +" </option> <?php
                                                foreach($supplier as $row)
                                                {
                                                    echo "<option value='$row->id_supplier'> $row->nama_supplier </option>";
                                                }
                                            ?>" +
                                 "  </select><input type='text' name='quantity[]' class='form-control form-line col-md-4' placeholder='Enter Quantity'>" +
                                 
                                 "  <a href='#' onclick='hapusElemen(\"#arow" + ke + "\"); return false;' class='btn btn-sm btn-danger' title='Hapus supplier ke-"+ ke +"'> <i class='material-icons'>remove</i> </a>" +
                                 "</p>";

                $("#form_agt").append(stre);
                ke = (ke-1) + 2;
                document.getElementById("ke").value = ke;

                $("#alert").hide();
              }
            }

            function hapusElemen(ke) {
                $("#alert").hide();
              $(ke).remove();

              var ke2 = document.getElementById("ke").value;
              ke3 = ke2-1;
              document.getElementById("ke").value = ke3;
            }
            //.jumlah anggota

      
    </script>

    <script type="text/javascript">
/* Dengan Rupiah */
var ni = document.getElementById('ni');
ni.addEventListener('keyup', function(e)
{
  ni.value = formatRupiah(this.value);
});

var amount = document.getElementById('amount');
amount.addEventListener('keyup', function(e)
{
  amount.value = formatRupiah(this.value, '$. ');
});


/* Fungsi */
function formatRupiah(bilangan, prefix)
{

  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split   = number_string.split(','),
    sisa    = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? '$. ' + rupiah : '');
}

function limitCharacter(event)
{
  key = event.which || event.keyCode;
  if ( key != 190 // Comma
     && key != 8 // Backspace
     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
     && (key < 48 || key > 57) // Non digit
     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
  {
    event.preventDefault();
    return false;
  }
}
</script>
</body>

</html>
