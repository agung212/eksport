<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class="material-icons">layers</i>
                                    <a href="<?= site_url('ore/C_stok'); ?>"> Stock </a>
                                </li>
                                <li class="active"><i class="material-icons">edit</i> Adjustmen Stock </li>
                            </ul><!-- /.breadcrumb -->
                        <div class="header">
                            <h2>
                                ADJUSTMEN STOCK
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-blue">
                                        <b>
                                            Adjustment Stock
                                        </b>
                                        
                                    </div>

                                    <form role="form" action="<?= site_url('ore/C_stok/adj_stok/'.$data->id_sm); ?>"  method="post" enctype='multipart/form-data'>
                                    <div class="row clearfix">
                                    <div class="body">

                                    <input type="hidden" name="id_sm" class="form-control" value="<?= $data->id_sm; ?>">
                                     <input type="hidden" name="stat_adj" class="form-control" value="1">

                                      <input type="hidden" name="ni_ktr" class="form-control" value="<?= $stok->ni; ?>" >
                                    <input type="hidden" name="ni_usd" class="form-control" value="<?= $stok->ni_usd; ?>" >
                                    <input type="hidden" name="mc_ktr" class="form-control" value="<?= $stok->mc; ?>" >
                                    <input type="hidden" name="mc_usd" class="form-control" value="<?= $stok->mc_usd; ?>" >
                                    <input type="hidden" name="sio_mgo_ktr" class="form-control" value="<?= $stok->sio_mgo; ?>" >
                                    <input type="hidden" name="sio_usd" class="form-control" value="<?= $stok->sio_usd; ?>" >

                                     <input type="hidden" name="cif_usd" class="form-control" value="<?= $stok->cif_usd; ?>" >
                                    <input type="hidden" name="dap_usd" class="form-control" value="<?= $stok->dap_usd; ?>" >
                                            
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                             <div class="form-group">
                                                <p>
                                                    <b>Seller Agent</b>
                                                </p>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="seller_agent" class="select2" required id="seller" data-live-search="true">
                                                    <option >Pilih Seller Agent</option>
                                                    <?php
                                                        foreach($seller as $row)
                                                        {   
                                    
                                                        if ($data->seller_agent == $row->seller_agent) {
                                                            echo '<option selected value="'.$row->seller_agent.'">'.$row->nama_seller.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->seller_agent.'">'.$row->nama_seller.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <p>
                                                    <b>Supplier Name</b>
                                                </p>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="supplier" class="select2" id="supplier" >
                                                     <option >Pilih Supplier</option>
                                                    <?php
                                                        foreach($supplier as $row)
                                                        {   
                                    
                                                        if ($data->supplier == $row->supplier) {
                                                            echo '<option selected value="'.$row->supplier.'">'.$row->nama_supplier.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->supplier.'">'.$row->nama_supplier.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                    
                                                </select>
                                                    
                                                </select>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                             <div class="form-group">
                                                <label>Periode Shipment </label>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="master_kontrak" data-live-search="false" class="select2" readonly="true" id="periode">
                                                    <?php
                                                        foreach($supplier as $row)
                                                        {   
                                    
                                                        if ($data->supplier == $row->supplier) {
                                                            echo '<option selected value="'.$row->id_kontrak.'">'.$row->tgl_awal." s/d ".$row->tgl_akhir.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->id_kontrak.'">'.$row->tgl_awal." s/d ".$row->tgl_akhir.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                </select>
                                                </select>
                                                </div>
                                            </div>
                                            </div>                                                     

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Tug Boat / Barge</label>
                                                <div class="form-line">
                                                    <input type="text" name="nama_tongkang" class="form-control" placeholder="Enter Tug Boat / Barge" value="<?= $data->nama_tongkang; ?>" required >
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Voyage Number</label>
                                                <div class="form-line">
                                                    <input type="text" name="voyage_number" class="form-control" placeholder="Enter Voyage Number" value="<?= $data->voyage_number; ?>" required="true">
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>BL Quantity</label>
                                                <div class="form-line">
                                                    <input type="text" name="jml_stok" class="form-control" placeholder="Enter BL Quantity" value="<?= $data->jml_stok; ?>" required="true">
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>BL Number</label>
                                                <div class="form-line">
                                                    <input type="text" name="bl_number" class="form-control" placeholder="Enter BL Number" value="<?= $data->bl_number; ?>" required="true">
                                                </div>
                                            </div>
                                            </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Time Arrived</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="tgl_arr" placeholder="Ex: 30/07/2016" value="<?= $data->tgl_arr; ?>" requaired>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Commence Discharge</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="comm_disch" placeholder="Ex: 30/07/2016" value="<?= $data->comm_disch; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Complete Discharge</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="compl_disch" placeholder="Ex: 30/07/2016" value="<?= $data->compl_disch; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                         <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Time Depatured</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="tgl_dep" placeholder="Ex: 30/07/2016" value="<?= $data->tgl_dep; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                       <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Admin</label>
                                                <div class="form-line">
                                                    <input type="text" name="user" class="form-control" value="<?= $user; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        
                        <h4 class="header">
                            Adjustment Lab
                        </h4>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="header bg-red">
                                       
                                        <b>
                                            After Coa
                                        </b>
                                    </div>

                                    <div class="body">
                
                                             <div class="form-group">
                                                <label>After COA Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="coa_qty" class="form-control" placeholder="Enter Coa Quantity" value="<?= $hasil_lab->coa_qty; ?>" required >
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="ni" class="form-control" placeholder="Enter Ni" value="<?= $hasil_lab->ni; ?>" required>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="fe" class="form-control" placeholder="Enter Fe" required value="<?= $hasil_lab->fe; ?>" >
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="sio" class="form-control" placeholder="Enter SiO2" required value="<?= $hasil_lab->sio; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mgo" class="form-control" placeholder="Enter MgO" required value="<?= $hasil_lab->mgo; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="aio" class="form-control" placeholder="Enter Al2O3" required value="<?= $hasil_lab->aio; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mc" class="form-control" placeholder="Enter Mc" required value="<?= $hasil_lab->mc; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="dokumen_coa" class="form-control" value="<?= $hasil_lab->picture; ?>">

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>
                                </div>
                            </div>
                        </div> <!-- end card -->

                        <div class="col-sm-6">
                            <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Intertek Lab
                                        </b>
                                        
                                    </div>

                                <div class="body">
                                    <div class="form-group">
                                                <label>Intertek Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="intertek_qty" class="form-control" placeholder="Enter Intertek Quantity" required  value="<?= $intertek->intertek_qty; ?>" >
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_ni" class="form-control" placeholder="Enter Ni" required value="<?= $intertek->ni; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_fe" class="form-control" placeholder="Enter Fe" required value="<?= $intertek->fe; ?>" >
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_sio" class="form-control" placeholder="Enter SiO2" required value="<?= $intertek->sio; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mgo" class="form-control" placeholder="Enter MgO" required  value="<?= $intertek->mgo; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_aio" class="form-control" placeholder="Enter Al2O3" required value="<?= $intertek->aio; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mc" class="form-control" placeholder="Enter Mc" required value="<?= $intertek->mc; ?>">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="in_dokumen" class="form-control" value="<?= $intertek->picture; ?>">

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>

                                </div>
                            </div>
                                <a href="<?= site_url('ore/C_stok'); ?>" class="btn btn-default">Kembali</a>
                                &nbsp; &nbsp;&nbsp; &nbsp;
                                 <input class="btn btn-info full-right" type="submit" name="submit" value="Submit">
                            </div>
                            </div> <!-- end card -->
                            </form>

                                    
                                    </form>
                                    <?= $this->session->flashdata("sukses"); ?>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<script type="text/javascript">
$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
  
  $("#seller").change(function(){ // Ketika user mengganti atau memilih data provinsi
    $("#supplier").hide(); // Sembunyikan dulu combobox kota nya
   
  
    $.ajax({
      type: "POST", // Method pengiriman data bisa dengan GET atau POST
       data: {seller_agent : $("#seller").val()},
      url: "<?php echo site_url('ore/C_stok/get_supplier');?>", // Isi dengan url/path file php yang dituju
      // data yang akan dikirim ke file yang dituju
      dataType: "json",
     
      success: function(response){ // Ketika proses pengiriman berhasil
        
        // set isi dari combobox kota
        // lalu munculkan kembali combobox kotanya
        $("#supplier").html(response.data_supplier).show();
        $('[name="id_agent"]').val($("#seller").val());
        
      },
      error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
        alert(thrownError); // Munculkan alert error
      }
    });
    });
});

    </script>

    <script type="text/javascript">
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
          
          $("#supplier").change(function(){ // Ketika user mengganti atau memilih data provinsi
            $("#periode").hide(); // Sembunyikan dulu combobox kota nya
           
          
            $.ajax({
              type: "POST", // Method pengiriman data bisa dengan GET atau POST
               data: {supplier : $("#supplier").val()},
              url: "<?php echo site_url('ore/C_stok/get_periode');?>", // Isi dengan url/path file php yang dituju
              // data yang akan dikirim ke file yang dituju
              dataType: "json",
             
              success: function(response){ // Ketika proses pengiriman berhasil
                
                // set isi dari combobox kota
                // lalu munculkan kembali combobox kotanya
                $("#periode").html(response.data_periode).show();
                $('[name="id_supplier"]').val($("#supplier").val());
                
                
              },
              error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(thrownError); // Munculkan alert error
              }
            });
            });
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
          
          $("#supplier").change(function(){ // Ketika user mengganti atau memilih data provinsi
            $("#id").hide(); // Sembunyikan dulu combobox kota nya
           
          
            $.ajax({
              type: "POST", // Method pengiriman data bisa dengan GET atau POST
               data: {supplier : $("#supplier").val()},
              url: "<?php echo site_url('ore/C_stok/get_id');?>", // Isi dengan url/path file php yang dituju
              // data yang akan dikirim ke file yang dituju
              dataType: "json",
             
              success: function(response){ // Ketika proses pengiriman berhasil
                
                // set isi dari combobox kota
                // lalu munculkan kembali combobox kotanya
                $("#id").html(response.data_id).show();
                $('[name="id_ktr_kecil"]').val($("#id").val());
                
                
              },
              error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(thrownError); // Munculkan alert error
              }
            });
            });
        });

    </script>
</body>

</html>
