<!DOCTYPE html>
<html>
<head>
	<title>
		
	</title>
</head>
<body>
	
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%">
                                        	<thead>
                                        		<th colspan="2" style="font:20px">After Coa RESULT</th>
                                        	</thead>
                                        	<tbody>
                                            <tr>
                                                <td class="b" width="20%"> %NI </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->ni; ?></td>
                                                <td class="b" width="20%"> %Al2O3 </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->aio; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %FE </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->fe; ?></td>
                                                <td class="b" width="20%"> %SiO2 / MgO </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->sio_mgo; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %SiO2 </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->sio; ?></td>
                                                 
                                                <td class="b" width="20%"> %Fe / Ni </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->fe_ni; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %MgO </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->mgo; ?></td>

                                                <td class="b" width="20%"> %Mc </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->mc; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> After COA Qty </td>
                                                <td width="20%">:  &nbsp; <?= $hasil_lab->coa_qty; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> Dokumen </td>
                                                <td width="20%">: <a href="<?= site_url('ore/C_stok/view_pdf/'.$hasil_lab->picture); ?>" target="blank"><?= $hasil_lab->picture; ?> </a></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <br>
                                        <br>
                                        <table width="100%">
                                        	<thead>
                                        		<th colspan="2" style="font:20px">Intertek RESULT</th>
                                        	</thead>
                                        	<tbody>
                                            <tr>
                                                <td class="b" width="20%"> %NI </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->ni; ?></td>
                                                <td class="b" width="20%"> %Al2O3 </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->aio; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %FE </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->fe; ?></td>
                                                <td class="b" width="20%"> %SiO2 / MgO </td>
                                                <td width="20%">:  &nbsp; <?= number_format($intertek->sio_mgo,2); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %SiO2 </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->sio; ?></td>
                                                <td class="b" width="20%"> %Fe / Ni </td>
                                                <td width="20%">:  &nbsp; <?= number_format($intertek->fe_ni,2); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> %MgO </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->mgo; ?></td>

                                                <td class="b" width="20%"> %Mc </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->mc; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> Intertek Qty </td>
                                                <td width="20%">:  &nbsp; <?= $intertek->intertek_qty; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="b" width="20%"> Dokumen </td>
                                                <td width="20%">: <a href="<?= site_url('ore/C_stok/view_pdf/'.$intertek->picture); ?>" target="blank"><?= $intertek->picture; ?> </a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </dl>
      
</body>
</html>