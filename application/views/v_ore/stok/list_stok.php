<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-14 col-md-14 col-sm-14 col-xs-14">
                    <div class="card">
                         <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li class="active">
                                    <i class="material-icons">layers</i>
                                     List Stock 
                                </li>
                            </ul><!-- /.breadcrumb -->

                        <div class="header">
                            <h2>
                                Stock
                            </h2>
                        </div>
                        <div class="body">
                        <a href="<?php echo site_url('ore/C_stok/tambah_stok'); ?>" class="btn btn-primary waves-effect">
                             <i class="material-icons">add</i
                               <span>NEW STOCK</span>
                        </a>

                        <br>
                        <br>
                        <?= $this->session->flashdata("sukses"); ?>
                        <div class="table-responsive">
                               <table class="table table-bordered table-striped table-hover" id="mytable">
                                    <thead >
                                        <tr>
                                            <th>No</th>
                                            <th>Seller Agent</th>
                                            <th>Supplier Name</th>
                                            <th>Tug Boat/Barge</th>
                                            <th>Voyage</th>
                                            <th>BL Qty</th>
                                            <th>BL Number</th>
                                            <th>Input Date</th>
                                             <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                                foreach ($stok as $row) {

                                                    $id = $row->id_sm;

                                                    $stok = $row->jml_stok;

                                                    echo "
                                                    <tr>
                                                        <td> $no </td>
                                                        <td> $row->nama_seller </td>
                                                        <td> $row->nama_supplier </td>
                                                        <td> $row->nama_tongkang</td>
                                                        <td> $row->voyage_number </td>
                                                        <td>".number_format($row->jml_stok)."</td>
                                                        <td> $row->bl_number</td>
                                                        <td> $row->tgl_input</td>
                                                        <td align='center'>";

                                                        //Kondisi Adjusment
                                                        $tgl=date('Y-m-05', strtotime('+1 month ', strtotime($row->tgl_input)));
                                                        //echo $tgl;
                                                        $tgl_sek=date('Y-m-d');

                                                        
                                                      if ($tgl_sek > $tgl) {
                                                        echo "<a href='C_stok/adj_stok/$id' class='green btn btn-primary' title='Add Lab Result'>
                                                                <i class='material-icons'>add</i>
                                                                <span>Adjusment</span>
                                                            </a>";
                                                      } else{
                                                        
                                                          if ($row->stat_lab != NULL) {
                                                              echo "
                                                              <a href='#' id='detail-row' class='detail-row green btn btn-default'  data-toggle='modal' data-target='#myModal' data-id='$id'>
                                                                    <i class='material-icons'>remove_red_eye</i>
                                                                </a>
                                                                <a href='C_stok/edit_stok/$id' class='green btn btn-warning' title='Edit Data'>
                                                                    <i class='material-icons'>edit</i>
                                                                </a>
                                                               
                                                                <a href='#' id='delete-row' class='delete-row red btn btn-danger' title='Delete' data-toggle='modal' data-target='#delModal' aria-hidden='true' data-id='$id'>
                                                                    <i class='material-icons'>delete</i>
                                                                </a>";
                                                            }else {
                                                              echo "
                                                             
                                                                <a href='C_stok/hasil_lab/$id' class='green btn btn-primary' title='Add Lab Result'>
                                                                    <i class='material-icons'>add</i>
                                                                </a>
                                      
                                                                 <a href='C_stok/edit_stok/$id' class='green btn btn-warning' title='Edit Data'>
                                                                    <i class='material-icons'>edit</i>
                                                                </a>
                                                               
                                                                <a href='#' id='delete-row' class='delete-row red btn btn-danger' title='Delete' data-toggle='modal' data-target='#delModal' aria-hidden='true' data-id='$id'>
                                                                    <i class='material-icons'>delete</i>
                                                                </a>
                                                            </td>"; }
                                        $no++;
                                    }
                                  }
                                    ?>
                                    </tbody>
                                </table>                           
                        </div>
                    </div>

                     <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?

                                <form role="form" action="<?= site_url('ore/C_stok/hapus_data/'); ?>"  method="post"> 

                                <input type="hidden" name="quantity" id="qty"  class="form-control"  placeholder="Insert Barge">
                                <input type="hidden" name="id_sm" id="id_sm"  class="form-control"  placeholder="Insert Barge">
                                <input type="hidden" name="id_kontrak" id="id_kontrak"  class="form-control"  placeholder="Insert Barge">
                                <input type="hidden" name="seller" id="seller"  class="form-control"  placeholder="Insert Barge">
                                <input type="hidden" name="supplier" id="supplier"  class="form-control"  placeholder="Insert Barge">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="submit" id="dlt-row" class="btn btn-danger dlt-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->


                        <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title header">Lab Result</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="fetched-data"></div>
                                </div>
                                <div class="modal-footer">
                                  <!-- <a href='C_stok/edit_stok/$id' class='green btn btn-warning' title='Edit Data'>
                                    <i class='material-icons'>edit</i><span>Edit</span>
                                  </a> -->
                                    <button type="button" id="delete" class="btn btn-danger delete"> Delete </button>

                                    <button type="button" id="edit" class="btn btn-warning edit"> Edit </button>

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    

   
</body>
<?php
//--> include data footer
$this->load->view('layout/foot');

?>

 <!-- inline scripts related to this page -->
    <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');

            $.ajax({
                         type : "post",
                         url  : "<?php echo base_url()?>index.php/ore/C_stok/get_sm",
                         dataType : "JSON",
                         data : {id:id},

                         success: function(data){
                          $.each(data,function(stok){
                            console.log(data.stok.jml_stok);
                              $('#ModalaAdd').modal('show');
                               $('#qty').val(data.stok.jml_stok);
                               $('#id_kontrak').val(data.stok.id_kontrak);
                               $('#seller').val(data.stok.seller_agent);
                               $('#supplier').val(data.stok.supplier);
                               $('#id_sm').val(data.stok.id_sm);
                        });
                         }
                     });
                     return false;
        });

        $(document).on('click', '#dlt-row', function(e){
            window.location = 'C_stok/hapus_data/' +id;
        });
        //.notifikasi hapus

        //Edit Hasil Lab
        $(document).on('click', '#edit', function(e){
            window.location = 'C_stok/edit_hasil_lab/' +id;
        });

        $(document).on('click', '#delete', function(e){
            window.location = 'C_stok/hapus_hasil_lab/' +id;
        });
        //.notifikasi hapus
        
        $(document).on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : 'C_stok/detail_lab/'+id,
                    data :  'id='+ id,
                    success : function(data){
                    $('.fetched-data').html(data);//menampilkan data ke dalam modal
                    }
                });
             });

        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "Previous ",
              sNext     : "Next"
             }
           }
        });
        //.dattables
      });
    </script>

    <!-- <script type="text/javascript">
    

            $(document).ready(function(){
            $('#myModal').on('click', '#detail-row', function (e) {
                e.preventDefault();
                id = $(this).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type : 'post',
                    url : 'C_stok/detail_lab/'+id,
                    data :  'id_sm='+ id,
                    success : function(data){
                    $('.fetched-data').html(data);//menampilkan data ke dalam modal
                    }
                });
             });
        });
  </script> -->

</html>
