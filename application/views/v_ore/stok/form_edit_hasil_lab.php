<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

<style type="text/css">
    .u {text-decoration: underline}
    .b {font-weight: bold}
    .i {font-style: italic}
    .c {text-align: center}
    .r {text-align: right;}
    .j {text-align: justify}

    .pad-3 {padding: 5px}
    .bot-bor {border-bottom: 1px black solid}

    .bg-color  {background-color: #f1f6a3}
    .bg-color5 {background-color: #1faeff}

    td {padding: 5px}
</style>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class="material-icons">layers</i>
                                    <a href="<?= site_url('ore/C_stok'); ?>"> Stock </a>
                                </li>
                                <li class="active"><i class="material-icons">edit</i> Edit Hasil Lab </li>
                            </ul><!-- /.breadcrumb -->
                        <div class="header">
                            <h2>
                                EDIT HASIL LAB
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-blue">
                                        <b>
                                            Data Stock
                                        </b>
                                        
                                    </div>

                                   <!--  <form role="form" action="<?= site_url('ore/C_stok/edit_stok/'.$data->id_sm); ?>"  method="post" enctype='multipart/form-data'>
                                    <div class="row clearfix">
                                    <div class="body"> -->

                                    <div class="body">
                                    <div class="row">
                                    <div class="col-sm-12">
                                        
                                        <dl id="dt-list-1" class="dl-horizontal">
                                        <table width="100%">
                                            <tr>
                                                <td class="b" width="30%"> Seller Agent </td>
                                                <td >:  &nbsp; <?= $stok->nama_seller; ?></td>
                                            </tr>
                                            <tr>
                                                
                                                <tr>
                                                   <td class="b"> Supplier </td>
                                                    <td >:  &nbsp; <?= $stok->nama_supplier; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Tug Boat / Barge </td>
                                                    <td >:  &nbsp; <?= $stok->nama_tongkang; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Bl Quantity </td>
                                                    <td >:  &nbsp; <?= $stok->jml_stok; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Time Arrived </td>
                                                    <td >:  &nbsp; <?= $stok->tgl_arr; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Commence Discharge </td>
                                                    <td >:  &nbsp; <?= $stok->comm_disch; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b"> Complete Discharge </td>
                                                    <td >:  &nbsp; <?= $stok->compl_disch; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="b">Time Depatured </td>
                                                    <td >:  &nbsp; <?= $stok->tgl_dep; ?></td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </dl>
                                </div>

                                </div>
                                </div>
                            </div> <!--  End Cards -->
                        
                    <form role="form" action="<?= site_url('ore/C_stok/edit_hasil_lab/'.$stok->id_sm); ?>"  method="post" enctype='multipart/form-data'>
                    
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            After Coa
                                        </b>
                                    </div>

                                    <div class="body">
                                    
                                    <input type="hidden" name="ni_ktr" class="form-control" value="<?= $stok->ni; ?>" >
                                    <input type="hidden" name="ni_usd" class="form-control" value="<?= $stok->ni_usd; ?>" >
                                    <input type="hidden" name="mc_ktr" class="form-control" value="<?= $stok->mc; ?>" >
                                    <input type="hidden" name="mc_usd" class="form-control" value="<?= $stok->mc_usd; ?>" >
                                    <input type="hidden" name="sio_mgo_ktr" class="form-control" value="<?= $stok->sio_mgo; ?>" >
                                    <input type="hidden" name="sio_usd" class="form-control" value="<?= $stok->sio_usd; ?>" >
                                    <input type="hidden" name="cif_usd" class="form-control" value="<?= $stok->cif_usd; ?>" >
                                    <input type="hidden" name="dap_usd" class="form-control" value="<?= $stok->dap_usd; ?>" >
                
                                             <div class="form-group">
                                                <label>After COA Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="coa_qty" class="form-control" placeholder="Enter Coa Quantity" value="<?= $hasil_lab->coa_qty; ?>" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="ni" class="form-control" placeholder="Enter Ni" value="<?= $hasil_lab->ni; ?>" required onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="fe" class="form-control" placeholder="Enter Fe" required value="<?= $hasil_lab->fe; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="sio" class="form-control" placeholder="Enter SiO2" required value="<?= $hasil_lab->sio; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mgo" class="form-control" placeholder="Enter MgO" required value="<?= $hasil_lab->mgo; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="aio" class="form-control" placeholder="Enter Al2O3" required value="<?= $hasil_lab->aio; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="mc" class="form-control" placeholder="Enter Mc" required value="<?= $hasil_lab->mc; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="dokumen_coa" class="form-control" accept="application/pdf" >

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>
                                </div>
                            </div>
                        </div> <!-- end card -->

                        <div class="col-sm-6">
                            <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Intertek Lab
                                        </b>
                                        
                                    </div>

                                <div class="body">
                                    <div class="form-group">
                                                <label>Intertek Qty</label>
                                                    <div class="form-line">
                                                        <input type="text" name="intertek_qty" class="form-control" placeholder="Enter Intertek Quantity" required  value="<?= $intertek->intertek_qty; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Ni</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_ni" class="form-control" placeholder="Enter Ni" required value="<?= $intertek->ni; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% Fe</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_fe" class="form-control" placeholder="Enter Fe" required value="<?= $intertek->fe; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% SiO2</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_sio" class="form-control" placeholder="Enter SiO2" required value="<?= $intertek->sio; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>% MgO</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mgo" class="form-control" placeholder="Enter MgO" required value="<?= $intertek->mgo; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Al2O3</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_aio" class="form-control" placeholder="Enter Al2O3" required value="<?= $intertek->aio; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Mc</label>
                                                    <div class="form-line">
                                                        <input type="text" name="in_mc" class="form-control" placeholder="Enter Mc" required value="<?= $intertek->mc; ?>" onkeydown="return limitCharacter(event);">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload Dokumen</label>
                                                    <div class="form-line">
                                                        <input type="file" name="in_dokumen" class="form-control" accept="application/pdf">

                                                    </div>
                                                    <i>*File max 8 Mb type .pdf</i>
                                            </div>

                                </div>
                            </div>
                                <a href="<?= site_url('ore/C_stok'); ?>" class="btn btn-default">Kembali</a>
                                &nbsp; &nbsp;&nbsp; &nbsp;
                                 <input class="btn btn-info full-right" type="submit" name="submit" value="Submit">
                            </div>
                            </div> <!-- end card -->
                            </form>

                                    
                                    </form>
                                    <?= $this->session->flashdata("sukses"); ?>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<!-- inline scripts related to this page -->
<script type="text/javascript">
$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
  
  $("#seller").change(function(){ // Ketika user mengganti atau memilih data provinsi
    $("#supplier").hide(); // Sembunyikan dulu combobox kota nya
   
  
    $.ajax({
      type: "POST", // Method pengiriman data bisa dengan GET atau POST
       data: {seller_agent : $("#seller").val()},
      url: "<?php echo site_url('ore/C_stok/get_supplier');?>", // Isi dengan url/path file php yang dituju
      // data yang akan dikirim ke file yang dituju
      dataType: "json",
     
      success: function(response){ // Ketika proses pengiriman berhasil
        
        // set isi dari combobox kota
        // lalu munculkan kembali combobox kotanya
        $("#supplier").html(response.data_supplier).show();
      },
      error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
        alert(thrownError); // Munculkan alert error
      }
    });
    });
});

    </script>

    <script type="text/javascript">
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
          
          $("#supplier").change(function(){ // Ketika user mengganti atau memilih data provinsi
            $("#periode").hide(); // Sembunyikan dulu combobox kota nya
           
          
            $.ajax({
              type: "POST", // Method pengiriman data bisa dengan GET atau POST
               data: {supplier : $("#supplier").val()},
              url: "<?php echo site_url('ore/C_stok/get_periode');?>", // Isi dengan url/path file php yang dituju
              // data yang akan dikirim ke file yang dituju
              dataType: "json",
             
              success: function(response){ // Ketika proses pengiriman berhasil
                
                // set isi dari combobox kota
                // lalu munculkan kembali combobox kotanya
                $("#periode").html(response.data_periode).show();
              },
              error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(thrownError); // Munculkan alert error
              }
            });
            });
        });

        function limitCharacter(event)
        {
          key = event.which || event.keyCode;
          if ( key != 190 // Comma
             && key != 8 // Backspace
             && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
             && (key < 48 || key > 57) // Non digit
             // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
            )
          {
            event.preventDefault();
            return false;
          }
        }

    </script>
</body>

</html>
