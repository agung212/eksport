<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    <ul class="breadcrumb">
                                <li>
                                    <i class='material-icons'>home</i>
                                    <a href="<?= site_url('ore/home'); ?>"> Home </a>
                                </li>
                                <li>
                                    <i class="material-icons">layers</i>
                                    <a href="<?= site_url('ore/C_stok'); ?>"> Stock </a>
                                </li>
                                <li class="active"><i class="material-icons">edit</i> Edit Stock </li>
                            </ul><!-- /.breadcrumb -->
                        <div class="header">
                            <h2>
                                EDIT STOCK
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-blue">
                                        <b>
                                            Data Stock
                                        </b>
                                        
                                    </div>

                                    <form role="form" action="<?= site_url('ore/C_stok/edit_stok/'.$data->id_sm); ?>"  method="post" >
                                    <div class="row clearfix">
                                    <div class="body">

                                    <input type="hidden" name="id_sm" class="form-control" value="<?= $data->id_sm; ?>">
                                    
                                    <!-- <input type="hidden" name="id_sm" class="form-control"> -->
                                    
                                    
                                    

                                     <input type="hidden" name="cif_usd" class="form-control" value="<?= $stok->cif_usd; ?>" >
                                    <input type="hidden" name="dap_usd" class="form-control" value="<?= $stok->dap_usd; ?>" >
                                            
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <p>
                                                    <b>Seller Agent</b>
                                                </p>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="seller_agent"  required id="seller" >
                                                    <option >Pilih Seller Agent</option>
                                                    <?php
                                                        foreach($seller as $row)
                                                        {   
                                    
                                                        if ($data->seller_agent == $row->seller_agent) {
                                                            echo '<option selected value="'.$row->seller_agent.'">'.$row->nama_seller.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->seller_agent.'">'.$row->nama_seller.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <p>
                                                    <b>Supplier Name</b>
                                                </p>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="supplier" class="select2" id="supplier" >
                                                    <option >Pilih Supplier</option>
                                                    <?php
                                                        foreach($supplier as $row)
                                                        {   
                                    
                                                        if ($data->supplier == $row->supplier) {
                                                            echo '<option selected value="'.$row->supplier.'">'.$row->nama_supplier.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->supplier.'">'.$row->nama_supplier.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Periode Shipment </label>
                                                <div class="form-line">
                                                <select class="form-control show-tick" name="master_kontrak" data-live-search="false" class="select2" readonly="true" id="periode">
                                                    <?php
                                                        foreach($supplier as $row)
                                                        {   
                                    
                                                        if ($data->supplier == $row->supplier) {
                                                            echo '<option selected value="'.$row->id_kontrak.'">'.$row->tgl_awal." s/d ".$row->tgl_akhir.'</option>';
                                                        } else{
                                                            echo '<option value="'.$row->id_kontrak.'">'.$row->tgl_awal." s/d ".$row->tgl_akhir.'</option>';
                                                        }
                                                        }
                                                    ?>
                                                </select>
                                                </div>
                                            </div>
                                            </div>                                          

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Tug Boat / Barge</label>
                                                <div class="form-line">
                                                    <input type="text" name="nama_tongkang" class="form-control" placeholder="Enter Tug Boat / Barge" value="<?= $data->nama_tongkang; ?>" required >
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Voyage Number</label>
                                                <div class="form-line">
                                                    <input type="text" name="voyage_number" class="form-control" placeholder="Enter Voyage Number" value="<?= $data->voyage_number; ?>" required="true" readonly>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>BL Quantity</label>
                                                <div class="form-line">
                                                    <input type="text" name="jml_stok" class="form-control" placeholder="Enter BL Quantity" value="<?= $data->jml_stok; ?>" required="true">
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>BL Number</label>
                                                <div class="form-line">
                                                    <input type="text" name="bl_number" class="form-control" placeholder="Enter BL Number" value="<?= $data->bl_number; ?>" required="true">
                                                </div>
                                            </div>
                                            </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Time Arrived</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="tgl_arr" placeholder="Ex: 30/07/2016" value="<?= $data->tgl_arr; ?>" requaired>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Commence Discharge</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="comm_disch" placeholder="Ex: 30/07/2016" value="<?= $data->comm_disch; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Complete Discharge</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="compl_disch" placeholder="Ex: 30/07/2016" value="<?= $data->compl_disch; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                         <div class="col-lg-6 col-md-6 col-xs-6">
                                        <label>Time Depatured</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datetimepicker" name="tgl_dep" placeholder="Ex: 30/07/2016" value="<?= $data->tgl_dep; ?>" required>
                                            </div>
                                        </div>
                                        </div>

                                       <div class="col-lg-6 col-md-6 col-xs-6">
                                            <div class="form-group">
                                                <label>Admin</label>
                                                <div class="form-line">
                                                    <input type="text" name="user" readonly class="form-control" value="<?= $user; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <select name="id" class="select2" id="id" hidden>
                                        <?php
                                            foreach($id as $row)
                                            {   
                        
                                            if ( $row->supplier == $data->$supplier) {
                                                echo '<option selected hidden value="'.$row->id.'">'.$row->id.'</option>';
                                            } else{
                                                echo '<option value="'.$row->id.'">'.$row->id.'</option>';
                                            }
                                            }
                                        ?>
                                    </select> 
                        
                
                                <a href="<?= site_url('ore/C_stok'); ?>" class="btn btn-default">Kembali</a>
                                &nbsp; &nbsp;&nbsp; &nbsp;
                                 <input class="btn btn-info full-right" type="submit" name="submit" value="submit">
                            </div>
                            </div> <!-- end card -->
                            </form>
                            
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
//--> include data footer
$this->load->view('layout/foot');

?>

<!-- inline scripts related to this page -->
<script type="text/javascript">
$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
  
  $("#seller").change(function(){ // Ketika user mengganti atau memilih data provinsi
    $("#supplier").hide(); // Sembunyikan dulu combobox kota nya
   
  
    $.ajax({
      type: "POST", // Method pengiriman data bisa dengan GET atau POST
       data: {seller_agent : $("#seller").val()},
      url: "<?php echo site_url('ore/C_stok/get_supplier');?>", // Isi dengan url/path file php yang dituju
      // data yang akan dikirim ke file yang dituju
      dataType: "json",
     
      success: function(response){ // Ketika proses pengiriman berhasil
        
        // set isi dari combobox kota
        // lalu munculkan kembali combobox kotanya
        $("#supplier").html(response.data_supplier).show();
        $('[name="id_agent"]').val($("#seller").val());
        
      },
      error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
        alert(thrownError); // Munculkan alert error
      }
    });
    });
});

    </script>

    <script type="text/javascript">
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
          
          $("#supplier").change(function(){ // Ketika user mengganti atau memilih data provinsi
            $("#periode").hide(); // Sembunyikan dulu combobox kota nya
           
          
            $.ajax({
              type: "POST", // Method pengiriman data bisa dengan GET atau POST
               data: {supplier : $("#supplier").val()},
              url: "<?php echo site_url('ore/C_stok/get_periode');?>", // Isi dengan url/path file php yang dituju
              // data yang akan dikirim ke file yang dituju
              dataType: "json",
             
              success: function(response){ // Ketika proses pengiriman berhasil
                
                // set isi dari combobox kota
                // lalu munculkan kembali combobox kotanya
                $("#periode").html(response.data_periode).show();
                $('[name="id_supplier"]').val($("#supplier").val());
                
                
              },
              error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(thrownError); // Munculkan alert error
              }
            });
            });
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
          
          $("#supplier").change(function(){ // Ketika user mengganti atau memilih data provinsi
            $("#id").hide(); // Sembunyikan dulu combobox kota nya
           
          
            $.ajax({
              type: "POST", // Method pengiriman data bisa dengan GET atau POST
               data: {supplier : $("#supplier").val()},
              url: "<?php echo site_url('ore/C_stok/get_id');?>", // Isi dengan url/path file php yang dituju
              // data yang akan dikirim ke file yang dituju
              dataType: "json",
             
              success: function(response){ // Ketika proses pengiriman berhasil
                
                // set isi dari combobox kota
                // lalu munculkan kembali combobox kotanya
                $("#id").html(response.data_id).show();
                $('[name="id_ktr_kecil"]').val($("#id").val());
                
                
              },
              error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(thrownError); // Munculkan alert error
              }
            });
            });
        });

    </script>

</body>

</html>
