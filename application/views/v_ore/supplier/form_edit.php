<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Supplier Edit
                                        </b>
                                        
                                    </div>

                                    <form role="form" action="<?= site_url('ore/C_supplier/edit_supplier/'. $data->id_supplier); ?>"  method="post">
                                    <div class="body">
                                       
                                            <label>Supplier Name</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="hidden" id="id_supplier" name="id_supplier" class="form-control" placeholder="Enter your supplier name" value="<?= $data->id_supplier; ?>">

                                                    <input type="text" id="supplier_name" name="nama_supplier" class="form-control" placeholder="Enter your seller name" value="<?= $data->nama_supplier; ?>">
                                                </div>
                                            </div>
                                            <input class="btn btn-info" type="submit" name="submit" value="Save"></input>
                                             &nbsp; &nbsp;
                                            <a href="<?= site_url('ore/C_supplier'); ?>" class="btn btn-default">
                                            Kembali
                                          </a>
                                    </div>
                                    </div>
                                    </form>
                                    <?= $this->session->flashdata("sukses"); ?>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   
</body>
<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>

<!-- inline scripts related to this page -->

</html>
