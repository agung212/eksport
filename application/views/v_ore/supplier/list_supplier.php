<!DOCTYPE html>
<html>

<?php
//--> include data header
$this->load->view('layout_ore/head');
//--> include data sidebar
$this->load->view('layout_ore/sidebar');

?>
<body class="theme-red">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-xs-14">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                        <div>
                            <?= $this->session->flashdata("sukses"); ?>
                                  <div class="card">
                                    <div class="header bg-red">
                                        <b>
                                            Input Supplier Name
                                        </b>
                                        
                                    </div>

                                    <form action="<?php echo base_url(); ?>index.php/ore/C_supplier/tambah_supplier"  method="post">
                                    <div class="body">
                                       
                                            <label>Supplier Name</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="supplier_name" name="nama_supplier" class="form-control" placeholder="Enter your supplier name">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>

                                            &nbsp; &nbsp;

                                            <button class="btn btn-danger m-t-15" type="reset">
                                                Cancel
                                            </button>

                                    </div>
                                    </div>
                        </div>
                        </form>
    
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="mytable">
                                    <thead >
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                                foreach ($supplier as $row) {

                                                    $id = $row->id_supplier;

                                                    echo "
                                                    <tr>
                                                        <td> $no </td>
                                                        <td> $row->nama_supplier </td>
                                                        <td align='center'>
                                                            <a href='C_supplier/edit_supplier/$id' class='green' title='Edit Data'>
                                                                <i class='material-icons'>edit</i>
                                                            </a>
                                                            
                                                            &nbsp;  &nbsp;
                                                            <a href='#' id='delete-row' class='delete-row red' title='Delete' data-toggle='modal' data-target='#myModal' aria-hidden='true' data-id='$id'>
                                                                <i class='material-icons'>delete</i>
                                                            </a>
                                                        </td>";
                                        $no++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                           
                        </div>
                    </div>
                    </div>

                    <!-- Konfirmasi Hapus Data -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 style="color:#bf1a1a" class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Confirm</h4>
                              </div>
                              <div class="modal-body">
                                You will not be able to recover this imaginary file! <br/>

                                Are you sure?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal"><i class='ace-icon fa fa-times'></i> Cancel </button>
                                <button type="button" id="del-row" class="btn btn-danger del-row"><i class='ace-icon fa fa-trash-o bigger-150'></i> Delete </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /konfirmasi -->

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

   
</body>
<?php
//--> include data footer
$this->load->view('layout_ore/foot');

?>

<!-- inline scripts related to this page -->
    <script type="text/javascript">
      $(function(){

        //notifikasi hapus
        $(document).on('click', '#delete-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#del-row', function(e){
            window.location = 'C_supplier/hapus_data/' +id;
        });
        //.notifikasi hapus

        //notifikasi edit
        $(document).on('click', '#edit-row', function(e){
            e.preventDefault();
            id = $(this).data('id');
        });
        $(document).on('click', '#edt-row', function(e){
            window.location = 'C_seller/hapus_data/' +id;
        });
        //.notifikasi edit

        //datatables
        $('#mytable').DataTable({
           "paginate"   : true,
           "sort"       : false,
           "lengthMenu" : [[10, 25, 50, 100], [10, 25, 50, 100]],
           "language"   :
           {
             "lengthMenu"       : "Lihat _MENU_ data",
             "search"           : "Cari data : ",
             "searchPlaceholder": "Cari ...",
             "zeroRecords"      : "Tidak ada data yang ditemukan",
             "emptyTable"       : "<center>Tidak ada data di dalam tabel</center>",
             "infoEmpty"        : "Tidak ada data yang ditampilkan",
             "info"             : "Menampilkan _START_ - _END_ dari _TOTAL_ data ",
             "infoFiltered"     : "(Hasil filter dari _MAX_ data)",
             oPaginate  :
             {
              sPrevious : "<i class='fa fa-angle-left'><i/>",
              sNext     : "<i class='fa fa-angle-right'><i/>"
             }
           }
        });
        //.dattables
      });
    </script>
</html>
