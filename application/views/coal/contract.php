<?php
$sl_coa= "<select name=coa[] class=form-control show-tick  >";
foreach ($parameter->result() as $key):
$sl_coa.= "<option value=". $key->id .">". $key->nm_spec ."</option>";
endforeach ;
$sl_coa.="</select>";
?>

<?php
$sl_coa1="<select name=coa1[] class=form-control show-tick>";
foreach ($satuan->result() as $key):
$sl_coa1.= "<option value=". $key->nm_spec .">". $key->nm_spec ."</option>";
endforeach ;
$sl_coa1.="</select>";
?>



<section class="content">
        <div class="container-fluid">
 <div class="row clearfix">
    <?php $this->load->helper('form'); ?>
 <form action="<?php echo base_url() ?>C_contract/insert_contract" method="POST" name="autoSumForm">
     <div class="demo-masked-input">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                COAL CONTRACT 
                                
                            </h2>
                          
                        </div>
                        <div class="body">
                           
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                        <b>Contract No (Master)</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="contract_no" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Supplier</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                                 <select class="form-control show-tick" data-live-search="true" name="supplier" required>
                                                    <?php foreach ($supplier->result() as $key): ?>
                                                <option value="<?php echo $key->id_sup; ?>"><?php echo $key->nm_supplier; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                           </div>
                                        </div>
                                    <div class="col-md-4">
                                        <b>Contract No (Sales Purchase)</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="sales_purcahse" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                   


                                    <div class="col-md-3">
                                        <b>Buyer</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <select class="form-control show-tick" data-live-search="true" name="buyer" required>
                                                    <?php foreach ($buy->result() as $key): ?>
                                                <option value="<?php echo $key->id_buy; ?>"><?php echo $key->nm_buy; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <b>Buyer Agent</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="buyer_agent" required>
                                                    <?php foreach ($buyer->result() as $key): ?>
                                                <option value="<?php echo $key->id_buyer; ?>"><?php echo $key->nm_buyer; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Payment Method</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                             <input name="payment_method" type="radio" id="radio_1" onClick="radio_check()"; value="LC" checked />
                                                    <label for="radio_1" >LC</label>
                                                    &nbsp &nbsp  &nbsp &nbsp  &nbsp &nbsp
                                              <input name="payment_method" type="radio" id="radio_2" onClick="radio_check()"; value="TT"  />
                                                    <label for="radio_2" >TT</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>LC No</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="lc_no" id="i2" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Quantity LC</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="qty_lc" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Amount LC</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="amount_lc" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Quantity Contract</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="bl" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Term Of Contract</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="term_of_contract" required>
                                                    <?php foreach ($terms->result() as $key): ?>
                                                <option value="<?php echo $key->id_terms; ?>"><?php echo $key->nm_terms; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <b>Port Of Loading</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="port_of_landing" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <b>Port Of Discharge</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="port_of_discharge" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    

                                    
                               </br>
                            </br>
                       
                             </div>
                            </div>
                        </div>
                    </div>
               
           
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                      
                             COAL SPECEFICATION
                            </h2>
                        </div>
                          <div class="body">
                            
                                <div class="row clearfix">
                          
                      
                                    <input id="idf" value="1" type="hidden" />
                                    <div class="col-md-4">
                                        <b>Parameter</b>
                                         <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="coa[]" required>
                                                    <?php foreach ($parameter->result() as $key): ?>
                                                <option value="<?php echo $key->id; ?>"><?php echo $key->nm_spec; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Typical</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="typical[]" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Rejection</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="rejection[]" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Satuan</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                                 <select class="form-control show-tick" data-live-search="true" name="coa1[]" required>
                                                    <?php foreach ($satuan->result() as $key): ?>
                                                <option value="<?php echo $key->nm_spec; ?>"><?php echo $key->nm_spec; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Remarks</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="remarks[]" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-1">
                                        <div class="input-group">
                                     <a class="btn btn-primary btn-sm"  onclick="addRincian('<?php echo $sl_coa;?>','<?php echo $sl_coa1;?>'); return false;"><i class="glyphicon glyphicon-plus"></i></a>
                                     </div>
                                     </div>
                                     

                                      
                                </div>
                            </div>
                       
                            
                                <div class="row clearfix">
                                    <div id="divAkun"></div>
                                </div>
                            </div>
                        
                   
                 
           


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               UNIT PRICE DETALIS   
                            </h2>
                          
                        </div>
                        <div class="body">
                             <b>UNIT PRICE SUPPLIER AGENT - BUYER AGENT</b></br></br>
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">
                                        <b>FOB Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fob_priceSup" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Freight</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="freightSup" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>FOB / CIF / CFR Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fcc_priceSup" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                     
                                </div>
                            </div>

                            <b>UNIT PRICE BUYER AGENT - BUYER</b></br></br>
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">
                                        <b>FOB Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fob_priceBuy" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Freight</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="freightBuy" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>FOB / CIF / CFR Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fcc_priceBuy" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               LAYTIME CALCULATION AT PORT OF DISCHARGE  
                            </h2>
                            
                        </div>
                        <div class="body">
                             
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">

                                    <b>Demurage Rate Per Day</b>
                                    <div class="row clearfix">
                                        <div class="col-md-4">
                                        <div class="input-group">
                                            
                                            <div class="form-line">
                                              <select class="form-control show-tick" data-live-search="true" name="sat_dem" required>
                                                <option>$</option>
                                                <option>Rp. </option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        <div class="form-line">
                                                <input type="text" name="demurage"  class="form-control float-right" onFocus="startCalc();" onBlur="stopCalc();">
                                         </div>
                                     </div>
                                    </div>
                                </div>
                                    </div>
                                     <div class="col-md-4">

                                    <b>Despatch Rate Per Day</b>
                                    <div class="row clearfix">
                                        <div class="col-md-4">
                                        <div class="input-group">
                                            
                                            <div class="form-line">
                                              <select class="form-control show-tick" data-live-search="true" name="sat_des" required>
                                                <option>$</option>
                                                <option>Rp. </option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        <div class="form-line">
                                                  <input type="text" name="despatch" class="form-control float-right" id="result"  required readonly>
                                         </div>
                                     </div>
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Discharging Rate Per Day</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">linear_scale</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="discharging" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <input type="submit" value="SAVE" class="btn btn-primary m-t-15 waves-effect">
                                    <input type="reset" value="RESET" class="btn btn-danger m-t-15 waves-effect">
                                     </div>
                                     </form>
                                </div>
                            </div>

                           
                                    
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                      </div>
    </section>
