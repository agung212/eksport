<?php
$sl_coa= "<select name=coa[] class=form-control show-tick  >";
foreach ($parameter->result() as $key):
$sl_coa.= "<option value=". $key->id .">". $key->nm_spec ."</option>";
endforeach ;
$sl_coa.="</select>";
?>

<?php
$sl_coa1="<select name=coa1[] class=form-control show-tick>";
foreach ($satuan->result() as $key):
$sl_coa1.= "<option value=". $key->nm_spec .">". $key->nm_spec ."</option>";
endforeach ;
$sl_coa1.="</select>";
?>

<?php
$sl_coa2="<select name=coa2[] class=form-control show-tick>";
foreach ($remarks->result() as $key):
$sl_coa2.= "<option value=". $key->nm_spec .">". $key->nm_spec ."</option>";
endforeach ;
$sl_coa2.="</select>";
?>

 
<section class="content">
        <div class="container-fluid">
 <div class="row clearfix">

    <?php $this->load->helper('form'); ?>
   
    <form action="<?php echo base_url() ?>C_contract/edit_contractAction" method="POST" name="autoSumForm">
    <?php foreach ($data->result() as $edit): ?> 
     <div class="demo-masked-input">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                COAL CONTRACT 
                                
                            </h2>
                          
                        </div>
                        <div class="body">
                           
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                        <b>Contract No (Master)</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="contract_no" class="form-control" value="<?php echo $edit->contract_no ?>" required>
                                                 <input type="hidden" name="contract_noLama" class="form-control" value="<?php echo $edit->contract_no ?>" required>
                                                 <input type="hidden" name="id_contract" class="form-control" value="<?php echo $edit->id_contract ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <b>Supplier</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                                 <select class="form-control show-tick" data-live-search="true" name="supplier" required>
                                                <option value="<?php echo $edit->id_sup ?>" selected><?php echo $edit->nm_supplier ?></option>
                                                    <?php foreach ($supplier->result() as $key): ?>
                                                <option value="<?php echo $key->id_sup; ?>"><?php echo $key->nm_supplier; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Contract No (Sales Purchase)</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="sales_purcahse" class="form-control" value="<?php echo $edit->sales_purchase ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-3">
                                        <b>Buyer</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="buyer" required>
                                                 <option value="<?php echo $edit->id_buy ?>" selected><?php echo $edit->nm_buy ?></option>
                                                    <?php foreach ($buy->result() as $key): ?>
                                                <option value="<?php echo $key->id_buy; ?>"><?php echo $key->nm_buy; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <b>Buyer Agent</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="buyer_agent" required>
                                                 <option value="<?php echo $edit->id_buyer ?>" selected><?php echo $edit->nm_buyer ?></option>
                                                    <?php foreach ($buyer->result() as $key): ?>
                                                <option value="<?php echo $key->id_buyer; ?>"><?php echo $key->nm_buyer; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Payment Method</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                             <input name="payment_method" type="radio" id="radio_1" onClick="radio_check()"; value="LC" <?php if($edit->payment_method == "LC") { echo "checked"; } ?> />
                                                    <label for="radio_1" >LC</label>
                                                    &nbsp &nbsp  &nbsp &nbsp  &nbsp &nbsp
                                              <input name="payment_method" type="radio" id="radio_2" onClick="radio_check()"; value="TT"  <?php if($edit->payment_method == "TT") { echo "checked"; } ?> />
                                                    <label for="radio_2" >TT</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>LC No</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="lc_no" id="i2" class="form-control" value="<?php echo $edit->lc_no ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Quantity LC</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="qty_lc" class="form-control" value="<?php echo $edit->quantity_lc ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Amount LC</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="amount_lc" class="form-control" value="<?php echo $edit->amount_lc ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Quantity Contract</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="bl" class="form-control" value="<?php echo $edit->bl ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Term Of Contract</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="term_of_contract" required>
                                                <option value="<?php echo $edit->id_terms ?>" selected><?php echo $edit->nm_terms ?></option>
                                                    <?php foreach ($terms->result() as $key): ?>
                                                <option value="<?php echo $key->id_terms; ?>"><?php echo $key->nm_terms; ?></option>
                                                    <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                     
                                    <div class="col-md-6">
                                        <b>Port Of Loading</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="port_of_landing" class="form-control" value="<?php echo $edit->port_of_landing ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <b>Port Of Discharge</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="port_of_discharge" class="form-control" value="<?php echo $edit->port_of_discharge ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                   
                               </br>
                            </br>
                       
                             </div>
                            </div>
                        </div>
                    </div>
               
           
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                      
                             COAL SPECEFICATION
                            </h2>
                        </div>
                        
                          <div class="body">

                             <?php
                              $tot = $specification->num_rows();
                              $no = 1;
                              foreach ($specification->result() as $spec): ?> 
                                <div class="row clearfix">                      
                                    <input id="idf" value="1" type="hidden" />
                                   
                                    <div class="col-md-4">
                                        <b>Parameter</b>
                                         <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                           <select class="form-control show-tick" data-live-search="true" name="coa[]" required>
                                                    <?php foreach ($parameter->result() as $key): ?>
                                                <?php if($spec->id_parameter == $key->id) { ?>
                                                <option value="<?php echo $key->id; ?>" selected><?php echo $key->nm_spec; ?></option> 
                                                <?php } else { ?>
                                                <option value="<?php echo $key->id; ?>"><?php echo $key->nm_spec; ?></option> 
                                                <?php } ?>
                                                <?php endforeach ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Typical</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="typical[]" class="form-control" value="<?php echo $spec->typical; ?>" required>
                                                 <input type="hidden" name="id_spec[]" class="form-control" value="<?php echo $spec->id_spec; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Rejection</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="rejection[]" class="form-control" value="<?php echo $spec->rejection; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Satuan</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                                 <select class="form-control show-tick" data-live-search="true" name="coa1[]" required>
                                                    <?php foreach ($satuan->result() as $key): ?>
                                                     <?php if($spec->id_satuan == $key->nm_spec) { ?>
                                                <option value="<?php echo $key->nm_spec; ?>" selected><?php echo $key->nm_spec; ?></option>
                                                <?php } else { ?>
                                                 <option value="<?php echo $key->nm_spec; ?>"><?php echo $key->nm_spec; ?></option>
                                                 <?php } ?>
                                                 <?php endforeach ?>
                                                
                                                </select>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Remarks</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="remarks[]" class="form-control" value="<?php echo $spec->id_remarks; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="input-group">
                                      <a href="<?php echo base_url() ?>C_contract/hapus_spec/<?php echo $spec->id_spec; ?>/<?php echo $edit->id_contract; ?>"><button type='button' class='btn btn-danger waves-effect' title='Hapus Rincian !'><i class='glyphicon glyphicon-trash'></i></button></a>
                                     </div>
                                     </div>
                                     <?php if ($tot == $no) { ?>
                                     <div class="col-md-1">
                                        <div class="input-group">
                                     <a class="btn btn-primary btn-sm"  onclick="addRincian('<?php echo $sl_coa;?>','<?php echo $sl_coa1;?>','<?php echo $sl_coa2;?>'); return false;"><i class="glyphicon glyphicon-plus"></i></a>
                                     </div>
                                     </div>
                                     <?php } ?>
                                 </div>
                                
                                
                                 <?php  $no++; endforeach ?>

                                     
                               
                            
                                <div class="row clearfix">
                                    <div id="divAkun"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
           


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               UNIT PRICE DETALIS   
                            </h2>
                          
                        </div>
                        <div class="body">
                             <b>UNIT PRICE SUPPLIER AGENT - BUYER AGENT</b></br></br>
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">
                                        <b>FOB Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fob_priceSup" class="form-control" value="<?php echo $edit->fob_sup ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Freight</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="freightSup" class="form-control" value="<?php echo $edit->freight_sup ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>FOB / CIF / CFR Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fcc_priceSup" class="form-control" value="<?php echo $edit->cfr_priceSup ?>">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                     
                                </div>
                            </div>

                            <b>UNIT PRICE BUYER AGENT - BUYER</b></br></br>
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">
                                        <b>FOB Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fob_priceBuy" class="form-control" value="<?php echo $edit->fob_buyer ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Freight</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="freightBuy" class="form-control" value="<?php echo $edit->freight_buyer ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>FOB / CIF / CFR Price</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="fcc_priceBuy" class="form-control" value="<?php echo $edit->cfr_priceBuyer ?>">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               LAYTIME CALCULATION AT PORT OF DISCHARGE  
                            </h2>
                            
                        </div>
                        <div class="body">
                             
                            <div class="demo-masked-input">
                               
                                <div class="row clearfix">

                                    <div class="col-md-4">
                                        <b>Demurage Rate Per Day</b>
                                        <div class="row clearfix">
                                        <div class="col-md-4">
                                        <div class="input-group">
                                            
                                            <div class="form-line">
                                              <select class="form-control show-tick" data-live-search="true" name="sat_dem" required>
                                                <?php if ($edit->sat_dem == "$"){ ?>
                                                <option selected>$</option>
                                                <option>Rp. </option>
                                                <?php } else { ?>
                                                <option>$</option>
                                                <option selected="">Rp. </option>
                                                <?php } ?>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        <div class="form-line">
                                                <input type="text" name="demurage" class="form-control" value="<?php echo $edit->demurage ?>" onFocus="startCalc();" onBlur="stopCalc();">
                                         </div>
                                     </div>
                                    </div>
                                </div>
                                        
                                    </div>
                                    <div class="col-md-4">
                                        <b>Despatch Rate Per Day</b>
                                        <div class="row clearfix">
                                        <div class="col-md-4">
                                        <div class="input-group">
                                            
                                            <div class="form-line">
                                              <select class="form-control show-tick" data-live-search="true" name="sat_des" required>
                                                <?php if ($edit->sat_dem == "$"){ ?>
                                                <option selected>$</option>
                                                <option>Rp. </option>
                                                <?php } else { ?>
                                                <option>$</option>
                                                <option selected="">Rp. </option>
                                                <?php } ?>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        <div class="form-line">
                                                <input type="text" name="despatch" class="form-control" value="<?php echo $edit->dispatch ?>" id="result" readonly>
                                         </div>
                                     </div>
                                    </div>
                                </div>
                                        
                                    </div>
                                    <div class="col-md-4">
                                        <b>Discharging Rate Per Day</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">linear_scale</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="discharging" class="form-control" value="<?php echo $edit->discharging ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <input type="submit" value="SAVE" class="btn btn-primary m-t-15 waves-effect">
                                    <input type="reset" value="RESET" class="btn btn-danger m-t-15 waves-effect">
                                     </div>
                                      </form>
                                </div>
                            </div>
                                   <?php endforeach ?>
                           
                                    
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                      </div>
    </section>
