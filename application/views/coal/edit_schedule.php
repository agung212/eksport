<section class="content">
        <div class="container-fluid">
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Input Coal Schedule
                            </h2>
                            <ul class="header-dropdown m-r--5">
                               
                            </ul>
                        </div>
                        <?php echo form_open('C_coal_schedule/edit_scheduleAction');?>
                        <?php foreach ($data->result() as $val): ?>
                        <div class="body">
                            <form class="form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Contract No</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                  <select class="form-control show-tick" data-live-search="true" name="id_contract" required>
                                                    <?php foreach ($data_contract->result() as $key): ?>
                                                    <?php if($key->id_contract == $val->id_contract) { ?>
                                                        <option value="<?php echo $key->id_contract; ?>" selected><?php echo $key->contract_no; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $key->id_contract; ?>"><?php echo $key->contract_no; ?></option>
                                                    <?php
                                                     }
                                                    ?>
                                                    <?php endforeach ?>
                                                  </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Material</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="material" class="form-control" placeholder="Enter your Material" value="<?php echo $val->material; ?>">
                                                 <input type="hidden" name="id_coalSch" class="form-control" value="<?php echo $val->id_coalSch; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Type </label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            
                                               <input name="type" type="radio" id="radio_1" value="Vessel" <?php if($val->type == "Vessel") { echo "checked"; } ?> />
                                                    <label for="radio_1" >Vessel</label>

                                                <input name="type" type="radio" id="radio_2"  value="Tug Barge" <?php if($val->type == "Tug Barge") { echo "checked"; } ?> />
                                                    <label for="radio_2">Tug Barge</label>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Vessel Name / Tug Barge Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="vessel" class="form-control" placeholder="Enter your Vessel Name" value="<?php echo $val->vessel_name; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Laycan Loading</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="laycan_loading" id="date-start" class="datepicker form-control" placeholder="Start Date" value="<?php echo $val->laycan_loading; ?>">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="laycan_loadingEnd" id="date-end" class="datepicker form-control" placeholder="End Date" value="<?php echo $val->laycan_end; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Estimate Morosi, Kendari</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="eta_morosi" class="datepicker form-control" placeholder="Enter your Estimate Morosi, Kendari" value="<?php echo $val->eta; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">BL / Final Quantity</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text"  name="estimate_total" class="form-control" placeholder="Enter your Estimate Total Quantity" value="<?php echo $val->eta_total; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Final Price(1)</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text"  name="final_price1" class="form-control" placeholder="Final Price(1)" value="<?php echo $val->estimate_daily; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Final Price(2)</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text"  name="final_price2" class="form-control" placeholder="Final Price(2)" value="<?php echo $val->minumum_stock; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 
                               
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                                    </div>
                                </div>
                            <?php endforeach ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>