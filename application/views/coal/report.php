<section class="content">
        <div class="container-fluid">

<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Report Contract   
                                <small></small>
                            </h2>
                            
                        </div>
                           <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Contract No</th>
                                            <th>Sales Purchase</th>
                                            <th>Supplier</th>
                                            <th>Buyer Agent</th>
                                            <th>Buyer</th>
                                            <th>LC No</th>
											<th>Quantity LC</th>
                                            <th>Amount LC</th>
                                            <th>Term Of Contract</th>
                                            <th>Port Of Landing</th>
                                            <th>Port Of Discharge</th>
                                            <th>BL / Final Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                          foreach ($coal_contract->result() as $key): ?>
                                        <tr>
											<td style="background: #f0f8ff"><a href="<?php echo base_url() ?>C_contract/view_contract/<?php echo $key->id_contract ?> "><?php echo $key->contract_no; ?></a></td>
											<td><?php echo $key->sales_purchase; ?></td>
											<td><?php echo $key->nm_supplier; ?></td>
											<td><?php echo $key->buyer; ?></td>
                                            <td><?php echo $key->nm_buyer; ?></td>
                                            <td><?php echo $key->lc_no; ?></td>
                                            <td><?php echo $key->quantity_lc; ?></td>
                                            <td><?php echo $key->amount_lc; ?></td>
                                            <td><?php echo $key->nm_terms; ?></td>
                                            <td><?php echo $key->port_of_landing; ?></td>
                                            <td><?php echo $key->port_of_discharge; ?></td>
                                            <td><?php echo $key->bl; ?></td>
                                            <td style="background: #f0f8ff"><a href="<?php echo base_url() ?>C_report/detail_report/<?php echo $key->id_contract ?>">DETAIL</a></td>
										</tr>
                                    <?php endforeach ?>
									</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             </div>
    </section>