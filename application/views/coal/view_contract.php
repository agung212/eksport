<section class="content">
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Detail Contract Coal</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                     <?php foreach ($coal_contract->result() as $key): ?>   
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url() ?>C_contract/input_contract">New Contract</a></li>
                                        <li><a href="<?php echo base_url() ?>C_contract/input_schedule/<?php echo $key->id_contract ?>">Input Coal Schedule</a></li>
                                        <li><a href="<?php echo base_url() ?>C_contract/edit_contract/<?php echo $key->id_contract ?>">Edit Contract</a></li>
                                        <li><a href="<?php echo base_url() ?>C_contract/hapus_contract/<?php echo $key->id_contract ?>">Delete Contract</a></li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                       
                        <div class="body">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">COAL CONTRACT</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">COAL SCHEDULE</a></li>
                                        
                                    </ul>
                              <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                             <div class="row clearfix">
                <!-- Basic Examples -->
                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          
                            <b><h4>Coal Contract</h4></b>
                            <table width="100%" >
                               
                                <tr>
                                    <td>Contract No(Master)</td>
                                    <td> : </td>
                                    <td> <?php echo $key->contract_no;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Sales Purchase</td>
                                    <td> : </td>
                                    <td> <?php echo $key->sales_purchase;  ?> </td>
                                </tr>
                                 <tr>
                                    <td>Supplier</td>
                                    <td> : </td>
                                    <td> <?php echo $key->nm_supplier;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Buyer Agent</td>
                                    <td> : </td>
                                    <td> <?php echo $key->nm_buyer;  ?> </td>
                                </tr>
                                 <tr>
                                    <td>Buyer </td>
                                    <td> : </td>
                                    <td> <?php echo $key->nm_buy;  ?> </td>
                                </tr>
                                  <tr>
                                    <td>Payment Method</td>
                                    <td> : </td>
                                    <td> <?php echo $key->payment_method;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td>LC No </td>
                                    <td> : </td>
                                    <td> <?php echo $key->lc_no;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Quantity LC </td>
                                    <td> : </td>
                                    <td> <?php echo $key->quantity_lc;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Amount LC </td>
                                    <td> : </td>
                                    <td> <?php echo $key->amount_lc;  ?> </td>
                                </tr>
                                 <tr>
                                    <td>Term Of Contract </td>
                                    <td> : </td>
                                    <td> <?php echo $key->nm_terms;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Port Of Loading </td>
                                    <td> : </td>
                                    <td> <?php echo $key->port_of_landing;  ?> </td>
                                </tr>
                                 <tr>
                                    <td>Port Of Discharge </td>
                                    <td> : </td>
                                    <td> <?php echo $key->port_of_discharge  ?> </td>
                                </tr>
                                <tr>
                                    <td>BL </td>
                                    <td> : </td>
                                    <td> <?php echo $key->bl;  ?> </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <b><h4>Unit Price Detalis</h4></b>
                        <b><h5>Unit Price Supplier Agent - Agent Buyer</h5></b>
                            <table width="100%" >
                                <tr>
                                    <td>FOB Price</td>
                                    <td> : </td>
                                    <td> <?php echo $key->fob_sup;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Freight</td>
                                    <td> : </td>
                                    <td> <?php echo $key->freight_sup;  ?> </td>
                                </tr>
                                 <tr>
                                    <td><?php echo $key->nm_terms; ?> Price </td>
                                    <td> : </td>
                                    <td> <?php echo $key->cfr_priceSup;  ?> </td>
                                </tr>
                            </table>
                        </br>
                        <b><h5>Unit Price Buyer Agent - Buyer</h5></b>
                        
                            <table width="100%" >
                                <tr>
                                    <td>FOB Price</td>
                                    <td> : </td>
                                    <td> <?php echo $key->fob_buyer;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Freight</td>
                                    <td> : </td>
                                    <td> <?php echo $key->freight_buyer;  ?> </td>
                                </tr>
                                 <tr>
                                    <td><?php echo $key->nm_terms; ?> Price </td>
                                    <td> : </td>
                                    <td> <?php echo $key->cfr_priceBuyer;  ?> </td>
                                </tr>
                            </table>
                           
                        </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <b><h4>Coal Specification</h4></b>
                     <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Parameter</th>
                                            <th>Typical</th>
                                            <th>Satuan</th>
                                            <th>Rejection</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          
                                          foreach ($specification->result() as $val): ?>
                                            <tr>
                                                <td><?php echo $val->nm_spec ?></td>
                                                <td><?php echo $val->typical ?></td>
                                                <td><?php echo $val->id_satuan ?></td>
                                                <td><?php echo $val->rejection ?></td>
                                                <td><?php echo $val->id_remarks ?></td>
                                               
                                            </tr>
                                         <?php
                                         
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <b><h4>Layrtime Calculation at Port Of Discharge</h4></b>
                       
                            <table width="100%" >
                                <tr>
                                    <td>Demurage Rate Per Day</td>
                                    <td> : </td>
                                    <td><?php echo $key->sat_dem; ?> <?php echo $key->demurage;  ?> </td>
                                </tr>
                                <tr>
                                    <td>Despatch Rate Per Day</td>
                                    <td> : </td>
                                    <td><?php echo $key->sat_des; ?> <?php echo $key->dispatch;  ?> </td>
                                </tr>
                                 <tr>
                                    <td>Discharging Rate Per Day </td>
                                    <td> : </td>
                                    <td> <?php echo $key->discharging;  ?> </td>
                                </tr>
                            </table>
                        </br>
                       
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

                     <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                         <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                           
                                            <th>Material</th>
                                            <th>Type</th>
                                            <th>Vessel Name / Tag Barge Name</th>
                                            <th>Laycan Loading</th>
                                            <th>Estimate Morosi, Kendari</th>
                                            <th>Estimate Total Quantity</th>
                                            <th>Final Unit Price(1)</th>
                                            <th>Final Unit Price(2)</th>
                                            <th>Status</th>
                                            <th>Premium(1)</th>
                                            <th>Premium(2)</th>
                                            <th>Action</th>                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                          foreach ($data->result() as $key): ?>
                                        <tr>
                                            <td><?php echo $key->material; ?></td>
                                            <td><?php echo $key->type; ?></td>
                                            <td><?php echo $key->vessel_name; ?></td>
                                            <td><?php echo $key->laycan_loading; ?> / <?php echo $key->laycan_end; ?></td>
                                            <td><?php echo $key->eta; ?></td>
                                            <td><?php echo $key->eta_total; ?></td>
                                            <td><?php echo $key->estimate_daily; ?></td>
                                            <td><?php echo $key->minumum_stock; ?></td>
                                            <td><?php if($key->status == 1) { echo "<span class='label label-success'>FINISH</span>"; } else { echo "<span class='label label-primary'>ON PROGRESS</span>"; } ?></td>
                                            <td><?php echo $key->premium_1; ?></td>
                                            <td><?php echo $key->premium_2; ?></td>
                                            <td>
                                                <a  href="<?php echo base_url() ?>C_coal_schedule/edit_schedule/<?php echo $key->id_coalSch ?>"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp || &nbsp
                                                    <a  href="<?php echo base_url() ?>C_coal_schedule/del_schedule/<?php echo $key->id_coalSch ?>/<?php echo $key->id_contract ?>" onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                            </td>
                                           
                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>            
                    </div>
                </div>
            </div>
    </section>
