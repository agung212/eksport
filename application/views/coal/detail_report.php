<section class="content">
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Detail Report </h2>
                            
                                     <?php foreach ($coal_contract->result() as $key): ?>   
                                 
                        </div>
                       
                        <div class="body">
                           
                             <div class="row clearfix">
                <!-- Basic Examples -->

                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          
                            
                            <table width="100%" >
                               
                                <tr>
                                    <td><b>Contract No(Master)</b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->contract_no;  ?> </b></td>
                                </tr>
                                <tr>
                                    <td><b>Sales Purchase</b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->sales_purchase;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td><b>Supplier </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->nm_supplier;  ?> </b></td>
                                </tr>
                                <tr>
                                    <td><b>Buyer Agent</b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->nm_buyer;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td><b>Buyer</b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->buyer;  ?> </b></td>
                                </tr>
                               
                                 <tr>
                                    <td><b>LC No</b></td>
                                    <td> : </td>
                                    <td><b> <?php echo $key->lc_no;  ?> </b></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        
                            <table width="100%" >
                               <tr>
                                    <td><b>Quantity LC </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->quantity_lc;  ?> </b></td>
                                </tr>
                                <tr>
                                    <td><b>Amount LC </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->amount_lc;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td><b>Term Of Contract </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->nm_terms;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td><b>Port Of Landing </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->nm_terms;  ?> </b></td>
                                </tr>
                                 <tr>
                                    <td><b>Port Of Discharge </b></td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->nm_terms;  ?> </b></td>
                                </tr>
                                <tr>
                                    <td><b>BL</b> </td>
                                    <td><b> : </b></td>
                                    <td><b> <?php echo $key->bl;  ?> </b></td>
                                </tr>
                            </table>
                       
                           
                        </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <b><h4></h4></b>
                     <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                 <thead>
                                    <tr>
                                            <th>Type</th>
                                            <th>Vessel Name</th>
                                            <th>Voyage Number</th>
                                            <th>BL Number</th>
                                            <th>BL Quantity</th>
                                            <th>Time Arrived</th>
                                            <th>Commence Discharge</th>
                                            <th>Complete Discharge</th>
                                            <th>Time Departured</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                          foreach ($discharging->result() as $val): ?>
                                        <tr>
                                            <td><?php echo $val->type; ?></td>
                                            <td><?php echo $val->vessel_name; ?></td>
                                            <td><?php echo $val->voyage_no; ?></td>
                                            <td><?php echo $val->bl_no; ?></td>
                                            <td><?php echo $val->bl_qty; ?></td>
                                            <td><?php echo $val->ta; ?></td>
                                            <td><?php echo $val->comm_disch; ?></td>
                                            <td><?php echo $val->comp_disch; ?></td>
                                            <td><?php echo $val->td; ?></td>
                                           
                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                    </div>

                
                            <?php endforeach ?>
                        </div>
    </section>
