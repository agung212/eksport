<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('assets/favicon.ico')?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/plugins/node-waves/waves.css')?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/plugins/animate-css/animate.css')?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/alert/css/sweetalert.css')?>">
</head>

<body>
  <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js')?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/node-waves/waves.js')?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.js')?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/js/admin.js')?>"></script>
    <script src="<?php echo base_url('assets/js/pages/examples/sign-in.js')?>"></script>
    <script src="<?php echo base_url('assets/alert/js/sweetalert.min.js')?>"></script>
    <script src="<?php echo base_url('assets/alert/js/qunit-1.18.0.js')?>"></script>
</body>

<?php if ($aksi == 'login') { ?>
 <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'FAILED',
            text:  'Username OR Password Incorrect',
            type: 'error',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>login');
    } ,1500);   
  </script>
 <?php } else if ($aksi == 'gagal') { ?>
    <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'FAILED',
            text:  'Data Already Exist',
            type: 'error',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>c_setting/index/<?php echo $stat; ?>');
    } ,1500);   
  </script>
  <?php } else if ($aksi == 'gagal_kontrak') { ?>
   <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'FAILED',
            text:  'Data Already Exist <?php echo $stat; ?>',
            type: 'error',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract');
    } ,1500);   
  </script>
 

  <?php } else if ($aksi == 'gagal_disc') { ?>
   <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'FAILED',
            text:  'FAILED',
            type: 'error',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_input_discharge');
    } ,1500);   
  </script>
  <?php } ?>
</html>