


<section class="content">
        <div class="container-fluid">
 <div class="row clearfix">
    <?php $this->load->helper('form'); ?>
    <?php echo form_open('C_input_discharge/insert_contract');?>
     <div class="demo-masked-input">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                INPUT DISCHARGE  
                            </h2>
                          
                        </div>
                        <div class="body">
                           
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                        <b>Type</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                              <select class="form-control" name="type" id="combokategori"  required>
                                                    <option value="">-- Please Select --</option>
                                                    <option value="Vessel">Vessel</option>
                                                    <option value="Tug Barge">Tug Barge</option>
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Vessel Name / Tug Barge Name</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                            <select class="form-control" name="vessel_name" id="subkategori" required>
                                                    <option value="">-- Please Select --</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <b>Voyage Number</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="voyage_number"  class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-4">
                                        <b>BL Number</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="bl_number"  class="form-control" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>BL Quantity</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="bl_qty"  class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Time Arrived</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="ta" class="form-control datepicker" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <b>Commence Discharge</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="comm_disch" class="form-control datepicker" >
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-4">
                                        <b>Complete Discharge</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="comp_disch"  class="form-control datepicker" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <b>Time Departured</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" name="td" class="form-control datepicker" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <input type="submit" value="SAVE" class="btn btn-primary m-t-15 waves-effect">
                                    <input type="reset" value="RESET" class="btn btn-danger m-t-15 waves-effect">
                                     </div>
                                      <?php echo form_close(); ?>
                                    
                               </br>
                            </br>
                       
                             </div>
                            </div>
                     </div>
           
                
                
           

                  
             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA DISCHARGE  
                            </h2>
                        </div>
                           <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Vessel Name</th>
                                            <th>Voyage Number</th>
                                            <th>BL Number</th>
                                            <th>BL Quantity</th>
                                            <th>Time Arrived</th>
                                            <th>Commence Discharge</th>
                                            <th>Complete Discharge</th>
                                            <th>Time Departured</th>
                                            <th>Action</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                          foreach ($data->result() as $key): ?>
                                        <tr>
                                            <td><?php echo $key->type; ?></td>
                                            <td><?php echo $key->vessel_name; ?></td>
                                            <td><?php echo $key->voyage_no; ?></td>
                                            <td><?php echo $key->bl_no; ?></td>
                                            <td><?php echo $key->bl_qty; ?></td>
                                            <td><?php echo $key->ta; ?></td>
                                            <td><?php echo $key->comm_disch; ?></td>
                                            <td><?php echo $key->comp_disch; ?></td>
                                            <td><?php echo $key->td; ?></td>
                                            <td> 
                                                <a  href="<?php echo base_url() ?>C_input_discharge/edit_disch/<?php echo $key->id_coalSch ?>/<?php echo $key->id_disch ?>/<?php echo $key->bl_qty ?>">
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </a> &nbsp || &nbsp
                                                <a  href="<?php echo base_url() ?>C_input_discharge/del_disch/<?php echo $key->id_coalSch ?>/<?php echo $key->id_disch ?>/<?php echo $key->bl_qty ?>" onclick="return confirm('Are You Sure Want To Delete Data?')">
                                                    <i class="icon-trash"></i>
                                                </a>
                                            </td>  
                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                      </div>
    </section>


