
<section class="content">

        <div class="container-fluid">

<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                SETTING
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <?php
                        switch ($status['status']) {
                        case '1' : ?>
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">SUPPLIER AGENT</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">BUYER AGENT</a></li>
                                        <li role="presentation"><a href="#profile_animation_3" data-toggle="tab">BUYER</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">TERM OF CONTRACT</a></li>
                                        <li role="presentation"><a href="#settings_animation_2" data-toggle="tab">SPECIFICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <?php 
                           $this->load->helper('form'); 
                           echo form_open('c_setting/edit_supplierAction');
                           foreach ($data->result() as $key): ?>
                           
                                <label for="email_address">Supplier Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nm_supplier" class="form-control" placeholder="Supplier Name" value="<?php echo $key->nm_supplier ?>" required>
                                        <input type="hidden" name="id_sup" class="form-control" value="<?php echo $key->id_sup ?>" >
                                        <input type="hidden" name="nm_supplier_lm" class="form-control" value="<?php echo $key->nm_supplier ?>" >
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php 
                            endforeach;
                            echo form_close(); 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($normal->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_supplier ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_supplier/<?php echo $key->id_sup ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_supplier/<?php echo $key->id_sup ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buyer');?>
                                <label for="email_address">Buyer Agent</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer Agent" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer Agent</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buyer->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buyer ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buyer/<?php echo $key->id_buyer ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buyer/<?php echo $key->id_buyer ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>

              <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_3">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER 
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buy');?>
                                <label for="email_address">Buyer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer" class="form-control" placeholder="Buyer" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buy ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buy/<?php echo $key->id_buy ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buy/<?php echo $key->id_buy ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_terms');?>
                                <label for="email_address">TERM OF CONTRACT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="terms" class="form-control" placeholder="TERM OF CONTRACT" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Term Of Contract</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($terms->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_terms ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_terms/<?php echo $key->id_terms ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_terms/<?php echo $key->id_terms ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                              <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SPECIFICATION
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_specification');?>
                                <label for="email_address">Specification</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="specification" class="form-control" placeholder="SPECIFICATION" required>
                                    </div>
                                </div>
                                <label for="email_address">Category</label>
                                <div class="form-group">
                                     <select class="form-control show-tick" name="category" required>
                                        <option value="">-- Please select --</option>
                                        <option value="1">Parameter</option>
                                        <option value="2">Satuan</option>
                                        <option value="3">Remarks</option>
                                        
                                    </select>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA PARAMETER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($parameter->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                               DATA SATUAN
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($satuan->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                            
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA REMARKS
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($remarks->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 
                                                    <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                              
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                        case '2':
                        ?>
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation"><a href="#home_animation_2" data-toggle="tab">SUPPLIER AGENT</a></li>
                                        <li role="presentation" class="active"><a href="#profile_animation_2" data-toggle="tab">BUYER AGENT</a></li>
                                         <li role="presentation"><a href="#profile_animation_3" data-toggle="tab">BUYER</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">TERM OF CONTRACT</a></li>
                                        <li role="presentation"><a href="#settings_animation_2" data-toggle="tab">SPECIFICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="home_animation_2">
                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <?php 
                           $this->load->helper('form'); 
                           echo form_open('c_setting/insert_seller');
                           ?>
                           
                                <label for="email_address">Supplier Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nm_supplier" class="form-control" placeholder="Supplier Name" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php 
                            echo form_close(); 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($normal->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_supplier ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_supplier/<?php echo $key->id_sup ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_supplier/<?php echo $key->id_sup ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="profile_animation_2">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/edit_buyerAction');?>
                           <?php foreach ($data->result() as $key): ?>
                                <label for="email_address">Buyer Agent</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer Agent" value="<?php echo $key->nm_buyer; ?>" required>
                                        <input type="hidden" name="id_buyer" class="form-control" value="<?php echo $key->id_buyer; ?>">
                                        <input type="hidden" name="buyer_agent_lm" class="form-control" value="<?php echo $key->nm_buyer; ?>">
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                                 <?php endforeach ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer Agent</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buyer->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buyer ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buyer/<?php echo $key->id_buyer ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buyer/<?php echo $key->id_buyer ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
             <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_3">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER 
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buy');?>
                                <label for="email_address">Buyer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buy ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buy/<?php echo $key->id_buy ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buy/<?php echo $key->id_buy ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_terms');?>
                                <label for="email_address">TERM OF CONTRACT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="terms" class="form-control" placeholder="TERM OF CONTRACT" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Term Of Contract</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($terms->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_terms ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_terms/<?php echo $key->id_terms ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_terms/<?php echo $key->id_terms ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                              <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SPECIFICATION
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_specification');?>
                                <label for="email_address">Specification</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="specification" class="form-control" placeholder="SPECIFICATION" required>
                                    </div>
                                </div>
                                <label for="email_address">Category</label>
                                <div class="form-group">
                                     <select class="form-control show-tick" name="category" required>
                                        <option value="">-- Please select --</option>
                                        <option value="1">Parameter</option>
                                        <option value="2">Satuan</option>
                                        <option value="3">Remarks</option>
                                        
                                    </select>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA PARAMETER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($parameter->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                               DATA SATUAN
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($satuan->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                            
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA REMARKS
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($remarks->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                  <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <?php
                        break;
                        case '3':
                        ?>
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation"><a href="#home_animation_2" data-toggle="tab">SUPPLIER AGENT</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">BUYER AGENT</a></li>
                                         <li role="presentation"><a href="#profile_animation_3" data-toggle="tab">BUYER</a></li>
                                        <li role="presentation" class="active"><a href="#messages_animation_2" data-toggle="tab">TERM OF CONTRACT</a></li>
                                        <li role="presentation"><a href="#settings_animation_2" data-toggle="tab">SPECIFICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="home_animation_2">
                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <?php 
                           $this->load->helper('form'); 
                           echo form_open('c_setting/insert_seller');
                           ?>
                           
                                <label for="email_address">Supplier Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nm_supplier" class="form-control" placeholder="Supplier Name" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php 
                            echo form_close(); 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($normal->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_supplier ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_supplier/<?php echo $key->id_sup ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_supplier/<?php echo $key->id_sup ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buyer');?>
                                <label for="email_address">Buyer Agent</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer Agent" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer Agent</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buyer ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buyer/<?php echo $key->id_buyer ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buyer/<?php echo $key->id_buyer ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                         <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_3">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER 
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buy');?>
                                <label for="email_address">Buyer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buy ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buy/<?php echo $key->id_buy ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buy/<?php echo $key->id_buy ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="messages_animation_2">
                                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/edit_termsAction');?>
                           <?php foreach ($data->result() as $key): ?>
                                <label for="email_address">TERM OF CONTRACT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="terms" class="form-control" placeholder="TERM OF CONTRACT" value="<?php echo $key->nm_terms; ?>" required>
                                        <input type="hidden" name="terms_lm" class="form-control" value="<?php echo $key->nm_terms; ?>">
                                        <input type="hidden" name="id_terms" class="form-control" value="<?php echo $key->id_terms; ?>">
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php endforeach ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Term Of Contract</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($terms->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_terms ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_terms/<?php echo $key->id_terms ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_terms/<?php echo $key->id_terms ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                              <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SPECIFICATION
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_specification');?>
                                <label for="email_address">Specification</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="specification" class="form-control" placeholder="SPECIFICATION" required>
                                    </div>
                                </div>
                                <label for="email_address">Category</label>
                                <div class="form-group">
                                     <select class="form-control show-tick" name="category" required>
                                        <option value="">-- Please select --</option>
                                        <option value="1">Parameter</option>
                                        <option value="2">Satuan</option>
                                        <option value="3">Remarks</option>
                                        
                                    </select>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA PARAMETER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($parameter->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                               DATA SATUAN
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($satuan->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                            
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA REMARKS
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($remarks->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                  <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                          <?php
                        break;
                        case '4':
                        ?>
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation"><a href="#home_animation_2" data-toggle="tab">SUPPLIER AGENT</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">BUYER AGENT</a></li>
                                         <li role="presentation"><a href="#profile_animation_3" data-toggle="tab">BUYER</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">TERM OF CONTRACT</a></li>
                                        <li role="presentation" class="active"><a href="#settings_animation_2" data-toggle="tab">SPECIFICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="home_animation_2">
                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <?php 
                           $this->load->helper('form'); 
                           echo form_open('c_setting/insert_seller');
                           ?>
                           
                                <label for="email_address">Supplier Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nm_supplier" class="form-control" placeholder="Supplier Name" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php 
                            echo form_close(); 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($normal->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_supplier ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_supplier/<?php echo $key->id_sup ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_supplier/<?php echo $key->id_sup ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buyer');?>
                                <label for="email_address">Buyer Agent</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer Agent" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer Agent</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buyer->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buyer ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buyer/<?php echo $key->id_buyer ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buyer/<?php echo $key->id_buyer ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                         <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_3">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER 
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buy');?>
                                <label for="email_address">Buyer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buy ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buy/<?php echo $key->id_buy ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buy/<?php echo $key->id_buy ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_terms');?>
                                <label for="email_address">TERM OF CONTRACT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="terms" class="form-control" placeholder="TERM OF CONTRACT" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Term Of Contract</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($terms->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_terms ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_terms/<?php echo $key->id_terms ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_terms/<?php echo $key->id_terms ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="settings_animation_2">
                                              <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SPECIFICATION
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/edit_specificationAction');?>
                           <?php foreach ($data->result() as $key): ?>
                                <label for="email_address">Specification</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="specification" class="form-control" value="<?php echo $key->nm_spec; ?>" placeholder="SPECIFICATION" required>
                                        <input type="hidden" name="id" class="form-control" value="<?php echo $key->id; ?>">
                                        <input type="hidden" name="specification_lm" class="form-control" value="<?php echo $key->nm_spec; ?>">
                                        <input type="hidden" name="category_lm" class="form-control" value="<?php echo $key->category; ?>">
                                    </div>
                                </div>
                                <label for="email_address">Category</label>
                                <div class="form-group">
                                     <select class="form-control show-tick" name="category" required>
                                        <?php if($key->category == 1)  {
                                            $value = "Parameter";
                                        } elseif ($key->category == 2) {
                                            $value = "Satuan";
                                        } elseif ($key->category == 3) {
                                            $value = "Remarks";
                                        } ?>
                                        <option value="<?php echo $key->category ?>" selected><?php echo $value; ?></option>
                                        <option value="1">Parameter</option>
                                        <option value="2">Satuan</option>
                                        <option value="3">Remarks</option>
                                        
                                    </select>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                                <?php endforeach ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA PARAMETER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($parameter->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                               DATA SATUAN
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($satuan->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                            
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA REMARKS
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($remarks->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                  <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php
                        break;
                        case '5':
                        ?>
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation"><a href="#home_animation_2" data-toggle="tab">SUPPLIER AGENT</a></li>
                                        <li role="presentation" ><a href="#profile_animation_2" data-toggle="tab">BUYER AGENT</a></li>
                                         <li role="presentation" class="active"><a href="#profile_animation_3" data-toggle="tab">BUYER</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">TERM OF CONTRACT</a></li>
                                        <li role="presentation"><a href="#settings_animation_2" data-toggle="tab">SPECIFICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="home_animation_2">
                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <?php 
                           $this->load->helper('form'); 
                           echo form_open('c_setting/insert_seller');
                           ?>
                           
                                <label for="email_address">Supplier Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nm_supplier" class="form-control" placeholder="Supplier Name" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php 
                            echo form_close(); 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier Name</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($normal->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_supplier ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_supplier/<?php echo $key->id_sup ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_supplier/<?php echo $key->id_sup ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="profile_animation_2">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER AGENT
                            </h2>
                        </div>
                         <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_buyer');?>
                                <label for="email_address">Buyer Agent</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer_agent" class="form-control" placeholder="Buyer Agent" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER AGENT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer Agent</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buyer->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buyer ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buyer/<?php echo $key->id_buyer ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buyer/<?php echo $key->id_buyer ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
             <div role="tabpanel" class="tab-pane animated fadeInRight active" id="profile_animation_3">
                                                  <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT BUYER 
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/edit_buyAction');?>
                           <?php foreach ($data->result() as $key): ?>
                                <label for="email_address">Buyer</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="buyer" class="form-control" placeholder="Buyer Agent" value="<?php echo $key->nm_buy; ?>" required>
                                        <input type="hidden" name="id_buy" class="form-control" value="<?php echo $key->id_buy; ?>">
                                        <input type="hidden" name="buyer_lm" class="form-control" value="<?php echo $key->nm_buy; ?>">
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                                 <?php endforeach ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA BUYER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Buyer</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($buy->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_buy ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_buy/<?php echo $key->id_buy ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_buy/<?php echo $key->id_buy ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                                            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_terms');?>
                                <label for="email_address">TERM OF CONTRACT</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="terms" class="form-control" placeholder="TERM OF CONTRACT" required>
                                    </div>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA TERM OF CONTRACT
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Term Of Contract</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($terms->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_terms ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_terms/<?php echo $key->id_terms ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_terms/<?php echo $key->id_terms ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
                                        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                              <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM INPUT SPECIFICATION
                            </h2>
                        </div>
                        <div class="body">
                           <?php $this->load->helper('form'); ?>
                           <?php echo form_open('c_setting/insert_specification');?>
                                <label for="email_address">Specification</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="specification" class="form-control" placeholder="SPECIFICATION" required>
                                    </div>
                                </div>
                                <label for="email_address">Category</label>
                                <div class="form-group">
                                     <select class="form-control show-tick" name="category" required>
                                        <option value="">-- Please select --</option>
                                        <option value="1">Parameter</option>
                                        <option value="2">Satuan</option>
                                        <option value="3">Remarks</option>
                                        
                                    </select>
                                </div>
                                <input type="submit" value="SUBMIT" class="btn btn-primary m-t-15 waves-effect">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Examples -->
                <!-- Full Body Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA PARAMETER
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($parameter->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                               DATA SATUAN
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($satuan->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                 <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                            
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               DATA REMARKS
                            </h2>
                        </div>
                        <div class="body">
                           <div class="table-responsive" >
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                          $no = 1;   
                                          foreach ($remarks->result() as $key): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $key->nm_spec ?></td>
                                                  <td>
                                                     <a class="btn icon-btn btn-success" href="<?php echo base_url() ?>c_setting/edit_specification/<?php echo $key->category ?>/<?php echo $key->id ?> "><i class="glyphicon glyphicon-pencil"></i></a>
                                                    <a class="btn icon-btn btn-danger" href="<?php echo base_url() ?>c_setting/del_specification/<?php echo $key->category ?>/<?php echo $key->id ?> " onclick="return confirm('Are You Sure Want To Delete Data?')"><i class="icon-trash"></i></a>
                                                </td>
                                            </tr>
                                         <?php
                                         $no++;
                                          endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        break; 
                        } ?>
                        


                    </div>
                </div>
            </div>
             </div>
    </section>
