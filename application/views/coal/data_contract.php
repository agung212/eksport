<section class="content">
        <div class="container-fluid">

<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Coal Contract   
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url() ?>c_contract/input_contract">Add Contract</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                           <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="example">
                                    <thead>
                                        <tr>
                                            <th>Contract No</th>
                                            <th>Sales Purchase</th>
                                            <th>Supplier Agent</th>
                                            <th>MV</th>
                                            <th>BL / Final Quantity</th>
                                            <th>Buyer Agent</th>
                                            <th>Buyer</th>
                                            <th>Term Of Contract</th>
                                            <th>Port Of Loading</th>
                                            <th>Port Of Discharge</th>
                                            
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                          foreach ($coal_contract->result() as $key): ?>
                                        <tr>
											<td style="background: #f0f8ff"><a href="<?php echo base_url() ?>C_contract/view_contract/<?php echo $key->id_contract ?> "><?php echo $key->contract_no; ?></a></td>
											<td><?php echo $key->sales_purchase; ?></td>
											<td><?php echo $key->nm_supplier; ?></td>
                                            <td>
                                            <?php
                                            $sql = "SELECT vessel_name FROM coal_schedule WHERE id_contract = '$key->id_contract'";
                                            $query = $this->db->query($sql); 
                                            if($query->num_rows() > 0 ) {
                                            foreach ($query->result() as $row) {
                                                echo "-".$row->vessel_name ."</br>";
                                                } 
                                            } ?>
                                            </td>
                                             <td>
                                            <?php
                                            $sql = "SELECT eta_total FROM coal_schedule WHERE id_contract = '$key->id_contract'";
                                            $query = $this->db->query($sql); 
                                            if($query->num_rows() > 0 ) {
                                            foreach ($query->result() as $row) {
                                                echo $row->eta_total ."</br>";
                                                } 
                                            } ?>
                                            </td>
                                            <td><?php echo $key->nm_buyer; ?></td>
                                            <td><?php echo $key->nm_buy; ?></td>
                                            
                                            <td><?php echo $key->nm_terms; ?></td>
                                            <td><?php echo $key->port_of_landing; ?></td>
                                            <td><?php echo $key->port_of_discharge; ?></td>
                                            
                                           
										</tr>
                                    <?php endforeach ?>
									</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             </div>
    </section>