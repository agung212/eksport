<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('assets/favicon.ico')?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/plugins/node-waves/waves.css')?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/plugins/animate-css/animate.css')?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/alert/css/sweetalert.css')?>">
</head>

<body>
  <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js')?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/node-waves/waves.js')?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.js')?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/js/admin.js')?>"></script>
    <script src="<?php echo base_url('assets/js/pages/examples/sign-in.js')?>"></script>
    <script src="<?php echo base_url('assets/alert/js/sweetalert.min.js')?>"></script>
    <script src="<?php echo base_url('assets/alert/js/qunit-1.18.0.js')?>"></script>
</body>
<?php switch($aksi) {
 case "login" : ?>
 <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Welcome <?php echo ucfirst($nama); ?>',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_home');
    } ,1500);   
  </script>
 <?php 
    break;
    case "sukses" :  ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Adding Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_setting/index/<?php echo $stat; ?>');
    } ,1500);   
  </script>
<?php 
    break;
    case "hapus" : ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Delete Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_setting/index/<?php echo $stat; ?>');
    } ,1500);   
  </script>

  <?php 
    break;
    case "update" : ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Update Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_setting/index/<?php echo $stat; ?>');
    } ,1500);   
  </script>

  <?php 
    break;
    case "sukses_kontrak" :  ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Adding Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract');
    } ,1500);   
  </script>
<?php 
    break;
    case "hapus_kontrak" : ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Delete Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract');
    } ,1500);   
  </script>

  <?php 
    break;
    case "update_kontrak" : ?>
     <script type='text/javascript'>
    setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Update Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract');
    } ,1500);   
  </script>

  <?php 
    break;
    case "hapus_spec" : ?>
     <script type='text/javascript'>
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract/edit_contract/<?php echo $stat; ?>');
    } ,0);   
  </script>

  <?php 
    break;
    case "sukses_schedule" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Added Data <?php echo $waktu; ?>',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract/view_contract/<?php echo $id_contract; ?>');
    } ,1500);   
  </script>

   <?php 
    break;
    case "update_schedule" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Update Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract/view_contract/<?php echo $id_contract; ?>');
    } ,1500);   
  </script>

  <?php 
    break;
    case "hapus_schedule" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Delete Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_contract/view_contract/<?php echo $id_contract; ?>');
    } ,1500);   
  </script>

   <?php 
    break;
    case "sukses_disc" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Added Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_input_discharge');
    } ,1500);   
  </script>

   <?php 
    break;
    case "hapus_disch" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Delete Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_input_discharge');
    } ,1500);   
  </script>

   <?php 
    break;
    case "update_disc" : ?>
     <script type='text/javascript'>
      setTimeout(function () { 
    
        swal({
            title: 'SUCCESS',
            text:  'Successful Update Data',
            type: 'success',
            timer: 3000,
            showConfirmButton: true
        });     
    },10);  
    window.setTimeout(function(){ 
        window.location.replace('<?php echo base_url()?>C_input_discharge');
    } ,1500);   
  </script>

<?php 
    break;
} ?>

</html>