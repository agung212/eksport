<?php $sistem = $this->session->userdata('sistem'); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>APPLICATION VDNI</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/plugins/node-waves/waves.css')?>" rel="stylesheet" />

     <link href="<?php echo base_url('assets/plugins/waitme/waitMe.css')?>" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/plugins/animate-css/animate.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')?>" rel="stylesheet" />
    <!-- Morris Chart Css-->
    <link href="<?php echo base_url('assets/plugins/morrisjs/morris.css')?>" rel="stylesheet" />
     <link href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.css')?>" rel="stylesheet" />

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')?>" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('assets/css/themes/all-themes.css')?>" rel="stylesheet" />
</head>
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url() ?>c_home">VDNI - APPLICATION </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <!-- Notifications -->
                <!-- #END# Notifications -->
                <!-- #END# Tasks -->


                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                        <i class="material-icons">view_headline</i>
                    </a>
                     <ul class="dropdown-menu">
                            <li class="header">SISTEM INFORMASI</li>
                            <li class="body">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="menu" style="overflow: hidden; width: auto; height: 254px;">
                                    <li <?php echo (empty($sistem['ORE']))?'hidden':'' ?>>
                                        <a href="<?php echo base_url(); ?>ore/home" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">business</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>ORE</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i><?php echo ($sistem['ORE'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['COAL']))?'hidden':'' ?>>
                                        <a href="<?php echo base_url(); ?>C_home" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>COAL</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i><?php echo ($sistem['COAL'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['LIMESTONE']))?'hidden':'' ?>>
                                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>LIMESTONE</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['LIMESTONE'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['EKSPOR']))?'hidden':'' ?>>
                                        <a href="<?php echo base_url(); ?>/Eksplore/ControllerSite" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>EKSPOR</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['EKSPOR'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li <?php echo (empty($sistem['SOLAR']))?'hidden':'' ?>>
                                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>SOLAR</h4>
                                                <p>
                                                    <i class="material-icons">play_for_work</i> <?php echo ($sistem['SOLAR'] == "F")?' FULL':' READ' ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>

                            </li>
                        </ul>
                </li>

            </ul>
        </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url('assets/images/user.png')?>" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h5><?php echo ucfirst($this->session->userdata('nama')); ?></b></h5></div>
                   
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo base_url(); ?>admin/login_admin/Logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="<?php echo base_url() ?>C_home">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>C_contract">
                            <i class="material-icons">content_copy</i>
                            <span>Coal Contract</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>C_coal_schedule">
                            <i class="material-icons">assignment</i>
                            <span>Coal Schedule</span>
                        </a>
                    </li>
                     <li>
                        <a href="<?php echo base_url() ?>C_input_discharge">
                            <i class="material-icons">assignment</i>
                            <span>Input Discharge</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>C_report">
                            <i class="material-icons">view_list</i>
                            <span>Report</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>c_setting">
                            <i class="material-icons col-red">donut_large</i>
                            <span>Setting</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">IT VDNI</a>.
                </div>
                <div class="version">
                   
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
       
    </section>

    <?php $this->load->view($content); ?>
  <!-- Select Plugin Js -->
   
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js')?>"></script>

    
    <!-- Jquery Core Js -->
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php  echo base_url('assets/plugins/node-waves/waves.js')?>"></script>

    <script src="<?php  echo base_url('assets/plugins/autosize/autosize.js')?>"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-countto/jquery.countTo.js')?>"></script>

    <script src="<?php echo base_url('assets/plugins/momentjs/moment.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')?>"></script>

   
    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/raphael/raphael.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/morrisjs/morris.js')?>"></script>


     <script src="<?php echo base_url('js/pages/forms/basic-form-elements.js')?>"></script>
    <!-- ChartJs -->
    <script src="<?php echo base_url('assets/plugins/chartjs/Chart.bundle.js')?>"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/flot-charts/jquery.flot.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-charts/jquery.flot.resize.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-charts/jquery.flot.pie.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-charts/jquery.flot.categories.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-charts/jquery.flot.time.js')?>"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-sparkline/jquery.sparkline.js')?>"></script>

      <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


  
    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/js/pages/tables/jquery-datatable.js')?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/js/admin.js')?>"></script>
   
    <script src="<?php echo base_url('assets/js/pages/forms/basic-form-elements.js')?>"></script>
    <!-- Demo Js -->
    <script src="<?php echo base_url('assets/js/demo.js')?>"></script>
    <script src="<?php echo base_url('assets/js/arf.js')?>"></script>
    <script src="<?php echo base_url('assets/tags_input/src/jquery.tagsinput.js')?>"></script>
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
    </script>
    <script type="text/javascript">
        $(document).ready(function()
    {
        <!-- handle event combobox kategori ketika nilainya di ganti -->
        $("#combokategori").change(function() {
        <!-- mendapatkan value dari combobox -->
        var idkategori = $(this).val();
        if (idkategori != "")
        {
         <!-- Request data sub kategori berdasarkan idkategori yang dipilih -->
            $.ajax({
            type:"post",
            url:"<?php echo base_url() ?>C_input_discharge/listKota",
            data:"id="+ idkategori,
            success: function(data){
             $("#subkategori").html(data);
        }
        });
        }
    });
    });
    </script>
    <script>
     $(document).ready(function() {
        $('#tags_1').tagsInput({
            width: 'auto'
        });
        $("input").val()
     });
    </script>
</body>

</html>
