<?php if ( ! defined('BASEPATH')) exit('Tidak ada akses langsung script diperbolehkan');
 
if ( ! function_exists('get_bulan'))
{
    function get_bulan()
    {
        $namaBulan = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December');
        return $namaBulan;
    }
}

if( ! function_exists('get_nama_bulan'))
{
    function get_nama_bulan($bln)
    {
        $namaBulan = get_bulan();
        return $namaBulan[(int)$bln];
    }
}
   
/*  End of file common_helper.php
    Location ./application/helpers/common_helper.php */