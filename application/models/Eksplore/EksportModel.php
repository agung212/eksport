<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EksportModel extends CI_Model {


	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './pdf/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '8192';
		$config['overwrite'] = true;
		$config['file_name'] = "CERTIFICATE_OF_QUALITY_".$filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

}
