<?php

class M_master_kontrak extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function cek_kontrak($master_kontrak)
	{
		$query = $this->db->get_where('tb_master_contract', array('master_kontrak' => $master_kontrak));
		$query = $query->num_rows();
		return $query;
	}

	//--> ambil semua data 
	function get_all_kontrak()
	{
		return $this->db->select('*')
										->from('tb_master_contract')
										//->where('status_pegawai', 'aktif')
										->join('tb_seller', 'tb_seller.id_seller = tb_master_contract.seller_agent')
										->order_by('id_kontrak', 'asc')
										->get()->result();
	}


	//--> ambil data tertentu
	function get_row_kontrak($id)
	{
		return $this->db->select('*')
										->from('tb_master_contract')
										->join('tb_seller', 'tb_seller.id_seller = tb_master_contract.seller_agent')
										->where('id_kontrak', $id)
										->get()->row();
	}

	function all_kontrak_kecil()
	{
		return $this->db->select('*')
										->from('tb_kontrak_kecil')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_kontrak_kecil.id_kontrak')
										->join('tb_seller', 'tb_seller.id_seller = tb_kontrak_kecil.seller_agent')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
										->order_by('id', 'asc')
										->get()->result();
	}


	function get_kontrak_kecil($id)
	{
		return $this->db->select('*')
										->from('tb_kontrak_kecil')
										->join('tb_seller', 'tb_seller.id_seller = tb_kontrak_kecil.seller_agent')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
										->where('id_kontrak', $id)
										->get()->result();
	}

	function get_kontrak_kecil_row($id)
	{
		return $this->db->select('*')
										->from('tb_kontrak_kecil')
										->join('tb_seller', 'tb_seller.id_seller = tb_kontrak_kecil.seller_agent')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
										->where('id_kontrak', $id)
										->get()->result();
	}

	function get_row_stok($id_kontrak)
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										// ->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										// ->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										// ->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->where('tb_stok_masuk.master_kontrak', $id_kontrak)
										->get()->row();
	}

	function get_id_max()
	{
		// $kode 	= "KTR";
		$sql  	= "SELECT max(id_kontrak) as max_id FROM tb_master_contract";
		$row  	= $this->db->query($sql)->row();
		$max_id = $row->max_id;
		// $max_no = substr($max_id,3);
		$new_no = $max_id + 1;
		$id 		= $new_no;
		return $id;
	}

	//--> tambah
	function insert_kontrak()
	{

		//insert Supplier
		$jum_supplier = $this->input->post('jml_supplier')-1;
		$supplier = $this->input->post('supplier');

		$jum_quantity = $this->input->post('jml_quantity')-1;
		$quantity = $this->input->post('quantity');

		

		if($jum_supplier < $jum_quantity)
		{
			$jml = $jum_quantity;
		}
		elseif($jum_supplier > $jum_quantity)
		{
			$jml = $jum_supplier;
		}
		else
		{
			$jml = $jum_supplier;
		}

		$jml_qty=0;

		$i=0;
		while($i < $jml)
		{
			//--> tambah sub_tim
			$data_supplier = array(
					'id_kontrak' => $this->input->post('id_kontrak'),
					'seller_agent' 	=> $this->input->post('seller_agent'),
					'supplier'    => $supplier[$i],
					'quantity'    => $quantity[$i],
					'st'    => $quantity[$i],
					'status'    => $this->input->post('status')
	
				);

			$jml_qty = $jml_qty + $quantity[$i];


			$this->db->insert('tb_kontrak_kecil', $data_supplier);

			$i++;
		}
		
		$cif_usd = $this->input->post('cif_usd');
		$dap_usd = $this->input->post('dap_usd');

		if ($cif_usd < $dap_usd) {
			$pilih = $dap_usd;
		} else{
			$pilih = $cif_usd;
		}

		$lc_amount = $jml_qty * $pilih;

		//Terms
		$b = $this->input->post('terms');
		$baru = "";

		for ($a=0; count($b)>=$a; $a++) { 
			$baru = $baru.$b[$a].",";
		}

		//Price IDR
		$ignore = array('Rp','.' ); 
		$price_idr=str_replace(',','.',str_replace($ignore,"",$this->input->post('price_idr')));

		//--> tambah 
		$data_kontrak = array(
				'id_kontrak'		=> $this->input->post('id_kontrak'),
				'user'		=> $this->input->post('user'),
				'master_kontrak' 	=> $this->input->post('master_kontrak'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'sales_kontrak' 	=> $this->input->post('sales_kontrak'),
				'swift_lc' 	=> $this->input->post('swift_lc'),
				'lc_amount' 	=> $lc_amount,
				'tgl_awal' 	=> $this->input->post('tgl_awal'),
				'tgl_akhir' 	=> $this->input->post('tgl_akhir'),
				'terms' 	=> $baru,
				'addendum' 	=> $this->input->post('addendum'),
				'ni' 	=> (double)$this->input->post('ni'),
				'ni_usd' 	=> $this->input->post('ni_usd'),
				'sio_mgo' 	=> (double)$this->input->post('sio_mgo'),
				'sio_usd' 	=> $this->input->post('sio_usd'),
				'keterangan' 	=> $this->input->post('keterangan'),
				'quantity' 	=> $jml_qty,
				'price_idr' 	=> $price_idr,
				'cif_usd' 	=> $cif_usd,
				'dap_usd' 	=> $dap_usd,
				'cif_sc_usd' 	=> $this->input->post('cif_sc_usd'),
				'dap_sc_usd' 	=> $this->input->post('dap_sc_usd'),
				'tgl_dibuat' 	=> date('Y-m-d H:i:s'),
				'status' 	=> $this->input->post('status'),
				'mc' 	=> $this->input->post('mc'),
				'mc_usd' 	=> $this->input->post('mc_usd'),
			);
		$this->db->insert('tb_master_contract', $data_kontrak);

		//$quantity = $this->db->query("SELECT seller_agent, SUM(quantity) FROM tb_kontrak_kecil GROUP BY id_kontrak")

		$data_kontrak = array(
				'master_kontrak' 	=> $this->input->post('id_kontrak'),
				'stok' 	=> $jml_qty,
				);
		$this->db->insert('tb_stok', $data_kontrak);

	}

	function addendum_update($id)
	{

		//insert Supplier
		$jum_supplier = $this->input->post('jml_supplier')-1;
		$supplier = $this->input->post('supplier');

		$jum_quantity = $this->input->post('jml_quantity')-1;
		$quantity = $this->input->post('quantity');

		$qty_sisa = $this->db->get_where('tb_kontrak_kecil', ['id_kontrak' => $id])->result();
        $st[]=0;

        foreach ($qty_sisa as $q) {
            $st[] = $q->quantity - $q->st; 
        }

        $j=1;
        while ( $j <= 2) {
            $s[] = $st[$j];
           

        $j++;
        }


		if($jum_supplier < $jum_quantity)
		{
			$jml = $jum_quantity;
		}
		elseif($jum_supplier > $jum_quantity)
		{
			$jml = $jum_supplier;
		}
		else
		{
			$jml = $jum_supplier;
		}

		$jml_qty=0;

		$i=0;
		while($i < $jml)
		{
			//--> tambah sub_tim
			$data_supplier = array(
					'id_kontrak' => $this->input->post('id_kontrak'),
					'seller_agent' 	=> $this->input->post('seller_agent'),
					'supplier'    => $supplier[$i],
					'quantity'    => $quantity[$i],
					'st'    => $quantity[$i]-$s[$i],
					'status'    => $this->input->post('status')
	
				);

			$jml_qty = $jml_qty + $quantity[$i];


			$this->db->insert('tb_kontrak_kecil', $data_supplier);

			$i++;
		}

		$data_s = array(
					'status'    => "NON AKTIF"
				);
		$this->db->where('id_kontrak', $id)
						 ->update('tb_kontrak_kecil', $data_s);
		
		$cif_usd = $this->input->post('cif_usd');
		$dap_usd = $this->input->post('dap_usd');

		if ($cif_usd < $dap_usd) {
			$pilih = $dap_usd;
		} else{
			$pilih = $cif_usd;
		}

		$lc_amount = $jml_qty * $pilih;

		//Terms
		$b = $this->input->post('terms');
		$baru = "";

		for ($a=0; count($b)>=$a; $a++) { 
			$baru = $baru.$b[$a].",";
		}

		//Price IDR
		$ignore = array('Rp','.' ); 
		$price_idr=str_replace(',','.',str_replace($ignore,"",$this->input->post('price_idr')));


		//--> update 
		$data_kontrak = array(
				'id_kontrak'		=> $this->input->post('id_kontrak'),
				'user'		=> $this->input->post('user'),
				'master_kontrak' 	=> $this->input->post('master_kontrak'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'sales_kontrak' 	=> $this->input->post('sales_kontrak'),
				'swift_lc' 	=> $this->input->post('swift_lc'),
				'lc_amount' 	=> $lc_amount,
				'tgl_awal' 	=> $this->input->post('tgl_awal'),
				'tgl_akhir' 	=> $this->input->post('tgl_akhir'),
				'terms' 	=> $baru,
				'addendum' 	=> $this->input->post('addendum'),
				'ni' 	=> (double)$this->input->post('ni'),
				'ni_usd' 	=> $this->input->post('ni_usd'),
				'sio_mgo' 	=> (double)$this->input->post('sio_mgo'),
				'sio_usd' 	=> $this->input->post('sio_usd'),
				'keterangan' 	=> $this->input->post('keterangan'),
				'quantity' 	=> $jml_qty,
				'price_idr' 	=> $price_idr,
				'cif_usd' 	=> $cif_usd,
				'dap_usd' 	=> $dap_usd,
				'cif_sc_usd' 	=> $this->input->post('cif_sc_usd'),
				'dap_sc_usd' 	=> $this->input->post('dap_sc_usd'),
				'tgl_dibuat' 	=> date('Y-m-d H:i:s'),
				'status' 	=> $this->input->post('status'),
				'mc' 	=> $this->input->post('mc'),
				'mc_usd' 	=> $this->input->post('mc_usd'),
			);
		$this->db->insert('tb_master_contract', $data_kontrak);

		$stok_masuk = $this->db->get_where('tb_stok_masuk', ['master_kontrak' => $id])->result();
        $jml_sm = 0;

        foreach ($stok_masuk as $sm) {

            $jml_sm = $jml_sm + $sm->jml_stok;
        }

		$stok_add =  $jml_qty - $jml_sm;

		$stok_kontrak = array(
				'master_kontrak' 	=> $this->input->post('id_kontrak'),
				'stok' 	=> $stok_add,
				);
		$this->db->insert('tb_stok', $stok_kontrak);
	}

	function update_kontrak($id_kontrak)
	{	

		$this->db->where('id_kontrak', $id_kontrak)
						 ->delete('tb_kontrak_kecil');

		//update Supplier
		$jum_supplier = $this->input->post('jml_supplier')-1;
		$supplier = $this->input->post('supplier');

		$jum_quantity = $this->input->post('jml_quantity')-1;
		$quantity = $this->input->post('quantity');

		

		if($jum_supplier < $jum_quantity)
		{
			$jml = $jum_quantity;
		}
		elseif($jum_supplier > $jum_quantity)
		{
			$jml = $jum_supplier;
		}
		else
		{
			$jml = $jum_supplier;
		}

		$jml_qty=0;

		$i=0;
		while($i < $jml)
		{
			//--> tambah sub_tim
			$data_supplier = array(
					'id_kontrak' => $this->input->post('id_kontrak'),
					'seller_agent' 	=> $this->input->post('seller_agent'),
					'supplier'    => $supplier[$i],
					'quantity'    => $quantity[$i],
					'st'	    => $quantity[$i],
					'status'    => $this->input->post('status')
	
				);

			$jml_qty = $jml_qty + $quantity[$i];


			$this->db->insert('tb_kontrak_kecil', $data_supplier);

			$i++;
		}
		
		$cif_usd = $this->input->post('cif_usd');
		$dap_usd = $this->input->post('dap_usd');

		if ($cif_usd < $dap_usd) {
			$pilih = $dap_usd;
		} else{
			$pilih = $cif_usd;
		}

		$lc_amount = $jml_qty * $pilih;

		//Price IDR
		$ignore = array('Rp','.' ); 
		$price_idr=str_replace(',','.',str_replace($ignore,"",$this->input->post('price_idr')));

		//Terms
		$b = $this->input->post('terms');
		$baru = "";

		for ($a=0; $a < count($b); $a++) { 
			$baru = $baru.$b[$a].",";
		}

		//--> update 
		$data_kontrak = array(
				'id_kontrak'		=> $this->input->post('id_kontrak'),
				'user'		=> $this->input->post('user'),
				'master_kontrak' 	=> $this->input->post('master_kontrak'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'sales_kontrak' 	=> $this->input->post('sales_kontrak'),
				'swift_lc' 	=> $this->input->post('swift_lc'),
				'lc_amount' 	=> $lc_amount,
				'tgl_awal' 	=> $this->input->post('tgl_awal'),
				'tgl_akhir' 	=> $this->input->post('tgl_akhir'),
				'terms' 	=> $baru,
				'addendum' 	=> $this->input->post('addendum'),
				'ni' 	=> (double)$this->input->post('ni'),
				'ni_usd' 	=> $this->input->post('ni_usd'),
				'sio_mgo' 	=> (double)$this->input->post('sio_mgo'),
				'sio_usd' 	=> $this->input->post('sio_usd'),
				'keterangan' 	=> $this->input->post('keterangan'),
				'quantity' 	=> $jml_qty,
				'price_idr' 	=> $price_idr,
				'cif_usd' 	=> $cif_usd,
				'dap_usd' 	=> $dap_usd,
				'cif_sc_usd' 	=> $this->input->post('cif_sc_usd'),
				'dap_sc_usd' 	=> $this->input->post('dap_sc_usd'),
				'tgl_dibuat' 	=> date('Y-m-d H:i:s'),
				'status' 	=> $this->input->post('status'),
				'mc' 	=> $this->input->post('mc'),
				'mc_usd' 	=> $this->input->post('mc_usd'),
			);
		//$this->db->insert('tb_master_contract', $data_kontrak);

		$this->db->where('id_kontrak', $id_kontrak)
						 ->update('tb_master_contract', $data_kontrak);

		$data_kontrak_stok = array(
				'master_kontrak' 	=> $this->input->post('id_kontrak'),
				'stok' 	=> $jml_qty,
				);
		$this->db->where('master_kontrak', $id_kontrak)
						 ->update('tb_stok', $data_kontrak_stok);

	}

	//--> update
	function update_status($id_kontrak)
	{
		
		//--> edit data
		$data_status = array(
				'id_kontrak'		=> $this->input->post('id_kontrak'),
				'status' 	=> $this->input->post('status'),
			);
		$this->db->where('id_kontrak', $id_kontrak)
						 ->update('tb_master_contract', $data_status);

		$this->db->where('id_kontrak', $id_kontrak)
						 ->update('tb_kontrak_kecil', $data_status);
	}

	//-- hapus data
	function hapus_data($id_kontrak)
	{
		
		$this->db->where('id_kontrak', $id_kontrak)
						 ->delete('tb_master_contract');

		$this->db->where('id_kontrak', $id_kontrak)
						 ->delete('tb_kontrak_kecil');

		$this->db->where('master_kontrak', $id_kontrak)
						 ->delete('tb_stok');
	}

	function hapus_supplier($id)
	{

		$this->db->where('id', $id)
						 ->delete('tb_kontrak_kecil');
	}

}