<?php

class M_stok extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	function cek_stok($stok)
	{
		$query = $this->db->get_where('tb_stok_masuk', array('voyage_number' => $stok));
		$query = $query->num_rows();
		return $query;
	}

	function get_all_stok()
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->order_by('id_sm', 'asc')
										->get()->result();
	}

	function get_supplier($id)
	{
		$output = "<option> -- Pilih Supplier -- </option>";
		$supplier   = $this->db->select('*')
										  ->from('tb_kontrak_kecil')
										  ->where('seller_agent', $id)
										  ->order_by('supplier', 'asc')		  
										  ->get()->result();

		foreach($supplier as $row)
		{			
			$output .= "<option value='$row->supplier'> $row->supplier </option>";
		}
		return $output;
	}

	function get_row_stok($id_sm)
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->where('tb_master_contract.status', 'AKTIF')
										->where('id_sm', $id_sm)
										->get()->row();
	}

	function get_stok()
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->get()->result();
	}

	function get_row_stok_remain($id_kontrak)
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										//->join('tb_hasil_lab', 'tb_hasil_lab.id_stok = tb_stok_masuk.id_sm')
										->where('tb_master_contract.id_kontrak', $id_kontrak)
										->get()->result();
	}

	function get_row_coa_remain($id_sm)
	{
		return $this->db->select('*')
										->from('tb_hasil_lab')
										//->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_stok_masuk', 'tb_stok_masuk.id_sm = tb_hasil_lab.id_stok')
										->where('tb_hasil_lab.id_stok', $id_sm)
										->get()->result();
	}

	function get_row_coa($id_sm)
	{
		return $this->db->select('*')
										->from('tb_hasil_lab')
										->join('tb_stok_masuk', 'tb_stok_masuk.id_sm = tb_hasil_lab.id_stok')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->where('tb_hasil_lab.id_stok', $id_sm)
										->get()->row();
	}

	function get_row_intertek($id_sm)
	{
		return $this->db->select('*')
										->from('tb_intertek')
										->join('tb_stok_masuk', 'tb_stok_masuk.id_sm = tb_intertek.id_stok')
										//->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->where('id_sm', $id_sm)
										->get()->row();
	}

	function get_seller()
	{
		return $this->db->select('*')
										->from('tb_kontrak_kecil')
										->join('tb_seller', 'tb_seller.id_seller = tb_kontrak_kecil.seller_agent')
										->where('status', 'aktif')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_kontrak_kecil.supplier')
										->where('status', 'aktif')
										->group_by('seller_agent')
										->order_by('id_kontrak', 'asc')
										->get()->result();
	}

	function get_all_lab()
	{
		return $this->db->select('*')
										->from('tb_hasil_lab')
										//->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										//->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->order_by('id_lab', 'asc')
										->get()->result();
	}

	function insert_stok()
	{	

		// $id_agent = $this->input->post('id_agent');
  //       $id_supplier = $this->input->post('id_supplier');
  //       $jml_stok = $this->input->post('jml_stok');

  //       $quantity = $this->db->get_where('tb_kontrak_kecil', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier])->row();

  //       $sisa_stok = $quantity->quantity - $jml_stok;
		// //--> tambah 
		$data_stok = array(
				'id_sm'		=> $this->input->post('id_sm'),
				'tgl_input' 	=> date('Y-m-d H:i:s'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'supplier' 	=> $this->input->post('supplier'),
				'nama_tongkang' 	=> $this->input->post('nama_tongkang'),
				'voyage_number' 	=> $this->input->post('voyage_number'),
				'bl_number' 	=> $this->input->post('bl_number'),
				'jml_stok' 	=> $this->input->post('jml_stok'),
				'sisa_stok' 	=> $this->input->post('jml_stok'),
				'tgl_arr' 	=> $this->input->post('tgl_arr'),
				'comm_disch' 	=> $this->input->post('comm_disch'),
				'compl_disch' 	=> $this->input->post('compl_disch'),
				'tgl_dep' 	=> $this->input->post('tgl_dep'),
				'user'	=> $this->input->post('user'),
				'master_kontrak'	=> $this->input->post('master_kontrak'),
			);
		$this->db->insert('tb_stok_masuk', $data_stok);

  //       $jml_stok = $this->input->post('jml_stok');

  //       $quantity = $this->db->get_where('tb_kontrak_kecil', ['seller_agent' => $id_agent, 'supplier'=> $id_supplier])->row();

  //       $sisa_stok = $quantity->quantity - $jml_stok;

		// $data_status = array(
		// 			'seller_agent' 	=> $id_agent,
		// 			'supplier'    => $id_supplier,
		// 			'status'    => "NON AKTIF",
		// 			'id'		=> $id,	
		// 			'st' 	=> $sisa_stok
	
		// 		);

		// $this->db->where('id', $id)
		// 				 ->update('tb_kontrak_kecil', $data_status);

	}

	function update_status($id_supplier, $id, $id_agent, $sisa_stok){


		$data_status = array(
					'seller_agent' 	=> $id_agent,
					'supplier'    => $id_supplier,
					// 'id_kontrak'		=> $id_kontrak,
					'id'			=> $id,	
					'status'    => "NON AKTIF",
					'st' 	=> $sisa_stok
	
				);

		$this->db->where('id', $id)
						->where('supplier', $id_supplier)
						 ->update('tb_kontrak_kecil', $data_status);


	}



	function update_ktr_kecil($id_supplier, $id, $id_agent, $sisa_stok){


		$data_status = array(
					'seller_agent' 	=> $id_agent,
					'supplier'    => $id_supplier,
					'id'		=> $id,	
					//'status'    => "NON AKTIF",
					'st' 	=> $sisa_stok
	
				);

		$this->db->where('id', $id)
					->where('supplier', $id_supplier)
						 ->update('tb_kontrak_kecil', $data_status);


	}

	function update_stok_qty($id_kontrak, $sisa_stok2){


		$data_status = array(
					'master_kontrak'		=> $id_kontrak,	
					//'status'    => "NON AKTIF",
					'stok' 	=> $sisa_stok2
	
				);

		$this->db->where('master_kontrak', $id_kontrak)
						 ->update('tb_stok', $data_status);


	}

	function insert_hasil_lab($id_sm)
	{
		//--> tambah 
		$nmdokcoa = str_replace(' ', '_', $_FILES['dokumen_coa']['name']);
		$tempatdokcoa = './assets/dokumen_coa/'. $nmdokcoa;

		$sio=$this->input->post('sio');
		$mgo=$this->input->post('mgo');
		$fe=$this->input->post('fe');
		$ni=$this->input->post('ni');

		$sio_mgo = $sio/$mgo;
		$fe_ni = $fe/$ni;

		//Perhitungan Ni stat
		$ni_kontrak=$this->input->post('ni_ktr');
		$ni_usd=$this->input->post('ni_usd');
		
		$sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		$sio_usd=$this->input->post('sio_usd');

		if ($ni < $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($ni > $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat_coa = 0;
		}

		//Sio/Mgo Stat
		if ($sio_mgo > $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} elseif ($sio_mgo < $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} else{
			$sio_mgo_stat = 0;
		}

		//MC
		$mc = $this->input->post('mc');
		$mc_kontrak=$this->input->post('mc_ktr');
		$mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($mc < $pisah[0]) {
			$mc_stat_coa = ($pisah[0] - $mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat_coa = ($pisah[1] - $mc) * $mc_usd;
		}else{
			$mc_stat_coa = 0;
		}

		//Final Coa Price
		$cif_usd=$this->input->post('cif_usd');
		$dap_usd=$this->input->post('dap_usd');

		if ($cif_usd > $dap_usd) {
			$final_coa_price = $cif_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		} else {
			$final_coa_price = $dap_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		}

		$data_lab = array(
				'id_stok'		=> $this->input->post('id_stok'),
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'ni' 	=> $ni,
				'fe' 	=> $fe,
				'fe_ni' 	=> number_format($fe_ni,3,'.',','),
				'sio' 	=> $sio,
				'mgo' 	=> $mgo,
				'sio_mgo' => number_format($sio_mgo,3,'.',','),
				'ni_stat' =>  $ni_stat_coa,
				'sio_mgo_stat' => number_format($sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('aio'),
				'mc' 	=> $mc,
				'picture'    => $nmdokcoa,
				'mc_stat' 	=> $mc_stat_coa,
			);
		$this->db->insert('tb_hasil_lab', $data_lab);
		//--> proses upload
		move_uploaded_file($_FILES['dokumen_coa']['tmp_name'], $tempatdokcoa);

		//Sisa Stok
		$qty = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id_sm])->row();
		$coa_qty = $this->input->post('coa_qty');

		$sisa_stok = $qty->sisa_stok - $coa_qty;

		$data_sisa = array('sisa_stok' => $sisa_stok, );

		$this->db->where('id_sm', $id_sm)
						 ->update('tb_stok_masuk', $data_sisa);


		//Intertek
		$nmdokin = str_replace(' ', '_', $_FILES['in_dokumen']['name']);
		$tempatdokin = './assets/dokumen_intertek/'. $nmdokin;

		//Perhitungan SIO/MGO dan NI/Fe
		$in_sio=$this->input->post('in_sio');
		$in_mgo=$this->input->post('in_mgo');
		$in_fe=$this->input->post('in_fe');
		$in_ni=$this->input->post('in_ni');

		$in_sio_mgo = $in_sio/$in_mgo;
		$in_fe_ni = $in_fe/$in_ni;

		//Perhitungan Ni stat
		// $ni_kontrak=$this->input->post('ni_ktr');
		// $ni_usd=$this->input->post('ni_usd');
		
		// $sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		// $sio_usd=$this->input->post('sio_usd');

		if ($in_ni < $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($in_ni > $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat = 0;
		}

		//Sio/Mgo Stat
		if ($in_sio_mgo > $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} elseif ($in_sio_mgo < $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} else{
			$in_sio_mgo_stat = 0;
		}

		//MC
		$in_mc = $this->input->post('in_mc');
		// $mc_kontrak=$this->input->post('mc_ktr');
		// $mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($in_mc < $pisah[0]) {
			$mc_stat = ($pisah[0] - $in_mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat = ($pisah[1] - $in_mc) * $mc_usd;
		}else{
			$mc_stat = 0;
		}
		
		//Final Unit Price
		if ($cif_usd > $dap_usd) {
			$final_unit_price = $cif_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		} else{
			$final_unit_price = $dap_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		}

		$data_intertek = array(
				'id_stok'		=> $this->input->post('id_stok'),
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'ni' 	=> $in_ni,
				'fe' 	=> $in_fe,
				'fe_ni' 	=> number_format($in_fe_ni,3,'.',','),
				'sio' 	=> $in_sio,
				'mgo' 	=> $in_mgo,
				'sio_mgo' =>  number_format($in_sio_mgo,3,'.',','),
				'ni_stat' => $ni_stat,
				'sio_mgo_stat' => number_format($in_sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('in_aio'),
				'mc' 	=> $in_mc,
				'mc_stat' 	=> $mc_stat,
				'picture'    => $nmdokin,
			);
		$this->db->insert('tb_intertek', $data_intertek);
		//--> proses upload
		move_uploaded_file($_FILES['in_dokumen']['tmp_name'], $tempatdokin);

		//Sisa Stok
		$qty = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id_sm])->row();
		$intertek_qty = $this->input->post('intertek_qty');

		$sisa_stok = $qty->sisa_stok - $intertek_qty;


		$data_stat = array(
				'id_sm'		=> $this->input->post('id_sm'),
				'sisa_stok'		=> $sisa_stok,
				'stat_lab' 	=> $this->input->post('stat_lab'),
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'price_intertek' => number_format($final_unit_price,3,'.',','),
				'price_afterCoa'=> number_format($final_coa_price,3,'.',','),
				);
		$this->db->where('id_sm', $id_sm)
						 ->update('tb_stok_masuk', $data_stat);

	}

	function update_hasil_lab($id)
	{
		//--> tambah 
		$nmdokcoa = str_replace(' ', '_', $_FILES['dokumen_coa']['name']);
		$tempatdokcoa = './assets/dokumen_coa/'. $nmdokcoa;

		$sio=$this->input->post('sio');
		$mgo=$this->input->post('mgo');
		$fe=$this->input->post('fe');
		$ni=$this->input->post('ni');

		$sio_mgo = $sio/$mgo;
		$fe_ni = $fe/$ni;

		//Perhitungan Ni stat
		$ni_kontrak=$this->input->post('ni_ktr');
		$ni_usd=$this->input->post('ni_usd');
		
		$sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		$sio_usd=$this->input->post('sio_usd');

		if ($ni < $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($ni > $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat_coa = 0;
		}

		//Sio/Mgo Stat
		if ($sio_mgo > $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} elseif ($sio_mgo < $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} else{
			$sio_mgo_stat = 0;
		}

		//MC
		$mc = $this->input->post('mc');
		$mc_kontrak=$this->input->post('mc_ktr');
		$mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($mc < $pisah[0]) {
			$mc_stat_coa = ($pisah[0] - $mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat_coa = ($pisah[1] - $mc) * $mc_usd;
		}else{
			$mc_stat_coa = 0;
		}

		//Final Coa Price
		$cif_usd=$this->input->post('cif_usd');
		$dap_usd=$this->input->post('dap_usd');

		if ($cif_usd > $dap_usd) {
			$final_coa_price = $cif_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		} else {
			$final_coa_price = $dap_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		}

		$data_lab = array(
				'id_stok'		=> $id,
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'ni' 	=> $ni,
				'fe' 	=> $fe,
				'fe_ni' 	=> number_format($fe_ni,3,'.',','),
				'sio' 	=> $sio,
				'mgo' 	=> $mgo,
				'sio_mgo' => number_format($sio_mgo,3,'.',','),
				'ni_stat' =>  $ni_stat_coa,
				'sio_mgo_stat' => number_format($sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('aio'),
				'mc' 	=> $mc,
				'picture'    => $nmdokcoa,
				'mc_stat' 	=> $mc_stat_coa,
			);
		$this->db->where('id_stok', $id)
					->update('tb_hasil_lab', $data_lab);
		//--> proses upload
		move_uploaded_file($_FILES['dokumen_coa']['tmp_name'], $tempatdokcoa);

		//Sisa Stok
		$qty = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id])->row();
		$coa_qty = $this->input->post('coa_qty');

		$sisa_stok = $qty->jml_stok - $coa_qty;

		$data_sisa = array('sisa_stok' => $sisa_stok, );

		$this->db->where('id_sm', $id)
						 ->update('tb_stok_masuk', $data_sisa);


		//Intertek
		$nmdokin = str_replace(' ', '_', $_FILES['in_dokumen']['name']);
		$tempatdokin = './assets/dokumen_intertek/'. $nmdokin;

		//Perhitungan SIO/MGO dan NI/Fe
		$in_sio=$this->input->post('in_sio');
		$in_mgo=$this->input->post('in_mgo');
		$in_fe=$this->input->post('in_fe');
		$in_ni=$this->input->post('in_ni');

		$in_sio_mgo = $in_sio/$in_mgo;
		$in_fe_ni = $in_fe/$in_ni;

		//Perhitungan Ni stat
		// $ni_kontrak=$this->input->post('ni_ktr');
		// $ni_usd=$this->input->post('ni_usd');
		
		// $sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		// $sio_usd=$this->input->post('sio_usd');

		if ($in_ni < $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($in_ni > $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat = 0;
		}

		//Sio/Mgo Stat
		if ($in_sio_mgo > $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} elseif ($in_sio_mgo < $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} else{
			$in_sio_mgo_stat = 0;
		}

		//MC
		$in_mc = $this->input->post('in_mc');
		// $mc_kontrak=$this->input->post('mc_ktr');
		// $mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($in_mc < $pisah[0]) {
			$mc_stat = ($pisah[0] - $in_mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat = ($pisah[1] - $in_mc) * $mc_usd;
		}else{
			$mc_stat = 0;
		}
		
		//Final Unit Price
		if ($cif_usd > $dap_usd) {
			$final_unit_price = $cif_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		} else{
			$final_unit_price = $dap_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		}

		$data_intertek = array(
				'id_stok'		=> $id,
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'ni' 	=> $in_ni,
				'fe' 	=> $in_fe,
				'fe_ni' 	=> number_format($in_fe_ni,3,'.',','),
				'sio' 	=> $in_sio,
				'mgo' 	=> $in_mgo,
				'sio_mgo' =>  number_format($in_sio_mgo,3,'.',','),
				'ni_stat' => $ni_stat,
				'sio_mgo_stat' => number_format($in_sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('in_aio'),
				'mc' 	=> $in_mc,
				'mc_stat' 	=> $mc_stat,
				'picture'    => $nmdokin,
			);
		$this->db->where('id_stok', $id)
						->update('tb_intertek', $data_intertek);
		//--> proses upload
		move_uploaded_file($_FILES['in_dokumen']['tmp_name'], $tempatdokin);

		//Sisa Stok
		$qty = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id])->row();
		$intertek_qty = $this->input->post('intertek_qty');

		$sisa_stok = $qty->jml_stok - $intertek_qty;


		$data_stat = array(
				'id_sm'		=> $id,
				'sisa_stok'		=> $sisa_stok,
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'price_intertek' => number_format($final_unit_price,3,'.',','),
				'price_afterCoa'=> number_format($final_coa_price,3,'.',','),
				);
		$this->db->where('id_sm', $id)
						 ->update('tb_stok_masuk', $data_stat);

	}

	function update_stok_sm($id_sm)
	{
		//--> tambah 
		$data_stok = array(
				'id_sm'		=> $this->input->post('id_sm'),
				//'tgl_input' 	=> date('Y-m-d H:i:s'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'supplier' 	=> $this->input->post('supplier'),
				'nama_tongkang' 	=> $this->input->post('nama_tongkang'),
				'voyage_number' 	=> $this->input->post('voyage_number'),
				'bl_number' 	=> $this->input->post('bl_number'),
				'jml_stok' 	=> $this->input->post('jml_stok'),
				'tgl_arr' 	=> $this->input->post('tgl_arr'),
				'comm_disch' 	=> $this->input->post('comm_disch'),
				'compl_disch' 	=> $this->input->post('compl_disch'),
				'tgl_dep' 	=> $this->input->post('tgl_dep'),
				'user'	=> $this->input->post('user'),
				
			);
		$this->db->where('id_sm', $id_sm)
						 ->update('tb_stok_masuk', $data_stok);
	}

	function update_stok($id_sm)
	{
		//--> tambah 
		$data_stok = array(
				'id_sm'		=> $this->input->post('id_sm'),
				//'tgl_input' 	=> date('Y-m-d H:i:s'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'supplier' 	=> $this->input->post('supplier'),
				'nama_tongkang' 	=> $this->input->post('nama_tongkang'),
				'voyage_number' 	=> $this->input->post('voyage_number'),
				'bl_number' 	=> $this->input->post('bl_number'),
				'jml_stok' 	=> $this->input->post('jml_stok'),
				'tgl_arr' 	=> $this->input->post('tgl_arr'),
				'comm_disch' 	=> $this->input->post('comm_disch'),
				'compl_disch' 	=> $this->input->post('compl_disch'),
				'tgl_dep' 	=> $this->input->post('tgl_dep'),
				'user'	=> $this->input->post('user'),
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
			);
		$this->db->where('id_sm', $id_sm)
						 ->update('tb_stok_masuk', $data_stok);


		$nmdokcoa = str_replace(' ', '_', $_FILES['dokumen_coa']['name']);
		$tempatdokcoa = './assets/dokumen_coa/'. $nmdokcoa;

		$sio=$this->input->post('sio');
		$mgo=$this->input->post('mgo');
		$fe=$this->input->post('fe');
		$ni=$this->input->post('ni');

		$sio_mgo = $sio/$mgo;
		$fe_ni = $fe/$ni;

		//Perhitungan Ni stat
		$ni_kontrak=$this->input->post('ni_ktr');
		$ni_usd=$this->input->post('ni_usd');
		
		$sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		$sio_usd=$this->input->post('sio_usd');

		if ($ni < $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($ni > $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat_coa = 0;
		}

		//Sio/Mgo Stat
		if ($sio_mgo > $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} elseif ($sio_mgo < $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} else{
			$sio_mgo_stat = 0;
		}

		//MC
		$mc = $this->input->post('mc');
		$mc_kontrak=$this->input->post('mc_ktr');
		$mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($mc < $pisah[0]) {
			$mc_stat_coa = ($pisah[0] - $mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat_coa = ($pisah[1] - $mc) * $mc_usd;
		}else{
			$mc_stat_coa = 0;
		}

		//Final Coa Price
		$cif_usd=$this->input->post('cif_usd');
		$dap_usd=$this->input->post('dap_usd');

		if ($cif_usd > $dap_usd) {
			$final_coa_price = $cif_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		} else {
			$final_coa_price = $dap_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		}

		$data_lab = array(
				'id_stok'		=> $this->input->post('id_sm'),
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'ni' 	=> $ni,
				'fe' 	=> $fe,
				'fe_ni' 	=> number_format($fe_ni,3,'.',','),
				'sio' 	=> $sio,
				'mgo' 	=> $mgo,
				'sio_mgo' => number_format($sio_mgo,3,'.',','),
				'ni_stat' =>  $ni_stat_coa,
				'sio_mgo_stat' => number_format($sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('aio'),
				'mc' 	=> $mc,
				'picture'    => $nmdokcoa,
				'mc_stat' 	=> $mc_stat_coa,
			);

		$this->db->where('id_stok', $id_sm)
						 ->update('tb_hasil_lab', $data_lab);
		//--> proses upload
		move_uploaded_file($_FILES['dokumen_coa']['tmp_name'], $tempatdokcoa);

		$nmdokin = str_replace(' ', '_', $_FILES['in_dokumen']['name']);
		$tempatdokin = './assets/dokumen_intertek/'. $nmdokin;

		//Perhitungan SIO/MGO dan NI/Fe
		$in_sio=$this->input->post('in_sio');
		$in_mgo=$this->input->post('in_mgo');
		$in_fe=$this->input->post('in_fe');
		$in_ni=$this->input->post('in_ni');

		$in_sio_mgo = $in_sio/$in_mgo;
		$in_fe_ni = $in_fe/$in_ni;

		//Perhitungan Ni stat
		// $ni_kontrak=$this->input->post('ni_ktr');
		// $ni_usd=$this->input->post('ni_usd');
		
		// $sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		// $sio_usd=$this->input->post('sio_usd');

		if ($in_ni < $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($in_ni > $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat = 0;
		}

		//Sio/Mgo Stat
		if ($in_sio_mgo > $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} elseif ($in_sio_mgo < $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} else{
			$in_sio_mgo_stat = 0;
		}

		//MC
		$in_mc = $this->input->post('in_mc');
		// $mc_kontrak=$this->input->post('mc_ktr');
		// $mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($in_mc < $pisah[0]) {
			$mc_stat = ($pisah[0] - $in_mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat = ($pisah[1] - $in_mc) * $mc_usd;
		}else{
			$mc_stat = 0;
		}
		
		//Final Unit Price
		if ($cif_usd > $dap_usd) {
			$final_unit_price = $cif_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		} else{
			$final_unit_price = $dap_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		}

		$data_intertek = array(
				'id_stok'		=> $this->input->post('id_sm'),
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'ni' 	=> $in_ni,
				'fe' 	=> $in_fe,
				'fe_ni' 	=> number_format($in_fe_ni,3,'.',','),
				'sio' 	=> $in_sio,
				'mgo' 	=> $in_mgo,
				'sio_mgo' =>  number_format($in_sio_mgo,3,'.',','),
				'ni_stat' => $ni_stat,
				'sio_mgo_stat' => number_format($in_sio_mgo_stat,3,'.',','),
				'aio' 	=> $this->input->post('in_aio'),
				'mc' 	=> $in_mc,
				'mc_stat' 	=> $mc_stat,
				'picture'    => $nmdokin,
			);

		$this->db->where('id_stok', $id_sm)
						 ->update('tb_intertek', $data_intertek);
		//--> proses upload
		move_uploaded_file($_FILES['in_dokumen']['tmp_name'], $tempatdokin);

	}

	//-- hapus data
	function hapus_data($id_sm, $qty_hps,  $seller, $supplier, $stok_hps, $id_kontrak)
	{

		$data_hapus = array('st' => $qty_hps, );

		$this->db->where('supplier', $supplier)
					->where('seller_agent', $seller)
						 ->update('tb_kontrak_kecil', $data_hapus);

		$data_sh = array('stok' => $stok_hps, );

		$this->db->where('master_kontrak', $id_kontrak)
					->update('tb_stok', $data_sh);
		
		$this->db->where('id_sm', $id_sm)
						 ->delete('tb_stok_masuk');

		$this->db->where('id_stok', $id_sm)
						 ->delete('tb_hasil_lab');

		$this->db->where('id_stok', $id_sm)
						 ->delete('tb_intertek');
	}

	//-- hapus data
	function hapus_lab($id)
	{	
		$qty = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id])->row();
		$sisa_stok = $qty->jml_stok;
		
		$data_stat = array(
				'id_sm'		=> $id,
				'sisa_stok'		=> $sisa_stok,
				'coa_qty' 	=> 0,
				'intertek_qty' 	=> 0,
				'price_intertek' => 0,
				'price_afterCoa'=> 0,
				'stat_lab' => NULL,
				);

		$this->db->where('id_sm', $id)
						 ->update('tb_stok_masuk', $data_stat);

		$this->db->where('id_stok', $id)
						 ->delete('tb_hasil_lab');

		$this->db->where('id_stok', $id)
						 ->delete('tb_intertek');
	}

	function adj_stok($id_sm)
	{
		$nmdokcoa = str_replace(' ', '_', $_FILES['dokumen_coa']['name']);
		$tempatdokcoa = './assets/dok_adj_coa/'. $nmdokcoa;

		$sio=$this->input->post('sio');
		$mgo=$this->input->post('mgo');
		$fe=$this->input->post('fe');
		$ni=$this->input->post('ni');

		$sio_mgo = $sio/$mgo;
		$fe_ni = $fe/$ni;

		//Perhitungan Ni stat
		$ni_kontrak=$this->input->post('ni_ktr');
		$ni_usd=$this->input->post('ni_usd');
		
		$sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		$sio_usd=$this->input->post('sio_usd');

		if ($ni < $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($ni > $ni_kontrak) {
			$ni_stat_coa = ($ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat_coa = 0;
		}

		//Sio/Mgo Stat
		if ($sio_mgo > $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} elseif ($sio_mgo < $sio_mgo_kontrak) {
			$sio_mgo_stat = ($sio_mgo_kontrak - $sio_mgo) * 100 * $sio_usd;
		} else{
			$sio_mgo_stat = 0;
		}

		//MC
		$mc = $this->input->post('mc');
		$mc_kontrak=$this->input->post('mc_ktr');
		$mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($mc < $pisah[0]) {
			$mc_stat_coa = ($pisah[0] - $mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat_coa = ($pisah[1] - $mc) * $mc_usd;
		}else{
			$mc_stat_coa = 0;
		}

		//Final Coa Price
		$cif_usd=$this->input->post('cif_usd');
		$dap_usd=$this->input->post('dap_usd');

		if ($cif_usd > $dap_usd) {
			$final_coa_price = $cif_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		} else {
			$final_coa_price = $dap_usd + $ni_stat_coa + $mc_stat_coa + $sio_mgo_stat;
		}


		$nmdokin = str_replace(' ', '_', $_FILES['in_dokumen']['name']);
		$tempatdokin = './assets/dok_adj_in/'. $nmdokin;

		//Perhitungan SIO/MGO dan NI/Fe
		$in_sio=$this->input->post('in_sio');
		$in_mgo=$this->input->post('in_mgo');
		$in_fe=$this->input->post('in_fe');
		$in_ni=$this->input->post('in_ni');

		$in_sio_mgo = $in_sio/$in_mgo;
		$in_fe_ni = $in_fe/$in_ni;

		//Perhitungan Ni stat
		// $ni_kontrak=$this->input->post('ni_ktr');
		// $ni_usd=$this->input->post('ni_usd');
		
		// $sio_mgo_kontrak=$this->input->post('sio_mgo_ktr');
		// $sio_usd=$this->input->post('sio_usd');

		if ($in_ni < $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} elseif ($in_ni > $ni_kontrak) {
			$ni_stat = ($in_ni - $ni_kontrak) * 100 * $ni_usd;
		} else{
			$ni_stat = 0;
		}

		//Sio/Mgo Stat
		if ($in_sio_mgo > $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} elseif ($in_sio_mgo < $sio_mgo_kontrak) {
			$in_sio_mgo_stat = ($sio_mgo_kontrak - $in_sio_mgo) * 100 * $sio_usd;
		} else{
			$in_sio_mgo_stat = 0;
		}

		//MC
		$in_mc = $this->input->post('in_mc');
		// $mc_kontrak=$this->input->post('mc_ktr');
		// $mc_usd=$this->input->post('mc_usd');

		$pisah = explode("-",$mc_kontrak);
		if ($in_mc < $pisah[0]) {
			$mc_stat = ($pisah[0] - $in_mc) * $mc_usd;
		} elseif($in_mc > $pisah[1]) {
			$mc_stat = ($pisah[1] - $in_mc) * $mc_usd;
		}else{
			$mc_stat = 0;
		}
		
		//Final Unit Price
		if ($cif_usd > $dap_usd) {
			$final_unit_price = $cif_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		} else{
			$final_unit_price = $dap_usd + $ni_stat + $mc_stat + $in_sio_mgo_stat;
		}

		//--> tambah 
		$data_stok = array(
				'id_sm'		=> $this->input->post('id_sm'),
				'tgl_input' 	=> date('Y-m-d H:i:s'),
				'seller_agent' 	=> $this->input->post('seller_agent'),
				'supplier' 	=> $this->input->post('supplier'),
				'nama_tongkang' 	=> $this->input->post('nama_tongkang'),
				'voyage_number' 	=> $this->input->post('voyage_number'),
				'bl_number' 	=> $this->input->post('bl_number'),
				'jml_stok' 	=> $this->input->post('jml_stok'),
				'tgl_arr' 	=> $this->input->post('tgl_arr'),
				'comm_disch' 	=> $this->input->post('comm_disch'),
				'compl_disch' 	=> $this->input->post('compl_disch'),
				'tgl_dep' 	=> $this->input->post('tgl_dep'),
				'user'	=> $this->input->post('user'),
				'price_intertek' => number_format($final_unit_price,3,'.',','),
				'price_afterCoa'=> number_format($final_coa_price,3,'.',','),
				'master_kontrak'	=> $this->input->post('master_kontrak'),
				
				'coa_qty' 	=> $this->input->post('coa_qty'),
				'ni_c' 	=> $ni,
				'fe_c' 	=> $fe,
				'fe_ni_c' 	=> $fe_ni,
				'sio_c' 	=> $sio,
				'mgo_c' 	=> $mgo,
				'sio_mgo_c' => $sio_mgo,
				'ni_stat_c' => $ni_stat_coa,
				'sio_mgoStat_c' => $sio_mgo_stat,
				'aio_c' 	=> $this->input->post('aio'),
				'mc_c' 	=> $mc,
				'picture_c'    => $nmdokcoa,
				'mc_stat_c' 	=> $mc_stat_coa,

				
				'intertek_qty' 	=> $this->input->post('intertek_qty'),
				'ni_i' 	=> $in_ni,
				'fe_i' 	=> $in_fe,
				'fe_ni_i' 	=> $in_fe_ni,
				'sio_i' 	=> $in_sio,
				'mgo_i' 	=> $in_mgo,
				'sio_mgo_i' => $in_sio_mgo,
				'ni_stat_i' => $ni_stat,
				'sio_mgoStat_i' => $in_sio_mgo_stat,
				'aio_i' 	=> $this->input->post('in_aio'),
				'mc_i' 	=> $in_mc,
				'mc_stat_i' 	=> $mc_stat,
				'picture_i'    => $nmdokin,
			);
		$this->db->insert('tb_stok_adj', $data_stok);

		// $data_stat_adj = array(
		// 		'id_sm'		=> $this->input->post('id_sm'),
		// 		'stat_adj' 	=> $this->input->post('stat_adj'),
		// 		);
		// $this->db->where('id_sm', $id_sm)
		// 				 ->update('tb_stok_masuk', $data_stat_adj);

		move_uploaded_file($_FILES['dokumen_coa']['tmp_name'], $tempatdokcoa);

		//--> proses upload
		move_uploaded_file($_FILES['in_dokumen']['tmp_name'], $tempatdokin);

	}
}