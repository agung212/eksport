<?php

class M_seller extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	//--> ambil semua data 
	function get_all_seller()
	{
		return $this->db->select('*')
										->from('tb_seller')
										//->where('status_pegawai', 'aktif')
										->order_by('id_seller', 'asc')
										->get()->result();
	}
	//--> ambil semua data pengguna non-aktif
	function get_all_pengguna_nonaktif()
	{
		return $this->db->select('*')
										->from('tb_pegawai')
										->where('status_pegawai', 'nonaktif')
										->order_by('id_pegawai', 'asc')
										->get()->result();
	}

	function cek_seller($nama_seller="")
	{
		$query = $this->db->get_where('tb_seller', array('nama_seller' => $nama_seller));
		$query = $query->num_rows();
		return $query;
	}

	//--> ambil data tertentu
	function get_row_seller($id)
	{
		return $this->db->select('*')
										->from('tb_seller')
										//->join('tb_user', 'tb_user.id_fk = tb_pegawai.id_pegawai')
										->where('id_seller', $id)
										->get()->row();
	}


	//--> tambah
	function insert_seller()
	{
		//--> tambah 
		$data_seller = array(
				'id_seller'		=> $this->input->post('id_seller'),
				'nama_seller' 	=> $this->input->post('nama_seller'),
			);
		$this->db->insert('tb_seller', $data_seller);

	}

	//--> update
	function update_seller($id_seller)
	{
		
		//--> edit data
		$data_seller = array(
				'id_seller'		=> $this->input->post('id_seller'),
				'nama_seller' 	=> $this->input->post('nama_seller'),
			);
		$this->db->where('id_seller', $id_seller)
						 ->update('tb_seller', $data_seller);
	}

	//-- hapus data
	function hapus_data($id_seller)
	{
		
		$this->db->where('id_seller', $id_seller)
						 ->delete('tb_seller');
	}

}