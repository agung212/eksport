<?php

class M_supplier extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function cek_supplier($nama_supplier="")
	{
		$query = $this->db->get_where('tb_supplier', array('nama_supplier' => $nama_supplier));
		$query = $query->num_rows();
		return $query;
	}
	//--> ambil semua data 
	function get_all_supplier()
	{
		return $this->db->select('*')
										->from('tb_supplier')
										//->where('status_pegawai', 'aktif')
										->order_by('id_supplier', 'asc')
										->get()->result();
	}
	//--> ambil semua data pengguna non-aktif
	function get_all_pengguna_nonaktif()
	{
		return $this->db->select('*')
										->from('tb_pegawai')
										->where('status_pegawai', 'nonaktif')
										->order_by('id_pegawai', 'asc')
										->get()->result();
	}

	//--> ambil data tertentu
	function get_row_supplier($id)
	{
		return $this->db->select('*')
										->from('tb_supplier')
										//->join('tb_user', 'tb_user.id_fk = tb_pegawai.id_pegawai')
										->where('id_supplier', $id)
										->get()->row();
	}


	//--> tambah
	function insert_supplier()
	{
		//--> tambah 
		$data_supplier = array(
				'id_supplier'		=> $this->input->post('id_supplier'),
				'nama_supplier' 	=> $this->input->post('nama_supplier'),
			);
		$this->db->insert('tb_supplier', $data_supplier);

	}

	//--> update
	function update_supplier($id_supplier)
	{
		
		//--> edit data
		$data_supplier = array(
				'id_supplier'		=> $this->input->post('id_supplier'),
				'nama_supplier' 	=> $this->input->post('nama_supplier'),
			);
		$this->db->where('id_supplier', $id_supplier)
						 ->update('tb_supplier', $data_supplier);
	}

	//-- hapus data
	function hapus_data($id_supplier)
	{
		
		$this->db->where('id_supplier', $id_supplier)
						 ->delete('tb_supplier');
	}

}