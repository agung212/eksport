<?php

class M_stok_adj extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	function cek_stok($stok)
	{
		$query = $this->db->get_where('tb_stok_masuk', array('voyage_number' => $stok));
		$query = $query->num_rows();
		return $query;
	}

	function jml_notif()
	{
		return $this->db->select("*, count(*) as jml_stat")
										->from('tb_stok_adj')
										//->like('master_kontrak','KTR')
										->where('sk', 0)
										->get()->row();
	}

	function jml_notif_fin()
	{
		return $this->db->select("*, count(*) as jml_fin")
										->from('tb_stok_adj')
										//->like('master_kontrak','KTR')
										->where('sk', 1)
										->get()->row();
	}

	function jml_notif_fin2()
	{
		return $this->db->select("*, count(*) as jml_fin")
										->from('tb_stok_adj')
										//->like('master_kontrak','KTR')
										->where('sk', 2)
										->get()->row();
	}

	function jml_notif_fin3()
	{
		return $this->db->select("*, count(*) as jml_fin")
										->from('tb_stok_adj')
										//->like('master_kontrak','KTR')
										->where('fin', 1)
										->get()->row();
	}

	function get_all_adj()
	{
		return $this->db->select('*')
										->from('tb_stok_adj')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_adj.seller_agent')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_adj.supplier')
										->order_by('id_adj', 'asc')
										->get()->result();
	}

	function get_row_adj($id)
	{
		return $this->db->select('*')
										->from('tb_stok_adj')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_adj.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_adj.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_adj.seller_agent')
										->where('id_adj', $id)
										->get()->row();
	}

	function get_row_sm($id_sm)
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->where('id_sm', $id_sm)
										->get()->row();
	}

	function get_row_coa($id_sm)
	{
		return $this->db->select('*')
										->from('tb_hasil_lab')
										->where('id_stok', $id_sm)
										->get()->row();
	}

	function get_row_intertek($id_sm)
	{
		return $this->db->select('*')
										->from('tb_intertek')
										->where('id_stok', $id_sm)
										->get()->row();
	}

	function update_status($id_adj)
	{
		
		//--> edit data
		$data_status = array(
				'id_adj'		=> $this->input->post('id_adj'),
				'fin' 	=> $this->input->post('status'),
			);

		$this->db->where('id_adj', $id_adj)
						 ->update('tb_stok_adj', $data_status);
	}

	function update_sk($id_adj)
	{
		
		//--> edit data
		$data_status = array(
				'id_adj'		=> $this->input->post('id_adj'),
				'sk' 	=> $this->input->post('status'),
			);

		$this->db->where('id_adj', $id_adj)
						 ->update('tb_stok_adj', $data_status);
	}

	function update_gm($id_adj, $id_sm)
	{
		$stok_masuk = $this->db->get_where('tb_stok_masuk', ['id_sm' => $id_sm])->row();
		$hasil_lab = $this->db->get_where('tb_hasil_lab', ['id_stok' => $id_sm])->row();
		$intertek = $this->db->get_where('tb_intertek', ['id_stok' => $id_sm])->row();
		
		$stok_adj = $this->db->get_where('tb_stok_adj', ['id_sm' => $id_sm])->row();

		$data_sm = array(
				'id_sm'			=> $stok_adj->id_sm,
				'jml_stok'		=> $stok_adj->jml_stok,
				'tgl_input'			=> $stok_adj->tgl_input,
				'sisa_stok'			=> $stok_adj->sisa_stok,
				'master_kontrak'			=> $stok_adj->master_kontrak,
				'seller_agent'			=> $stok_adj->seller_agent,
				'supplier'			=> $stok_adj->supplier,
				'nama_tongkang'			=> $stok_adj->nama_tongkang,
				'tgl_arr'			=> $stok_adj->tgl_arr,
				'tgl_dep'			=> $stok_adj->tgl_dep,
				'comm_disch'			=> $stok_adj->comm_disch,
				'compl_disch'			=> $stok_adj->compl_disch,
				'price_afterCoa'			=> $stok_adj->price_afterCoa,
				'price_intertek'			=> $stok_adj->price_intertek,
				'voyage_number'			=> $stok_adj->voyage_number,
				'bl_number'			=> $stok_adj->bl_number,
				);
		$this->db->where('id_sm', $id_sm)
						 ->update('tb_stok_masuk', $data_sm);

		$data_lab = array(
				'coa_qty'			=> $stok_adj->coa_qty,
				'ni'			=> $stok_adj->ni_c,
				'fe'			=> $stok_adj->fe_c,
				'sio'			=> $stok_adj->sio_c,
				'mgo'			=> $stok_adj->mgo_c,
				'aio'			=> $stok_adj->aio_c,
				'sio_mgo'			=> $stok_adj->sio_mgo_c,
				'fe_ni'			=> $stok_adj->fe_ni_c,
				'mc'			=> $stok_adj->mc_c,
				'ni_stat'			=> $stok_adj->ni_stat_c,
				'mc_stat'			=> $stok_adj->mc_stat_c,
				'sio_mgo_stat'			=> $stok_adj->sio_mgoStat_c,
				);
		$this->db->where('id_stok', $id_sm)
						 ->update('tb_hasil_lab', $data_lab);

		$data_intertek = array(
				'intertek_qty'			=> $stok_adj->intertek_qty,
				'ni'			=> $stok_adj->ni_i,
				'fe'			=> $stok_adj->fe_i,
				'sio'			=> $stok_adj->sio_i,
				'mgo'			=> $stok_adj->mgo_i,
				'aio'			=> $stok_adj->aio_i,
				'sio_mgo'			=> $stok_adj->sio_mgo_i,
				'fe_ni'			=> $stok_adj->fe_ni_i,
				'mc'			=> $stok_adj->mc_i,
				'ni_stat'			=> $stok_adj->ni_stat_i,
				'mc_stat'			=> $stok_adj->mc_stat_i,
				'sio_mgo_stat'			=> $stok_adj->sio_mgoStat_i,
				);
		$this->db->where('id_stok', $id_sm)
						 ->update('tb_intertek', $data_intertek);

		//Data Dari Stok Masuk Ke Adjustment Stok
		$data_status = array(
				'id_adj'		=> $this->input->post('id_adj'),
				'id_sm'			=> $stok_masuk->id_sm,
				'jml_stok'		=> $stok_masuk->jml_stok,
				'tgl_input'			=> $stok_masuk->tgl_input,
				'sisa_stok'			=> $stok_masuk->sisa_stok,
				'master_kontrak'			=> $stok_masuk->master_kontrak,
				'seller_agent'			=> $stok_masuk->seller_agent,
				'supplier'			=> $stok_masuk->supplier,
				'nama_tongkang'			=> $stok_masuk->nama_tongkang,
				'tgl_arr'			=> $stok_masuk->tgl_arr,
				'tgl_dep'			=> $stok_masuk->tgl_dep,
				'comm_disch'			=> $stok_masuk->comm_disch,
				'compl_disch'			=> $stok_masuk->compl_disch,
				'price_afterCoa'			=> $stok_masuk->price_afterCoa,
				'price_intertek'			=> $stok_masuk->price_intertek,
				'voyage_number'			=> $stok_masuk->voyage_number,
				'bl_number'			=> $stok_masuk->bl_number,
				//Intertek
				'intertek_qty'			=> $intertek->intertek_qty,
				'ni_i'			=> $intertek->ni,
				'fe_i'			=> $intertek->fe,
				'sio_i'			=> $intertek->sio,
				'mgo_i'			=> $intertek->mgo,
				'aio_i'			=> $intertek->aio,
				'sio_mgo_i'			=> $intertek->sio_mgo,
				'fe_ni_i'			=> $intertek->fe_ni,
				'mc_i'			=> $intertek->mc,
				'ni_stat_i'			=> $intertek->ni_stat,
				'mc_stat_i'			=> $intertek->mc_stat,
				'sio_mgoStat_i'			=> $intertek->sio_mgo_stat,
				//Hasil Lab Coa
				'coa_qty'			=> $hasil_lab->coa_qty,
				'ni_c'			=> $hasil_lab->ni,
				'fe_c'			=> $hasil_lab->fe,
				'sio_c'			=> $hasil_lab->sio,
				'mgo_c'			=> $hasil_lab->mgo,
				'aio_c'			=> $hasil_lab->aio,
				'sio_mgo_c'			=> $hasil_lab->sio_mgo,
				'fe_ni_c'			=> $hasil_lab->fe_ni,
				'mc_c'			=> $hasil_lab->mc,
				'ni_stat_c'			=> $hasil_lab->ni_stat,
				'mc_stat_c'			=> $hasil_lab->mc_stat,
				'sio_mgoStat_c'			=> $hasil_lab->sio_mgo_stat,
				'gm' 	=> $this->input->post('status'),
			);

		$this->db->where('id_adj', $id_adj)
						 ->update('tb_stok_adj', $data_status);
	}

	function reject($id_adj)
	{
		
		//--> edit data
		$data_status = array(
				'id_adj'		=> $this->input->post('id_adj'),
				'fin' 	=> $this->input->post('status'),
				'sk' 	=> $this->input->post('status'),
				'gm' 	=> $this->input->post('status'),
			);

		$this->db->where('id_adj', $id_adj)
						 ->update('tb_stok_adj', $data_status);
	}
}