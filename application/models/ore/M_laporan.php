<?php

class M_laporan extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function get_row_laporanStok($id_kontrak)
	{
		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->where('tb_master_contract.id_kontrak', $id_kontrak)
										->get()->row();
	}

	function get_timearr(){
		$tgl_awal = $this->input->post('tgl_awal');
     	$tgl_akhir = $this->input->post('tgl_akhir');

		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->where('tgl_arr >=', $tgl_awal)
										->where('tgl_arr <=', $tgl_akhir)
										->get()->result();
	}

	function get_comm_disch(){
		$tgl_awal = $this->input->post('tgl_awal');
     	$tgl_akhir = $this->input->post('tgl_akhir');

		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->where('comm_disch >=', $tgl_awal)
										->where('comm_disch <=', $tgl_akhir)
										->get()->result();
	}

	function get_comp_disch(){
		$tgl_awal = $this->input->post('tgl_awal');
     	$tgl_akhir = $this->input->post('tgl_akhir');

		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->where('compl_disch >=', $tgl_awal)
										->where('compl_disch <=', $tgl_akhir)
										->get()->result();
	}

	function get_timedep(){
		$tgl_awal = $this->input->post('tgl_awal');
     	$tgl_akhir = $this->input->post('tgl_akhir');

		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->where('tgl_dep >=', $tgl_awal)
										->where('tgl_dep <=', $tgl_akhir)
										->get()->result();
	}

	function get_input(){
		$tgl_awal = $this->input->post('tgl_awal');
     	$tgl_akhir = $this->input->post('tgl_akhir');

		return $this->db->select('*')
										->from('tb_stok_masuk')
										->join('tb_master_contract', 'tb_master_contract.id_kontrak = tb_stok_masuk.master_kontrak')
										->join('tb_supplier', 'tb_supplier.id_supplier = tb_stok_masuk.supplier')
										->join('tb_seller', 'tb_seller.id_seller = tb_stok_masuk.seller_agent')
										->order_by('tb_stok_masuk.id_sm', 'asc')
										->group_by('tb_stok_masuk.master_kontrak')
										->where('tgl_input >=', $tgl_awal)
										->where('tgl_input <=', $tgl_akhir)
										->get()->result();
	}
}