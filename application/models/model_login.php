<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
 
	function cek_user($username="", $password="")
	{
		$query = $this->db->get_where('tb_user', array('username' => $username, 'password' => $password));
		$query = $query->result_array();
		return $query;
	}

	function get_user($username, $sis)
  {
     	$query = $this->db->get_where('tb_user', array('username' => $username, 'sistem' => $sis));
     	$query = $query->result_array();
      
      if($query){
          return $query[0];
      }
  }

  // function get_pegawai($username)
  // {
  // 	return $this->db->select('*')
  // 					 				->from('tb_user')
  // 					 				->join('tb_pegawai', 'tb_pegawai.id_pegawai = tb_user.id_fk')
  // 					 				->where('username', $username)
  // 					 				->get()->row();
  // }

}