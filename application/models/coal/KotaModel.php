<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KotaModel extends CI_Model {
	
	public function viewByProvinsi($vessel){
		$this->db->where('type', $vessel);
		$this->db->where('status', 0);
		$result = $this->db->get('coal_schedule')->result(); // Tampilkan semua data kota berdasarkan id provinsi
		
		return $result; 
	}
}