<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_discharge extends CI_Model {
	
	public function viewByProvinsi($stock){
		$this->db->where('id_contract', $stock);
		$result = $this->db->get('stock_coal')->result(); 
		
		return $result; 
	}

	public function coal_schedule($id){
		$this->db->where('id_coalSch', $id);
		$result = $this->db->get('coal_schedule')->result(); // Tampilkan semua data 
		
		return $result; 
	}
}